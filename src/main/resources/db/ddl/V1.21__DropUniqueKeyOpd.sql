alter table `ehms_apt_prescriptions` drop foreign key `EHMS__PRESCRIPTIONS__APPOINTMENTS__FK`;

alter table `ehms_apt_prescriptions` drop foreign key `EHMS__PRESCRIPTIONS__DRUG_MASTER__FK`;

ALTER TABLE ehms_apt_prescriptions DROP INDEX PRESCRIPTIONS_UNIQUEKEY;


ALTER TABLE `ehms_apt_prescriptions`
ADD CONSTRAINT `EHMS__PRESCRIPTIONS__APPOINTMENTS__FK` FOREIGN KEY (`appointment_id`) REFERENCES `ehms_appointments` (`appointment_id`),
ADD CONSTRAINT `EHMS__PRESCRIPTIONS__DRUG_MASTER__FK` FOREIGN KEY (`drug_master_id`) REFERENCES `ehms_drug_master` (`drug_master_id`);
  

