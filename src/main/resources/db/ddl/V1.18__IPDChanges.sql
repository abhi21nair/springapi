ALTER TABLE `ehms_appointments`
ADD `department_id` bigint(20) unsigned AFTER `time_spent`,
ADD `bed_id` bigint(20) unsigned AFTER `department_id`,
ADD CONSTRAINT `EHMS_APPOINTMENTS__DEPARTMENTS__FK` FOREIGN KEY (`department_id`) REFERENCES `ehms_departments` (`department_id`),
ADD CONSTRAINT `EHMS_APPOINTMENTS__BEDS__FK` FOREIGN KEY (`bed_id`) REFERENCES `ehms_beds` (`bed_id`);

ALTER TABLE `ehms_beds`
ADD `current_patient_id` bigint(20) unsigned AFTER `ward_id`,
ADD CONSTRAINT `EHMS_BEDS__PATIENTS__FK` FOREIGN KEY (`current_patient_id`) REFERENCES `ehms_patients` (`patient_id`);