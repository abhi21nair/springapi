ALTER TABLE `ehms`.`ehms_users`
ADD `is_system_user` BOOLEAN AFTER `employee_id`;

UPDATE `ehms`.`ehms_users`
SET  `is_system_user` =  CASE  
                        WHEN `employee_id` is NULL THEN 1
                        WHEN `employee_id` is NOT NULL THEN 0
                    END;
                    
ALTER TABLE `ehms`.`ehms_users` MODIFY `is_system_user` BOOLEAN NOT NULL;

ALTER TABLE `ehms`.`ehms_employees` MODIFY COLUMN customer_id bigint(20) unsigned NULL;

ALTER TABLE `ehms`.`ehms_departments` MODIFY COLUMN customer_id bigint(20) unsigned NULL;