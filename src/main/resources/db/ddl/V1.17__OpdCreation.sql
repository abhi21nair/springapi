CREATE TABLE `ehms`.`ehms_apt_complaints`(
`complaint_id` SERIAL PRIMARY KEY,
`appointment_id` bigint(20) unsigned NOT NULL,
`pathology_test_master_id` bigint(20) unsigned,
`complaints` varchar(1500),
`diagnosis` varchar(1500),
`examination_pulse` varchar(300),
`respiration` varchar(300),
`temperature` varchar(300),
`height` varchar(300),
`weight` varchar(300),
`blood_pressure` varchar(300),
`general_notes` varchar(1500),
`lab_notes` varchar(1500),
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
UNIQUE KEY `COMPLAINTS_UNIQUEKEY` (`pathology_test_master_id`,`appointment_id`),
CONSTRAINT `EHMS__COMPLAINTS__APPOINTMENTS__FK` FOREIGN KEY (`appointment_id`) REFERENCES `ehms_appointments` (`appointment_id`),
CONSTRAINT `EHMS__COMPLAINTS__PATHOLOGY__FK` FOREIGN KEY (`pathology_test_master_id`) REFERENCES `ehms_pathology_test_master` (`pathology_test_master_id`),
CONSTRAINT `EHMS__COMPLAINTS__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS__COMPLAINTS__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_apt_prescriptions`(
`prescription_id` SERIAL PRIMARY KEY,
`appointment_id` bigint(20) unsigned NOT NULL,
`drug_master_id` bigint(20) unsigned,
`route` varchar(100),
`quantity` varchar(300),
`duration` int,
`duration_unit` varchar(50),
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
UNIQUE KEY `PRESCRIPTIONS_UNIQUEKEY` (`drug_master_id`,`appointment_id`),
CONSTRAINT `EHMS__PRESCRIPTIONS__DRUG_MASTER__FK` FOREIGN KEY (`drug_master_id`) REFERENCES `ehms_drug_master` (`drug_master_id`),
CONSTRAINT `EHMS__PRESCRIPTIONS__APPOINTMENTS__FK` FOREIGN KEY (`appointment_id`) REFERENCES `ehms_appointments` (`appointment_id`),
CONSTRAINT `EHMS__PRESCRIPTIONS__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS__PRESCRIPTIONS__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_apt_referrals`(
`referral_id` SERIAL PRIMARY KEY,
`appointment_id` bigint(20) unsigned NOT NULL,
`department_id` bigint(20) unsigned,
`provisional_notes` varchar(1500),
`mlc_case` boolean,
`instruction_to_warden` varchar(1500),
`general_notes` varchar(1500),
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
UNIQUE KEY `REFERRALS_UNIQUEKEY` (`department_id`,`appointment_id`),
CONSTRAINT `EHMS__REFERRALS__DEPARTMENTS__FK` FOREIGN KEY (`department_id`) REFERENCES `ehms_departments` (`department_id`),
CONSTRAINT `EHMS__REFERRALS__APPOINTMENTS__FK` FOREIGN KEY (`appointment_id`) REFERENCES `ehms_appointments` (`appointment_id`),
CONSTRAINT `EHMS__REFERRALS__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS__REFERRALS__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_apt_various_services`(
`various_service_id` SERIAL PRIMARY KEY,
`appointment_id` bigint(20) unsigned NOT NULL,
`tarrif_service_id` bigint(20) unsigned,
`quantity` Decimal(50,2),
`total_amount` Decimal(50,2),
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
UNIQUE KEY `VARIOUS_SERVICES_UNIQUEKEY` (`tarrif_service_id`,`appointment_id`),
CONSTRAINT `EHMS__VARIOUS_SERVICES__TARRIF_WISE_SERVICES__FK` FOREIGN KEY (`tarrif_service_id`) REFERENCES `ehms_tarrif_wise_service` (`tarrif_service_id`),
CONSTRAINT `EHMS__VARIOUS_SERVICES__APPOINTMENTS__FK` FOREIGN KEY (`appointment_id`) REFERENCES `ehms_appointments` (`appointment_id`),
CONSTRAINT `EHMS__VARIOUS_SERVICES__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS__VARIOUS_SERVICES__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_apt_examinations`(
`examination_id` SERIAL PRIMARY KEY,
`appointment_id` bigint(20) unsigned NOT NULL,
`local_examination` varchar(1500),
`central_nervous_system` varchar(1500),
`respiratory_system` varchar(1500),
`cardio_vascular_system` varchar(1500),
`abdominal_system` varchar(1500),
`obg/gynic_examination` varchar(1500),
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS__EXAMINATIONS__APPOINTMENTS__FK` FOREIGN KEY (`appointment_id`) REFERENCES `ehms_appointments` (`appointment_id`),
CONSTRAINT `EHMS__EXAMINATIONS__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS__EXAMINATIONS__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;