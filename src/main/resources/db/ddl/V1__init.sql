CREATE TABLE `ehms`.`ehms_titles`(
`title_id` SERIAL PRIMARY KEY,
`title_name` varchar(50) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_users` (
`user_id` SERIAL PRIMARY KEY,
`username` varchar(45) NOT NULL unique,
`password` varchar(200) NOT NULL,
`security_question` varchar(200),
`security_answer` varchar(200),
`is_reset_password_on_login` TINYINT(1),
`enabled` BOOLEAN NOT NULL,
`employee_id` bigint(20) unsigned,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_USERS_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_USERS_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Initial table creation

CREATE TABLE `ehms`.`ehms_countries` (
`country_id` SERIAL PRIMARY KEY,
`country_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_states` (
`state_id` SERIAL PRIMARY KEY,
`state_name` varchar(100) NOT NULL,
`country_id` bigint(20) unsigned NOT NULL, 
CONSTRAINT `EHMS_STATES_COUNTRIES__FK` FOREIGN KEY (`country_id`) REFERENCES `ehms_countries` (`country_id`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_cities` (
`city_id` SERIAL PRIMARY KEY,
`city_name` varchar(100) NOT NULL,
`state_id` bigint(20) unsigned NOT NULL,
CONSTRAINT `EHMS_CITIES_STATES__FK` FOREIGN KEY (`state_id`) REFERENCES `ehms_states` (`state_id`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_addresses` (
`address_id` SERIAL PRIMARY KEY,
`address1` varchar(200) NOT NULL,
`address2` varchar(200),
`address3` varchar(200),
`street` varchar(100),
`region` varchar(100),
`city_id` bigint(20) unsigned NOT NULL,
`county` varchar(100),
`zipcode` varchar(100) NOT NULL,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_ADDRESSES_CITIES__FK` FOREIGN KEY (`city_id`) REFERENCES `ehms_cities` (`city_id`),
CONSTRAINT `EHMS_ADDRESSES_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_ADDRESSES_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_languages` (
`language_id` SERIAL PRIMARY KEY,
`language_code` varchar(45) NOT NULL,
`language_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_modules` (
`module_id` SERIAL PRIMARY KEY,
`module_name` varchar(100) NOT NULL,
`module_code` varchar(50) NOT NULL,
`is_active` BOOLEAN NOT NULL,
`is_queueable` BOOLEAN NOT NULL DEFAULT FALSE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_buying_types` (
`buying_type_id` SERIAL PRIMARY KEY,
`buying_type_name` varchar(100) NOT NULL,
`is_active` BOOLEAN NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_buying_preferences` (
`buying_preference_id` SERIAL PRIMARY KEY,
`buying_preference_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_privileges` (
`privilege_id` SERIAL PRIMARY KEY,
`privilege_code` varchar(45) NOT NULL,
`privilege_description` varchar(200) NOT NULL,
`is_system_privilege` BOOLEAN not null,
`module_id` bigint unsigned not null,
CONSTRAINT EHMS_PRIVILLEGE_MODULES__FK FOREIGN KEY (module_id) REFERENCES ehms_modules (module_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_subscription_types` (
`subscription_type_id` SERIAL PRIMARY KEY,
`subscription_type_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_departments` (
`department_id` SERIAL PRIMARY KEY,
`department_name` varchar(45) NOT NULL,
`department_description` VARCHAR(100) NOT NULL,
`department_type` VARCHAR(20) NOT NULL,
`country_code_for_department_phone_number` varchar(45),
`department_phone_number` varchar(45),
`is_active` boolean NOT NULL DEFAULT TRUE,
`customer_id` bigint(20) unsigned NOT NULL,
`hospital_id` bigint(20) unsigned,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
UNIQUE KEY `DEPARTMENT_UNIQUEKEY` (`department_name`,`customer_id`,`hospital_id`),
CONSTRAINT `EHMS_DEPARTMENTS_USERS_CREATED__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_DEPARTMENTS_USERS_UPDATED__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_employees` (
`employee_id` SERIAL PRIMARY KEY,
`title_id` bigint(20) unsigned NOT NULL,
CONSTRAINT `EHMS_EMPLOYEES_TITLES__FK` FOREIGN KEY (`title_id`) REFERENCES `ehms_titles` (`title_id`),
`first_name` varchar(100) NOT NULL,
`middle_name` varchar(100),
`last_name` varchar(100) NOT NULL,
`employee_code` varchar(20) NOT NULL,
`employee_type` int NOT NULL,
`designation_id` bigint(20) unsigned NOT NULL,
`department_id` bigint(20) unsigned,
CONSTRAINT `EHMS_EMPLOYEES_DEPARTMENTS__FK` FOREIGN KEY (`department_id`) REFERENCES `ehms_departments` (`department_id`),
`birth_date` DATETIME,
`birth_year` INT,
`is_dob_available` BOOLEAN NOT NULL,
`joining_date` DATETIME,
`gender` varchar(20) NOT NULL,
`marital_status` varchar(20),
`nationality` varchar(20) NOT NULL,
`adhar_no` varchar(30),
`other_id_type` bigint(20) unsigned,
`other_id_no` varchar(30),
`other_id_valid_till` DATETIME,
`pan_no` varchar(30),
`tan_no` varchar(30),
`qualification` varchar(100),
`specialization_id` bigint(20) unsigned,
`joining_type_id` bigint(20) unsigned,
`hospital_share_pc` DECIMAL(50,2),
`hospital_share_rs` DECIMAL(50,2),
`doctor_share_pc` DECIMAL(50,2),
`doctor_share_rs` DECIMAL(50,2),
`email` varchar(30),
`country_code_for_mobile_number` varchar(10) NOT NULL,
`mobile_number` varchar(20) NOT NULL,
`country_code_for_contact_number` varchar(10),
`contact_number` varchar(20),
`nominee_name` varchar(30),
`nominee_relation` varchar(30),
`current_address_id` bigint(20) unsigned NOT NULL,
`permanent_address_id` bigint(20) unsigned NOT NULL,
CONSTRAINT `EHMS_EMPLOYEES_CURRENT_ADDRESSES__FK` FOREIGN KEY (`current_address_id`) REFERENCES `ehms_addresses` (`address_id`),
CONSTRAINT `EHMS_EMPLOYEES_PERM_ADDRESSES__FK` FOREIGN KEY (`permanent_address_id`) REFERENCES `ehms_addresses` (`address_id`),
`hospital_id` bigint(20) unsigned,
`customer_id` bigint(20) unsigned NOT NULL,
`is_active` BOOLEAN NOT NULL DEFAULT TRUE,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `UNIQUE_KEY_EMPLOYEE` UNIQUE (employee_code,customer_id,hospital_id),
CONSTRAINT `EHMS_EMPLOYEES_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_EMPLOYEES_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE `ehms`.`ehms_customers` (
`customer_id` SERIAL PRIMARY KEY,
`organization_code` varchar(100) NOT NULL UNIQUE KEY,
`customer_name` varchar(100) NOT NULL,
`country_code_for_contact_number` varchar(10) NOT NULL,
`contact_number` varchar(45) NOT NULL,
`country_code_for_secondary_contact_number` varchar(10),
`sec_contact_number`varchar(45),
`is_active` BOOLEAN NOT NULL,
`demo_days` INT,
`key_user_id` bigint(20) unsigned,
`address_id` bigint(20) unsigned NOT NULL,
`referral_type` varchar(50),
`referral_person_name` varchar(100),
`buying_type_id` bigint(20) unsigned NOT NULL,
`buying_preference_id` bigint(20) unsigned NOT NULL,
`subscription_type_id` bigint(20) unsigned NOT NULL,
`buying_type_quantity` bigint(20) unsigned NOT NULL,
`expiry_date` DATETIME NOT NULL,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
UNIQUE KEY `uniquekey` (`customer_name`),
CONSTRAINT `EHMS_CUSTOMERS_KEYUSER__FK` FOREIGN KEY (`key_user_id`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_CUSTOMERS_ADDRESSES__FK` FOREIGN KEY (`address_id`) REFERENCES `ehms_addresses` (`address_id`),
CONSTRAINT `EHMS_CUSTOMERS_BUYING_TYPES__FK` FOREIGN KEY (`buying_type_id`) REFERENCES `ehms_buying_types` (`buying_type_id`),
CONSTRAINT `EHMS_CUSTOMERS_BUYING_PREFERENCES__FK` FOREIGN KEY (`buying_preference_id`) REFERENCES `ehms_buying_preferences` (`buying_preference_id`),    
CONSTRAINT `EHMS_CUSTOMERS_SUBSCRIPTION_TYPES__FK` FOREIGN KEY (`subscription_type_id`) REFERENCES `ehms_subscription_types` (`subscription_type_id`),
CONSTRAINT `EHMS_CUSTOMERS_USERS_CREATED__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_CUSTOMERS_USERS_UPDATED__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_hospitals` (
`hospital_id` SERIAL PRIMARY KEY,
`hospital_name` varchar(100) NOT NULL,
`hospital_code` varchar(100) NOT NULL,
`is_active` BOOLEAN NOT NULL,
`country_code_for_contact_number` varchar(10) NOT NULL,
`contact_number` varchar(45) NOT NULL,
`fax_number` varchar(45),
`country_code_for_phone_number` varchar(10) NOT NULL,
`phone_number` varchar(45) NOT NULL,
`key_user_id` bigint(20) unsigned,
`address_id` bigint(20) unsigned NOT NULL,
`customer_id` bigint(20) unsigned NOT NULL,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` datetime,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT UC_HOSPITAL_NAME UNIQUE (`hospital_name`, `customer_id`),
CONSTRAINT `EHMS_HOSPITALS_CUSTOMERS__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
CONSTRAINT `EHMS_HOSPITALS_ADDRESSES__FK` FOREIGN KEY (`address_id`) REFERENCES `ehms_addresses` (`address_id`),
CONSTRAINT `EHMS_HOSPITALS_KEYEMPLOYEE__FK` FOREIGN KEY (`key_user_id`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_HOSPITALS_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_HOSPITALS_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table `ehms`.`ehms_departments` add CONSTRAINT `EHMS_CUSTOMER_DEPARTMENTS__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`);
alter table `ehms`.`ehms_departments` add CONSTRAINT `EHMS_HOSPITAL_DEPARTMENTS__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`);
alter table `ehms`.`ehms_employees` add CONSTRAINT `EHMS_EMPLOYEES_HOSPITALS__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`);
alter table `ehms`.`ehms_employees` add  CONSTRAINT `EHMS_EMPLOYEES_CUSTOMERS__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`);

CREATE TABLE `ehms`.`ehms_roles` (
`role_id` SERIAL PRIMARY KEY,
`role_name` varchar(100) NOT NULL,
`description` varchar(200),
`is_active` BOOLEAN NOT NULL,
`is_system_role` BOOLEAN not null,
`customer_id` bigint(20) unsigned,
`hospital_id` bigint(20) unsigned,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
UNIQUE KEY `ROLE_UNIQUEKEY` (`role_name`,`customer_id`,`hospital_id`),
CONSTRAINT `EHMS_ROLE_CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
CONSTRAINT `EHMS_ROLE_HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`),
CONSTRAINT `EHMS_ROLES_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_ROLES_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_link_role_privileges` (
`role_privilege_id` SERIAL PRIMARY KEY,
`role_id` bigint(20) unsigned NOT NULL,
`privilege_id` bigint(20) unsigned NOT NULL,
CONSTRAINT `EHMS_LINK_ROLE_PRIVILEGES_ROLES__FK` FOREIGN KEY (`role_id`) REFERENCES `ehms_roles` (`role_id`),
CONSTRAINT `EHMS_LINK_ROLE_PRIVILEGES_PRIVILEGES__FK` FOREIGN KEY (`privilege_id`) REFERENCES `ehms_privileges` (`privilege_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_link_user_role` (
`user_role_id` SERIAL PRIMARY KEY,
`user_id` bigint(20) unsigned NOT NULL,
`role_id` bigint(20) unsigned NOT NULL,
CONSTRAINT `EHMS_LINK_USER_ROLE_USERS__FK` FOREIGN KEY (`user_id`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_LINK_USER_ROLE_ROLES_FK` FOREIGN KEY (`role_id`) REFERENCES `ehms_roles` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE ehms_users ADD CONSTRAINT EHMS_USER_EMPLOYEE_UPDATEDBY__FK FOREIGN KEY (employee_id) REFERENCES ehms_employees(employee_id);

CREATE TABLE `ehms`.`ehms_link_customer_module` (
`customer_module_id` SERIAL PRIMARY KEY,
`module_id` bigint(20) unsigned NOT NULL,
`customer_id` bigint(20) unsigned NOT NULL,
CONSTRAINT `EHMS_LINK_CUSTOMER_MODULE_MODULES__FK` FOREIGN KEY (`module_id`) REFERENCES `ehms_modules` (`module_id`),
CONSTRAINT `EHMS_LINK_CUSTOMER_MODULE_CUSTOMERS__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_payment_modes` (
`payment_mode_id` SERIAL PRIMARY KEY,
`payment_mode_name` varchar(100) NOT NULL,
`is_active` BOOLEAN NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_payment_status` (
`payment_status_id` SERIAL PRIMARY KEY,
`payment_status_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_customer_payments` (
`customer_payment_id` SERIAL PRIMARY KEY,
`customer_id` bigint(20) unsigned NOT NULL,
`cost_of_product` DECIMAL(50,2) unsigned NOT NULL,
`gst_number` varchar(200),
`pan_number` varchar(200),
`payment_status_id` bigint(50) unsigned NOT NULL,
`amc_addition` varchar(200),
`other_service_1` varchar(200),
`other_service_1_type` varchar(200),
`other_service_2` varchar(200),
`other_service_2_type` varchar(200),
`discount` DECIMAL(50,2),
`discount_valid_from` DATETIME,
`discount_valid_to` DATETIME,
`discount_in_percent` DECIMAL(50,3) unsigned,
`value` DECIMAL(50,2) unsigned,
`discount_in_percent_valid_from` DATETIME,
`discount_in_percent_valid_to` DATETIME,
`tax1` DECIMAL(50,2) unsigned,
`tax2` DECIMAL(50,2) unsigned,
`tax3` DECIMAL(50,2) unsigned,
`total_billing_amount` DECIMAL(50,2) unsigned NOT NULL,
`payment_mode_id` bigint(20) unsigned NOT NULL,
`expiry_date` DATETIME NOT NULL,
`action_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`action_by`  bigint(50) unsigned NOT NULL,   
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
UNIQUE KEY `CUSTOMER_PAYMENT_UNIQUEKEY` (`customer_id`,`created_dt`),
CONSTRAINT `EHMS_CUSTOMER_PAYMENTS_USERS_FK` FOREIGN KEY (`action_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_CUSTOMER_PAYMENTS_CUSTOMERS__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
CONSTRAINT `EHMS_CUSTOMER_PAYMENTS_PAYMENT_MODES__FK` FOREIGN KEY (`payment_mode_id`) REFERENCES `ehms_payment_modes` (`payment_mode_id`),
CONSTRAINT `EHMS_CUSTOMER_PAYMENTS_PAYMENT_STATUS__FK` FOREIGN KEY (`payment_status_id`) REFERENCES `ehms_payment_status` (`payment_status_id`),
CONSTRAINT `EHMS_CUSTOMER_PAYMENTS_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_CUSTOMER_PAYMENTS_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)  
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `ehms_patients` (
`patient_id` SERIAL PRIMARY KEY,
`patient_code` varchar(100) NOT NULL UNIQUE KEY,
`title_id`bigint(20) unsigned NOT NULL,
`patient_first_name` varchar(100) NOT NULL,
`patient_middle_name` varchar(100),
`patient_last_name` varchar(100) NOT NULL,
`patient_mother_name` varchar(100),
`patient_father_name` varchar(100),
`birth_date` DATETIME NOT NULL, 
`customer_id` bigint(20) unsigned NOT NULL,
`hospital_id` bigint(20) unsigned NOT NULL,
`country_code_for_phone_number` varchar(10) NOT NULL,
`phone_number` varchar(45) NOT NULL,
`country_code_for_mobile_number` varchar(10) NOT NULL,
`mobile_number` varchar(45) NOT NULL,
`address_id` bigint(20) unsigned NOT NULL,
`gender` VARCHAR(10) NOT NULL,
`marital_status` VARCHAR(20) NOT NULL,
`contact_person` VARCHAR(100),
`contact_relation` VARCHAR(50),
`insurance_company_name` VARCHAR(100),
`insurance_account_number` VARCHAR(50),
`insurance_validity_date` DATE,
`email` varchar(100),
`referby_title` VARCHAR(10),
`referby_first_name` VARCHAR(50),
`referby_middle_name` VARCHAR(50),
`referby_last_name` VARCHAR(50),
`referby_email` VARCHAR(100),
`referby_contact_number` VARCHAR(45),
CONSTRAINT `EHMS_PATIENTS_CUSTOMERS__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
CONSTRAINT `EHMS_PATIENTS_HOSPITALS__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`),
CONSTRAINT `EHMS_PATIENTS_TITLES__FK` FOREIGN KEY (`title_id`) REFERENCES `ehms_titles` (`title_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_misc_type` (
`misc_type_id` SERIAL PRIMARY KEY,
`misc_type_name` varchar(45) NOT NULL UNIQUE,
`misc_type_description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_misc_values` (
`misc_type_value_id` SERIAL PRIMARY KEY,
`misc_type_id` bigint(20) unsigned NOT NULL,
`misc_value` varchar(200),
`customer_id` bigint(20) unsigned,
`hospital_id` bigint(20) unsigned,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
UNIQUE KEY `uniquekey` (`misc_type_id`,`misc_value`,`customer_id`, `hospital_id`),
CONSTRAINT `EHMS_MT_VALUE_MT__FK` FOREIGN KEY (`misc_type_id`) REFERENCES `ehms_misc_type` (`misc_type_id`),
CONSTRAINT `EHMS_MT_CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
CONSTRAINT `EHMS_MT_HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`),
CONSTRAINT `EHMS_MT_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_MT_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ehms`.`ehms_employees` ADD CONSTRAINT EHMS_EMPLOYEES_DESIGNATION_MISCTYPE_VALUES__FK FOREIGN KEY (`designation_id`) REFERENCES `ehms_misc_values`(`misc_type_value_id`);
ALTER TABLE `ehms`.`ehms_employees` ADD CONSTRAINT EHMS_EMPLOYEES_OTHERIDTYPE_MISCTYPE_VALUES__FK FOREIGN KEY (`other_id_type`) REFERENCES `ehms_misc_values`(`misc_type_value_id`);
ALTER TABLE `ehms`.`ehms_employees` ADD CONSTRAINT EHMS_EMPLOYEES_SPECIALIZATION_MISCTYPE_VALUES__FK FOREIGN KEY (`specialization_id`) REFERENCES `ehms_misc_values`(`misc_type_value_id`);
ALTER TABLE `ehms`.`ehms_employees` ADD CONSTRAINT EHMS_EMPLOYEES_JOINING_TYPE_MISCTYPE_VALUES__FK FOREIGN KEY (`joining_type_id`) REFERENCES `ehms_misc_values`(`misc_type_value_id`);

CREATE TABLE `ehms`.`ehms_wards` (
`ward_id` SERIAL PRIMARY KEY,
`floor_id` bigint(20) unsigned NOT NULL,
`ward_name` varchar(20)  NOT NULL,
`ward_type_id` bigint(20) unsigned NOT NULL,
`bed_category_id` bigint(20) unsigned NOT NULL,
`bed_charges_full_day` DOUBLE NOT NULL,
`bed_charges_hourly` DOUBLE  NOT NULL,
`hospital_id` bigint(20) unsigned NOT NULL,
`customer_id` bigint(20) unsigned NOT NULL,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_WARD__MISCTYPE_BED_CATEGORY_VALUES__FK` FOREIGN KEY (`bed_category_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
CONSTRAINT `EHMS_WARD__MISCTYPE_FLOOR_VALUES__FK` FOREIGN KEY (`floor_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
CONSTRAINT `EHMS_WARD__MISCTYPE_WARD_TYPE_VALUES__FK` FOREIGN KEY (`ward_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
CONSTRAINT `EHMS_WARD__CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
CONSTRAINT `EHMS_WARD__HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`),
CONSTRAINT `EHMS_WARD__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_WARD__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_WARD__UNIQUE` UNIQUE ( `customer_id`, `hospital_id`, `ward_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_beds` (
`bed_id` SERIAL PRIMARY KEY,
`bed_code` varchar(20) NOT NULL,
`ward_id` bigint(20) unsigned NOT NULL,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_WARD_CREATION__FK` FOREIGN KEY (`ward_id`) REFERENCES `ehms_wards` (`ward_id`),
CONSTRAINT `EHMS_BED_CREATION_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_BED_CREATION_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_BED__UNIQUE` UNIQUE (`ward_id`, `bed_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
CREATE TABLE `ehms`.`ehms_doc_schedules` (
`doctor_schedule_id` SERIAL PRIMARY KEY,
`doctor_id` bigint(50) unsigned NOT NULL,
`day` int NOT NULL,
`time_from` Time NOT NULL,
`time_to` Time NOT NULL,
`appointment_type_id` bigint(50) unsigned NOT NULL,
`no_of_appointment` int NOT NULL,
`doctor_shift_id` bigint(50) unsigned,
`doctor_fees` Decimal(50,2) NOT NULL,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_DOC_SCHEDULES_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_DOC_SCHEDULES_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_DOC_SCHEDULES_EMPLOYEES__FK` FOREIGN KEY (`doctor_id`) REFERENCES `ehms_employees` (`employee_id`),
CONSTRAINT `EHMS_DOC_SCHEDULES_APPOINTMENT_TYPE_MISCTYPE_VALUES__FK` FOREIGN KEY (`appointment_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
CONSTRAINT `EHMS_DOC_SCHEDULES_DOCTOR_SHIFT_MISCTYPE_VALUES__FK` FOREIGN KEY (`doctor_shift_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_tarrif_wise_service` (
`tarrif_service_id` SERIAL PRIMARY KEY,
`tarrif_service_name_id` bigint(50) unsigned NOT NULL,
`department_id` bigint(50) unsigned NOT NULL,
`service_type_id` bigint(50) unsigned NOT NULL,
`normal_service_rate`  Decimal(50,2) NOT NULL,
`special_service_rate`  Decimal(50,2) NOT NULL,
`doctor_share_in_percent`  Decimal(50,2) NOT NULL,
`doctor_share_in_rupees` Decimal(50,2) NOT NULL,
`customer_id` bigint(20) unsigned NOT NULL,
`hospital_id` bigint(20) unsigned NOT NULL,
`is_active` boolean NOT NULL DEFAULT TRUE,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_TARRIF_WISE_SERVICE__CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
CONSTRAINT `EHMS_TARRIF_WISE_SERVICE__HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`),
CONSTRAINT `EHMS_TARRIF_WISE_SERVICE__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_TARRIF_WISE_SERVICE__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`), 
CONSTRAINT `EHMS_TARRIF_WISE_SERVICE__DEPARTMENTS__FK` FOREIGN KEY (`department_id`) REFERENCES `ehms_departments` (`department_id`),
CONSTRAINT `EHMS_TARRIF_SERVICE_SERVICE_NAME__MISCTYPE_VALUES__FK` FOREIGN KEY (`tarrif_service_name_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
CONSTRAINT `EHMS_TARRIF_SERVICE_SERVICE_TYPE__MISCTYPE_VALUES__FK` FOREIGN KEY (`service_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_material_instrument` (
`material_instrument_id` SERIAL PRIMARY KEY,
`material_instrument_name` varchar(300) NOT NULL,
`instrument_material_type_id` bigint(50) unsigned NOT NULL,
`department_id` bigint(50) unsigned NOT NULL,
`material_instrument_sub_name` varchar(300),
`charges`  Decimal(50,2) NOT NULL,
`instrument_material_number` varchar(300),
`customer_id` bigint(20) unsigned NOT NULL,
`hospital_id` bigint(20) unsigned NOT NULL,
`is_active` boolean NOT NULL DEFAULT TRUE,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_MATERIAL_INSTRUMENT__DEPARTMENTS__FK` FOREIGN KEY (`department_id`) REFERENCES `ehms_departments` (`department_id`),
CONSTRAINT `EHMS_MATERIAL_INSTRUMENT__CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
CONSTRAINT `EHMS_MATERIAL_INSTRUMENT__HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`),
CONSTRAINT `EHMS_MATERIAL_INSTRUMENT__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_MATERIAL_INSTRUMENT__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`), 
CONSTRAINT `EHMS_MATERIAL_INSTRUMENT__MISCTYPE_VALUES__FK` FOREIGN KEY (`instrument_material_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
CREATE TABLE `ehms`.`ehms_pathology_test_master` (
  `pathology_test_master_id` SERIAL PRIMARY KEY,
  `test_name` varchar(100)  NOT NULL,
  `sub_test_name` varchar(100),
  `lab_id` bigint(20) unsigned NOT NULL,
  `normal_value` varchar(20) NOT NULL,
  `value_type_id` bigint(20) unsigned NOT NULL ,
  `gender_wise_value` int NOT NULL ,
  `unit`varchar(50) NOT NULL,
  `specimen_type_id` bigint(20) unsigned NOT NULL,
  `container_type_id` bigint(20) unsigned NOT NULL,
  `result_type_id` bigint(20) unsigned NOT NULL,
  `created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_dt` DATETIME,
  `created_by` bigint(20) unsigned,
  `updated_by` bigint(20) unsigned,
  `customer_id` bigint(20) unsigned NOT NULL,
  `hospital_id` bigint(20) unsigned NOT NULL,
  `is_active` boolean NOT NULL DEFAULT TRUE,
  CONSTRAINT `EHMS_PATHOLOGY_TEST_MASTER__CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
  CONSTRAINT `EHMS_PATHOLOGY_TEST_MASTER__HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`),
  CONSTRAINT `EHMS_PATHOLOGY_TEST_MASTER_DEPARTMENT_FK` FOREIGN KEY (`lab_id`) REFERENCES `ehms_departments` (`department_id`),
  CONSTRAINT `EHMS_PATHOLOGY_TEST_MASTER__MISCTYPE_VALUE_TYPE_VALUES__FK` FOREIGN KEY (`value_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
  CONSTRAINT `EHMS_PATHOLOGY_TEST_MASTER__MISCTYPE_SPECIMEN_TYPE_VALUES__FK` FOREIGN KEY (`specimen_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
  CONSTRAINT `EHMS_PATHOLOGY_TEST_MASTER__MISCTYPE_CONTAINER_TYPE_VALUES__FK` FOREIGN KEY (`container_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
  CONSTRAINT `EHMS_PATHOLOGY_TEST_MASTER__MISCTYPE_RESULT_TYPE_VALUES__FK` FOREIGN KEY (`result_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
  CONSTRAINT `EHMS_PATHOLOGY_TEST_MASTER__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
  CONSTRAINT `EHMS_PATHOLOGY_TEST_MASTER__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_ot_master` (
  `ot_master_id` SERIAL PRIMARY KEY,
  `ot_name` varchar(30)  NOT NULL,
  `ot_type_id` bigint(20) unsigned NOT NULL,
  `ot_number` bigint(20) NOT NULL,
  `ot_location` VARCHAR(50) NOT NULL,
  `ot_charges` Decimal(50,2) NOT NULL ,
  `created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_dt` DATETIME,
  `created_by` bigint(20) unsigned,
  `updated_by` bigint(20) unsigned,
  `customer_id` bigint(20) unsigned NOT NULL,
  `hospital_id` bigint(20) unsigned NOT NULL,
  `is_active` boolean NOT NULL DEFAULT TRUE,
  CONSTRAINT `EHMS_OT_MASTER__CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
  CONSTRAINT `EHMS_OT_MASTER__HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`),
  CONSTRAINT `EHMS_OT_MASTER_DEPARTMENT_FK` FOREIGN KEY (`ot_type_id`) REFERENCES `ehms_departments` (`department_id`),
  CONSTRAINT `EHMS_OT_MASTER__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
  CONSTRAINT `EHMS_OT_MASTER__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_various_price_master` (
`various_price_master_id` SERIAL PRIMARY KEY,
`price_master_service_name_id` bigint(20) unsigned NOT NULL,
`department_id` bigint(50) unsigned NOT NULL,
`price_master_group_type_id` bigint(20) unsigned NOT NULL,
`price_master_sub_group_type_id` bigint(20) unsigned NOT NULL,
`normal_service_rate`  Decimal(50,2) NOT NULL,
`corporate_rate_in_percent`  Decimal(50,2) ,
`corporate_rate_in_ruppes` Decimal(50,2) ,
`cghs_rate_in_percent`  Decimal(50,2) ,
`cghs_rate_in_ruppes` Decimal(50,2) ,
`emergency_rate_in_percent`  Decimal(50,2),
`emergency_rate_in_ruppes` Decimal(50,2),
`other_rate_in_percent`  Decimal(50,2),
`other_rate_in_ruppes` Decimal(50,2),
`customer_id` bigint(20) unsigned NOT NULL,
`hospital_id` bigint(20) unsigned NOT NULL,
`is_active` boolean NOT NULL DEFAULT TRUE,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
 CONSTRAINT `EHMS_VARIOUS_PRICE_MASTER__CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
 CONSTRAINT `EHMS_VARIOUS_PRICE_MASTER__HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`),
 CONSTRAINT `EHMS_VARIOUS_PRICE_MASTER__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
 CONSTRAINT `EHMS_VARIOUS_PRICE_MASTER__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`), 
 CONSTRAINT `EHMS_VARIOUS_PRICE_MASTER__DEPARTMENTS__FK` FOREIGN KEY (`department_id`) REFERENCES `ehms_departments` (`department_id`),
 CONSTRAINT `EHMS_VARIOUS_PRICE_MASTER_SERVICE_NAME__MISCTYPE_VALUES__FK` FOREIGN KEY (`price_master_service_name_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
 CONSTRAINT `EHMS_VARIOUS_PRICE_MASTER_GROUP_TYPE__MISCTYPE_VALUES__FK` FOREIGN KEY (`price_master_group_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
 CONSTRAINT `EHMS_VARIOUS_PRICE_MASTER_SUB_GROUP_TYPE__MISCTYPE_VALUES__FK` FOREIGN KEY (`price_master_sub_group_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 
CREATE TABLE `ehms`.`ehms_tpa_insurance` (
`tpa_insurance_id` SERIAL PRIMARY KEY,
`tpa_insurance_type_id` bigint(50) unsigned NOT NULL,
`tpa_insurance_name` varchar(50) NOT NULL,
`tpa_insurance_number` varchar(50) NOT NULL,
`address_id` bigint(50) unsigned NOT NULL,
`country_code_for_phone_number` varchar(10) NOT NULL,
`phone_number` varchar(20) NOT NULL,
`country_code_for_mobile_number` varchar(10) NOT NULL,
`mobile_number` varchar(20) NOT NULL,
`effective_from_date` DATETIME NOT NULL,
`effective_to_date` DATETIME NOT NULL,
`key_contact_person` varchar(30) NOT NULL,
`country_code_for_key_contact_person_number` varchar(10) NOT NULL,
`key_contact_person_number` varchar(20) NOT NULL,
`tpa_insurance_claim_type_id` bigint(50) unsigned NOT NULL,
`customer_id` bigint(20) unsigned NOT NULL,
`hospital_id` bigint(20) unsigned NOT NULL,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
 CONSTRAINT `EHMS_TPA_INSURANCE__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
 CONSTRAINT `EHMS_TPA_INSURANCE__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`), 
 CONSTRAINT `EHMS_TPA_INSURANCE__CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
 CONSTRAINT `EHMS_TPA_INSURANCE__HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`),
 CONSTRAINT `EHMS_TPA_INSURANCE__ADDRESSES__FK` FOREIGN KEY (`address_id`) REFERENCES `ehms_addresses` (`address_id`), 
 CONSTRAINT `EHMS_TPA_INSURANCE_INSURANCE_TYPE__MISCTYPE_VALUES__FK` FOREIGN KEY (`tpa_insurance_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
 CONSTRAINT `EHMS_TPA_INSURANCE_INSURANCE_CLAIM_TYPE__MISCTYPE_VALUES__FK` FOREIGN KEY (`tpa_insurance_claim_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
CREATE TABLE `ehms`.`ehms_vendor_master` (
`vendor_master_id` SERIAL PRIMARY KEY,
`vendor_name` varchar(100) NOT NULL,
`vendor_type_id` bigint(50) unsigned NOT NULL,
`department_id` bigint(50) unsigned NOT NULL,
`address_id` bigint(50) unsigned NOT NULL,
`phone_no` bigint(50) ,
`mobile_no` bigint(50) ,
`key_contact_person_name` varchar(30) NOT NULL,
`key_contact_person_number`  bigint(50) NOT NULL,
`country_code_mobile_number`  varchar(30) ,
`country_code_contact_number`  varchar(30) ,
`email` varchar(100),
`gst_no` varchar(50),
`dl_no` varchar(50),
`pan_no` varchar(50),
`tan_no` varchar(50),
`shop_act_no` varchar(50),
`vendor_no` varchar(50),
`opening_balance` Decimal(50,2),
`customer_id` bigint(20) unsigned NOT NULL,
`hospital_id` bigint(20) unsigned NOT NULL,
`is_active` boolean NOT NULL DEFAULT TRUE,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_VENDOR_MASTER__CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
CONSTRAINT `EHMS_VENDOR_MASTER__HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`),
CONSTRAINT `EHMS_VENDOR_MASTER__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_VENDOR_MASTER__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`), 
CONSTRAINT `EHMS_VENDOR_MASTER__ADDRESSES__FK` FOREIGN KEY (`address_id`) REFERENCES `ehms_addresses` (`address_id`),
CONSTRAINT `EHMS_VENDOR_MASTER__DEPARTMENTS__FK` FOREIGN KEY (`department_id`) REFERENCES `ehms_departments` (`department_id`),
CONSTRAINT `EHMS_VENDOR_MASTER_VENDOR_TYPE__MISCTYPE_VALUES__FK` FOREIGN KEY (`vendor_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
CREATE TABLE `ehms`.`ehms_sponsor_corporate` (
`sponsor_corporate_id` SERIAL PRIMARY KEY,
`sponsor_corporate_type_id` bigint(50) unsigned NOT NULL,
`sponsor_corporate_name` varchar(50) NOT NULL,
`sponsor_corporate_number` varchar(50) NOT NULL,
`country_code_for_phone_number` varchar(10) NOT NULL,
`phone_number` varchar(20) NOT NULL,
`country_code_for_mobile_number` varchar(10) NOT NULL,
`mobile_number` varchar(20) NOT NULL,
`address_id` bigint(50) unsigned NOT NULL,
`effective_from_date` DATETIME NOT NULL,
`effective_to_date` DATETIME NOT NULL,
`key_contact_person` varchar(30) NOT NULL,
`country_code_for_key_contact_person_number` varchar(10) NOT NULL,
`key_contact_person_number` varchar(20) NOT NULL,
`maximum_limit_patient_wise` bigint(50) unsigned,
`maximum_limit_sponsor_corporate_wise` bigint(50) unsigned,
`customer_id` bigint(20) unsigned NOT NULL,
`hospital_id` bigint(20) unsigned NOT NULL,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_SPONSOR_CORPORATE_SPONSOR_TYPE__MISCTYPE_VALUES__FK` FOREIGN KEY (`sponsor_corporate_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
CONSTRAINT `EHMS_SPONSOR_CORPORATE__ADDRESSES__FK` FOREIGN KEY (`address_id`) REFERENCES `ehms_addresses` (`address_id`), 
CONSTRAINT `EHMS_SPONSOR_CORPORATE__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_SPONSOR_CORPORATE__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`), 
CONSTRAINT `EHMS_SPONSOR_CORPORATE__CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
CONSTRAINT `EHMS_SPONSOR_CORPORATE__HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
CREATE TABLE `ehms`.`ehms_vehicles` (
`vehicle_id` SERIAL PRIMARY KEY,
`vehicle_name` varchar(100) NOT NULL,
`vehicle_type_id` bigint(50) unsigned NOT NULL,
`vehicle_number` varchar(100) NOT NULL,
`chasis_number` varchar(100) NOT NULL,
`year_of_model` int NOT NULL,
`vehicle_color` int NOT NULL,
`vehicle_manufacturer` int,
`average_per_km` bigint(50) unsigned NOT NULL,
`type_of_fuel` int NOT NULL,
`fuel_capacity` Decimal(50,2),
`insurance_number` varchar(100),
`insurance_number_validity` varchar(100),
`rto_registration_number` varchar(100),
`rto_registration_number_validity` varchar(100),
`gps_enabled` int,
`customer_id` bigint(20) unsigned NOT NULL,
`hospital_id` bigint(20) unsigned NOT NULL,
`is_active` boolean NOT NULL DEFAULT TRUE,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_VEHICLES_VEHICLE_TYPE__MISCTYPE_VALUES__FK` FOREIGN KEY (`vehicle_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
CONSTRAINT `EHMS_VEHICLES__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_VEHICLES__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`), 
CONSTRAINT `EHMS_VEHICLES__CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
CONSTRAINT `EHMS_VEHICLES__HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_roster_of_staff` (
`roster_of_staff_id` SERIAL PRIMARY KEY,
`employee_id` bigint(50) unsigned NOT NULL,
`staff_shift_id` bigint(50) unsigned,
`day` int ,
`time_from` Time NOT NULL,
`time_to` Time NOT NULL,
`date_from` DATETIME NOT NULL,
`date_to` DATETIME NOT NULL,
`weekly_off` int,
`remark` varchar(200),
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_ROSTER_OF_STAFF__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_ROSTER_OF_STAFF__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_ROSTER_OF_STAFF_NAME__EMPLOYEE__FK` FOREIGN KEY (`employee_id`) REFERENCES `ehms_employees` (`employee_id`),
CONSTRAINT `EHMS_ROSTER_OF_STAFF_STAFF_SHIFT_MISCTYPE_VALUES__FK` FOREIGN KEY (`staff_shift_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
CREATE TABLE `ehms`.`ehms_roster_of_nurse` (
`roster_of_nurse_id` SERIAL PRIMARY KEY,
`employee_id` bigint(50) unsigned NOT NULL,
`nurse_shift_id` bigint(50) unsigned,
`time_from` Time NOT NULL,
`time_to` Time NOT NULL,
`day` int,
`date_from` DATETIME NOT NULL,
`date_to` DATETIME NOT NULL,
`weekly_off` int,
`remark` varchar(200),
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_ROSTER_OF_NURSE__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_ROSTER_OF_NURSE__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`), 
CONSTRAINT `EHMS_ROSTER_OF_NURSE_NAME__EMPLOYEE__FK` FOREIGN KEY (`employee_id`) REFERENCES `ehms_employees` (`employee_id`),
CONSTRAINT `EHMS_ROSTER_OF_NURSE_NURSE_SHIFT_MISCTYPE_VALUES__FK` FOREIGN KEY (`nurse_shift_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 CREATE TABLE `ehms`.`ehms_vehicle_routes` (
`vehicle_route_id` SERIAL PRIMARY KEY,
`vehicle_id` bigint(50) unsigned NOT NULL,     
`route` varchar(300) NOT NULL,
`source` varchar(300) NOT NULL,
`destination` varchar(300) NOT NULL,
`route_km` bigint(10) unsigned NOT NULL,
`gps_based_route` int,
`route_validity_from` DATETIME,
`route_validity_to` DATETIME,
`is_active` boolean NOT NULL DEFAULT TRUE,
`customer_id` bigint(20) unsigned NOT NULL,
`hospital_id` bigint(20) unsigned NOT NULL,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_VEHICLE_ROUTES__VEHICLES__FK` FOREIGN KEY (`vehicle_id`) REFERENCES `ehms_vehicles` (`vehicle_id`),
CONSTRAINT `EHMS_VEHICLE_ROUTES__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_VEHICLE_ROUTES__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`), 
CONSTRAINT `EHMS_VEHICLE_ROUTES__CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
CONSTRAINT `EHMS_VEHICLE_ROUTES__HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
