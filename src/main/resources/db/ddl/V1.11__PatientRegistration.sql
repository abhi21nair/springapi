DROP TABLE `ehms`.`ehms_patients`;

CREATE TABLE `ehms`.`ehms_patients` (
`patient_id` SERIAL PRIMARY KEY,
`patient_code` VARCHAR(100) NOT NULL,
`title_id` bigint(20) unsigned NOT NULL,
CONSTRAINT `EHMS_PATIENTS_TITLES__FK` FOREIGN KEY (`title_id`) REFERENCES `ehms_titles` (`title_id`),
`first_name` varchar(25) NOT NULL,
`middle_name` varchar(25),
`last_name` varchar(25) NOT NULL,
`birth_date` DATETIME,
`birth_year` INT,
`is_dob_available` BOOLEAN NOT NULL,
`gender` varchar(20) NOT NULL,
`marital_status` varchar(20),
`current_address_id` bigint(20) unsigned,
`permanent_address_id` bigint(20) unsigned,
`contact_number` varchar(20),
`mobile_number` varchar(20) NOT NULL,
`nationality` varchar(50),
`passport_number` varchar(30),
`valid_till` DATETIME,
`adhar_no` varchar(30),
`reference_doctor` varchar(100),
`relative_name` varchar(100),
`relation` varchar(20),
`religion` varchar(20),
`allergy` varchar(100),
`sponsor_corporate_id` bigint(20) unsigned,
`nominee_type_id` bigint(20) unsigned,
`nominee_name` varchar(100),
`hospital_id` bigint(20) unsigned NOT NULL,
`customer_id` bigint(20) unsigned NOT NULL,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
UNIQUE KEY `uniquekey` (`patient_code`),
CONSTRAINT `EHMS_PATIENT_CURRENT_ADDRESSES__FK` FOREIGN KEY (`current_address_id`) REFERENCES `ehms_addresses` (`address_id`),
CONSTRAINT `EHMS_PATIENT_PERM_ADDRESSES__FK` FOREIGN KEY (`permanent_address_id`) REFERENCES `ehms_addresses` (`address_id`),
CONSTRAINT `EHMS_PATIENT_SPONSOR_CORP__FK` FOREIGN KEY (`sponsor_corporate_id`) REFERENCES `ehms_sponsor_corporate` (`sponsor_corporate_id`),
CONSTRAINT `EHMS_PATIENT_NOM_TYPE_MISCTYPE_VALUES__FK` FOREIGN KEY (`nominee_type_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
CONSTRAINT `EHMS_PATIENT_REG_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_PATIENT_REG_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;