CREATE TABLE `ehms`.`ehms_appointments` (
`appointment_id` SERIAL PRIMARY KEY,
`customer_id` bigint(20) unsigned,
`hospital_id` bigint(20) unsigned,
`module_id` bigint(50) unsigned, 
`doctor_id` bigint(50) unsigned, 
`patient_id` bigint(50) unsigned NOT NULL, 
`doctor_schedule_id` bigint(50) unsigned, 
`from_time` DATETIME NOT NULL,
`to_time` DATETIME NOT NULL,
`status` int NOT NULL,
`report_time` DATETIME,
`appointment_start_time` DATETIME,
`appointment_end_time` DATETIME,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned NOT NULL,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS_APPOINTMENTS__MODULES__FK` FOREIGN KEY (`module_id`) REFERENCES `ehms_modules` (`module_id`),
CONSTRAINT `EHMS_APPOINTMENTS__EMPLOYEES__FK` FOREIGN KEY (`doctor_id`) REFERENCES `ehms_employees` (`employee_id`),
CONSTRAINT `EHMS_APPOINTMENTS__PATIENTS__FK` FOREIGN KEY (`patient_id`) REFERENCES `ehms_patients` (`patient_id`),
CONSTRAINT `EHMS_APPOINTMENTS__DOC_SCHEDULES__FK` FOREIGN KEY (`doctor_schedule_id`) REFERENCES `ehms_doc_schedules` (`doctor_schedule_id`),
CONSTRAINT `EHMS_APPOINTMENTS__USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS_APPOINTMENTS__USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`), 
CONSTRAINT `EHMS_APPOINTMENTS__CUSTOMER__FK` FOREIGN KEY (`customer_id`) REFERENCES `ehms_customers` (`customer_id`),
CONSTRAINT `EHMS_APPOINTMENTS__HOSPITAL__FK` FOREIGN KEY (`hospital_id`) REFERENCES `ehms_hospitals` (`hospital_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create index from_time_idx on `ehms`.`ehms_appointments` (customer_id, hospital_id, from_time);