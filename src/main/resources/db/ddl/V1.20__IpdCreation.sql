CREATE TABLE `ehms`.`ehms_daily_progress_notes`(
`daily_progress_note_id` SERIAL PRIMARY KEY,
`appointment_id` bigint(20) unsigned NOT NULL,
`patient_id` bigint(20) unsigned NOT NULL,
`doctor_day_progress_notes` varchar(300),
`doctor_night_progress_notes` varchar(300),
`previous_notes` varchar(300),
`progress_date` DATETIME,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS__DPN__APPOINTMENTS__FK` FOREIGN KEY (`appointment_id`) REFERENCES `ehms_appointments` (`appointment_id`),
CONSTRAINT `EHMS__DPN__PATIENTS__FK` FOREIGN KEY (`patient_id`) REFERENCES `ehms_patients` (`patient_id`),
CONSTRAINT `EHMS__DPN_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS__DPN_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_diet_master`(
`diet_master_id` SERIAL PRIMARY KEY,
`appointment_id` bigint(20) unsigned NOT NULL,
`patient_id` bigint(20) unsigned NOT NULL,
`diet_date` DATETIME,
`morning_tea` varchar(100),
`breakfast` varchar(100),
`lunch` varchar(100),
`evening_tea` varchar(100),
`dinner` varchar(100),
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS__DIET_MASTER__APPOINTMENTS__FK` FOREIGN KEY (`appointment_id`) REFERENCES `ehms_appointments` (`appointment_id`),
CONSTRAINT `EHMS__DIET_MASTER__PATIENTS__FK` FOREIGN KEY (`patient_id`) REFERENCES `ehms_patients` (`patient_id`),
CONSTRAINT `EHMS__DIET_MASTER_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS__DIET_MASTER_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_discharge_sheets`(
`discharge_sheet_id` SERIAL PRIMARY KEY,
`appointment_id` bigint(20) unsigned NOT NULL,
`patient_id` bigint(20) unsigned NOT NULL,
`discharge_condition_id` bigint(20) unsigned NOT NULL,
`discharge_date` DATETIME,
`death_date` DATETIME,
`cause_of_death` varchar(300),
`advice` varchar(300),
`next_appointment` varchar(300),
`other_details` varchar(300),
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS__DS__APPOINTMENTS__FK` FOREIGN KEY (`appointment_id`) REFERENCES `ehms_appointments` (`appointment_id`),
CONSTRAINT `EHMS__DS__PATIENTS__FK` FOREIGN KEY (`patient_id`) REFERENCES `ehms_patients` (`patient_id`),
CONSTRAINT `EHMS__DS__MISCTYPE_DISCHARGE_VALUES__FK` FOREIGN KEY (`discharge_condition_id`) REFERENCES `ehms_misc_values` (`misc_type_value_id`),
CONSTRAINT `EHMS__DS_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS__DS_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ehms`.`ehms_patient_postmortems`(
`patient_postmortem_id` SERIAL PRIMARY KEY,
`appointment_id` bigint(20) unsigned NOT NULL,
`patient_id` bigint(20) unsigned NOT NULL,
`death_date` DATETIME,
`cause_of_death` varchar(100),
`dead_body_handed` int,
`dead_body_handed_to` varchar(100),
`morgue_time` Time,
`postmortem_time` Time,
`created_dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_dt` DATETIME,
`created_by` bigint(20) unsigned,
`updated_by` bigint(20) unsigned,
CONSTRAINT `EHMS__PP__APPOINTMENTS__FK` FOREIGN KEY (`appointment_id`) REFERENCES `ehms_appointments` (`appointment_id`),
CONSTRAINT `EHMS__PP__PATIENTS__FK` FOREIGN KEY (`patient_id`) REFERENCES `ehms_patients` (`patient_id`),
CONSTRAINT `EHMS__PP_USERS_CREATEDBY__FK` FOREIGN KEY (`created_by`) REFERENCES `ehms_users` (`user_id`),
CONSTRAINT `EHMS__PP_USERS_UPDATEDBY__FK` FOREIGN KEY (`updated_by`) REFERENCES `ehms_users` (`user_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
