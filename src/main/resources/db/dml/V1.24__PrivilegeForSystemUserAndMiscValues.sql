INSERT INTO `ehms`.`ehms_privileges`(`privilege_id`,`privilege_code`,`privilege_description`, `is_system_privilege`, `module_id`)VALUES(34, 'SYSTEM_EMPLOYEE_MANAGEMENT', 'System level Employee', '1', '1');
INSERT INTO `ehms`.`ehms_link_role_privileges`(`role_id`,`privilege_id`) VALUES(1,34);

INSERT INTO `ehms`.`ehms_departments` (`department_id`, `department_name`, `department_description`, `department_type`, `is_active`) VALUES ('3', 'Accounts', 'Account Department', 'departmentType3', '1');
INSERT INTO `ehms`.`ehms_departments` (`department_id`, `department_name`, `department_description`, `department_type`, `is_active`) VALUES ('4', 'Admin', 'Admin Department', 'departmentType4', '1');
INSERT INTO `ehms`.`ehms_departments` (`department_id`, `department_name`, `department_description`, `department_type`, `is_active`) VALUES ('5', 'Other', 'Other Department', 'departmentType5', '1');
INSERT INTO `ehms`.`ehms_departments` (`department_id`, `department_name`, `department_description`, `department_type`, `is_active`) VALUES ('6', 'HR', 'Human Resourse Department', 'departmentType6', '1');
INSERT INTO `ehms`.`ehms_departments` (`department_id`, `department_name`, `department_description`, `department_type`, `is_active`) VALUES ('7', 'Sales', 'Sales Department', 'departmentType7', '1');
INSERT INTO `ehms`.`ehms_departments` (`department_id`, `department_name`, `department_description`, `department_type`, `is_active`) VALUES ('8', 'IT', 'Information Technology Department', 'departmentType8', '1');

INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`) VALUES ((select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'DESIGNATION'), 'Administrator');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`) VALUES ((select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'DESIGNATION'), 'Manager');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`) VALUES ((select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'DESIGNATION'), 'Staff');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`) VALUES ((select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'DESIGNATION'), 'HR');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`) VALUES ((select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'DESIGNATION'), 'Junior Software Developer');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`) VALUES ((select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'DESIGNATION'), 'Senior Software Developer');

INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`) VALUES ((select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'JOINING_TYPE'), 'Temporary');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`) VALUES ((select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'JOINING_TYPE'), 'Permanent');

INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`) VALUES ((select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'OTHER_ID_TYPE'), 'Id Card');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`) VALUES ((select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'OTHER_ID_TYPE'), 'Other Id');

INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`) VALUES ((select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'SPECIALIZATION'), 'Bbnisys');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`) VALUES ((select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'SPECIALIZATION'), 'Other');
