INSERT INTO `ehms`.`ehms_privileges`(`privilege_id`,`privilege_code`,`privilege_description`, `is_system_privilege`, `module_id`)VALUES(28,'SYSTEM_USER_MANAGEMENT','system user management', 1, 1);
INSERT INTO `ehms`.`ehms_link_role_privileges`(`role_privilege_id`,`role_id`,`privilege_id`) VALUES(35,1,28);

INSERT INTO `ehms`.`ehms_privileges`(`privilege_id`,`privilege_code`,`privilege_description`, `is_system_privilege`, `module_id`)VALUES(29,'SYSTEM_ROLE_MANAGEMENT','system role management', 1, 1);
INSERT INTO `ehms`.`ehms_link_role_privileges`(`role_privilege_id`,`role_id`,`privilege_id`) VALUES(36,1,29);


