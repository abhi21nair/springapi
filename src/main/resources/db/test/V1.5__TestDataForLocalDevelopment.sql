
INSERT INTO `ehms_addresses`(`address_id`,`address1`,`address2`,`street`,`region`,`city_id`,`zipcode`) VALUES (1,'401 1st Main Street','Near XYZ','jm road','camp',1,'411003');

INSERT INTO `ehms`.`ehms_customers`(`organization_code`,`customer_name`,`country_code_for_contact_number`,`contact_number`,`is_active`,`address_id`,`referral_type`,`referral_person_name`,`buying_type_id`,`buying_preference_id`,`subscription_type_id`,`buying_type_quantity`, `expiry_date`)VALUES('Demo001','Dinanath','+91','1234567891',true,1, 'Friend','Ajit',1,1,1,20, (NOW() + INTERVAL 30 DAY));

INSERT INTO `ehms`.`ehms_hospitals` (`hospital_id`, `hospital_name`, `hospital_code`, `is_active`, `country_code_for_contact_number`, `contact_number`, `country_code_for_phone_number`, `phone_number`, `customer_id`, `address_id`) VALUES ('1', 'kumudini-1', 'EHMS_HOSP_KUMUDINI_001', true, '91', '9874563214', '91', '8965412377', '1', '1');

INSERT INTO `ehms`.`ehms_departments` (`department_id`, `department_name`, `department_description`, `department_type`, `is_active`, `customer_id`) VALUES ('1', 'OT', 'Operation Theatre', 'departmentType1', true, '1');
INSERT INTO `ehms`.`ehms_departments` (`department_id`, `department_name`, `department_description`, `department_type`, `is_active`, `customer_id`) VALUES ('2', 'OPD', 'Out Patient Department', 'departmentType2', true, '1');

INSERT INTO `ehms`.`ehms_misc_values`(`misc_type_id`,`misc_value`,`customer_id`,`hospital_id`)VALUES( (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'OTHER_ID_TYPE'),'Driving Lic',1,null);
INSERT INTO `ehms`.`ehms_misc_values`(`misc_type_id`,`misc_value`,`customer_id`,`hospital_id`)VALUES( (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'OTHER_ID_TYPE'),'Other',1,null);
INSERT INTO `ehms`.`ehms_misc_values`(`misc_type_id`,`misc_value`,`customer_id`,`hospital_id`)VALUES( (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'OTHER_ID_TYPE'),'Hospital ID Card',1,1);
INSERT INTO `ehms`.`ehms_misc_values`(`misc_type_id`,`misc_value`,`customer_id`,`hospital_id`)VALUES( (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'SPECIALIZATION'),'Allergist',1,null);
INSERT INTO `ehms`.`ehms_misc_values`(`misc_type_id`,`misc_value`,`customer_id`,`hospital_id`)VALUES( (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'SPECIALIZATION'),'Neurologist',1,null);
INSERT INTO `ehms`.`ehms_misc_values`(`misc_type_id`,`misc_value`,`customer_id`,`hospital_id`)VALUES( (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'SPECIALIZATION'),'Other',1,1);
INSERT INTO `ehms`.`ehms_misc_values`(`misc_type_id`,`misc_value`,`customer_id`,`hospital_id`)VALUES( (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'JOINING_TYPE'),'Temporary',1,null);
INSERT INTO `ehms`.`ehms_misc_values`(`misc_type_id`,`misc_value`,`customer_id`,`hospital_id`)VALUES( (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'JOINING_TYPE'),'Permanent',1,null);
INSERT INTO `ehms`.`ehms_misc_values`(`misc_type_id`,`misc_value`,`customer_id`,`hospital_id`)VALUES( (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'DESIGNATION'),'Jr Doctor',1,null);
INSERT INTO `ehms`.`ehms_misc_values`(`misc_type_id`,`misc_value`,`customer_id`,`hospital_id`)VALUES( (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'DESIGNATION'),'Technician',1,null);
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'STAFF_SHIFT'), 'staff shift 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'STAFF_SHIFT'), 'staff shift 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'STAFF_SHIFT'), 'staff shift 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'NURSE_SHIFT'), 'nurse shift 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'NURSE_SHIFT'), 'nurse shift 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'NURSE_SHIFT'), 'nurse shift 3', '1', '1');



-- insert 2 employee
INSERT INTO `ehms`.`ehms_employees`
(`employee_id`,
`title_id`,
`first_name`,
`middle_name`,
`last_name`,
`employee_code`,
`employee_type`,
`designation_id`,
`department_id`,
`birth_date`,
`birth_year`,
`is_dob_available`,
`joining_date`,
`gender`,
`marital_status`,
`nationality`,
`adhar_no`,
`other_id_type`,
`other_id_no`,
`other_id_valid_till`,
`pan_no`,
`tan_no`,
`qualification`,
`specialization_id`,
`joining_type_id`,
`hospital_share_pc`,
`hospital_share_rs`,
`doctor_share_pc`,
`doctor_share_rs`,
`email`,
`country_code_for_mobile_number`,
`mobile_number`,
`country_code_for_contact_number`,
`contact_number`,
`nominee_name`,
`nominee_relation`,
`current_address_id`,
`permanent_address_id`,
`hospital_id`,
`customer_id`,
`is_active`)
VALUES
(1,
1,
'Mohan',
'Deepak',
'Patil',
'EMP001',
1,
(select misc_type_value_id from ehms.ehms_misc_values where misc_type_id = (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'DESIGNATION') and misc_value = 'Customer Admin'),
1,
'1988-10-10',
1988,
1,
'2008-10-10',
'M',
'married',
'Indian',
'1232-1212132-1212143',
(select misc_type_value_id from ehms.ehms_misc_values where misc_type_id = (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'OTHER_ID_TYPE') and misc_value = 'Driving Lic'),
'DDDD7778',
'2040-10-10',
'AEDFT5342D',
'AZARDFG8787',
'DHMS',
(select misc_type_value_id from ehms.ehms_misc_values where misc_type_id = (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'SPECIALIZATION') and misc_value = 'Neurologist'),
(select misc_type_value_id from ehms.ehms_misc_values where misc_type_id = (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'JOINING_TYPE') and misc_value = 'Permanent'),
1,
1000.5,
2,
2000.10,
'test@test.com',
'91',
'1234567890',
'91',
'0987654321',
'Deepak Patil',
'Father',
1,
1,
null,
1,
1);

INSERT INTO `ehms`.`ehms_employees`
(`employee_id`,
`title_id`,
`first_name`,
`middle_name`,
`last_name`,
`employee_code`,
`employee_type`,
`designation_id`,
`department_id`,
`birth_date`,
`birth_year`,
`is_dob_available`,
`joining_date`,
`gender`,
`marital_status`,
`nationality`,
`adhar_no`,
`other_id_type`,
`other_id_no`,
`other_id_valid_till`,
`pan_no`,
`tan_no`,
`qualification`,
`specialization_id`,
`joining_type_id`,
`hospital_share_pc`,
`hospital_share_rs`,
`doctor_share_pc`,
`doctor_share_rs`,
`email`,
`country_code_for_mobile_number`,
`mobile_number`,
`country_code_for_contact_number`,
`contact_number`,
`nominee_name`,
`nominee_relation`,
`current_address_id`,
`permanent_address_id`,
`hospital_id`,
`customer_id`,
`is_active`)
VALUES
(2,
1,
'Vaibhav',
'Neeraj',
'Kale',
'EMP002',
2,
(select misc_type_value_id from ehms.ehms_misc_values where misc_type_id = (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'DESIGNATION') and misc_value = 'Technician'),
2,
null,
1990,
0,
'2006-11-11',
'M',
'Single',
'Indian',
null,
null,
null,
null,
null,
null,
'Technician',
null,
null,
null,
null,
null,
null,
null,
'91',
'1234567890',
null,
null,
null,
null,
1,
1,
1,
1,
1);

INSERT INTO `ehms`.`ehms_employees` (`employee_id`, `title_id`, `first_name`, `middle_name`, `last_name`,`employee_code`, `employee_type`, `designation_id`, `department_id`, `birth_date`, `birth_year`, `is_dob_available`, `joining_date`, `gender`, `marital_status`, `nationality`, `adhar_no`, `other_id_type`, `other_id_no`, `other_id_valid_till`, `pan_no`, `tan_no`, `qualification`, `specialization_id`, `joining_type_id`, `hospital_share_pc`, `hospital_share_rs`, `doctor_share_pc`, `doctor_share_rs`, `email`, `country_code_for_mobile_number`, `mobile_number`, `country_code_for_contact_number`, `contact_number`, `nominee_name`, `nominee_relation`, `current_address_id`, `permanent_address_id`, `hospital_id`, `customer_id`, `is_active`) VALUES ('3', '2', 'Mansi', 'deepak', 'Kalbhor', 'EMP003', 0, '3', '2', '1989-06-10 00:00:00', '1989', '1', '2005-11-11 00:00:00', 'F', 'Single', 'Indian', '4555-7899899-4545454', '2', 'DCYPK788L', '2030-10-10 00:00:00', 'JHJHJHJ8888', 'KJJBNJJK545', 'MBBS', '6', '2', '3', '200', '2', '988', 'demo@gmail.com', '91', '9874563214', '91', '9874563214', 'yogesh patel', 'Friend', '1', '1', '1', '1', '1');

INSERT INTO `ehms`.`ehms_users`(`user_id`,`username`,`password`,`security_question`,`security_answer`,`is_reset_password_on_login`,`enabled`,`employee_id`)VALUES(2,'customer_admin','$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi','first teacher name','Amit',1,true,1);
INSERT INTO `ehms`.`ehms_link_user_role` VALUES(2,2,2);

INSERT INTO `ehms`.`ehms_users`(`user_id`,`username`,`password`,`security_question`,`security_answer`,`is_reset_password_on_login`,`enabled`,`employee_id`)VALUES(3,'hospital_admin','$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi','first teacher name','Amit',1,true,2);
INSERT INTO `ehms`.`ehms_link_user_role` VALUES(3,3,3);

update `ehms`.`ehms_customers` set key_user_id = 2 where customer_id = 1;
update `ehms`.`ehms_hospitals` set key_user_id = 3 where hospital_id < 10;

INSERT INTO `ehms`.`ehms_link_customer_module` values (1, 1, 1);
INSERT INTO `ehms`.`ehms_link_customer_module` values (2, 2, 1);

INSERT INTO `ehms`.`ehms_customer_payments` (`customer_payment_id`, `customer_id`, `cost_of_product`, `gst_number` , `pan_number`,`payment_status_id`,`amc_addition`, `other_service_1`, `other_service_1_type`,`other_service_2`, `other_service_2_type`,`discount`, `discount_valid_from`, `discount_valid_to`, `discount_in_percent`, `value`, `discount_in_percent_valid_from`, `discount_in_percent_valid_to`, `tax1`, `tax2`,`tax3`, `total_billing_amount`, `payment_mode_id`,`expiry_date`,`action_by`,`created_DT`) VALUES ('1', '1', '9000','55545454','55454545','1','demo1Amcaddition', 'service1','service1type', 'service2', 'service2type','500', '2016-12-12', '2017-12-12', '30', '1000', '2016-12-12', '2017-12-12', '8000', '8000', '2000', '90000', '1','2017-12-12','1',CURRENT_TIMESTAMP);

insert into ehms_misc_values (misc_type_id, misc_value,customer_id,hospital_id) values ((select misc_type_id from ehms_misc_type where misc_type_name = 'FLOOR'), 'Fisrt Floor',1,1);
insert into ehms_misc_values (misc_type_id, misc_value,customer_id,hospital_id) values ((select misc_type_id from ehms_misc_type where misc_type_name = 'FLOOR'), 'Second Floor',1,1);
insert into ehms_misc_values (misc_type_id, misc_value,customer_id,hospital_id) values ((select misc_type_id from ehms_misc_type where misc_type_name = 'WARD_TYPE'), 'General Ward',1,1);
insert into ehms_misc_values (misc_type_id, misc_value,customer_id,hospital_id) values ((select misc_type_id from ehms_misc_type where misc_type_name = 'WARD_TYPE'), 'Childern ward',1,1);
insert into ehms_misc_values (misc_type_id, misc_value,customer_id,hospital_id) values ((select misc_type_id from ehms_misc_type where misc_type_name = 'WARD_TYPE'), 'Surgical ward',1,1);
insert into ehms_misc_values (misc_type_id, misc_value,customer_id,hospital_id) values ((select misc_type_id from ehms_misc_type where misc_type_name = 'WARD_TYPE'), 'Maternity ward',1,1);
insert into ehms_misc_values (misc_type_id, misc_value,customer_id,hospital_id) values ((select misc_type_id from ehms_misc_type where misc_type_name = 'BED_CATEGORY'), 'Manual',1,1);
insert into ehms_misc_values (misc_type_id, misc_value,customer_id,hospital_id) values ((select misc_type_id from ehms_misc_type where misc_type_name = 'BED_CATEGORY'), 'Semi Electric',1,1);
insert into ehms_misc_values (misc_type_id, misc_value,customer_id,hospital_id) values ((select misc_type_id from ehms_misc_type where misc_type_name = 'BED_CATEGORY'), 'Full Electric',1,1);
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'DOCTOR_SHIFT'), 'First Shift', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'DOCTOR_SHIFT'), 'Second Shift', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'DOCTOR_SHIFT'), 'Third Shift', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'APPOINTMENT_TYPE'), 'Consulting', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'APPOINTMENT_TYPE'), 'Telephonic', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'APPOINTMENT_TYPE'), 'Operation', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'TARRIF/SERVICE_NAME'), 'Tarrif Service 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'TARRIF/SERVICE_NAME'), 'Tarrif Service 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'TARRIF/SERVICE_NAME'), 'Tarrif Service 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'SERVICE_TYPE'), 'Service Type 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'SERVICE_TYPE'), 'Service Type 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'SERVICE_TYPE'), 'Service Type 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'MATERIAL_TYPE'), 'Material Type 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'MATERIAL_TYPE'), 'Material Type 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'MATERIAL_TYPE'), 'Material Type 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PATHOLOGY_TEST_VALUE_TYPE'), 'pathology test value type 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PATHOLOGY_TEST_VALUE_TYPE'), 'pathology test value type 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PATHOLOGY_TEST_VALUE_TYPE'), 'pathology test value type 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PATHOLOGY_SPECIMEN_TYPE'), 'pathology specimen Type 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PATHOLOGY_SPECIMEN_TYPE'), 'pathology specimen Type 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PATHOLOGY_SPECIMEN_TYPE'), 'pathology specimen Type 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PATHOLOGY_CONTAINER_TYPE'), 'pathology container Type 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PATHOLOGY_CONTAINER_TYPE'), 'pathology container Type 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PATHOLOGY_CONTAINER_TYPE'), 'pathology container Type 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PATHOLOGY_RESULT_TYPE'), 'pathology result Type 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PATHOLOGY_RESULT_TYPE'), 'pathology result Type 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PATHOLOGY_RESULT_TYPE'), 'pathology result Type 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'VARIOUS_PRICE_MASTER_SERVICE_NAME'), 'various price master Service 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'VARIOUS_PRICE_MASTER_SERVICE_NAME'), 'various price master Service 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'VARIOUS_PRICE_MASTER_SERVICE_NAME'), 'various price master Service 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PRICE_MASTER_GROUP_TYPE'), 'price master group Type 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PRICE_MASTER_GROUP_TYPE'), 'price master group Type 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PRICE_MASTER_GROUP_TYPE'), 'price master group Type 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PRICE_MASTER_SUB_GROUP_TYPE'), 'price master sub group Type 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PRICE_MASTER_SUB_GROUP_TYPE'), 'price master sub group Type 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PRICE_MASTER_SUB_GROUP_TYPE'), 'price master sub group Type 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'TPA/INSURANCE_TYPE'), 'Tpa/Insurance 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'TPA/INSURANCE_TYPE'), 'Tpa/Insurance 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'TPA/INSURANCE_TYPE'), 'Tpa/Insurance 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'TPA/INSURANCE_CLAIM_TYPE'), 'Tpa/Insurance Claim 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'TPA/INSURANCE_CLAIM_TYPE'), 'Tpa/Insurance Claim 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'TPA/INSURANCE_CLAIM_TYPE'), 'Tpa/Insurance Claim 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'VENDOR_TYPE'), 'vendor type 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'VENDOR_TYPE'), 'vendor type 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'VENDOR_TYPE'), 'vendor type 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'SPONSOR/CORPORATE_TYPE'), 'Sponsor/Corporate type 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'SPONSOR/CORPORATE_TYPE'), 'Sponsor/Corporate type 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'SPONSOR/CORPORATE_TYPE'), 'Sponsor/Corporate type 3', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'VEHICLE_TYPE'), 'Basic Life Support Ambulance', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'VEHICLE_TYPE'), 'Advanced Life Support Ambulance', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'VEHICLE_TYPE'), 'Air Ambulance', '1', '1');


INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'DRUG_TYPE'), 'drug type 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'DRUG_TYPE'), 'drug type 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'DRUG_TYPE'), 'drug type 3', '1', '1');

INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'STORE_NAME'), 'store name 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'STORE_NAME'), 'store name 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'STORE_NAME'), 'store name 3', '1', '1');

INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PACKING'), 'packing 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PACKING'), 'packing 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'PACKING'), 'packing 3', '1', '1');

INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'DISEASE'), 'disease 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'DISEASE'), 'disease 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'DISEASE'), 'disease 3', '1', '1');

INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'CATEGORY'), 'category 1', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'CATEGORY'), 'category 2', '1', '1');
INSERT INTO `ehms`.`ehms_misc_values` (`misc_type_id`, `misc_value`, `customer_id`, `hospital_id`) VALUES ((select misc_type_id from ehms_misc_type where misc_type_name = 'CATEGORY'), 'category 3', '1', '1');
