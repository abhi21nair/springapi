package com.ehms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ehms_subscription_types")
public class SubscriptionType implements Serializable {
	
	public static final Long SUBSCRIPTION_TYPE_MONTHLY = 1l;
	public static final Long SUBSCRIPTION_TYPE_YEARLY = 2l;
	public static final Long SUBSCRIPTION_TYPE_LIFETIME = 3l;
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "subscription_type_id")
	private Long subscriptionTypeId;

	@Column(name = "subscription_type_name")
	private String subscriptionTypeName;

	public SubscriptionType(Long subscriptionTypeId) {
		this.subscriptionTypeId = subscriptionTypeId;
	}
}