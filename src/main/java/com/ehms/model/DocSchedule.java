package com.ehms.model;

import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "ehms_doc_schedules")
public class DocSchedule extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;
	public static final String TIME_MAX = "24:00:00";
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "doctor_schedule_id")
	private Long doctorScheduleId;

	@ManyToOne
	@JoinColumn(name = "doctor_id")
	private Employee doctor;

	private int day;

	@Column(name = "time_from")
	private Time timeFrom;

	@Column(name = "time_to")
	private Time timeTo;

	@Column(name = "appointment_type_id")
	private Long appointmentType;

	@Column(name = "no_of_appointment")
	private int noOfAppointment;

	@Column(name = "doctor_shift_id")
	private Long doctorShift;

	@Column(name = "doctor_fees")
	private Double doctorFees;

	public DocSchedule(Long doctorScheduleId) {
		this.doctorScheduleId = doctorScheduleId;
	}
}
