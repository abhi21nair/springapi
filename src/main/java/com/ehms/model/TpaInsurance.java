package com.ehms.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "ehms_tpa_insurance")
public class TpaInsurance extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tpa_insurance_id")
	private Long tpaInsuranceId;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id")
	private Address address;

	@Column(name = "country_code_for_key_contact_person_number")
	private String countryCodeForKeyContactPersonNumber;

	@Column(name = "country_code_for_mobile_number")
	private String countryCodeForMobileNumber;

	@Column(name = "country_code_for_phone_number")
	private String countryCodeForPhoneNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "effective_from_date")
	private Date effectiveFromDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "effective_to_date")
	private Date effectiveToDate;

	@Column(name = "key_contact_person")
	private String keyContactPerson;

	@Column(name = "key_contact_person_number")
	private String keyContactPersonNumber;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "tpa_insurance_name")
	private String tpaInsuranceName;

	@Column(name = "tpa_insurance_number")
	private String tpaInsuranceNumber;

	@Column(name = "tpa_insurance_claim_type_id")
	private Long tpaInsuranceClaimTypeId;

	@Column(name = "tpa_insurance_type_id")
	private Long tpaInsuranceTypeId;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;

}
