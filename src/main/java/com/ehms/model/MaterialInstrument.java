package com.ehms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_material_instrument")
public class MaterialInstrument extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "material_instrument_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long materialInstrumentId;

	private Double charges;

	@Column(name = "instrument_material_number")
	private String materialInstrumentNumber;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "material_instrument_name")
	private String materialInstrumentName;

	@Column(name = "material_instrument_sub_name")
	private String materialInstrumentSubName;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@Column(name = "department_id")
	private Long departmentId;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;

	@Column(name = "instrument_material_type_id")
	private Long materialInstrumentTypeId;
}
