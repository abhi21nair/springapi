package com.ehms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.ehms.security.JwtUser;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Setter
@Getter
public abstract class BaseAuditedEntity implements Serializable {

	private static final long serialVersionUID = -4657237291040889851L;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
	@Column(name = "created_dt")
	private Date createdDt;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "created_by")
	private Long createdBy;

	@Column(name = "updated_by")
	private Long updatedBy;

	@PrePersist
	void onCreate() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		JwtUser currentUser = (JwtUser) auth.getPrincipal();
		this.setCreatedDt(new Date());
		this.setCreatedBy(currentUser.getuserId());
	}

	@PreUpdate
	void onPersist() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		JwtUser currentUser = (JwtUser) auth.getPrincipal();
		this.setUpdatedDt(new Date());
		this.setUpdatedBy(currentUser.getuserId());
	}

}
