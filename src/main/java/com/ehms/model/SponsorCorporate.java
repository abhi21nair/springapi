package com.ehms.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "ehms_sponsor_corporate")
public class SponsorCorporate extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sponsor_corporate_id")
	private Long sponsorCorporateId;

	@Column(name = "country_code_for_key_contact_person_number")
	private String countryCodeForKeyContactPersonNumber;

	@Column(name = "country_code_for_mobile_number")
	private String countryCodeForMobileNumber;

	@Column(name = "country_code_for_phone_number")
	private String countryCodeForPhoneNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "effective_from_date")
	private Date effectiveFromDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "effective_to_date")
	private Date effectiveToDate;

	@Column(name = "key_contact_person")
	private String keyContactPerson;

	@Column(name = "key_contact_person_number")
	private String keyContactPersonNumber;

	@Column(name = "maximum_limit_patient_wise")
	private Long maximumLimitPatientWise;

	@Column(name = "maximum_limit_sponsor_corporate_wise")
	private Long maximumLimitSponsorCorporateWise;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "sponsor_corporate_name")
	private String sponsorCorporateName;

	@Column(name = "sponsor_corporate_number")
	private String sponsorCorporateNumber;

	@Column(name = "sponsor_corporate_type_id")
	private Long sponsorCorporateTypeId;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id")
	private Address address;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;

}
