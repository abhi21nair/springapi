package com.ehms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_buying_preferences")
public class BuyingPreference implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final Long BUYING_PREFERENCE_IMMEDIATE = 1l;
	public static final Long BUYING_PREFERENCE_TRIAL = 2l;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "buying_preference_id")
	private Long buyingPreferenceId;

	@Column(name = "buying_preference_name")
	private String buyingPreferenceName;

	public BuyingPreference(Long buyingPreferenceId) {
		super();
		this.buyingPreferenceId = buyingPreferenceId;
	}
}
