package com.ehms.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "ehms_hospitals")
public class Hospital extends BaseAuditedEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public Hospital(Long hospitalId) {
		this.hospitalId = hospitalId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "hospital_id")
	private Long hospitalId;

	@Column(name = "hospital_name")
	private String hospitalName;
	
	@Column(name = "hospital_code")
	private String hospitalCode;

	@Column(name = "is_active")
	private Boolean isActive;
	
	@Column(name = "country_code_for_contact_number")
	private String countryCodeForContactNumber;
	
	@Column(name = "country_code_for_phone_number")
	private String countryCodeForPhoneNumber;
	
	@Column(name = "contact_number")
	private String contactNumber;
	
	@Column(name = "phone_number")
	private String phoneNumber;
	
	@Column(name = "fax_number")
	private String faxNumber;

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "address_id")
	private Address address;

	@ManyToOne
	@JoinColumn(name = "key_user_id")
	private User keyUser;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@OneToMany(mappedBy = "hospital")
	private List<Patient> patients;

}