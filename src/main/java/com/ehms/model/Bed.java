package com.ehms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_beds")
public class Bed extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "bed_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long bedId;

	@Column(name = "bed_code")
	private String bedCode;

	@ManyToOne
	@JoinColumn(name = "ward_id")
	private Ward ward;

	@OneToOne
	@JoinColumn(name = "current_patient_id")
	private Patient patient;

}