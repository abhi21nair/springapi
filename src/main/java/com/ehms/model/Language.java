
package com.ehms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.*;

/**
 * The persistent class for the role database table.
 * 
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_languages")
public class Language implements Serializable {
	private static final long serialVersionUID = 1L;

	public Language(Long languageId) {
		this.languageId = languageId;
	}

	@Id
	@Column(name = "language_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long languageId;

	@Column(name = "language_code")
	private String languageCode;

	@Column(name = "language_name")
	private String languageName;

}
