package com.ehms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "ehms_apt_referrals")
public class AppointmentReferral extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "referral_id")
	private Long referralId;

	@Column(name = "department_id")
	private Long departmentId;

	@Column(name = "provisional_notes")
	private String provisionalNotes;

	@Column(name = "mlc_case")
	private Boolean mlcCase;

	@Column(name = "instruction_to_warden")
	private String instructionToWarden;

	@Column(name = "general_notes")
	private String generalNotes;

	@Column(name = "appointment_id")
	private Long appointmentId;
}
