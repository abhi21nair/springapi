package com.ehms.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_manufacturing_company")
public class ManufacturingCompany extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "manufacturing_company_id")
	private Long manufacturingCompanyId;

	@Column(name = "cc_for_key_contact_person_number")
	private String ccForKeyContactPersonNumber;

	@Column(name = "country_code_for_mobile_number")
	private String countryCodeForMobileNumber;

	@Column(name = "country_code_for_phone_number")
	private String countryCodeForPhoneNumber;

	@Column(name = "dl_no")
	private String dlNo;

	private String email;

	@Column(name = "gst_no")
	private String gstNo;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "key_contact_person")
	private String keyContactPerson;

	@Column(name = "key_contact_person_number")
	private String keyContactPersonNumber;

	@Column(name = "manufacture_name")
	private String manufactureName;

	@Column(name = "manufacture_number")
	private String manufactureNumber;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "opening_balance")
	private Double openingBalance;

	@Column(name = "pan_no")
	private String panNo;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "shop_act_no")
	private String shopActNo;

	@Column(name = "tan_no")
	private String tanNo;

	@Column(name = "vendor_supplier_no")
	private String vendorSupplierNo;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id")
	private Address address;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;
}
