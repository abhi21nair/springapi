package com.ehms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "ehms_diet_master")
public class DietMaster extends BaseAuditedEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "diet_master_id")
	private Long dietMasterId;

	@Column(name = "appointment_id")
	private Long appointmentId;

	@Column(name = "patient_id")
	private Long patientId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "diet_date")
	private Date dietDate;

	@Column(name = "morning_tea")
	private String morningTea;

	private String breakfast;

	private String lunch;

	@Column(name = "evening_tea")
	private String eveningTea;

	private String dinner;
}
