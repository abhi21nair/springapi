package com.ehms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "ehms_salary_leave_structure")
public class SalaryLeaveStructure extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "salary_leave_structure_id")
	private Long salaryLeaveStructureId;

	@ManyToOne
	@JoinColumn(name = "employee_id")
	private Employee employee;

	@Column(name = "basic_salary")
	private Double basicSalary;

	@Column(name = "hra")
	private Double hra;

	@Column(name = "epf")
	private Double epf;

	@Column(name = "ta")
	private Double ta;

	@Column(name = "da")
	private Double da;

	@Column(name = "tds")
	private Double tds;

	@Column(name = "income_tax")
	private Double incomeTax;

	@Column(name = "professional_tax")
	private Double professionalTax;

	@Column(name = "personal_loan")
	private Double personalLoan;

	@Column(name = "vehical_loan")
	private Double vehicalLoan;

	@Column(name = "house_loan")
	private Double houseLoan;

	@Column(name = "cca")
	private Double cca;

	@Column(name = "panel_deduction")
	private Double panelDeduction;

	@Column(name = "special_allowance")
	private Double specialAllowance;

	@Column(name = "other_allowance")
	private Double otherAllowance;

	@Column(name = "security")
	private Double security;

	@Column(name = "arrears")
	private Double arrears;

	@Column(name = "benefits")
	private Double benefits;

	@Column(name = "bonus")
	private Double bonus;

	@Column(name = "increments")
	private Double increments;

	@Column(name = "over_time")
	private Double overTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_from")
	private Date dateFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_to")
	private Date dateTo;

}
