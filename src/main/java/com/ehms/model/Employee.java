package com.ehms.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The persistent class for the ehms_employees database table.
 * 
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_employees")

public class Employee extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;
	public static final int EMPLOYEE_TYPE_STAFF = 0;
	public static final int EMPLOYEE_TYPE_NURSE = 1;
	public static final int EMPLOYEE_TYPE_DOCTOR = 2;

	public Employee(Long employeeId) {
		this.employeeId = employeeId;
	}

	@Id
	@Column(name = "employee_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long employeeId;

	@ManyToOne
	@JoinColumn(name = "title_id")
	private Title title;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "employee_code")
	private String employeeCode;

	@Column(name = "employee_type")
	private Integer employeeType;

	@ManyToOne
	@JoinColumn(name = "designation_id")
	private MiscValue designation;

	@ManyToOne
	@JoinColumn(name = "department_id")
	private Department department;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "birth_date")
	private Date birthDate;

	@Column(name = "birth_year")
	private Integer birthYear;

	@Column(name = "is_dob_available")
	private Boolean isDobAvailable;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "joining_date")
	private Date joiningDate;

	private String gender;

	@Column(name = "marital_status")
	private String maritalStatus;

	private String nationality;

	@Column(name = "adhar_no")
	private String adharNo;

	@ManyToOne
	@JoinColumn(name = "other_id_type")
	private MiscValue otherIdType;

	@Column(name = "other_id_no")
	private String otherIdNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "other_id_valid_till")
	private Date otherIdValidTill;

	@Column(name = "pan_no")
	private String panNo;

	@Column(name = "tan_no")
	private String tanNo;

	private String qualification;

	@ManyToOne
	@JoinColumn(name = "specialization_id")
	private MiscValue specialization;

	@ManyToOne
	@JoinColumn(name = "joining_type_id")
	private MiscValue joiningType;

	@Column(name = "hospital_share_pc")
	private BigDecimal hospitalSharePc;

	@Column(name = "hospital_share_rs")
	private BigDecimal hospitalShareRs;

	@Column(name = "doctor_share_pc")
	private BigDecimal doctorSharePc;

	@Column(name = "doctor_share_rs")
	private BigDecimal doctorShareRs;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "current_address_id")
	private Address currentAddress;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "permanent_address_id")
	private Address permanentAddress;

	private String email;

	@Column(name = "country_code_for_contact_number")
	private String countryCodeForContactNumber;

	@Column(name = "contact_number")
	private String contactNumber;

	@Column(name = "country_code_for_mobile_number")
	private String countryCodeForMobileNumber;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "nominee_name")
	private String nomineeName;

	@Column(name = "nominee_relation")
	private String nomineeRelation;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;

	@Column(name = "is_active")
	private Boolean isActive;

}