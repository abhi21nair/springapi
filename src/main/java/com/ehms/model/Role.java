package com.ehms.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The persistent class for the role database table.
 * 
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_roles")
public class Role extends BaseAuditedEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String ROLE_NAME_CUSTOMER_ADMIN = "CUSTOMER_ADMIN";
	public static final String ROLE_NAME_HOSPITAL_ADMIN = "HOSPITAL_ADMIN";
	public static final String ROLE_NAME_SYSTEM_USER = "SYSTEM_USER";

	public Role(Long roleId) {
		this.roleId = roleId;
	}

	@Id
	@Column(name = "role_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long roleId;

	@Column(name = "role_name")
	private String roleName;

	private String description;

	@Column(name = "is_active")
	private Boolean isActive;
	
	@Column(name = "is_system_role")
	private Boolean isSystemRole;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;
	
	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;
	
	@OneToMany(mappedBy = "role", cascade = CascadeType.ALL)
	private List<LinkRolePrivilege> linkRolePrivileges;

}
