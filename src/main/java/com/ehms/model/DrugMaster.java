package com.ehms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_drug_master")
public class DrugMaster extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "drug_master_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long drugMasterId;

	@Column(name = "generic_name")
	private String genericName;

	@Column(name = "medicine_name")
	private String medicineName;

	@Column(name = "brand_name")
	private String brandName;

	@Column(name = "drug_type_id")
	private Long drugTypeId;

	@Column(name = "store_name_id")
	private Long storeNameId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expiry_date")
	private Date expiryDate;

	@Column(name = "strength")
	private Double strength;

	@Column(name = "unit")
	private String unit;

	@Column(name = "min_order")
	private Long minOrder;

	@Column(name = "max_order")
	private Long maxOrder;

	@Column(name = "qty")
	private Double qty;

	@JoinColumn(name = "packing_id")
	private Long packingId;

	@Column(name = "batch_number")
	private String batchNumber;

	@Column(name = "purchase_price")
	private Double purchasePrice;

	@Column(name = "cost_price_incl_all_tax")
	private Double costPriceInclAllTax;

	@Column(name = "mrp")
	private Double mrp;

	@Column(name = "disease_id")
	private Long diseaseId;

	@Column(name = "category_id")
	private Long categoryId;

	@Column(name = "tax_in_percent")
	private Double taxInPercent;

	@Column(name = "tax_in_rupees")
	private Double taxInRupees;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;

}
