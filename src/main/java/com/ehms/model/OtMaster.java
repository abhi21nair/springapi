package com.ehms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_ot_master")
public class OtMaster extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ot_master_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long otMasterId;

	@Column(name = "ot_name")
	private String otName;

	@ManyToOne
	@JoinColumn(name = "ot_type_id")
	private Department otType;

	@Column(name = "ot_number")
	private Long otNo;

	@Column(name = "ot_location")
	private String otLocation;

	@Column(name = "ot_charges")
	private Double otCharges;

	@Column(name = "is_active")
	private Boolean isActive;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;

}