package com.ehms.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "ehms_roster_of_staff")
public class RosterOfStaff extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;
	public static final String TIME_MAX = "24:00:00";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "roster_of_staff_id")
	private Long rosterOfStaffId;

	@ManyToOne
	@JoinColumn(name = "employee_id")
	private Employee staffName;

	@Column(name = "staff_shift_id")
	private Long staffShift;

	@Column(name = "time_from")
	private Time timeFrom;

	@Column(name = "time_to")
	private Time timeTo;

	private Integer day;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_from")
	private Date dateFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_to")
	private Date dateTo;

	@Column(name = "weekly_off")
	private Integer weeklyOff;

	@Column(name = "remark")
	private String remark;

}
