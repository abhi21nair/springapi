package com.ehms.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "ehms_customers")
public class Customer extends BaseAuditedEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public Customer(Long customerId) {
		this.customerId = customerId;
	}

	@Id
	@Column(name = "customer_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long customerId;

	@Column(name = "organization_code")
	private String organizationCode;

	@Column(name = "customer_name")
	private String customerName;

	@Column(name = "country_code_for_contact_number")
	private String countryCodeForContactNumber;

	@Column(name = "contact_number")
	private String contactNumber;

	@Column(name = "country_code_for_secondary_contact_number")
	private String countryCodeForSecondaryNumber;

	@Column(name = "sec_contact_number")
	private String secContactNumber;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "demo_days")
	private Integer demoDays;

	@Column(name = "buying_type_quantity")
	private Long buyingTypeQuantity;

	@Column(name = "referral_person_name")
	private String referralPersonName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expiry_date")
	private Date expiryDate;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "key_user_id")
	private User keyUser;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id")
	private Address address;

	@Column(name = "referral_type")
	private String referralType;

	@ManyToOne
	@JoinColumn(name = "buying_preference_id")
	private BuyingPreference buyingPreference;

	@ManyToOne
	@JoinColumn(name = "subscription_type_id")
	private SubscriptionType subscriptionType;

	@ManyToOne
	@JoinColumn(name = "buying_type_id")
	private BuyingType buyingType;

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	private List<LinkCustomerModule> linkCustomerModules;

	@OneToMany(mappedBy = "customer")
	private List<Hospital> hospitals;

	@OneToMany(mappedBy = "customer")
	private List<Patient> patients;

}