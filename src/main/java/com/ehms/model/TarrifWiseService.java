package com.ehms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "ehms_tarrif_wise_service")
public class TarrifWiseService extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "tarrif_service_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long tarrifServiceId;

	@Column(name = "department_id")
	private Long departmentId;

	@Column(name = "doctor_share_in_percent")
	private Double doctorShareInPercent;

	@Column(name = "doctor_share_in_rupees")
	private Double doctorShareInRupees;

	@Column(name = "normal_service_rate")
	private Double normalServiceRate;

	@Column(name = "service_type_id")
	private Long serviceTypeId;

	@Column(name = "special_service_rate")
	private Double specialServiceRate;

	@ManyToOne
	@JoinColumn(name = "tarrif_service_name_id")
	private MiscValue tarrifServiceName;

	@Column(name = "is_active")
	private Boolean isActive;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;

}
