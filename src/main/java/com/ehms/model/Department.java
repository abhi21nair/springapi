package com.ehms.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ehms_departments")
public class Department extends BaseAuditedEntity {

	public Department(Long departmentId) {
		super();
		this.departmentId = departmentId;
	}

	private static final long serialVersionUID = 2044485863434770336L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "department_id")
	private Long departmentId;

	@Column(name = "department_name")
	private String departmentName;

	@Column(name = "department_description")
	private String departmentDescription;

	@Column(name = "department_type")
	private String departmentType;

	@Column(name = "country_code_for_department_phone_number")
	private String countryCodeForDepartmentPhoneNumber;

	@Column(name = "department_phone_number")
	private String departmentPhoneNumber;

	@Column(name = "is_active")
	private Boolean isActive;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;

	@OneToMany(mappedBy = "department")
	private List<Employee> employee;

}