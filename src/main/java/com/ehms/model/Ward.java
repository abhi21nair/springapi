package com.ehms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_wards")
public class Ward extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;
	
	public Ward(Long wardId) {
		this.wardId = wardId;
	}

	@Id
	@Column(name = "ward_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long wardId;

	@Column(name = "floor_id")
	private Long floorId;

	@Column(name = "ward_name")
	private String wardName;

	@Column(name = "ward_type_id")
	private Long wardTypeId;

	@Column(name = "bed_category_id")
	private Long bedCategoryId;

	@Column(name = "bed_charges_full_day")
	private Double bedChargesFullDay;

	@Column(name = "bed_charges_hourly")
	private Double bedChargesHourly;
	
	@OneToMany(mappedBy = "ward", cascade = CascadeType.ALL)
	private List<Bed> beds;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;
}