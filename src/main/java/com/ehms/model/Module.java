package com.ehms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_modules")
public class Module implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "module_id")
	private Long moduleId;

	@Column(name = "module_name")
	private String moduleName;

	@Column(name = "module_code")
	private String moduleCode;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_queueable")
	private Boolean isQueueable;

	public Module(Long moduleId) {
		this.moduleId = moduleId;
	}

}