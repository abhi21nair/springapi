package com.ehms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "ehms_vehicles")
public class Vehicle extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "vehicle_id")
	private Long vehicleId;

	@Column(name = "average_per_km")
	private Long averagePerKm;

	@Column(name = "chasis_number")
	private String chasisNumber;

	@Column(name = "fuel_capacity")
	private Double fuelCapacity;

	@Column(name = "gps_enabled")
	private Integer gpsEnabled;

	@Column(name = "insurance_number")
	private String insuranceNumber;

	@Column(name = "insurance_number_validity")
	private String insuranceNumberValidity;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "rto_registration_number")
	private String rtoRegistrationNumber;

	@Column(name = "rto_registration_number_validity")
	private String rtoRegistrationNumberValidity;

	@Column(name = "type_of_fuel")
	private Integer typeOfFuel;

	@Column(name = "vehicle_color")
	private Integer vehicleColor;

	@Column(name = "vehicle_manufacturer")
	private Integer vehicleManufacturer;

	@Column(name = "vehicle_name")
	private String vehicleName;

	@Column(name = "vehicle_number")
	private String vehicleNumber;

	@Column(name = "year_of_model")
	private Integer yearOfModel;

	@Column(name = "vehicle_type_id")
	private Long vehicleTypeId;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;

	public Vehicle(Long vehicleId) {
		super();
		this.vehicleId = vehicleId;
	}

}
