package com.ehms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="ehms_misc_values")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MiscValue extends BaseAuditedEntity {
	
	public static final String MISC_VALUE_CUSTOMER_ADMIN = "Customer Admin";
	public static final String MISC_VALUE_HOSPITAL_ADMIN = "Hospital Admin";

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="misc_type_value_id")
	private Long miscTypeValueId;

	@Column(name="customer_id")
	private Long customerId;

	@Column(name="hospital_id")
	private Long hospitalId;

	@Column(name="misc_type_id")
	private Long miscTypeId;

	@Column(name="misc_value")
	private String miscValue;

	public MiscValue(Long miscTypeValueId) {
		super();
		this.miscTypeValueId = miscTypeValueId;
	}

}