package com.ehms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "ehms_apt_complaints")
public class AppointmentComplaint extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "complaint_id")
	private Long complaintId;

	private String complaints;

	private String diagnosis;

	@Column(name = "examination_pulse")
	private String examinationPulse;

	private String respiration;

	private String temperature;

	private String height;

	private String weight;

	@Column(name = "blood_pressure")
	private String bloodPressure;

	@Column(name = "general_notes")
	private String generalNotes;

	@Column(name = "pathology_test_master_id")
	private Long pathologyTestMasterId;

	@Column(name = "lab_notes")
	private String labNotes;

	@Column(name = "appointment_id")
	private Long appointmentId;
}
