package com.ehms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "ehms_daily_progress_notes")
public class DailyProgressNote extends BaseAuditedEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "daily_progress_note_id")
	private Long dailyProgressNoteId;

	@Column(name = "appointment_id")
	private Long appointmentId;

	@Column(name = "progress_date")
	private Date progressDate;

	@Column(name = "patient_id")
	private Long patientId;

	@Column(name = "doctor_day_progress_notes")
	private String doctorDayProgressNotes;

	@Column(name = "doctor_night_progress_notes")
	private String doctorNightProgressNotes;

	@Column(name = "previous_notes")
	private String previousNotes;
}
