package com.ehms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_customer_payments")
public class CustomerPayment extends BaseAuditedEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "customer_payment_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long customerPaymentId;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@Column(name = "cost_of_product")
	private Double costOfProduct;

	@Column(name = "gst_number")
	private String gstNumber;

	@Column(name = "pan_number")
	private String panNumber;

	@ManyToOne
	@JoinColumn(name = "payment_status_id")
	private PaymentStatus paymentStatus;

	@Column(name = "amc_addition")
	private String amcAddition;

	@Column(name = "other_service_1")
	private String otherService1;

	@Column(name = "other_service_2")
	private String otherService2;

	@Column(name = "other_service_1_type")
	private String otherService1Type;

	@Column(name = "other_service_2_type")
	private String otherService2Type;

	@Column(name = "discount")
	private Double discount;

	@Column(name = "discount_valid_from")
	private Date discountValidFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "discount_valid_to")
	private Date discountValidTo;

	@Column(name = "discount_in_percent")
	private Double discountInPercent;

	@Column(name = "value")
	private Double value;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "discount_in_percent_valid_from")
	private Date discountInPercentValidFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "discount_in_percent_valid_to")
	private Date discountInPercentValidTo;

	@Column(name = "tax1")
	private Double tax1;

	@Column(name = "tax2")
	private Double tax2;

	@Column(name = "tax3")
	private Double tax3;

	@Column(name = "total_billing_amount")
	private Double totalBillingAmount;

	@ManyToOne
	@JoinColumn(name = "payment_mode_id")
	private PaymentMode paymentMode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expiry_date")
	private Date expiryDate;

	@ManyToOne
	@JoinColumn(name = "action_by")
	private User actionBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "action_time")
	private Date actionTime;
}
