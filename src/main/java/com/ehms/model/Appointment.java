package com.ehms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "ehms_appointments")
public class Appointment extends BaseAuditedEntity {

	private static final long serialVersionUID = 1L;
	public static final Integer APPOINTMENT_SCHEDULED = 1;
	public static final Integer APPOINTMENT_IN_PROGESS = 2;
	public static final Integer APPOINTMENT_ON_HOLD = 3;
	public static final Integer APPOINTMENT_COMPLETE = 4;
	public static final Integer APPOINTMENT_CANCEL = 5;
	public static final Long IPD_MODULE_ID = 5L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "appointment_id")
	private Long appointmentId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "module_id")
	private Module module;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "doctor_id")
	private Employee doctor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "patient_id")
	private Patient patient;

	@Column(name = "time_spent")
	private Long timeSpent;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "doctor_schedule_id")
	private DocSchedule docSchedule;

	@Column(name = "from_time")
	private Timestamp fromTime;

	@Column(name = "to_time")
	private Timestamp toTime;

	private Integer status;

	@Column(name = "report_time")
	private Timestamp reportTime;

	@Column(name = "appointment_start_time")
	private Timestamp appointmentStartTime;

	@Column(name = "appointment_end_time")
	private Timestamp appointmentEndTime;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bed_id")
	private Bed bed;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department_id")
	private Department department;

}
