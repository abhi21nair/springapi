package com.ehms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "ehms_apt_prescriptions")
public class AppointmentPrescription extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "prescription_id")
	private Long prescriptionId;

	@Column(name = "drug_master_id")
	private Long drugMasterId;

	private String route;

	private String quantity;

	private Integer duration;

	@Column(name = "duration_unit")
	private String durationUnit;

	@Column(name = "appointment_id")
	private Long appointmentId;
}
