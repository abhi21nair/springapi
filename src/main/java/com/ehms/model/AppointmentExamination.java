package com.ehms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "ehms_apt_examinations")
public class AppointmentExamination extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "examination_id")
	private Long examinationId;

	@Column(name = "local_examination")
	private String localExamination;

	@Column(name = "central_nervous_system")
	private String centralNervousSystem;

	@Column(name = "respiratory_system")
	private String respiratorySystem;

	@Column(name = "cardio_vascular_system")
	private String cardioVascularSystem;

	@Column(name = "abdominal_system")
	private String abdominalSystem;

	@Column(name = "`obg/gynic_examination`")
	private String obgGynicExamination;

	@Column(name = "appointment_id")
	private Long appointmentId;
}
