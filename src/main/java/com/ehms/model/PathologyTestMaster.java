package com.ehms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_pathology_test_master")
public class PathologyTestMaster extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "pathology_test_master_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long pathologyTestMasterId;

	@Column(name = "test_name")
	private String testName;

	@Column(name = "sub_test_name")
	private String subTestName;

	@ManyToOne
	@JoinColumn(name = "lab_id")
	private Department lab;

	@Column(name = "normal_value")
	private Long normalValue;

	@Column(name = "value_type_id")
	private Long valueTypeId;

	@Column(name = "gender_wise_value")
	private Integer genderWiseValue;

	@Column(name = "unit")
	private String unit;

	@Column(name = "specimen_type_id")
	private Long specimenTypeId;

	@Column(name = "container_type_id")
	private Long containerTypeId;

	@Column(name = "result_type_id")
	private Long resultTypeId;

	@Column(name = "is_active")
	private Boolean isActive;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;

}