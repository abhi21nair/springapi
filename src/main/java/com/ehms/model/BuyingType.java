package com.ehms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_buying_types")
public class BuyingType implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "buying_type_id")
	private Long buyingTypeId;

	@Column(name = "buying_type_name")
	private String buyingTypeName;

	@JsonIgnore
	@Getter(onMethod = @__(@JsonIgnore))
	@Column(name = "is_active")
	private Boolean isActive;

	public BuyingType(Long buyingTypeId) {
		this.buyingTypeId = buyingTypeId;
	}

}
