package com.ehms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "ehms_payment_status")
public class PaymentStatus implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final Long PAYMENT_STATUS_PENDING = 1l;
	public static final Long PAYMENT_STATUS_APPROVED = 2l;
	public static final Long PAYMENT_STATUS_REJECTED = 3l;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "payment_status_id")
	private Long paymentStatusId;
	
	@Column(name = "payment_status_name")
	private String paymentStatusName;
	
	public PaymentStatus(Long paymentStatusId) {
		super();
		this.paymentStatusId = paymentStatusId;
	}

}
