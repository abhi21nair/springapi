package com.ehms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "ehms_vehicle_routes")
public class VehicleRoute extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "vehicle_route_id")
	private Long vehicleRouteId;

	private String destination;

	@Column(name = "gps_based_route")
	private Integer gpsBasedRoute;

	private String route;

	@Column(name = "route_km")
	private Long routeKm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "route_validity_from")
	private Date routeValidityFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "route_validity_to")
	private Date routeValidityTo;

	private String source;

	@ManyToOne
	@JoinColumn(name="vehicle_id")
	private Vehicle vehicle;
	
	@Column(name = "is_active")
	private Boolean isActive;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;
}
