package com.ehms.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_patients")
public class Patient extends BaseAuditedEntity {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "patient_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long patientId;

	@Column(name = "patient_code")
	private String patientCode;

	@Column(name = "title_id")
	private Long titleId;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "last_name")
	private String lastName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "birth_date")
	private Date birthDate;

	@Column(name = "birth_year")
	private Integer birthYear;

	@Column(name = "is_dob_available")
	private Boolean isDobAvailable;

	@Column(name = "gender")
	private String gender;

	@Column(name = "marital_status")
	private String maritalStatus;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "current_address_id")
	private Address currentAddress;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "permanent_address_id")
	private Address permanentAddress;

	@Column(name = "contact_number")
	private String contactNumber;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "nationality")
	private String nationality;

	@Column(name = "passport_number")
	private String passportNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "valid_till")
	private Date validTill;

	@Column(name = "adhar_no")
	private String adharNo;

	@Column(name = "reference_doctor")
	private String referenceDoctor;

	@Column(name = "relative_name")
	private String relativeName;

	@Column(name = "relation")
	private String relation;

	@Column(name = "religion")
	private String religion;

	@Column(name = "allergy")
	private String allergy;

	@Column(name = "sponsor_corporate_id")
	private Long sponsorCorporateId;

	@Column(name = "nominee_type_id")
	private Long nomineeType;

	@Column(name = "nominee_name")
	private String nomineeName;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;

	public Patient(Long patientId) {
		this.patientId = patientId;
	}
}
