package com.ehms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ehms_misc_type")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MiscType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "misc_type_id")
	private Long miscTypeId;

	@Column(name = "misc_type_description")
	private String miscTypeDescription;

	@Column(name = "misc_type_name")
	private String miscTypeName;

}