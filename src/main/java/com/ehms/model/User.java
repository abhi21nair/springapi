package com.ehms.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "ehms_users")
public class User extends BaseAuditedEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userId;

	private String username;

	@JsonIgnore
	@Getter(onMethod = @__(@JsonIgnore))
	private String password;

	@Column(name = "is_reset_password_on_login")
	private Boolean IsResetPasswordOnLogin;

	@Column(name = "security_question")
	private String securityQuestion;

	@Column(name = "security_answer")
	private String securityAuestion;

	private Boolean enabled;

	@ManyToOne
	@JoinColumn(name = "employee_id")
	private Employee employee;
	
	@Column(name = "is_system_user")
	private Boolean isSystemUser;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<LinkUserRole> linkUserRoles;

}