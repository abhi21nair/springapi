package com.ehms.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor

@Table(name = "ehms_vendor_master")
public class VendorMaster extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "vendor_master_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long vendorMasterId;

	@Column(name = "vendor_name")
	private String vendorName;

	@Column(name = "vendor_type_id")
	private Long vendorTypeId;

	@ManyToOne
	@JoinColumn(name = "department_id")
	private Department department;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id")
	private Address address;

	@Column(name = "phone_no")
	private Long phoneNo;
	
	@Column(name = "mobile_no")
	private Long mobileNo;
	
	@Column(name = "key_contact_person_name")
	private String keyContactPersonName;

	@Column(name = "key_contact_person_number")
	private Long keyContactPersonNumber;
	
	@Column(name = "country_code_mobile_number")
	private String countryCodeMobileNumber;
	
	@Column(name = "country_code_contact_number")
	private String countryCodeContactNumber;

	@Column(name = "email")
	private String email;

	@Column(name = "gst_no")
	private String gstNo;

	@Column(name = "dl_no")
	private String dlNo;

	@Column(name = "pan_no")
	private String panNo;

	@Column(name = "tan_no")
	private String tanNo;

	@Column(name = "shop_act_no")
	private String shopActNo;

	@Column(name = "vendor_no")
	private String vendorNo;

	@Column(name = "opening_balance")
	private Double openingBalance;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;

	@Column(name = "is_active")
	private Boolean isActive;
}
