package com.ehms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.*;

/**
 * The persistent class for the role database table.
 * 
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_privileges")
public class Privilege implements Serializable {
	private static final long serialVersionUID = 1L;

	public Privilege(Long previlegeId) {
		this.privilegeId = previlegeId;
	}

	@Id
	@Column(name = "privilege_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long privilegeId;

	@Column(name = "privilege_code")
	private String privilegeCode;

	@Column(name = "privilege_description")
	private String description;
	
	@Column(name = "is_system_privilege")
	private Boolean isSystemPrivilege;
	
	@ManyToOne
	@JoinColumn(name = "module_id")
	private Module module;

}
