package com.ehms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ehms_various_price_master")
public class VariousPriceMaster extends BaseAuditedEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "various_price_master_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long variousPriceMasterId;
	
	@ManyToOne
	@JoinColumn(name = "price_master_service_name_id")
	private MiscValue priceMasterServiceName;

	@ManyToOne
	@JoinColumn(name = "department_id")
	private Department department;
	
	@Column(name = "price_master_group_type_id")
	private Long priceMasterGroupTypeId;

	@Column(name = "price_master_sub_group_type_id")
	private Long priceMasterSubGroupTypeId;

	@Column(name = "normal_service_rate")
	private Double normalServiceRate;

	@Column(name = "corporate_rate_in_percent")
	private Double corporateRateInPercent;
	
	@Column(name = "corporate_rate_in_ruppes")
	private Double corporateRateInRuppes;
	
	@Column(name = "cghs_rate_in_percent")
	private Double cghsRateInPercent;
	
	@Column(name = "cghs_rate_in_ruppes")
	private Double cghsRateInRuppes;
	
	@Column(name = "emergency_rate_in_percent")
	private Double emergencyRateInPercent;
	
	@Column(name = "emergency_rate_in_ruppes")
	private Double emergencyRateInRuppes;
	
	@Column(name = "other_rate_in_percent")
	private Double otherRateInPercent;
	
	@Column(name = "other_rate_in_ruppes")
	private Double otherRateInRuppes;
	
	@Column(name = "is_active")
	private Boolean isActive;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "hospital_id")
	private Hospital hospital;


}