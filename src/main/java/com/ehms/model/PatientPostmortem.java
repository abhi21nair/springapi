package com.ehms.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "ehms_patient_postmortems")
public class PatientPostmortem extends BaseAuditedEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "patient_postmortem_id")
	private Long patientPostmortemId;

	@Column(name = "appointment_id")
	private Long appointmentId;

	@Column(name = "patient_id")
	private Long patientId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "death_date")
	private Date deathDate;

	@Column(name = "cause_of_death")
	private String causeOfDeath;

	@Column(name = "dead_body_handed")
	private int deadBodyHanded;

	@Column(name = "dead_body_handed_to")
	private String deadBodyHandedTo;

	@Column(name = "morgue_time")
	private Time morgueTime;

	@Column(name = "postmortem_time")
	private Time postmortemTime;
}
