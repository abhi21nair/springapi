package com.ehms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "ehms_discharge_sheets")
public class DischargeSheet extends BaseAuditedEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "discharge_sheet_id")
	private Long dischargeSheetId;

	@Column(name = "appointment_id")
	private Long appointmentId;

	@Column(name = "patient_id")
	private Long patientId;

	@Column(name = "discharge_condition_id")
	private Long dischargeCoditionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "discharge_date")
	private Date dischargeDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "death_date")
	private Date deathDate;

	@Column(name = "cause_of_death")
	private String causeOfDeath;

	private String advice;

	@Column(name = "next_appointment")
	private String nextAppointment;

	@Column(name = "other_details")
	private String otherDetails;
}
