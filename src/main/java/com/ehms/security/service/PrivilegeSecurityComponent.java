package com.ehms.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.ehms.security.JwtUser;
import com.ehms.services.user.UserService;

@Component
public class PrivilegeSecurityComponent {

	@Autowired
	private UserService userService;

	public boolean hasPrivilege(String... privilegeNames) {
		Assert.isTrue(privilegeNames != null && privilegeNames.length != 0, "Privilege name not provided");
		boolean hasPrivilege = false;
		JwtUser user = userService.getCurrentUser();
		for (String privilegeName : privilegeNames) {
			if (user != null && user.getPrivileges() != null && user.getPrivileges().contains(privilegeName)) {
				hasPrivilege = true;
				break;
			}
		}
		return hasPrivilege;
	}
}
