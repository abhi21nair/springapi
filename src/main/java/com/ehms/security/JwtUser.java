package com.ehms.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.ehms.model.Customer;
import com.ehms.model.Employee;
import com.ehms.model.Hospital;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class JwtUser implements UserDetails {

	private static final long serialVersionUID = 8194630483264464352L;

	private Long userId;

	@JsonIgnore
	private String password;

	private String username;

	private String securityQuestion;

	private String securityAuestion;

	private Boolean IsResetPasswordOnLogin;

	private boolean enabled;

	private Set<String> privileges;

	private Long customerId;

	private Long hospitalId;

	private Long employeeId;
	
	private Boolean isSystemUser;

	private Boolean isDoctor = false;

	@JsonIgnore
	private Set<GrantedAuthority> authorities;

	public JwtUser() {
	}

	public JwtUser(Long userId, String username, String password, String securityQuestion, String securityAuestion, Boolean IsResetPasswordOnLogin, boolean enabled,
			Set<GrantedAuthority> authorities, Set<String> privileges, Employee employee, Boolean isSystemUser) {
		this.userId = userId;
		this.username = username;
		this.securityQuestion = securityQuestion;
		this.securityAuestion = securityAuestion;
		this.IsResetPasswordOnLogin = IsResetPasswordOnLogin;
		this.password = password;
		this.enabled = enabled;
		this.authorities = authorities;
		this.privileges = privileges;
        this.isSystemUser = isSystemUser;
		if (employee != null) {
			this.employeeId = employee.getEmployeeId();
			Customer cust = employee.getCustomer();
			if (cust != null) {
				this.customerId = cust.getCustomerId();
				Hospital hops = employee.getHospital();
				if (hops != null) {
					this.hospitalId = hops.getHospitalId();
				}
			}
			if (employee.getEmployeeType() != null && Employee.EMPLOYEE_TYPE_DOCTOR == employee.getEmployeeType()) {
				this.isDoctor = true;
			}

		}
	}

	public Long getuserId() {
		return userId;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	public String getsecurityQuestion() {
		return securityQuestion;
	}

	public String getsecurityAuestion() {
		return securityAuestion;
	}

	public Boolean IsResetPasswordOnLogin() {
		return IsResetPasswordOnLogin;
	}

	public void setPrivileges(Set<String> privileges) {
		this.privileges = privileges;
	}

	public Set<String> getPrivileges() {
		return privileges;
	}

	@JsonIgnore
	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		if (authorities == null) {
			authorities = new HashSet<>();
		}
		return authorities;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public Long getHospitalId() {
		return hospitalId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public Boolean getIsDoctor() {
		return isDoctor;
	}
	
	public Boolean getIsSystemUser() {
		return isSystemUser;
	}

	public void setIsSystemUser(Boolean isSystemUser) {
		this.isSystemUser = isSystemUser;
	}

}