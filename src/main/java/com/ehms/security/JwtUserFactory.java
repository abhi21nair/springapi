package com.ehms.security;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.ehms.model.LinkRolePrivilege;
import com.ehms.model.LinkUserRole;
import com.ehms.model.Role;
import com.ehms.model.User;

public final class JwtUserFactory {

	private JwtUserFactory() {
	}

	public static JwtUser create(User user) {
		Set<GrantedAuthority> setOfRoles = new HashSet<>();
		Set<String> roles = new HashSet<>();
		Set<String> privileges = new HashSet<>();
		populateSets(user.getLinkUserRoles(), setOfRoles, roles);
		populatePrivilages(user.getLinkUserRoles(), setOfRoles, privileges);
		return new JwtUser(user.getUserId(), user.getUsername(), user.getPassword(), user.getSecurityQuestion(), user.getSecurityAuestion(), user.getIsResetPasswordOnLogin(), user.getEnabled(),
				setOfRoles, privileges, user.getEmployee(), user.getIsSystemUser());
	}

	private static void populatePrivilages(List<LinkUserRole> linkUserRoles, Set<GrantedAuthority> setOfRoles, Set<String> privileges) {
		if (linkUserRoles != null && !linkUserRoles.isEmpty()) {
			for (LinkUserRole linkUserRole : linkUserRoles) {
				Role role = linkUserRole.getRole();
				if (role != null && role.getLinkRolePrivileges() != null && !role.getLinkRolePrivileges().isEmpty()) {
					for (LinkRolePrivilege linkRolePrivilege : role.getLinkRolePrivileges()) {
						if (linkRolePrivilege != null && linkRolePrivilege.getPrivilege() != null) {
							setOfRoles.add(new SimpleGrantedAuthority(linkRolePrivilege.getPrivilege().getPrivilegeCode()));
							privileges.add(linkRolePrivilege.getPrivilege().getPrivilegeCode());
						}
					}
				}
			}
		}
	}

	private static void populateSets(List<LinkUserRole> linkUserRoles, Set<GrantedAuthority> setOfRoles, Set<String> roles) {
		if (linkUserRoles != null) {
			for (LinkUserRole linkUserRole : linkUserRoles) {
				if (linkUserRole != null && linkUserRole.getRole() != null) {
					setOfRoles.add(new SimpleGrantedAuthority(linkUserRole.getRole().getRoleName()));
					roles.add(linkUserRole.getRole().getRoleName());
				}
			}
		}
	}
}