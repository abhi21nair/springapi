package com.ehms.security.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.User;
import com.ehms.vos.user.UserVO;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByUsernameIgnoreCase(String userName);

	User findByUserIdNotAndUsernameIgnoreCase(Long userId, String userName);

	@Query("select new com.ehms.vos.user.UserVO(user.userId, user.username, user.enabled, user.employee.employeeId, user.employee.firstName, user.employee.middleName, user.employee.lastName) from User user where user.employee.customer.customerId = ?2 and user.userId <> ?4 and user.employee.hospital.hospitalId = ?3 and UPPER(user.username) like UPPER(?1)")
	Page<UserVO> findUsersOfHospital(String searchText, Long customerId, Long hospitalId, Long userId, Pageable pageRequest);

	@Query("select new com.ehms.vos.user.UserVO(user.userId, user.username, user.enabled) from User user where user.isSystemUser = 1 and UPPER(user.username) like UPPER(?1) and user.userId <> 1")
	Page<UserVO> findSystemUsers(String searchText, Pageable pageRequest);

	@Modifying
	@Query(value = "update ehms_users set password=? where username = ?", nativeQuery = true)
	void updateUserPassword(String password, String username);
}
