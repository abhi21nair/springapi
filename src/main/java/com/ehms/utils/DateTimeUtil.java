package com.ehms.utils;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.ehms.exception.EhmsBusinessException;

public class DateTimeUtil {

	private static final DateFormat FORMATTER = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	private static final DateFormat FORMATTER_FOR_TIME = new SimpleDateFormat("HH:mm:ss");
	private static final DateFormat FORMATTER_IGNORE_TIME = new SimpleDateFormat("dd-MM-yyyy 00:00:00");

	private DateTimeUtil() {
		super();
	}

	public static Date getEndTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}

	public static Date addCalanderField(int calenderField, int amountToAdd) {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(calenderField, amountToAdd);
		Date expiryDate = c.getTime();
		return expiryDate;
	}

	public static String formatDate(Date dateToFormat) {
		if (dateToFormat != null) {
			return FORMATTER.format(dateToFormat);
		}
		return "";
	}

	public static String formatDateWithoutTime(Date dateToFormat) {
		if (dateToFormat != null) {
			return FORMATTER_IGNORE_TIME.format(dateToFormat);
		}
		return "";
	}

	public static String formatTime(Date timeToFormat) {
		if (timeToFormat != null) {
			return FORMATTER_FOR_TIME.format(timeToFormat);
		}
		return "";
	}

	public static Time parseTime(String timeToParse) throws ParseException {
		if (timeToParse != null && !timeToParse.isEmpty()) {
			Time time = new Time(FORMATTER_FOR_TIME.parse(timeToParse).getTime());
			return time;
		}
		return null;
	}

	public static Timestamp parseTimestamp(String timeStampToParse) throws ParseException, EhmsBusinessException {
		if (timeStampToParse != null && !timeStampToParse.isEmpty()) {
			if (parseDate(timeStampToParse) == null) {
				throw new EhmsBusinessException("Timestamp can not be null");
			}
			Timestamp timeStamp = new Timestamp(parseDate(timeStampToParse).getTime());
			return timeStamp;
		}
		return null;
	}

	public static Date parseDate(String dateToParse) throws ParseException {
		if (dateToParse != null && !dateToParse.isEmpty()) {
			return FORMATTER.parse(dateToParse);
		}
		return null;
	}

	public static int getDayNumber(Date date) throws EhmsBusinessException {
		if (date == null) {
			throw new EhmsBusinessException("Input date sent to determine day of week can not be null");
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.DAY_OF_WEEK);
	}

}
