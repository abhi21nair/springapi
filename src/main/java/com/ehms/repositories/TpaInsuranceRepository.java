package com.ehms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.TpaInsurance;
import com.ehms.vos.TpaInsuranceVO;

public interface TpaInsuranceRepository extends JpaRepository<TpaInsurance, Long> {

	@Query("select new com.ehms.vos.TpaInsuranceVO(t.tpaInsuranceId,t.tpaInsuranceName,t.tpaInsuranceClaimTypeId,t.tpaInsuranceTypeId,DATE_FORMAT(t.effectiveFromDate, '%d-%m-%Y %H:%i:%s'),DATE_FORMAT(t.effectiveToDate, '%d-%m-%Y %H:%i:%s')) from TpaInsurance t where t.customer.customerId = ?2 and t.hospital.hospitalId = ?3 and (UPPER(t.tpaInsuranceName) like UPPER(?1))")
	Page<TpaInsuranceVO> findTpaInsurances(String string, Long customerId, Long hospitalId, Pageable pageRequest);

	@Query("select t from TpaInsurance t where t.customer.customerId = ?2 and t.hospital.hospitalId = ?3 and t.tpaInsuranceId = ?1")
	TpaInsurance findTpaInsurance(Long tpaInsuranceId, Long customerId, Long hospitalId);

}
