package com.ehms.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.State;
import com.ehms.vos.StateVO;

public interface StateRepository extends JpaRepository<State, Long> {

	@Query("SELECT new com.ehms.vos.StateVO(s.stateId, s.stateName, country.countryId) FROM State s JOIN s.country country ")
	List<StateVO> findAllStates();
}
