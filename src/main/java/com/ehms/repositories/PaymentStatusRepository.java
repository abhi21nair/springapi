package com.ehms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ehms.model.PaymentStatus;

public interface PaymentStatusRepository extends JpaRepository<PaymentStatus, Long> {

}
