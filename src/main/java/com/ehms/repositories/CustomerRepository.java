package com.ehms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.Customer;
import com.ehms.vos.CustomerVO;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	@Query("SELECT new com.ehms.vos.CustomerVO(c.customerId, c.customerName, c.organizationCode, c.expiryDate ,count (distinct hospitals), count(distinct l), c.isActive) " + " FROM Customer c LEFT JOIN c.linkCustomerModules l  LEFT JOIN c.hospitals hospitals  "
			+ "WHERE UPPER(c.customerName) like UPPER(?1) or UPPER(c.organizationCode) like UPPER(?2)  GROUP BY c.customerId")
	Page<CustomerVO> findCustomers(String searchTextOrganizationCode, String searchTextCustomerName, Pageable pageRequest);

	Customer findByCustomerNameIgnoreCase(String name);

	Customer findByCustomerIdNotAndCustomerNameIgnoreCase(Long customerId, String customerName);
}
