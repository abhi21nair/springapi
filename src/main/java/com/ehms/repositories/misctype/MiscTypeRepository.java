package com.ehms.repositories.misctype;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ehms.model.MiscType;

public interface MiscTypeRepository extends JpaRepository<MiscType, Long> {

	List<MiscType> findAllByOrderByMiscTypeName();

	MiscType findByMiscTypeName(String miscTypeName);
}
