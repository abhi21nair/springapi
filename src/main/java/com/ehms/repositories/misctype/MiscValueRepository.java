package com.ehms.repositories.misctype;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.MiscValue;
import com.ehms.vos.misctype.MiscValueVO;

public interface MiscValueRepository extends JpaRepository<MiscValue, Long> {

	@Query("select new com.ehms.vos.misctype.MiscValueVO(mv.miscTypeValueId, mv.miscValue, mv.customerId, mv.hospitalId, mv.miscTypeId ) from MiscValue mv where mv.miscTypeId = ?1 and mv.customerId = ?2 and mv.hospitalId is null order by mv.miscValue")
	List<MiscValueVO> findCustomerMiscValues(Long miscTypeId, Long customerId);

	@Query("select new com.ehms.vos.misctype.MiscValueVO(mv.miscTypeValueId, mv.miscValue, mv.customerId, mv.hospitalId, mv.miscTypeId ) from MiscValue mv where mv.miscTypeId = ?1 and (((mv.customerId = ?2  and mv.hospitalId = ?3)) or ((mv.customerId = ?2  and mv.hospitalId is null))) order by mv.miscValue")
	List<MiscValueVO> findHospitalMiscValues(Long miscTypeId, Long customerId, Long hospitalId);

	@Query(value = "select misc_type_value_id from ehms.ehms_misc_values where misc_type_id = (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'DESIGNATION') and misc_value = ?1", nativeQuery = true)
	Long findSystemDesignationMiscTypeId(String miscValue);

	@Query(value = "select misc_type_value_id from ehms.ehms_misc_values where misc_type_id = (select misc_type_id from ehms.ehms_misc_type where misc_type_name = 'DESIGNATION') and upper(misc_value) = upper(?1) and customer_id = ?2 and hospital_id is null and misc_type_value_id <> ?3", nativeQuery = true)
	Long findCustomerDesignationMiscTypeId(String miscValue, Long customerId, Long valueId);

	List<MiscValue> findByCustomerIdIsNullAndHospitalIdIsNull();

	List<MiscValue> findByCustomerIdAndHospitalIdIsNull(Long customerId);

	@Query("Select v from MiscValue v where (v.customerId = ?1 and v.hospitalId is null) or (v.customerId = ?1 and v.hospitalId = ?2)")
	List<MiscValue> findByCustomerIdOrHospitalId(Long customerId, Long hospitalId);

}
