package com.ehms.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.Vehicle;
import com.ehms.vos.VehicleVO;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

	@Query("select new com.ehms.vos.VehicleVO(v.vehicleId, v.insuranceNumberValidity, v.rtoRegistrationNumberValidity, v.typeOfFuel, v.vehicleName, v.vehicleNumber, v.yearOfModel, v.vehicleTypeId, v.isActive) from Vehicle v where v.customer.customerId = ?2 and v.hospital.hospitalId = ?3 and (UPPER(v.vehicleName) like UPPER(?1))")
	Page<VehicleVO> findVehicles(String string, Long customerId, Long hospitalId, Pageable pageRequest);

	@Query("select v from Vehicle v where v.customer.customerId = ?2 and v.hospital.hospitalId = ?3 and v.vehicleId = ?1")
	Vehicle findVehicle(Long vehicleId, Long customerId, Long hospitalId);

	@Query("select new com.ehms.vos.VehicleVO(v.vehicleId,v.vehicleName,v.vehicleNumber) from Vehicle v where v.customer.customerId = ?1 and v.hospital.hospitalId = ?2")
	List<VehicleVO> getVehicleList(Long customerId, Long hospitalId);
}
