package com.ehms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.CustomerPayment;
import com.ehms.vos.CustomerPaymentVO;

public interface CustomerPaymentRepository extends JpaRepository<CustomerPayment, Long> {

	@Query("Select new com.ehms.vos.CustomerPaymentVO(cp.customerPaymentId, cp.paymentStatus.paymentStatusId, cp.customer.customerId, cp.totalBillingAmount) from CustomerPayment cp where cp.createdDt = (select max(createdDt) from CustomerPayment cip where cip.customer.customerId = ?1 group by cip.customer.customerId ) and cp.customer.customerId = ?2")
	CustomerPaymentVO getLatestCustomerPaymentDetails(Long customerId, Long customerId2);
}
