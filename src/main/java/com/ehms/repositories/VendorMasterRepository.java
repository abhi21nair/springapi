package com.ehms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.ehms.model.VendorMaster;
import com.ehms.vos.VendorMasterVO;

public interface VendorMasterRepository extends JpaRepository<VendorMaster, Long> {

	@Query("select new com.ehms.vos.VendorMasterVO(v.vendorMasterId,v.vendorName,v.vendorTypeId,v.department.departmentId,v.address.city.cityId, v.gstNo,v.vendorNo,v.isActive) from VendorMaster v  where v.customer.customerId = ?2 and v.hospital.hospitalId = ?3 and (UPPER(v.vendorName) like UPPER(?1))")
	Page<VendorMasterVO> findVendorMasters(String searchText, Long customerId, Long hospitalId, Pageable pageRequest);

}