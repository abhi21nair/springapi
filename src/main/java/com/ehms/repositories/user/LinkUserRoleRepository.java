package com.ehms.repositories.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.LinkUserRole;
import com.ehms.vos.user.LinkUserRoleVO;

public interface LinkUserRoleRepository extends JpaRepository<LinkUserRole, Long> {

	@Modifying
	@Query(value = "delete from ehms_link_user_role where role_id in ( select role_id from (select role_id from ehms_roles where role_id in (select role_id from ehms_link_user_role where user_id = ?) and is_system_role = 0) a) and user_id = ?", nativeQuery = true)
	void deleteByUserId(Long userId, Long userId2);

	@Query("select new com.ehms.vos.user.LinkUserRoleVO(l.user.userId, l.role.roleId) from LinkUserRole l where l.role.isSystemRole = 1 and l.user.userId in (?1)")
	List<LinkUserRoleVO> getUserRolesListForUsers(List<Long> userIds);
	
	@Modifying
	@Query(value = "delete from ehms_link_user_role where role_id in ( select role_id from (select role_id from ehms_roles where role_id in (select role_id from ehms_link_user_role where user_id = ?) and is_system_role = 1) a) and user_id = ?", nativeQuery = true)
	void deleteBySystemUserId(Long userId, Long userId2);

}
