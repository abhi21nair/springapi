package com.ehms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.VehicleRoute;
import com.ehms.vos.VehicleRouteVO;

public interface VehicleRouteRepository extends JpaRepository<VehicleRoute, Long> {

	@Query("select new com.ehms.vos.VehicleRouteVO(v.vehicleRouteId, v.destination, v.route, v.routeKm, v.source, v.vehicle.vehicleId, v.vehicle.vehicleName, v.vehicle.vehicleNumber) from VehicleRoute v where v.customer.customerId = ?2 and v.hospital.hospitalId = ?3 and (UPPER(v.route) like UPPER(?1))")
	Page<VehicleRouteVO> findVehicleRoutes(String searchText, Long customerId, Long hospitalId, Pageable pageRequest);

	@Query("select v from VehicleRoute v where v.customer.customerId = ?2 and v.hospital.hospitalId = ?3 and v.vehicleRouteId = ?1")
	VehicleRoute findVehicleRoute(Long vehicleRouteId, Long customerId, Long hospitalId);
}
