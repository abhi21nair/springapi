package com.ehms.repositories.appointment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.AppointmentVariousService;
import com.ehms.vos.appointments.AppointmentVariousServiceVO;

public interface AppointmentVariousServiceRepository extends JpaRepository<AppointmentVariousService, Long> {

	@Query("select new com.ehms.vos.appointments.AppointmentVariousServiceVO(vs) from AppointmentVariousService vs where vs.appointmentId = ?1 ORDER BY vs.createdDt DESC")
	List<AppointmentVariousServiceVO> findAppointmentVariousServices(Long appointmentId);

	@Query("select new com.ehms.vos.appointments.AppointmentVariousServiceVO(vs.variousServiceId,vs.tarrifWiseServiceId,vs.quantity,vs.totalAmount,vs.appointmentId) from AppointmentVariousService vs where vs.appointmentId in (?1) ORDER BY vs.createdDt DESC")
	List<AppointmentVariousServiceVO> findAppointmentVariousServicesForMultipleIds(List<Long> appointmentId);
}
