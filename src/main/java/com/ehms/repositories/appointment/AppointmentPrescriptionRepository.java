package com.ehms.repositories.appointment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.AppointmentPrescription;
import com.ehms.vos.appointments.AppointmentPrescriptionVO;

public interface AppointmentPrescriptionRepository extends JpaRepository<AppointmentPrescription, Long> {

	@Query("select new com.ehms.vos.appointments.AppointmentPrescriptionVO(p) from AppointmentPrescription p where p.appointmentId = ?1 ORDER BY p.createdDt DESC")
	List<AppointmentPrescriptionVO> findAppointmentPrescriptions(Long appointmentId);

	@Query("select new com.ehms.vos.appointments.AppointmentPrescriptionVO(p.prescriptionId,p.drugMasterId,p.route,p.quantity,p.duration,p.durationUnit,p.appointmentId) from AppointmentPrescription p where p.appointmentId in (?1) ORDER BY p.createdDt DESC")
	List<AppointmentPrescriptionVO> findAppointmentPrescriptionsForMultipleIds(List<Long> appointmentId);
}
