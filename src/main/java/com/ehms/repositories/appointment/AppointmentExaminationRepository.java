package com.ehms.repositories.appointment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.AppointmentExamination;
import com.ehms.vos.appointments.AppointmentExaminationVO;

public interface AppointmentExaminationRepository extends JpaRepository<AppointmentExamination, Long> {

	@Query("select new com.ehms.vos.appointments.AppointmentExaminationVO(e) from AppointmentExamination e where e.appointmentId = ?1 ORDER BY e.createdDt DESC")
	List<AppointmentExaminationVO> findAppointmentExaminations(Long appointmentId);

	@Query("select new com.ehms.vos.appointments.AppointmentExaminationVO(e.examinationId,e.localExamination,e.centralNervousSystem,e.respiratorySystem,e.cardioVascularSystem,e.abdominalSystem,e.obgGynicExamination,e.appointmentId) from AppointmentExamination e where e.appointmentId in (?1) ORDER BY e.createdDt DESC")
	List<AppointmentExaminationVO> findAppointmentExaminationsForMultipleIds(List<Long> appointmentIds);
}
