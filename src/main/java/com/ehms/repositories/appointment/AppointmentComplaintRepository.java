package com.ehms.repositories.appointment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ehms.model.AppointmentComplaint;
import com.ehms.vos.appointments.AppointmentComplaintVO;

public interface AppointmentComplaintRepository extends JpaRepository<AppointmentComplaint, Long> {

	@Query("select new com.ehms.vos.appointments.AppointmentComplaintVO(c) from AppointmentComplaint c where c.appointmentId = ?1 ORDER BY c.createdDt DESC")
	List<AppointmentComplaintVO> findAppointmentComplaints(Long appointmentId);

	@Query("select new com.ehms.vos.appointments.AppointmentComplaintVO(c.complaintId,c.complaints,c.diagnosis,c.examinationPulse,c.respiration,c.temperature,c.height,c.weight,c.bloodPressure,c.generalNotes,c.pathologyTestMasterId,c.labNotes,c.appointmentId) from AppointmentComplaint c where c.appointmentId in:ids ORDER BY c.createdDt DESC")
	List<AppointmentComplaintVO> findAppointmentComplaintsForMultipleIds(@Param("ids")List<Long> appointmentId);
}
