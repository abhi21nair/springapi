package com.ehms.repositories.appointment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.AppointmentReferral;
import com.ehms.vos.appointments.AppointmentReferralVO;

public interface AppointmentReferralRepository extends JpaRepository<AppointmentReferral, Long> {

	@Query("select new com.ehms.vos.appointments.AppointmentReferralVO(r) from AppointmentReferral r where r.appointmentId = ?1 ORDER BY r.createdDt DESC")
	List<AppointmentReferralVO> findAppointmentReferrals(Long appointmentId);

	@Query("select new com.ehms.vos.appointments.AppointmentReferralVO(r.referralId,r.departmentId,r.provisionalNotes,r.mlcCase,r.instructionToWarden,r.generalNotes,r.appointmentId) from AppointmentReferral r where r.appointmentId in (?1) ORDER BY r.createdDt DESC")
	List<AppointmentReferralVO> findAppointmentReferralsForMultipleIds(List<Long> appointmentId);
}
