package com.ehms.repositories.appointment;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.Appointment;
import com.ehms.vos.appointments.AppointmentStatsDBVO;
import com.ehms.vos.appointments.AppointmentVO;
import com.ehms.vos.appointments.PatientAppointmentHistoryVO;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

	@Query("select new com.ehms.vos.appointments.AppointmentStatsDBVO(a.module.moduleId, a.docSchedule.doctorScheduleId, count(1), sum(case when a.reportTime is null then 0 else 1 end)) from Appointment a where a.customer.customerId = ?1 and a.hospital.hospitalId = ?2 and a.fromTime >= DATE_FORMAT(?3, '%Y-%m-%d 00:00:00') and  a.fromTime <= DATE_FORMAT(?3, '%Y-%m-%d 23:59:59') group by a.module.moduleId, a.docSchedule.doctorScheduleId")
	List<AppointmentStatsDBVO> findAppointmentsStatsOfAppointmentDate(Long customerId, Long hospitalId, Date appointmentDate);

	@Query("select new com.ehms.vos.appointments.AppointmentVO(a.appointmentId, a.customer.customerId, a.hospital.hospitalId, a.module.moduleId, a.doctor.employeeId, a.patient.patientId, a.patient.titleId, a.patient.firstName, a.patient.middleName, a.patient.lastName, a.patient.mobileNumber, a.patient.patientCode, a.docSchedule.doctorScheduleId, a.timeSpent, DATE_FORMAT(a.fromTime, '%d-%m-%Y %H:%i:%s'), DATE_FORMAT(a.toTime, '%d-%m-%Y %H:%i:%s'), a.status, DATE_FORMAT(a.reportTime, '%d-%m-%Y %H:%i:%s'), DATE_FORMAT(a.appointmentStartTime, '%d-%m-%Y %H:%i:%s'), DATE_FORMAT(a.appointmentEndTime, '%d-%m-%Y %H:%i:%s')) from Appointment a where a.customer.customerId = ?2 and a.hospital.hospitalId = ?3 and a.fromTime >= DATE_FORMAT(?4, '%Y-%m-%d 00:00:00') and  a.fromTime <= DATE_FORMAT(?4, '%Y-%m-%d 23:59:59') and (((a.module.moduleId = ?5 and a.docSchedule.doctorScheduleId is null)  or ( a.doctor.employeeId = ?6 and a.docSchedule.doctorScheduleId = ?7 and a.module.moduleId is null))) and (upper(a.patient.patientCode) like upper(?1) or upper(a.patient.firstName) like upper(?1) or upper(a.patient.lastName) like upper(?1) or upper(a.patient.mobileNumber) like (?1))")
	Page<AppointmentVO> getQueueDetails(String searchText, Long customerId, Long hospitalId, Date appointmentDate, Long moduleId, Long doctorId, Long doctorScheduleId, Pageable pageRequest);

	@Query("select new com.ehms.vos.appointments.PatientAppointmentHistoryVO(a.appointmentId, DATE_FORMAT(a.reportTime, '%d-%m-%Y %H:%i:%s'), DATE_FORMAT(a.appointmentStartTime, '%d-%m-%Y %H:%i:%s'), DATE_FORMAT(a.appointmentEndTime, '%d-%m-%Y %H:%i:%s'), d.employeeId, d.title.titleId, d.firstName, d.middleName, d.lastName, a.module.moduleId) from Appointment a left join a.doctor d where a.customer.customerId = ?1 and a.hospital.hospitalId = ?2 and a.patient.patientId = ?3 and a.status = 4 ORDER BY a.reportTime DESC")
	Page<PatientAppointmentHistoryVO> getPatientHistory(Long customerId, Long hospitalId, Long patientId, Pageable pageRequest);
}
