package com.ehms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ehms.model.SubscriptionType;

public interface SubscriptionTypeRepository extends JpaRepository<SubscriptionType, Long> {

}
