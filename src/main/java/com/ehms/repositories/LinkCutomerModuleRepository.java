package com.ehms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.LinkCustomerModule;

public interface LinkCutomerModuleRepository extends JpaRepository<LinkCustomerModule, Long> {

	@Modifying
	@Query("delete from LinkCustomerModule link where link.customer.customerId = ?1")
	void deleteByCustomerId(Long customerId);
}
