package com.ehms.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.Hospital;
import com.ehms.vos.HospitalVO;

public interface HospitalRepository extends JpaRepository<Hospital, Long> {

	@Query("SELECT new com.ehms.vos.HospitalVO(h.hospitalId, h.hospitalName, h.hospitalCode, h.isActive)"
			+" FROM Hospital h WHERE h.customer.customerId = ?2 and (UPPER(h.hospitalName) like UPPER(?1) or UPPER(h.hospitalCode) like UPPER(?1))")
	Page<HospitalVO> findHospitals(String searchTextHospName, Long customerId, Pageable pageRequest);

	Hospital findByCustomerCustomerIdAndHospitalNameIgnoreCase(Long customerId, String hospitalName);

	Hospital findByCustomerCustomerIdAndHospitalIdNotAndHospitalNameIgnoreCase(Long customerId, Long hospitalId, String hospitalName);

	@Query("SELECT new com.ehms.vos.HospitalVO(h.hospitalId, h.hospitalName, h.hospitalCode) from Hospital h where h.customer.customerId = ?1")
	List<HospitalVO> getHospitalList(Long customerId);

}
