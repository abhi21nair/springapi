package com.ehms.repositories.masterdata;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ehms.model.Title;

public interface TitleRepository extends JpaRepository<Title, Long> {

}