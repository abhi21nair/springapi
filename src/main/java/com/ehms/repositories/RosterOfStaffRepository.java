package com.ehms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.RosterOfStaff;
import com.ehms.vos.RosterOfStaffVO;

public interface RosterOfStaffRepository extends JpaRepository<RosterOfStaff, Long> {

	@Query("select new com.ehms.vos.RosterOfStaffVO(r.rosterOfStaffId,r.staffName.employeeId,r.staffName.firstName,r.staffName.middleName,r.staffName.lastName,r.staffName.title.titleName,r.staffShift,DATE_FORMAT(r.timeFrom, '%H:%i:%s'),DATE_FORMAT(r.timeTo, '%H:%i:%s'),DATE_FORMAT(r.dateFrom, '%d-%m-%Y %H:%i:%s'),DATE_FORMAT(r.dateTo, '%d-%m-%Y %H:%i:%s')) from RosterOfStaff r where r.staffName.customer.customerId = ?2 and r.staffName.hospital.hospitalId = ?3 and (UPPER(r.staffName.firstName) like UPPER(?1))")
	Page<RosterOfStaffVO> findRosterOfStaff(String searchText, Long customerId, Long hospitalId, Pageable pageRequest);
}
