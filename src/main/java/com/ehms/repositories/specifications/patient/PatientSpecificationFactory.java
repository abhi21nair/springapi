package com.ehms.repositories.specifications.patient;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

import com.ehms.model.Patient;
import com.ehms.repositories.specifications.SpecificationFactory;
import com.ehms.vos.PatientVO;

@Component
public class PatientSpecificationFactory extends SpecificationFactory<Patient> {

	public Specification<Patient> extendedPatientSearch(PatientVO patientVO, Long customerId, Long hospitalId) {
		Specifications<Patient> patientSpecification = Specifications.where(containsCustomer(customerId)).and(containsHospital(hospitalId));
		if (patientVO.getPatientCode() != null && !patientVO.getPatientCode().isEmpty()) {
			patientSpecification = patientSpecification.and(containsEqual("patientCode", patientVO.getPatientCode()));
		}
		if (patientVO.getFirstName() != null && !patientVO.getFirstName().isEmpty()) {
			patientSpecification = patientSpecification.and(containsLike("firstName", patientVO.getFirstName()));
		}
		if (patientVO.getMiddleName() != null && !patientVO.getMiddleName().isEmpty()) {
			patientSpecification = patientSpecification.and(containsLike("middleName", patientVO.getMiddleName()));
		}
		if (patientVO.getLastName() != null && !patientVO.getLastName().isEmpty()) {
			patientSpecification = patientSpecification.and(containsLike("lastName", patientVO.getLastName()));
		}
		if (patientVO.getGender() != null && !patientVO.getGender().isEmpty()) {
			patientSpecification = patientSpecification.and(containsEqual("gender", patientVO.getGender()));
		}
		if (patientVO.getMobileNumber() != null && !patientVO.getMobileNumber().isEmpty()) {
			patientSpecification = patientSpecification.and(containsEqual("mobileNumber", patientVO.getMobileNumber()));
		}
		return patientSpecification;
	}
}
