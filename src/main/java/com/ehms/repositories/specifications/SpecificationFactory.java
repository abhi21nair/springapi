package com.ehms.repositories.specifications;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;

import org.springframework.data.jpa.domain.Specification;

import com.ehms.model.Customer;
import com.ehms.model.Hospital;
import com.ehms.model.Patient;

public class SpecificationFactory<T> {

	public Specification<T> containsLike(String attribute, String value) {
		return (root, query, cb) -> cb.like(root.get(attribute), "%" + value + "%");
	}

	public Specification<T> containsEqual(String attribute, String value) {
		return (root, query, cb) -> cb.equal(root.get(attribute), value);
	}

	public Specification<T> containsEqual(String attribute, Long value) {
		return (root, query, cb) -> cb.equal(root.get(attribute), value);
	}

	public Specification<Patient> containsHospital(Long value) {
		return (root, query, cb) -> {
			final Join<Patient, Hospital> hospital = root.join("hospital", JoinType.INNER);
			return cb.equal(hospital.get("hospitalId"), value);
		};
	}

	public Specification<Patient> containsCustomer(Long value) {
		return (root, query, cb) -> {
			final Join<Patient, Customer> hospital = root.join("customer", JoinType.INNER);
			return cb.equal(hospital.get("customerId"), value);
		};
	}
}