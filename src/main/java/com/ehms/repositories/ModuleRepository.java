package com.ehms.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import com.ehms.model.Module;

public interface ModuleRepository extends JpaRepository<Module, Long> {

}
