package com.ehms.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.City;
import com.ehms.vos.CityVO;

public interface CityRepository extends JpaRepository<City, Long> {

	@Query("SELECT new com.ehms.vos.CityVO(c.cityId, c.cityName, state.stateId) FROM City c JOIN c.state state ")
	List<CityVO> findAllCities();
}
