package com.ehms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.ehms.model.VariousPriceMaster;
import com.ehms.vos.VariousPriceMasterVO;

public interface VariousPriceMasterRepository extends JpaRepository<VariousPriceMaster, Long> {
	@Query("select new com.ehms.vos.VariousPriceMasterVO(v.variousPriceMasterId,v.priceMasterServiceName.miscTypeValueId,v.priceMasterServiceName.miscValue,v.department.departmentId,v.priceMasterGroupTypeId,v.priceMasterSubGroupTypeId,v.normalServiceRate,v.isActive) from VariousPriceMaster v where v.customer.customerId = ?2 and v.hospital.hospitalId = ?3 and (UPPER(v.priceMasterServiceName.miscValue) like UPPER(?1))")
	Page<VariousPriceMasterVO> findVariousPriceMasters(String searchText, Long customerId, Long hospitalId,
			Pageable pageRequest);

	@Query("select v from VariousPriceMaster v where v.customer.customerId = ?2 and v.hospital.hospitalId = ?3 and v.variousPriceMasterId = ?1")
	VariousPriceMaster findVariousPriceMaster(Long variousPriceMasterId, Long customerId, Long hospitalId);
}
