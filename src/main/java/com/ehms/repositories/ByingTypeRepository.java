package com.ehms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ehms.model.BuyingType;

public interface ByingTypeRepository extends JpaRepository<BuyingType, Long> {

}
