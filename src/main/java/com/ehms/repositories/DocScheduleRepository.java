package com.ehms.repositories;

import java.sql.Time;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ehms.model.DocSchedule;
import com.ehms.vos.DocScheduleVO;

public interface DocScheduleRepository extends JpaRepository<DocSchedule, Long> {

	@Query("select new com.ehms.vos.DocScheduleVO(d.doctorScheduleId,d.doctor.employeeId,d.doctor.firstName,d.doctor.middleName,d.doctor.lastName,d.day,d.appointmentType,DATE_FORMAT(d.timeFrom, '%H:%i:%s'),DATE_FORMAT(d.timeTo, '%H:%i:%s')) from DocSchedule d where d.doctor.customer.customerId = ?2 and d.doctor.hospital.hospitalId = ?3 and (UPPER(d.doctor.firstName) like UPPER(?1) or UPPER(d.doctor.lastName) like UPPER(?1))")
	Page<DocScheduleVO> findDocSchedules(String searchText, Long customerId, Long hospitalId, Pageable pageRequest);

	@Query("select new com.ehms.vos.DocScheduleVO(d.doctorScheduleId,d.doctor.employeeId, d.doctor.title.titleId, d.doctor.firstName, d.doctor.middleName,d.doctor.lastName, d.doctor.designation.miscTypeValueId, d.day,d.appointmentType,DATE_FORMAT(d.timeFrom, '%H:%i:%s'),DATE_FORMAT(d.timeTo, '%H:%i:%s'), d.noOfAppointment) from DocSchedule d where d.doctor.customer.customerId = ?1 and d.doctor.hospital.hospitalId = ?2 and d.day = ?3")
	List<DocScheduleVO> getDoctorSchedulesAtHospitalForDay(Long customerId, Long hospitalId, int day);

	@Query("Select ds.doctorScheduleId from DocSchedule ds where ds.doctor.id = :doctorId and ((:timeFrom <= ds.timeFrom and :timeTo >= ds.timeTo) or (:timeFrom > ds.timeFrom and :timeTo > ds.timeTo and :timeFrom < ds.timeTo) or (:timeFrom >= ds.timeFrom and :timeFrom < ds.timeTo) or (:timeTo > ds.timeFrom and :timeTo < ds.timeTo)) and ds.day = :day and (ds.doctorScheduleId <> :doctorScheduleId or :doctorScheduleId is null)")
	List<Long> findSchedulesForDoctor(@Param("doctorId") Long doctorId, @Param("day") int day, @Param("timeFrom") Time timeFrom, @Param("timeTo") Time timeTo,
			@Param("doctorScheduleId") Long doctorScheduleId);
}
