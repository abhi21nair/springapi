package com.ehms.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.Ward;
import com.ehms.vos.WardBedFlatVO;
import com.ehms.vos.WardVO;

public interface WardRepository extends JpaRepository<Ward, Long> {

	@Query("SELECT new com.ehms.vos.WardVO(w.wardId, w.floorId, w.wardName, w.wardTypeId, count( distinct b.bedId), w.customer.customerId, w.hospital.hospitalId) FROM Ward w left join w.beds b WHERE UPPER(w.wardName) like UPPER(?1) and w.customer.customerId = ?2 and w.hospital.hospitalId = ?3 group by w.wardId")
	Page<WardVO> findWardsOfHospital(String wardName, Long customerId, Long hospitalId, Pageable pageRequest);

	@Query("select w.wardId from Ward w where UPPER(w.wardName) = UPPER(?1) and w.customer.customerId = ?2 and w.hospital.hospitalId = ?3")
	Long findByWardNameIgnoreCaseAndCustomerHospital(String wardName, Long customerId, Long hospitalId);

	@Query("select w.wardId from Ward w where UPPER(w.wardName) = UPPER(?1) and w.customer.customerId = ?2 and w.hospital.hospitalId = ?3 and w.wardId <> ?4")
	Long findByWardNameIgnoreCaseAndCustomerHospitalWardIdNot(String wardName, Long customerId, Long hospitalId, Long wardId);

	@Query("select new com.ehms.vos.WardBedFlatVO(b.ward.wardId, b.ward.wardName, b.bedId, b.bedCode) from Bed b where b.patient.patientId is NULL and b.ward.customer.customerId = ?1 and b.ward.hospital.hospitalId = ?2 order by b.ward.wardName,b.bedCode")
	List<WardBedFlatVO> findAvailableBeds(Long customerId, Long hospitalId);
}
