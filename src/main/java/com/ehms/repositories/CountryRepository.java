package com.ehms.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.Country;
import com.ehms.vos.CountryVO;

public interface CountryRepository extends JpaRepository<Country, Long> {

	@Query("SELECT new com.ehms.vos.CountryVO(c.countryId, c.countryName) FROM Country c")
	List<CountryVO> findAllCountries();
}
