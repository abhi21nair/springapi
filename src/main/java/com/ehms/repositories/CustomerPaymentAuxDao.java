package com.ehms.repositories;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.ApprovalListVO;
import com.ehms.vos.BillingAndPaymentVO;
import com.ehms.vos.PaginationResponseDto;

@Repository
public class CustomerPaymentAuxDao  {
	
	@Resource
    private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public PaginationResponseDto<BillingAndPaymentVO> getBillingAndPaymentDetails(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText) {
		PaginationResponseDto<BillingAndPaymentVO> result = new PaginationResponseDto<BillingAndPaymentVO>();
		String countQuery = "select count(1) as total_customers from ehms_customers where upper(customer_name) like upper(?) or upper(organization_code) like upper(?)";
		Query cntQuery = entityManager.createNativeQuery(countQuery);
		cntQuery.setParameter(1, "%"+ searchText + "%");
		cntQuery.setParameter(2, "%"+ searchText + "%");
		Number count = (Number) cntQuery.getSingleResult();
		int totalCount = 0;
		if (count != null) {
			totalCount = count.intValue();
		}
		if (totalCount > 0) {
			String query = " select customer_id, customer_name, organization_code, branches, buying_type_id, buying_type_quantity, patients, expiry_date, "
					+ " case  "
					+ " 		when buying_type_id = 1 and branches > buying_type_quantity then 1  "
					+ "         when buying_type_id = 2 and patients > buying_type_quantity then 1  "
					+ "         else 0  "
					+ " 	end as sort_order, "
					+ " case when date (expiry_date) > date(now()) then 1 when date(expiry_date) = date (now()) then 2 else 3 end as expiry_status"
					+ "  from ( "
					+ " SELECT  "
					+ "     ec.customer_id,  "
					+ "     ec.customer_name,  "
					+ "     ec.organization_code,  "
					+ "     (select count(1) from ehms_hospitals eh where eh.customer_id = ec.customer_id ) branches,  "
					+ "     ec.buying_type_id, "
					+ "     ec.buying_type_quantity,  "
					+ "     (select count(1) from ehms_patients ep where ep.customer_id = ec.customer_id ) patients, "
					+ "     ec.expiry_date "
					+ " FROM  "
					+ "     ehms_customers ec) customer_info where upper(customer_name) like upper(?)  or upper(organization_code)  like upper(?)"
	        		+  appendBillingPaymentSortBy(sortOn, sortOrder);
	       
			Query nq = entityManager.createNativeQuery(query).setFirstResult(page * pageSize).setMaxResults(pageSize);
			nq.setParameter(1, "%"+ searchText + "%");
			nq.setParameter(2, "%"+ searchText + "%");
			List<BillingAndPaymentVO> billingAndPaymentList = populateBillingandPaymentVos(nq.getResultList());

			if (billingAndPaymentList != null) {
				result.setCount(billingAndPaymentList.size());
				result.setPage(page);
				result.setTotalItems(new Long(totalCount));
				int totalPages = totalCount / pageSize;
				if (totalCount % pageSize > 0) {
					totalPages++;
				}
				result.setTotalPages((totalPages));
				result.setItems(billingAndPaymentList);
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public PaginationResponseDto<ApprovalListVO> getBillingAndPaymentApprovalDetails(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText) {
		PaginationResponseDto<ApprovalListVO> result = new PaginationResponseDto<ApprovalListVO>();
		String countQuery = "select count(1) as total_customers from ehms_customers where upper(customer_name) like upper(?) or upper(organization_code) like upper(?)";
		Query cntQuery = entityManager.createNativeQuery(countQuery);
		cntQuery.setParameter(1, "%"+ searchText + "%");
		cntQuery.setParameter(2, "%"+ searchText + "%");
		Number count = (Number) cntQuery.getSingleResult();
		int totalCount = 0;
		if (count != null) {
			totalCount = count.intValue();
		}
		if (totalCount > 0) {
			String query = " SELECT  "
					+ "     customer_id, "
					+ "     customer_name, "
					+ "     organization_code, "
					+ "     branches, "
					+ "     buying_type_id, "
					+ "     buying_type_quantity, "
					+ "     patients, "
					+ " expiry_date, "
					+ "     STR_TO_DATE(SUBSTRING_INDEX(payment_details, '^', 1) , '%Y-%m-%d %H:%i:%s') AS transaction_date, "
					+ "     convert(SUBSTRING_INDEX(SUBSTRING_INDEX(payment_details, '^', 2), "
					+ "             '^', "
					+ "             - 1), UNSIGNED) AS payment_id, "
					+ "     convert(SUBSTRING_INDEX(SUBSTRING_INDEX(payment_details, '^',  3), "
					+ "             '^', "
					+ "             -1), DECIMAL) AS billed_amount, "
					+ "     convert(SUBSTRING_INDEX(SUBSTRING_INDEX(payment_details, '^',  4), "
					+ "             '^', "
					+ "             -1), UNSIGNED) AS  payment_mode_id, "
					+ "     convert(SUBSTRING_INDEX(payment_details, '^', - 1), UNSIGNED) AS payment_status_id "
					+ " FROM "
					+ "     (SELECT  "
					+ "         ec.customer_id, "
					+ "             ec.customer_name, "
					+ "             ec.organization_code, "
					+ "             (SELECT  "
					+ "                     COUNT(*) "
					+ "                 FROM "
					+ "                     ehms_hospitals eh "
					+ "                 WHERE "
					+ "                     eh.customer_id = ec.customer_id) branches, "
					+ "             ec.buying_type_id, "
					+ "             ec.buying_type_quantity, "
					+ "             (SELECT  "
					+ "                     COUNT(*) "
					+ "                 FROM "
					+ "                     ehms_patients ep "
					+ "                 WHERE "
					+ "                     ep.customer_id = ec.customer_id) patients, "
					+ "             ec.expiry_date, "
					+ "             (SELECT  "
					+ "                     CONCAT(created_dt, '^', customer_payment_id, '^', total_billing_amount, '^', payment_mode_id, '^', payment_status_id) "
					+ "                 FROM "
					+ "                     `ehms_customer_payments` "
					+ "                 WHERE "
					+ "                     customer_id = ec.customer_id "
					+ "                 ORDER BY created_dt DESC "
					+ "                 LIMIT 1) payment_details "
					+ "     FROM "
					+ "         ehms_customers ec) data "
					+ " Where upper(customer_name) like upper(?) or upper(organization_code) like upper(?) "
	        		+  appendApprovalSortBy(sortOn, sortOrder);
	       
			Query nq = entityManager.createNativeQuery(query).setFirstResult(page * pageSize).setMaxResults(pageSize);
			nq.setParameter(1, "%"+ searchText + "%");
			nq.setParameter(2, "%"+ searchText + "%");
			List<ApprovalListVO> approvalList = populateApprovalVos(nq.getResultList());

			if (approvalList != null) {
				result.setCount(approvalList.size());
				result.setPage(page);
				result.setTotalItems(new Long(totalCount));
				int totalPages = totalCount / pageSize;
				if (totalCount % pageSize > 0) {
					totalPages++;
				}
				result.setTotalPages((totalPages));
				result.setItems(approvalList);
			}
		}
		return result;
	
	}

	private String appendBillingPaymentSortBy(String sortOn, String sortOrder) {
		String orderBy = "order by ";
		switch (sortOn) {
		case "customerName":
			orderBy += "ASC".equalsIgnoreCase(sortOrder) ? " customer_name " : "customer_name desc ";
			break;
		case "organizationCode":
			orderBy += "ASC".equalsIgnoreCase(sortOrder) ? " organization_code " : "organization_code desc ";
			break;
		case "default":
			orderBy += " sort_order desc, expiry_date, customer_name ";
			break;
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
		return orderBy;
	}
	
	private String appendApprovalSortBy(String sortOn, String sortOrder) {
		String orderBy = "order by ";
		switch (sortOn) {
		case "customerName":
			orderBy += "ASC".equalsIgnoreCase(sortOrder) ? " customer_name " : "customer_name desc ";
			break;
		case "organizationCode":
			orderBy += "ASC".equalsIgnoreCase(sortOrder) ? " organization_code " : "organization_code desc ";
			break;
		case "txDate":
			orderBy += "ASC".equalsIgnoreCase(sortOrder) ? " transaction_date " : "transaction_date desc ";
			break;
		case "paymentStatus":
			orderBy += " -payment_status_id desc";
			break;
		case "default":
			orderBy += " -payment_status_id desc, transaction_date ";
			break;
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
		return orderBy;
	}

	private List<BillingAndPaymentVO> populateBillingandPaymentVos(List<Object> resultList) {
		List<BillingAndPaymentVO> resultVoList = new ArrayList<BillingAndPaymentVO>();
		BillingAndPaymentVO vo;
		if (resultList != null) {
			for (Object resultRow : resultList) {
				if (resultRow instanceof Object[]) {
					Object[] ary = (Object[]) resultRow;
					vo = new BillingAndPaymentVO();
					vo.setCustomerId(((BigInteger) ary[0]).longValue());
					vo.setCustomerName(((String) ary[1]));
					vo.setOrganizationCode(((String) ary[2]));
					vo.setNoOfBranches(((BigInteger) ary[3]).longValue());
					vo.setBuyingTypeId(((BigInteger) ary[4]).longValue());
					vo.setBuyingTypeQuantity(((BigInteger) ary[5]).longValue());
					vo.setNoOfPatients(((BigInteger) ary[6]).longValue());
					if (ary[7] != null && ary[7] instanceof Timestamp) {
						vo.setExpiryDate(DateTimeUtil.formatDate((Timestamp) ary[7]));
					}
					vo.setPaymentStatus(((BigInteger) ary[9]).intValue());
					resultVoList.add(vo);
				}
			}
		}
		return resultVoList;
	}
	
	private List<ApprovalListVO> populateApprovalVos(List<Object> resultList) {
		List<ApprovalListVO> resultVoList = new ArrayList<ApprovalListVO>();
		ApprovalListVO vo;
		if (resultList != null) {
			for (Object resultRow : resultList) {
				if (resultRow instanceof Object[]) {
					Object[] ary = (Object[]) resultRow;
					vo = new ApprovalListVO();
					vo.setCustomerId(((BigInteger) ary[0]).longValue());
					vo.setCustomerName(((String) ary[1]));
					vo.setOrganizationCode(((String) ary[2]));
					vo.setNoOfBranches(((BigInteger) ary[3]).longValue());
					vo.setBuyingTypeId(((BigInteger) ary[4]).longValue());
					vo.setBuyingTypeQuantity(((BigInteger) ary[5]).longValue());
					vo.setNoOfPatients(((BigInteger) ary[6]).longValue());
					if (ary[7] != null && ary[7] instanceof Timestamp) {
						vo.setExpiryDate(DateTimeUtil.formatDate((Timestamp) ary[7]));
					}
					if (ary[8] != null && ary[8] instanceof Timestamp) {
						vo.setTxDate(DateTimeUtil.formatDate((Timestamp) ary[8]));
					}
					
					if (ary[9] != null) {
						vo.setCustomerPaymentId(((BigInteger) ary[9]).longValue());
					}
					
					if (ary[10] != null) {
						vo.setAmount(((BigDecimal) ary[10]).doubleValue());
					}
					
					if (ary[11] != null) {
						vo.setPaymentModeId(((BigInteger) ary[11]).longValue());
					}
					
					if (ary[12] != null) {
						vo.setPaymentStatusId(((BigInteger) ary[12]).longValue());
					}
					resultVoList.add(vo);
				}
			}
		}
		return resultVoList;
	}
}
