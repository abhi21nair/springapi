package com.ehms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.ManufacturingCompany;
import com.ehms.vos.ManufacturingCompanyVO;

public interface ManufacturingCompanyRepository extends JpaRepository<ManufacturingCompany, Long> {

	@Query("select new com.ehms.vos.ManufacturingCompanyVO(m.manufacturingCompanyId, m.manufactureName, m.manufactureNumber, m.mobileNumber, m.vendorSupplierNo, m.address.city.cityName) from ManufacturingCompany m where m.customer.customerId = ?2 and m.hospital.hospitalId = ?3 and (UPPER(m.manufactureName) like UPPER(?1))")
	Page<ManufacturingCompanyVO> findManufacturingCompanies(String searchText, Long customerId, Long hospitalId,
			Pageable pageRequest);

	@Query("select m from ManufacturingCompany m where m.customer.customerId = ?2 and m.hospital.hospitalId = ?3 and m.manufacturingCompanyId = ?1")
	ManufacturingCompany findManufacturingComapny(Long manufacturingCompanyId, Long customerId, Long hospitalId);

}
