package com.ehms.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.DailyProgressNote;
import com.ehms.vos.DailyProgressNoteVO;

public interface DailyProgressNoteRepository extends JpaRepository<DailyProgressNote, Long> {

	@Query("select new com.ehms.vos.DailyProgressNoteVO(dpn) from DailyProgressNote dpn where dpn.patientId = ?1")
	List<DailyProgressNoteVO> findDailyProgressNotes(Long patientId);

	@Query("select new com.ehms.vos.DailyProgressNoteVO(dpn.dailyProgressNoteId,dpn.appointmentId,dpn.patientId,DATE_FORMAT(dpn.progressDate, '%d-%m-%Y %H:%i:%s'),dpn.doctorDayProgressNotes,dpn.doctorNightProgressNotes,dpn.previousNotes) from DailyProgressNote dpn where dpn.appointmentId in(?1) ORDER BY dpn.createdDt DESC")
	List<DailyProgressNoteVO> findDailyProgressNoteForMultipleIds(List<Long> appointmentId);
}
