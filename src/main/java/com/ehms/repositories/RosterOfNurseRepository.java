package com.ehms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.ehms.model.RosterOfNurse;
import com.ehms.vos.RosterOfNurseVO;

public interface RosterOfNurseRepository extends JpaRepository<RosterOfNurse, Long> {

	@Query("select new com.ehms.vos.RosterOfNurseVO(r.rosterOfNurseId,r.nurseName.employeeId,r.nurseName.firstName,r.nurseName.middleName,r.nurseName.lastName,r.nurseName.title.titleName,r.nurseShift,DATE_FORMAT(r.timeFrom, '%H:%i:%s'),DATE_FORMAT(r.timeTo, '%H:%i:%s'),DATE_FORMAT(r.dateFrom, '%d-%m-%Y %H:%i:%s'),DATE_FORMAT(r.dateTo, '%d-%m-%Y %H:%i:%s')) from RosterOfNurse r where r.nurseName.customer.customerId = ?2 and r.nurseName.hospital.hospitalId = ?3 and (UPPER(r.nurseName.firstName) like UPPER(?1))")
	Page<RosterOfNurseVO> findRosterOfNurse(String searchText,Long customerId, Long hospitalId,Pageable pageRequest);
}
