package com.ehms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ehms.model.Bed;

public interface BedRepository extends JpaRepository<Bed, Long> {

	@Query("Select b.bedId from Bed b where UPPER(bedCode) = UPPER(?1) and b.ward.wardId = ?2")
	Long findByBedCodeIgnoreCaseAndWard(String bedCode, Long wardId);

	@Query("Select b.bedId from Bed b where UPPER(bedCode) = UPPER(?1) and b.ward.wardId = ?2 and b.bedId <> ?3")
	Long findByBedCodeIgnoreCaseAndWardAndBedIdNot(String bedCode, Long wardId);
	
	@Modifying
	@Query("UPDATE Bed SET current_patient_id = null WHERE id = :id")
	void freeBedByBedId(@Param("id") Long id);
}
