package com.ehms.repositories.queue;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.Appointment;
import com.ehms.vos.queue.QueueResponseVO;

public interface QueueRepository extends JpaRepository<Appointment, Long> {

	@Query("select new com.ehms.vos.queue.QueueResponseVO(a.appointmentId, a.customer.customerId, a.hospital.hospitalId, a.module.moduleId, a.doctor.employeeId, a.timeSpent, DATE_FORMAT(a.fromTime, '%d-%m-%Y %H:%i:%s'), DATE_FORMAT(a.toTime, '%d-%m-%Y %H:%i:%s'), a.status, DATE_FORMAT(a.reportTime, '%d-%m-%Y %H:%i:%s'), DATE_FORMAT(a.appointmentStartTime, '%d-%m-%Y %H:%i:%s'), DATE_FORMAT(a.appointmentEndTime, '%d-%m-%Y %H:%i:%s'), a.patient.patientId, a.patient.titleId, a.patient.patientCode, a.patient.firstName, a.patient.middleName, a.patient.lastName, a.patient.mobileNumber, a.patient.allergy, DATE_FORMAT(a.patient.birthDate, '%d-%m-%Y %H:%i:%s'), a.patient.birthYear, a.patient.isDobAvailable, b.bedId, b.bedCode, w.wardId,  w.wardName, d.departmentId) from Appointment a left join a.bed b left join b.ward w left join a.department d where a.customer.customerId = ?2 and a.hospital.hospitalId = ?3 and a.reportTime is not null and a.status in (1, 2, 3) and a.fromTime >= DATE_FORMAT(?4, '%Y-%m-%d 00:00:00') and  a.fromTime <= DATE_FORMAT(?4, '%Y-%m-%d 23:59:59') and (((a.module.moduleId = ?5 and a.docSchedule.doctorScheduleId is null and a.doctor.employeeId is null)  or ( a.doctor.employeeId = ?6 and a.module.moduleId is null))) and (upper(a.patient.patientCode) like upper(?1) or upper(a.patient.firstName) like upper(?1) or upper(a.patient.lastName) like upper(?1) or upper(a.patient.mobileNumber) like (?1))")
	Page<QueueResponseVO> getQueueDetails(String string, Long customerId, Long hospitalId, Date queryDate, Long moduleId, Long doctorId, Pageable pageRequest);

}
