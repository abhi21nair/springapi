package com.ehms.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.Employee;
import com.ehms.vos.EmployeeVO;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	@Query("select new com.ehms.vos.EmployeeVO(e.employeeId, e.firstName, e.lastName, e.employeeCode, e.designation.miscTypeValueId, e.department.departmentId, e.specialization.miscTypeValueId, e.countryCodeForMobileNumber, e.mobileNumber, e.email) from Employee e where e.customer.customerId = ?2 and e.hospital.hospitalId = ?3 and e.employeeId <> ?4 and (UPPER(e.firstName) like UPPER(?1) or UPPER(e.lastName) like UPPER(?1))")
	Page<EmployeeVO> findEmployeesOfHospital(String searchText, Long customerId, Long hospitalId, Long employeeId, Pageable pageRequest);

	@Query("select new com.ehms.vos.EmployeeVO(e.employeeId, e.firstName, e.middleName, e.lastName, e.employeeCode) from Employee e where e.customer.customerId = ?1 and e.hospital.hospitalId = ?2 and not exists (select 1 from User u where u.employee.employeeId = e.employeeId))")
	List<EmployeeVO> getEmployeeWithoutEhmsAccount(Long customerId, Long hospitalId);
	
	@Query("select new com.ehms.vos.EmployeeVO(e.employeeId, e.firstName, e.middleName, e.lastName, e.employeeCode) from Employee e where e.customer.customerId is null and e.hospital.hospitalId is null and not exists (select 1 from User u where u.employee.employeeId = e.employeeId))")
	List<EmployeeVO> getSystemEmployeeWithoutEhmsAccount();

	@Query("select new com.ehms.vos.EmployeeVO(e.employeeId, e.title.titleId, e.firstName, e.middleName, e.lastName, e.employeeCode) from Employee e where e.customer.customerId = ?1 and e.hospital.hospitalId = ?2")
	List<EmployeeVO> getEmployeeListForAllEmployeeTypes(Long customerId, Long hospitalId);

	@Query("Select e.employeeId from Employee e where UPPER(e.employeeCode) = UPPER(?1) and e.customer.customerId =?2 and e.hospital.hospitalId = ?3")
	Long findEmployeesForCustomerAndHospital(String employeeCode, Long customerId, Long hospitalId);

	@Query("Select e.employeeId from Employee e where UPPER(e.employeeCode) = UPPER(?1) and e.customer.customerId is null and e.hospital.hospitalId is null")
	Long findSystemEmployees(String employeeCode);

	@Query("select new com.ehms.vos.EmployeeVO(e.employeeId, e.title.titleId, e.firstName, e.middleName, e.lastName, e.employeeCode) from Employee e where e.customer.customerId = ?1 and e.hospital.hospitalId = ?2 and e.employeeType=?3")
	List<EmployeeVO> getEmployeeList(Long customerId, Long hospitalId, Integer employeeType);

	@Query("select new com.ehms.vos.EmployeeVO(e.employeeId, e.firstName, e.lastName, e.employeeCode, e.designation.miscTypeValueId, e.department.departmentId, e.specialization.miscTypeValueId, e.countryCodeForMobileNumber, e.mobileNumber, e.email) from Employee e where (UPPER(e.firstName) like UPPER(?1) or UPPER(e.lastName) like UPPER(?1)) and e.customer.customerId is null and e.hospital.hospitalId is null")
	Page<EmployeeVO> findSystemEmployees(String string, Pageable pageRequest);

	@Query("select e from Employee e where e.employeeId = ?1 and e.customer.customerId is null and e.hospital.hospitalId is null")
	Employee findSystemEmployee(Long employeeId);
}
