package com.ehms.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.DischargeSheet;
import com.ehms.vos.DischargeSheetVO;

public interface DischargeSheetRepository extends JpaRepository<DischargeSheet, Long> {

	@Query("select new com.ehms.vos.DischargeSheetVO(ds) from DischargeSheet ds where ds.patientId = ?1")
	List<DischargeSheetVO> findDischargeSheets(Long patientId);

	@Query("select new com.ehms.vos.DischargeSheetVO(ds.dischargeSheetId,ds.appointmentId,ds.patientId,ds.dischargeCoditionId,DATE_FORMAT(ds.dischargeDate, '%d-%m-%Y %H:%i:%s'),DATE_FORMAT(ds.deathDate, '%d-%m-%Y %H:%i:%s'),DATE_FORMAT(ds.causeOfDeath, '%d-%m-%Y %H:%i:%s'),ds.advice,ds.nextAppointment,ds.otherDetails) from DischargeSheet ds where ds.appointmentId in(?1) ORDER BY ds.createdDt DESC")
	List<DischargeSheetVO> findDischargeSheetForMultipleIds(List<Long> appointmentId);
}
