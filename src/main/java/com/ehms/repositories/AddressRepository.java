package com.ehms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ehms.model.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {
	
}
