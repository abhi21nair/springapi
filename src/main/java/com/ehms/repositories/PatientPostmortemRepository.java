package com.ehms.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.PatientPostmortem;
import com.ehms.vos.PatientPostmortemVO;

public interface PatientPostmortemRepository extends JpaRepository<PatientPostmortem, Long> {

	@Query("select new com.ehms.vos.PatientPostmortemVO(p) from PatientPostmortem p where p.patientId = ?1")
	List<PatientPostmortemVO> findPatientPostmortems(Long patientId);

	@Query("select new com.ehms.vos.PatientPostmortemVO(p.patientPostmortemId,p.appointmentId,p.patientId,DATE_FORMAT(p.deathDate, '%d-%m-%Y %H:%i:%s'),DATE_FORMAT(p.causeOfDeath, '%d-%m-%Y %H:%i:%s'),p.deadBodyHanded,p.deadBodyHandedTo,DATE_FORMAT(p.morgueTime, '%d-%m-%Y %H:%i:%s'),DATE_FORMAT(p.postmortemTime, '%H:%i:%s')) from PatientPostmortem p where p.appointmentId in(?1) ORDER BY p.createdDt DESC")
	List<PatientPostmortemVO> findPatientPostmortemsForMultipleIds(List<Long> appointmentId);
}
