package com.ehms.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.Department;
import com.ehms.vos.DepartmentVO;

public interface DepartmentRepository extends JpaRepository<Department, Long> {

	@Query("SELECT new com.ehms.vos.DepartmentVO(d.departmentId, d.departmentName, d.isActive, d.customer.customerId,d.departmentType) FROM Department d"
			+ " WHERE d.customer.customerId = ?1 and d.hospital is null")
	List<DepartmentVO> findCustomerDepartments(Long customerId);

	@Query("SELECT new com.ehms.vos.DepartmentVO(d.departmentId, d.departmentName, d.isActive, d.customer.customerId, d.hospital.hospitalId,d.departmentType) FROM Department d"
			+ " WHERE (d.customer.customerId = ?1 and d.hospital is null) or (d.customer.customerId = ?1 and d.hospital.hospitalId = ?2)")
	List<DepartmentVO> findCustomerHospitalDepartments(Long customerId, Long hospitalId);

	@Query("Select d.departmentId from Department d where UPPER(d.departmentName) = UPPER(?1) and d.customer.customerId is null and d.hospital.hospitalId is null")
	Long findDepartmentByName(String departmentName);

	@Query("Select d.departmentId from Department d where UPPER(d.departmentName) = UPPER(?1) and d.departmentId <> ?2 and d.customer.customerId is null and d.hospital.hospitalId is null")
	Long findDepartmentByNameAndId(String departmentName, Long departmentId);

	@Query("Select d.departmentId from Department d where UPPER(d.departmentName) = UPPER(?1) and d.customer.customerId =?2")
	Long findDepartmentForCustomerByName(String departmentName, Long customerId);

	@Query("Select d.departmentId from Department d where UPPER(d.departmentName) = UPPER(?1) and d.departmentId <> ?2 and d.customer.customerId =?3")
	Long findDepartmentForCustomerByNameAndId(String departmentName, Long departmentId, Long customerId);

	@Query("Select d.departmentId from Department d where UPPER(d.departmentName) = UPPER(?1) and d.customer.customerId = ?2 and d.hospital.hospitalId =?3")
	Long findDepartmentForCustomerAndHospitalByName(String departmentName, Long customerId, Long hospitalId);

	@Query("Select d.departmentId from Department d where UPPER(d.departmentName) = UPPER(?1) and d.departmentId <> ?2 and d.customer.customerId = ?3 and d.hospital.hospitalId =?4")
	Long findDepartmentForCustomerAndHospitalByNameAndId(String departmentName, Long departmentId, Long customerId, Long hospitalId);

	@Query("SELECT new com.ehms.vos.DepartmentVO(d.departmentId, d.departmentName, d.isActive, d.customer.customerId,d.departmentType) FROM Department d"
			+ " WHERE d.customer.customerId is null and d.hospital.hospitalId is null")
	List<DepartmentVO> findSystemDepartments();

}
