package com.ehms.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.MaterialInstrument;
import com.ehms.vos.MaterialInstrumentVO;

public interface MaterialInstrumentRepository extends JpaRepository<MaterialInstrument, Long> {

	@Query("select new com.ehms.vos.MaterialInstrumentVO(m.materialInstrumentId,m.materialInstrumentNumber,m.isActive,m.materialInstrumentName,m.departmentId,m.materialInstrumentTypeId) from MaterialInstrument m where m.customer.customerId = ?2 and m.hospital.hospitalId = ?3 and (UPPER(m.materialInstrumentName) like UPPER(?1))")
	Page<MaterialInstrumentVO> findMaterialInstruments(String searchText, Long customerId, Long hospitalId,
			Pageable pageRequest);

	@Query("select m from MaterialInstrument m where m.customer.customerId = ?2 and m.hospital.hospitalId = ?3 and m.materialInstrumentId = ?1")
	MaterialInstrument findMaterialInstrument(Long materialInstrumentId, Long customerId, Long hospitalId);

	@Query("select new com.ehms.vos.MaterialInstrumentVO(m.materialInstrumentId,m.materialInstrumentNumber,m.isActive,m.materialInstrumentName,m.departmentId,m.materialInstrumentTypeId) from MaterialInstrument m where m.customer.customerId = ?1 and m.hospital.hospitalId = ?2")
	List<MaterialInstrumentVO> getMaterialInstrument(Long customerId, Long hospitalId);		 
	

}
