package com.ehms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.ehms.model.CustomerPayment;
import com.ehms.vos.BillingPaymentHistoryGridVO;

public interface BillingPaymentHistoryGridRepository extends JpaRepository<CustomerPayment, Long> {

	@Query("SELECT new com.ehms.vos.BillingPaymentHistoryGridVO(c.createdDt, c.costOfProduct, c.paymentMode.paymentModeId,c.paymentStatus.paymentStatusId,c.actionBy.userId,c.actionTime) FROM CustomerPayment c where c.customer.customerId=?1 ")
	Page<BillingPaymentHistoryGridVO> findGridHistory(Long id, Pageable pageRequest);
}