package com.ehms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ehms.model.BuyingPreference;

public interface BuyingPreferenceRepository extends JpaRepository<BuyingPreference, Long> {

}
