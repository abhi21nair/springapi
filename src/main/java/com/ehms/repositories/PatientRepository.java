package com.ehms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.Patient;
import com.ehms.vos.PatientVO;

public interface PatientRepository extends JpaRepository<Patient, Long>, JpaSpecificationExecutor<Patient> {

	@Query("select new com.ehms.vos.PatientVO(p.patientId, p.titleId, p.firstName, p.lastName, p.gender, p.currentAddress.city.cityId, p.mobileNumber) from Patient p where "
			+ "p.customer.customerId = ?2 and p.hospital.hospitalId = ?3 and (UPPER(p.firstName) like UPPER(?1) or UPPER(p.lastName) like UPPER(?1))")
	Page<PatientVO> getPatientList(String searchText, Long customerId, Long hospitalId, Pageable pageRequest);

}
