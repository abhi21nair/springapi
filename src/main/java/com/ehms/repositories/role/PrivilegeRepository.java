package com.ehms.repositories.role;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.Privilege;
import com.ehms.vos.role.PrivilegeVO;

public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {

	@Query("select new com.ehms.vos.role.PrivilegeVO(p.privilegeId, p.privilegeCode, p.description, p.module.moduleId) from Privilege p where p.isSystemPrivilege = 0 and p.module.moduleId in (select module.moduleId from LinkCustomerModule lcm where lcm.customer.customerId = ?1)")
	List<PrivilegeVO> getAvailablePrivileges(Long customerId);

	@Query("select new com.ehms.vos.role.PrivilegeVO(p.privilegeId, p.privilegeCode, p.description, p.module.moduleId) from Privilege p where p.isSystemPrivilege = 1")
	List<PrivilegeVO> getAvailableSystemPrivileges();

}
