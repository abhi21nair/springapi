package com.ehms.repositories.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.LinkRolePrivilege;

public interface LinkRolePrivilegeRepository extends JpaRepository<LinkRolePrivilege, Long> {

	@Modifying
	@Query("delete from LinkRolePrivilege link where link.role.roleId = ?1")
	void deleteByRoleId(Long roleId);
}
