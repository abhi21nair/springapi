package com.ehms.repositories.role;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.Role;
import com.ehms.vos.role.RoleVO;

public interface RoleRepository extends JpaRepository<Role, Long> {

	@Query("select new com.ehms.vos.role.RoleVO (r.roleId, r.roleName, r.description, r.isActive, r.customer.customerId, r.hospital.hospitalId) from Role r where r.isSystemRole = 0 and (UPPER(r.roleName) like UPPER(?1)) and r.customer.customerId = ?2 and r.hospital is null")
	Page<RoleVO> findCustomerRoles(String searchString, Long customerId, Pageable pageRequest);

	@Query("select r from Role r where r.roleId = ?1 and r.isSystemRole = 1 and r.roleName not in('BBNISYS_ADMIN','CUSTOMER_ADMIN','HOSPITAL_ADMIN') and r.customer.customerId is null and r.hospital.hospitalId is null")
	Role findSystemRole(Long roleId);

	@Query("select new com.ehms.vos.role.RoleVO (r.roleId, r.roleName, r.description, r.isActive, r.customer.customerId, r.hospital.hospitalId) from Role r where r.isSystemRole = 1 and (UPPER(r.roleName) like UPPER(?1)) and r.roleName not in('BBNISYS_ADMIN','CUSTOMER_ADMIN','HOSPITAL_ADMIN') and r.customer.customerId is null and r.hospital.hospitalId is null")
	Page<RoleVO> findSystemRoles(String searchString, Pageable pageRequest);

	@Query("select new com.ehms.vos.role.RoleVO (r.roleId, r.roleName, r.description, r.isActive, r.customer.customerId, r.hospital.hospitalId) from Role r where r.isSystemRole = 0 and (UPPER(r.roleName) like UPPER(?1)) and r.customer.customerId = ?2 and r.hospital.hospitalId = ?3")
	Page<RoleVO> findHospitalRoles(String searchString, Long customerId, Long hospitalId, Pageable pageRequest);

	@Query("Select r from Role r where r.isSystemRole = 1 and r.customer.customerId is null and r.hospital.hospitalId is null and r.roleName like UPPER(?1)")
	Role getSystemRoleDetailsForRoleName(String roleName);

	@Query("Select r from Role r where r.roleName like UPPER(?1) and r.isSystemRole = 0  and r.customer.customerId = ?2")
	Role findCustomerRoleByName(String name, Long customerId);

	@Query("Select r.roleId from Role r where r.roleName = UPPER(?1) and r.isSystemRole = 1  and r.customer.customerId is null and r.hospital.hospitalId is null")
	Long findRolesByName(String roleName);

	@Query("Select r.roleId from Role r where r.roleName = UPPER(?1) and r.isSystemRole = 1  and r.roleId <> ?2 and r.customer.customerId is null and r.hospital.hospitalId is null")
	Long findRoleByNameAndId(String roleName, Long roleId);

	@Query("Select r.roleId from Role r where r.roleName = UPPER(?1) and r.isSystemRole = 0 and r.roleId <> ?2 and r.customer.customerId =?3 and r.hospital.hospitalId is null")
	Long findRoleByNameAndIdForCustomer(String roleName, Long roleId, Long customerId);

	@Query("Select r.roleId from Role r where r.roleName = UPPER(?1) and r.isSystemRole = 0 and r.customer.customerId =?2 and r.hospital.hospitalId is null")
	Long findRoleByNameForCustomer(String roleName, Long customerId);

	@Query("Select r.roleId from Role r where r.roleName = UPPER(?1) and r.isSystemRole = 0 and r.customer.customerId = ?2 and r.hospital.hospitalId =?3")
	Long findRoleByNameForCustomerAndHospital(String roleName, Long customerId, Long hospitalId);

	@Query("Select r.roleId from Role r where r.roleName = UPPER(?1) and r.isSystemRole = 0 and r.roleId <> ?2 and r.customer.customerId = ?3 and r.hospital.hospitalId =?4")
	Long findRoleByNameAndIdForCustomerAndHospital(String roleName, Long roleId, Long customerId, Long hospitalId);

	@Query("Select new com.ehms.vos.role.RoleVO(r.roleId, r.roleName) from Role r where r.isSystemRole = 1 and r.customer.customerId is null and r.hospital is null ")
	List<RoleVO> getSystemRoleList();

	@Query("Select new com.ehms.vos.role.RoleVO(r.roleId, r.roleName) from Role r where r.isSystemRole = 0 and r.customer.customerId =?1 and r.hospital is null ")
	List<RoleVO> getCustomerRoleList(Long customerId);

	@Query("Select new com.ehms.vos.role.RoleVO(r.roleId, r.roleName) from Role r where r.isSystemRole = 0 and ((r.customer.customerId =?1 and r.hospital is null) or (r.customer.customerId = ?1 and r.hospital.hospitalId = ?2))")
	List<RoleVO> getHospitalRoleList(Long customerId, Long hospitalId);
}
