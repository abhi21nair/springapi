package com.ehms.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.PathologyTestMaster;
import com.ehms.vos.PathologyTestMasterVO;

public interface PathologyTestMasterRepository extends JpaRepository<PathologyTestMaster, Long> {

	@Query("select new com.ehms.vos.PathologyTestMasterVO(p.pathologyTestMasterId,p.testName, p.subTestName, p.lab.departmentId,p.lab.departmentName,p.specimenTypeId,p.isActive) from PathologyTestMaster p where p.customer.customerId = ?2 and p.hospital.hospitalId = ?3 and (UPPER(p.testName) like UPPER(?1))")
	Page<PathologyTestMasterVO> findpathologyTestMasterDetails(String searchText, Long customerId, Long hospitalId, Pageable pageRequest);

	@Query("select new com.ehms.vos.PathologyTestMasterVO(p) from PathologyTestMaster p where p.customer.customerId = ?1 and p.hospital.hospitalId = ?2)")
	List<PathologyTestMasterVO> getPathologyTestMastersList(Long customerId, Long hospitalId);
}
