package com.ehms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ehms.model.PaymentMode;

public interface PaymentModeRepository extends JpaRepository<PaymentMode, Long> {

}
