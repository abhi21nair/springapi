package com.ehms.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.DietMaster;
import com.ehms.vos.DietMasterVO;

public interface DietMasterRepository extends JpaRepository<DietMaster, Long> {

	@Query("select new com.ehms.vos.DietMasterVO(dm) from DietMaster dm where dm.patientId = ?1")
	List<DietMasterVO> findDiets(Long patientId);

	@Query("select new com.ehms.vos.DietMasterVO(dm.dietMasterId,dm.appointmentId,dm.patientId,DATE_FORMAT(dm.dietDate, '%d-%m-%Y %H:%i:%s'),dm.morningTea,dm.breakfast,dm.lunch,dm.eveningTea,dm.dinner) from DietMaster dm where dm.appointmentId in(?1) ORDER BY dm.createdDt DESC")
	List<DietMasterVO> findDietMastersForMultipleIds(List<Long> appointmentId);
}
