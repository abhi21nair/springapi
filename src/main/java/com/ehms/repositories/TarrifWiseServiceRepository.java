package com.ehms.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.TarrifWiseService;
import com.ehms.vos.TarrifWiseServiceVO;

public interface TarrifWiseServiceRepository extends JpaRepository<TarrifWiseService, Long> {
	@Query("select new com.ehms.vos.TarrifWiseServiceVO(t.tarrifServiceId,t.tarrifServiceName.miscTypeValueId,t.tarrifServiceName.miscValue,t.serviceTypeId,t.departmentId,t.isActive) from TarrifWiseService t where t.customer.customerId = ?2 and t.hospital.hospitalId = ?3 and (UPPER(t.tarrifServiceName.miscValue) like UPPER(?1))")
	Page<TarrifWiseServiceVO> findTarrifWiseServices(String searchText, Long customerId, Long hospitalId,
			Pageable pageRequest);

	@Query("select t from TarrifWiseService t where t.customer.customerId = ?2 and t.hospital.hospitalId = ?3 and t.tarrifServiceId = ?1")
	TarrifWiseService findTarrifWiseService(Long tarrifServiceId, Long customerId, Long hospitalId);

	@Query("select new com.ehms.vos.TarrifWiseServiceVO(t) from TarrifWiseService t where t.customer.customerId = ?1 and t.hospital.hospitalId = ?2")
	List<TarrifWiseServiceVO> getTarrifWiseServices(Long customerId, Long hospitalId);
}
