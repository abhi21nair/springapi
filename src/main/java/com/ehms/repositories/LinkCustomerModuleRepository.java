package com.ehms.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.LinkCustomerModule;

public interface LinkCustomerModuleRepository extends JpaRepository<LinkCustomerModule, Long> {
	
	/*@Query(value ="select m.module_id from ehms_link_customer_module lm, ehms_modules m where m.module_id = lm.module_id and m.is_queueable = 1 and lm.customer_id = ?1", nativeQuery=true)*/
	@Query("Select lm.module.moduleId from LinkCustomerModule lm where lm.module.isQueueable = 1 and lm.customer.customerId = ?1")
	List<Long> findQueueableCustomerModuleIds(Long customerId);
}
