
package com.ehms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.ehms.model.OtMaster;
import com.ehms.vos.OtMasterVO;

public interface OtMasterRepository extends JpaRepository<OtMaster, Long> {

	@Query("select new com.ehms.vos.OtMasterVO(o.otMasterId,o.otName, o.otType.departmentId, o.otLocation,o.isActive) from OtMaster o  where o.customer.customerId = ?2 and o.hospital.hospitalId = ?3 and (UPPER(o.otName) like UPPER(?1))")
	Page<OtMasterVO> findOtMasterDetails(String searchText, Long customerId, Long hospitalId, Pageable pageRequest);
}