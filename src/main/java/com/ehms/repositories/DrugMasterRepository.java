package com.ehms.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.DrugMaster;
import com.ehms.vos.DrugMasterVO;

public interface DrugMasterRepository extends JpaRepository<DrugMaster, Long> {
	@Query("select new com.ehms.vos.DrugMasterVO(d.drugMasterId,d.genericName,d.medicineName,d.brandName,d.drugTypeId,d.strength,d.mrp,d.categoryId) from DrugMaster d where d.customer.customerId = ?2 and d.hospital.hospitalId = ?3 and (UPPER(d.genericName) like UPPER(?1))")
	Page<DrugMasterVO> findDrugMasters(String searchText, Long customerId, Long hospitalId, Pageable pageRequest);

	@Query("select new com.ehms.vos.DrugMasterVO(d) from DrugMaster d where d.customer.customerId = ?1 and d.hospital.hospitalId = ?2")
	List<DrugMasterVO> getDrugMasters(Long customerId, Long hospitalId);
	
	/*@Query("select new com.ehms.vos.DrugMasterVO(d.drugMasterId,d.genericName,d.medicineName,d.brandName,d.drugTypeId,d.storeNameId,d.expiryDate,d.strength,d.unit,d.minOrder,d.maxOrder,d.qty,d.packingId,d.batchNumber,d.purchasePrice,d.costPriceInclAllTax,d.mrp,d.diseaseId,d.categoryId,d.taxInPercent,d.taxInRupees,d.customerId,d.hospitalId) from DrugMaster d where d.customer.customerId) from DrugMaster d where d.customer.customerId = ?1 and d.hospital.hospitalId = ?2")
	List<DrugMasterVO> getDrugMasters(Long customerId, Long hospitalId);*/
}
