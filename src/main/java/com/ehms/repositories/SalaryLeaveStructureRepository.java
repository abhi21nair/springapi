
package com.ehms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.SalaryLeaveStructure;
import com.ehms.vos.SalaryLeaveStructureVO;

public interface SalaryLeaveStructureRepository extends JpaRepository<SalaryLeaveStructure, Long> {

	@Query("select new com.ehms.vos.SalaryLeaveStructureVO(s.salaryLeaveStructureId,s.employee.employeeId,s.employee.firstName,s.employee.middleName,s.employee.lastName,s.employee.title.titleId,s.employee.employeeCode,s.basicSalary,DATE_FORMAT(s.dateFrom, '%d-%m-%Y %H:%i:%s'),DATE_FORMAT(s.dateTo, '%d-%m-%Y %H:%i:%s')) from SalaryLeaveStructure s where s.employee.customer.customerId = ?2 and s.employee.hospital.hospitalId = ?3 and (UPPER(s.employee.firstName) like UPPER(?1)) or (UPPER(s.employee.lastName) like UPPER(?1))")
	Page<SalaryLeaveStructureVO> findSalaryLeaveStructure(String searchText, Long customerId, Long hospitalId, Pageable pageRequest);
}
