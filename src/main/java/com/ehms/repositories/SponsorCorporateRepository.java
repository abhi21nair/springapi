package com.ehms.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ehms.model.SponsorCorporate;
import com.ehms.vos.SponsorCorporateVO;

public interface SponsorCorporateRepository extends JpaRepository<SponsorCorporate, Long> {

	@Query("select new com.ehms.vos.SponsorCorporateVO(s.sponsorCorporateId,DATE_FORMAT(s.effectiveFromDate, '%d-%m-%Y %H:%i:%s'),DATE_FORMAT(s.effectiveToDate, '%d-%m-%Y %H:%i:%s'),s.sponsorCorporateName,s.sponsorCorporateTypeId) from SponsorCorporate s where s.customer.customerId = ?2 and s.hospital.hospitalId = ?3 and (UPPER(s.sponsorCorporateName) like UPPER(?1))")
	Page<SponsorCorporateVO> findSponsorsCorporates(String string, Long customerId, Long hospitalId, Pageable pageRequest);

	@Query("select s from SponsorCorporate s where s.customer.customerId = ?2 and s.hospital.hospitalId = ?3 and s.sponsorCorporateId = ?1")
	SponsorCorporate findSponsorCorporate(Long sponsorCorporateId, Long customerId, Long hospitalId);

	@Query("select new com.ehms.vos.SponsorCorporateVO(s.sponsorCorporateId, s.sponsorCorporateName) from SponsorCorporate s where s.customer.customerId = ?1 and s.hospital.hospitalId = ?2")
	List<SponsorCorporateVO> getSponsorCorporateList(Long customerId, Long hospitalId);
}
