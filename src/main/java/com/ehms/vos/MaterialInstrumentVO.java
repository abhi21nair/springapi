package com.ehms.vos;

import com.ehms.model.MaterialInstrument;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class MaterialInstrumentVO {
	private Long materialInstrumentId;
	private Double charges;
	private String materialInstrumentNumber;
	private Boolean isActive;
	private String materialInstrumentName;
	private String materialInstrumentSubName;
	private Long customerId;
	private Long departmentId;
	private Long hospitalId;
	private Long materialInstrumentTypeId;

	public MaterialInstrumentVO(Long materialInstrumentId, String materialInstrumentNumber, boolean isActive,
			String materialInstrumentName, Long departmentId, Long materialInstrumentTypeId) {
		super();
		this.materialInstrumentId = materialInstrumentId;
		this.materialInstrumentNumber = materialInstrumentNumber;
		this.isActive = isActive;
		this.materialInstrumentName = materialInstrumentName;
		this.departmentId = departmentId;
		this.materialInstrumentTypeId = materialInstrumentTypeId;
	}

	public MaterialInstrumentVO(MaterialInstrument materialInstrument) {
		super();
		this.materialInstrumentId = materialInstrument.getMaterialInstrumentId();
		this.charges = materialInstrument.getCharges();
		this.materialInstrumentNumber = materialInstrument.getMaterialInstrumentNumber();
		this.isActive = materialInstrument.getIsActive();
		this.materialInstrumentName = materialInstrument.getMaterialInstrumentName();
		this.materialInstrumentSubName = materialInstrument.getMaterialInstrumentSubName();
		if (materialInstrument.getCustomer() != null) {
			this.customerId = materialInstrument.getCustomer().getCustomerId();
		}
		this.departmentId = materialInstrument.getDepartmentId();
		if (materialInstrument.getHospital() != null) {
			this.hospitalId = materialInstrument.getHospital().getHospitalId();
			this.materialInstrumentTypeId = materialInstrument.getMaterialInstrumentTypeId();
		}
	}

}
