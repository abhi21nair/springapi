package com.ehms.vos;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class WardVO {
	private Long wardId;
	private Long floorId;
	private String wardName;
	private Long wardTypeId;
	private Long bedCategoryId;
	private Double bedChargesFullDay;
	private Double bedChargesHourly;
	private Long customerId;
	private Long hospitalId;
	private Long noOfBeds;
	private List<BedVO> bedVO = new ArrayList<>();

	public WardVO(Long wardId, Long floorId, String wardName, Long wardTypeId, Long noOfBeds, Long customerId, Long hospitalId) {
		this.wardId = wardId;
		this.floorId = floorId;
		this.wardName = wardName;
		this.wardTypeId = wardTypeId;
		this.noOfBeds = noOfBeds;
		this.customerId = customerId;
		this.hospitalId = hospitalId;
	}

	public WardVO(Long wardId, String wardName) {
		super();
		this.wardId = wardId;
		this.wardName = wardName;
	}

	public void addBed(WardBedFlatVO wardBedVO) {
		BedVO bedVO = new BedVO();
		bedVO.setBedId(wardBedVO.getBedId());
		bedVO.setBedCode(wardBedVO.getBedCode());
		this.bedVO.add(bedVO);
	}
}
