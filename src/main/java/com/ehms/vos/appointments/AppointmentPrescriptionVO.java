package com.ehms.vos.appointments;

import com.ehms.model.AppointmentPrescription;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AppointmentPrescriptionVO {

	private Long prescriptionId;
	private Long drugMasterId;
	private String route;
	private String quantity;
	private Integer duration;
	private String durationUnit;
	private Long appointmentId;

	public AppointmentPrescriptionVO(AppointmentPrescription appointmentPrescription) {
		this.prescriptionId = appointmentPrescription.getPrescriptionId();
		this.drugMasterId = appointmentPrescription.getDrugMasterId();
		this.route = appointmentPrescription.getRoute();
		this.quantity = appointmentPrescription.getQuantity();
		this.duration = appointmentPrescription.getDuration();
		this.durationUnit = appointmentPrescription.getDurationUnit();
		this.appointmentId = appointmentPrescription.getAppointmentId();
	}
}
