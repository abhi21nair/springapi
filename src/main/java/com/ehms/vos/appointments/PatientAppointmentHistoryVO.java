package com.ehms.vos.appointments;

import java.util.ArrayList;
import java.util.List;

import com.ehms.vos.DailyProgressNoteVO;
import com.ehms.vos.DietMasterVO;
import com.ehms.vos.DischargeSheetVO;
import com.ehms.vos.PatientPostmortemVO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class PatientAppointmentHistoryVO {
	private Long appointmentId;
	private String reportTime;
	private String appointmentStartTime;
	private String appointmentEndTime;
	private Long doctorId;
	private Long titleId;
	private String firstName;
	private String middleName;
	private String lastName;
	private Long moduleId;
	private List<AppointmentComplaintVO> appointmentComplaints = new ArrayList<>();
	private List<AppointmentExaminationVO> appointmentExaminations = new ArrayList<>();
	private List<AppointmentPrescriptionVO> appointmentPrescriptions = new ArrayList<>();
	private List<AppointmentReferralVO> appointmentReferrals = new ArrayList<>();
	private List<AppointmentVariousServiceVO> appointmentVariousServices = new ArrayList<>();
	private List<DailyProgressNoteVO> dailyProgressNotes = new ArrayList<>();
	private List<DietMasterVO> dietMasters = new ArrayList<>();
	private List<DischargeSheetVO> dischargeSheets = new ArrayList<>();
	private List<PatientPostmortemVO> patientPostmortems = new ArrayList<>();

	public PatientAppointmentHistoryVO(Long appointmentId, String reportTime, String appointmentStartTime, String appointmentEndTime, Long doctorId, Long titleId, String firstName, String middleName,
			String lastName, Long moduleId) {
		super();
		this.appointmentId = appointmentId;
		this.reportTime = reportTime;
		this.appointmentStartTime = appointmentStartTime;
		this.appointmentEndTime = appointmentEndTime;
		this.doctorId = doctorId;
		this.titleId = titleId;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.moduleId = moduleId;
	}

	public void addAppointmentComplaint(AppointmentComplaintVO appointmentComplaintVO) {
		if (this.appointmentComplaints == null) {
			this.appointmentComplaints = new ArrayList<>();
		}
		this.appointmentComplaints.add(appointmentComplaintVO);
	}

	public void addAppointmentExamination(AppointmentExaminationVO appointmentExaminationVO) {
		if (this.appointmentExaminations == null) {
			this.appointmentExaminations = new ArrayList<>();
		}
		this.appointmentExaminations.add(appointmentExaminationVO);
	}

	public void addAppointmentPrescription(AppointmentPrescriptionVO appointmentPrescriptionVO) {
		if (this.appointmentPrescriptions == null) {
			this.appointmentPrescriptions = new ArrayList<>();
		}
		this.appointmentPrescriptions.add(appointmentPrescriptionVO);
	}

	public void addAppointmentReferral(AppointmentReferralVO appointmentReferralVO) {
		if (this.appointmentReferrals == null) {
			this.appointmentReferrals = new ArrayList<>();
		}
		this.appointmentReferrals.add(appointmentReferralVO);
	}

	public void addAppointmentVariousService(AppointmentVariousServiceVO appointmentVariousServiceVO) {
		if (this.appointmentVariousServices == null) {
			this.appointmentVariousServices = new ArrayList<>();
		}
		this.appointmentVariousServices.add(appointmentVariousServiceVO);
	}

	public void addAppointmentDailyProgressNotes(DailyProgressNoteVO dailyProgressNoteVO) {
		if (this.dailyProgressNotes == null) {
			this.dailyProgressNotes = new ArrayList<>();
		}
		this.dailyProgressNotes.add(dailyProgressNoteVO);
	}

	public void addAppointmentDietMaster(DietMasterVO dietMasterVO) {
		if (this.dietMasters == null) {
			this.dietMasters = new ArrayList<>();
		}
		this.dietMasters.add(dietMasterVO);
	}

	public void addAppointmentDischargeSheet(DischargeSheetVO dischargeSheetVO) {
		if (this.dischargeSheets == null) {
			this.dischargeSheets = new ArrayList<>();
		}
		this.dischargeSheets.add(dischargeSheetVO);
	}

	public void addAppointmentPatientPostmortem(PatientPostmortemVO patientPostmortemVO) {
		if (this.patientPostmortems == null) {
			this.patientPostmortems = new ArrayList<>();
		}
		this.patientPostmortems.add(patientPostmortemVO);
	}
}
