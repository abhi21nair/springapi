package com.ehms.vos.appointments;

import com.ehms.model.AppointmentComplaint;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AppointmentComplaintVO {
	private Long complaintId;
	private String complaints;
	private String diagnosis;
	private String examinationPulse;
	private String respiration;
	private String temperature;
	private String height;
	private String weight;
	private String bloodPressure;
	private String generalNotes;
	private Long pathologyTestMasterId;
	private String labNotes;
	private Long appointmentId;

	public AppointmentComplaintVO(AppointmentComplaint appointmentComplaint) {
		this.complaintId = appointmentComplaint.getComplaintId();
		this.complaints = appointmentComplaint.getComplaints();
		this.diagnosis = appointmentComplaint.getDiagnosis();
		this.examinationPulse = appointmentComplaint.getExaminationPulse();
		this.respiration = appointmentComplaint.getRespiration();
		this.temperature = appointmentComplaint.getTemperature();
		this.height = appointmentComplaint.getHeight();
		this.weight = appointmentComplaint.getWeight();
		this.bloodPressure = appointmentComplaint.getBloodPressure();
		this.generalNotes = appointmentComplaint.getGeneralNotes();
		this.pathologyTestMasterId = appointmentComplaint.getPathologyTestMasterId();
		this.labNotes = appointmentComplaint.getLabNotes();
		this.appointmentId = appointmentComplaint.getAppointmentId();
	}
}
