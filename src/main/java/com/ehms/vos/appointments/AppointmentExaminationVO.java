package com.ehms.vos.appointments;

import com.ehms.model.AppointmentExamination;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AppointmentExaminationVO {

	private Long examinationId;
	private String localExamination;
	private String centralNervousSystem;
	private String respiratorySystem;
	private String cardioVascularSystem;
	private String abdominalSystem;
	private String obgGynicExamination;
	private Long appointmentId;

	public AppointmentExaminationVO(AppointmentExamination appointmentExamination) {
		this.examinationId = appointmentExamination.getExaminationId();
		this.localExamination = appointmentExamination.getLocalExamination();
		this.centralNervousSystem = appointmentExamination.getCentralNervousSystem();
		this.respiratorySystem = appointmentExamination.getRespiratorySystem();
		this.cardioVascularSystem = appointmentExamination.getCardioVascularSystem();
		this.abdominalSystem = appointmentExamination.getAbdominalSystem();
		this.obgGynicExamination = appointmentExamination.getObgGynicExamination();
		this.appointmentId = appointmentExamination.getAppointmentId();
	}
}
