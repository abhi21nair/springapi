package com.ehms.vos.appointments;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AppointmentHistoryVO {

	private List<AppointmentComplaintVO> appointmentComplaintVO;
	private List<AppointmentExaminationVO> appointmentExaminationVO;
	private List<AppointmentPrescriptionVO> appointmentPrescriptionVO;
	private List<AppointmentReferralVO> appointmentReferralVO;
	private List<AppointmentVariousServiceVO> appointmentVariousServiceVO;
}
