package com.ehms.vos.appointments;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AppointmentVO {

	private Long appointmentId;
	private Long customerId;
	private Long hospitalId;
	private Long moduleId;
	private Long doctorId;
	private Long patientId;
	private Long patientTitleId;
	private String patientfirstName;
	private String patientMiddleName;
	private String patientLastName;
	private String patientMobileNumber;
	private String patientCode;
	private Long docScheduleId;
	private Long timeSpent;
	private String fromTime;
	private String toTime;
	private Integer status;
	private String reportTime;
	private String appointmentStartTime;
	private String appointmentEndTime;
	private Long bedId;
	private Long departmentId;

	public AppointmentVO(Long appointmentId, Long moduleId, Long docScheduleId, String reportTime) {
		this.appointmentId = appointmentId;
		this.moduleId = moduleId;
		this.docScheduleId = docScheduleId;
		this.reportTime = reportTime;
	}

	public AppointmentVO(Long appointmentId, Long customerId, Long hospitalId, Long moduleId, Long doctorId, Long patientId, Long patientTitleId, String patientfirstName,
			String patientMiddleName, String patientLastName, String patientMobileNumber, String patientCode, Long docScheduleId, Long timeSpent, String fromTime, String toTime,
			Integer status, String reportTime, String appointmentStartTime, String appointmentEndTime) {
		super();
		this.appointmentId = appointmentId;
		this.customerId = customerId;
		this.hospitalId = hospitalId;
		this.moduleId = moduleId;
		this.doctorId = doctorId;
		this.patientId = patientId;
		this.patientTitleId = patientTitleId;
		this.patientfirstName = patientfirstName;
		this.patientMiddleName = patientMiddleName;
		this.patientLastName = patientLastName;
		this.patientMobileNumber = patientMobileNumber;
		this.patientCode = patientCode;
		this.docScheduleId = docScheduleId;
		this.timeSpent = timeSpent;
		this.fromTime = fromTime;
		this.toTime = toTime;
		this.status = status;
		this.reportTime = reportTime;
		this.appointmentStartTime = appointmentStartTime;
		this.appointmentEndTime = appointmentEndTime;
	}

}
