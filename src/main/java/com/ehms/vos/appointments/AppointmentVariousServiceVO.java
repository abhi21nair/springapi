package com.ehms.vos.appointments;

import com.ehms.model.AppointmentVariousService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AppointmentVariousServiceVO {

	private Long variousServiceId;
	private Long tarrifWiseServiceId;
	private Double quantity;
	private Double totalAmount;
	private Long appointmentId;

	public AppointmentVariousServiceVO(AppointmentVariousService appointmentVariousService) {
		this.variousServiceId = appointmentVariousService.getVariousServiceId();
		this.tarrifWiseServiceId = appointmentVariousService.getTarrifWiseServiceId();
		this.quantity = appointmentVariousService.getQuantity();
		this.totalAmount = appointmentVariousService.getTotalAmount();
		this.appointmentId = appointmentVariousService.getAppointmentId();
	}
}
