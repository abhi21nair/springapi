package com.ehms.vos.appointments;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AppointmentStatsDBVO {
	
	private Long moduleId;
	private Long doctorScheduleId;
	private Long noOfPatients;
	private Long noOfReportedPatients;

}
