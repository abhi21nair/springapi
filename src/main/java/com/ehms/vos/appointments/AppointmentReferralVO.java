package com.ehms.vos.appointments;

import com.ehms.model.AppointmentReferral;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AppointmentReferralVO {

	private Long referralId;
	private Long departmentId;
	private String provisionalNotes;
	private Boolean mlcCase;
	private String instructionToWarden;
	private String generalNotes;
	private Long appointmentId;

	public AppointmentReferralVO(AppointmentReferral appointmentReferral) {
		this.referralId = appointmentReferral.getReferralId();
		this.departmentId = appointmentReferral.getDepartmentId();
		this.provisionalNotes = appointmentReferral.getProvisionalNotes();
		this.mlcCase = appointmentReferral.getMlcCase();
		this.instructionToWarden = appointmentReferral.getInstructionToWarden();
		this.generalNotes = appointmentReferral.getGeneralNotes();
		this.appointmentId = appointmentReferral.getAppointmentId();
	}
}
