package com.ehms.vos.appointments;

import com.ehms.vos.DocScheduleVO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AppointmentStatsVO {

	private Long moduleId;
	private DocScheduleVO doctorSchedule;
	private Long totalAppointments = 0l;
	private Long reportedPatients = 0l;
	private Boolean isDoctorAppointmentStats;

	public AppointmentStatsVO(Long moduleId) {
		this.moduleId = moduleId;
		this.isDoctorAppointmentStats = false;
	}

	public AppointmentStatsVO(DocScheduleVO doctorSchedule) {
		this.doctorSchedule = doctorSchedule;
		this.isDoctorAppointmentStats = true;
	}
	
	public void incrementTotalAppointments() {
		this.totalAppointments++;
	}
	
	public void incrementReportedPatients() {
		this.reportedPatients++;
	}
}
