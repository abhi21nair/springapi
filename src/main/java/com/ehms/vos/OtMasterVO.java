package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class OtMasterVO {
	private Long otMasterId;
	private String otName;
	private Long otTypeId;
	private Long otNo;
	private String otLocation;
	private Double otCharges;
	private Boolean isActive;
	private Long customerId;
	private Long hospitalId;

	public OtMasterVO(Long otMasterId, String otName, Long otTypeId, String otLocation, Boolean isActive) {
		this.otName = otName;
		this.otTypeId = otTypeId;
		this.otLocation = otLocation;
		this.isActive = isActive;
		this.otMasterId = otMasterId;
	}
}
