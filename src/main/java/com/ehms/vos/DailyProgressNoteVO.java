package com.ehms.vos;

import com.ehms.model.DailyProgressNote;
import com.ehms.utils.DateTimeUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class DailyProgressNoteVO {
	private Long dailyProgressNoteId;
	private Long appointmentId;
	private Long patientId;
	private String progressDate;
	private String doctorDayProgressNotes;
	private String doctorNightProgressNotes;
	private String previousNotes;

	public DailyProgressNoteVO(DailyProgressNote dailyProgressNote) {
		this.dailyProgressNoteId = dailyProgressNote.getDailyProgressNoteId();
		this.appointmentId = dailyProgressNote.getAppointmentId();
		this.patientId = dailyProgressNote.getPatientId();
		this.progressDate = DateTimeUtil.formatDate(dailyProgressNote.getProgressDate());
		this.doctorDayProgressNotes = dailyProgressNote.getDoctorDayProgressNotes();
		this.doctorNightProgressNotes = dailyProgressNote.getDoctorNightProgressNotes();
		this.previousNotes = dailyProgressNote.getPreviousNotes();
	}
}
