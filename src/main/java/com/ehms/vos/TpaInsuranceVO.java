package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class TpaInsuranceVO {

	private Long tpaInsuranceId;
	private AddressVO address;
	private String countryCodeForKeyContactPersonNumber;
	private String countryCodeForMobileNumber;
	private String countryCodeForPhoneNumber;
	private String effectiveFromDate;
	private String effectiveToDate;
	private String keyContactPerson;
	private String keyContactPersonNumber;
	private String mobileNumber;
	private String phoneNumber;
	private String tpaInsuranceName;
	private String tpaInsuranceNumber;
	private Long tpaInsuranceClaimTypeId;
	private Long tpaInsuranceTypeId;
	private Long customerId;
	private Long hospitalId;

	public TpaInsuranceVO(Long tpaInsuranceId, String tpaInsuranceName, Long tpaInsuranceClaimTypeId,
			Long tpaInsuranceTypeId, String effectiveFromDate, String effectiveToDate) {
		super();
		this.tpaInsuranceId = tpaInsuranceId;
		this.tpaInsuranceName = tpaInsuranceName;
		this.tpaInsuranceClaimTypeId = tpaInsuranceClaimTypeId;
		this.tpaInsuranceTypeId = tpaInsuranceTypeId;
		this.effectiveFromDate = effectiveFromDate;
		this.effectiveToDate = effectiveToDate;

	}
}
