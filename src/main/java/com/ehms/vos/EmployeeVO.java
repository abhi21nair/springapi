package com.ehms.vos;

import java.math.BigDecimal;

import com.ehms.model.Employee;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class EmployeeVO {

	public EmployeeVO(Long employeeId, String firstName, String lastName, String employeeCode, Long designationId, Long departmentId, Long specializationId, String countryCodeForMobileNumber,
			String mobileNumber, String email) {
		this.employeeId = employeeId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.employeeCode = employeeCode;
		this.designationId = designationId;
		this.departmentId = departmentId;
		this.specializationId = specializationId;
		this.countryCodeForMobileNumber = countryCodeForMobileNumber;
		this.mobileNumber = mobileNumber;
		this.email = email;
	}

	public EmployeeVO(Long employeeId, String firstName, String middleName, String lastName, String employeeCode) {
		this.employeeId = employeeId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		this.employeeCode = employeeCode;
	}

	public EmployeeVO(Long employeeId, Long titleId, String firstName, String middleName, String lastName, String employeeCode) {
		super();
		this.employeeId = employeeId;
		this.titleId = titleId;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.employeeCode = employeeCode;
	}

	public EmployeeVO(Employee employee) {
		this.employeeId = employee.getEmployeeId();
		this.titleId = employee.getTitle().getTitleId();
		this.firstName = employee.getFirstName();
		this.middleName = employee.getMiddleName();
		this.lastName = employee.getLastName();
		this.employeeCode = employee.getEmployeeCode();
	}

	private Long employeeId;
	private Long titleId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String employeeCode;
	private Integer employeeType;
	private Long designationId;
	private String designation;
	private Long departmentId;
	private String birthDate;
	private Integer birthYear;
	private Boolean isDOBAvailable;
	private String joiningDate;
	private String gender;
	private String maritalStatus;
	private String nationality;
	private String adharNo;
	private Long otherIdTypeId;
	private String otherIdNo;
	private String otherIdValidTill;
	private String panNo;
	private String tanNo;
	private String qualification;
	private Long specializationId;
	private Long joiningTypeId;
	private BigDecimal hospitalSharePc;
	private BigDecimal hospitalShareRs;
	private BigDecimal doctorSharePc;
	private BigDecimal doctorShareRs;
	private AddressVO currentAddress;
	private AddressVO permanentAddress;
	private String email;
	private String countryCodeForContactNumber;
	private String contactNumber;
	private String countryCodeForMobileNumber;
	private String mobileNumber;
	private String nomineeName;
	private String nomineeRelation;
	private Long customerId;
	private Long hospitalId;
	private Boolean isActive;
}
