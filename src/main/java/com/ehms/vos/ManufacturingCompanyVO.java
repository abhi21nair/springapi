package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ManufacturingCompanyVO {
	private Long manufacturingCompanyId;
	private String ccForKeyContactPersonNumber;
	private String countryCodeForMobileNumber;
	private String countryCodeForPhoneNumber;
	private String dlNo;
	private String email;
	private String gstNo;
	private Boolean isActive;
	private String keyContactPerson;
	private String keyContactPersonNumber;
	private String manufactureName;
	private String manufactureNumber;
	private String mobileNumber;
	private Double openingBalance;
	private String panNo;
	private String phoneNumber;
	private String shopActNo;
	private String tanNo;
	private String vendorSupplierNo;
	private AddressVO address;
	private Long customerId;
	private Long hospitalId;
	private String cityName;

	public ManufacturingCompanyVO(Long manufacturingCompanyId, String manufactureName, String manufactureNumber,
			String mobileNumber, String vendorSupplierNo, String cityName) {
		super();
		this.manufacturingCompanyId = manufacturingCompanyId;
		this.manufactureName = manufactureName;
		this.manufactureNumber = manufactureNumber;
		this.mobileNumber = mobileNumber;
		this.vendorSupplierNo = vendorSupplierNo;
		this.cityName = cityName;
	}

}
