package com.ehms.vos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class ResponseVO {
	
	public enum Status {SUCCESS, FAILURE};
	
	private Status status;
	private String message;

}
