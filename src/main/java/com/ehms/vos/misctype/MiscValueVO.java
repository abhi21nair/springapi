package com.ehms.vos.misctype;

import com.ehms.model.MiscValue;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class MiscValueVO {

	public MiscValueVO(MiscValue miscValue) {
		this.miscTypeValueId = miscValue.getMiscTypeValueId();
		this.miscValue = miscValue.getMiscValue();
		this.customerId = miscValue.getCustomerId();
		this.hospitalId = miscValue.getHospitalId();
		this.miscTypeId = miscValue.getMiscTypeId();
	}

	private Long miscTypeValueId;
	private String miscValue;
	private Long customerId;
	private Long hospitalId;
	private Long miscTypeId;

}