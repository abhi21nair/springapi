package com.ehms.vos.misctype;

import java.util.List;

import com.ehms.model.MiscType;
import com.ehms.vos.misctype.MiscValueVO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class MiscTypeValuesVO {
	private MiscType miscType;
	private List<MiscValueVO> miscValues;

}