package com.ehms.vos;

import com.ehms.vos.user.UserVO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class HospitalVO {

	private Long hospitalId;
	private String hospitalName;
	private Boolean isActive;
	private AddressVO address;
	private UserVO keyUser;
	private Long customerId;
	private String countryCodeForContactNumber;
	private String countryCodeForPhoneNumber;
	private String contactNumber;
	private String phoneNumber;
	private String faxNumber;
	private String hospitalCode;

	public HospitalVO(Long hospitalId, String hospitalName, String hospitalCode, boolean isActive) {
		this.hospitalId = hospitalId;
		this.hospitalName = hospitalName;
		this.hospitalCode = hospitalCode;
		this.isActive = isActive;
	}
	
	public HospitalVO(Long hospitalId, String hospitalName, String hospitalCode) {
		this.hospitalId = hospitalId;
		this.hospitalName = hospitalName;
		this.hospitalCode = hospitalCode;
	}
}
