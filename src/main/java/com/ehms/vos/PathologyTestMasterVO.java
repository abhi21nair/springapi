package com.ehms.vos;

import com.ehms.model.PathologyTestMaster;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class PathologyTestMasterVO {

	private Long pathologyTestMasterId;
	private String testName;
	private String subTestName;
	private Long labId;
	private String labName;
	private Long normalValue;
	private Long valueTypeId;
	private Integer genderWiseValue;
	private String unit;
	private Long specimenTypeId;
	private Long containerTypeId;
	private Long resultTypeId;
	private Boolean isActive;
	private Long customerId;
	private Long hospitalId;

	public PathologyTestMasterVO(Long pathologyTestMasterId, String testName, String subTestName, Long labId, String labName, Long specimenTypeId, Boolean isActive) {
		this.pathologyTestMasterId = pathologyTestMasterId;
		this.testName = testName;
		this.subTestName = subTestName;
		this.labId = labId;
		this.labName = labName;
		this.specimenTypeId = specimenTypeId;
		this.isActive = isActive;
	}

	public PathologyTestMasterVO(PathologyTestMaster pathologyTestMaster) {
		this.pathologyTestMasterId = pathologyTestMaster.getPathologyTestMasterId();
		this.testName = pathologyTestMaster.getTestName();
		this.subTestName = pathologyTestMaster.getSubTestName();
		if (pathologyTestMaster.getLab() != null) {
			this.labId = pathologyTestMaster.getLab().getDepartmentId();
			this.labName = pathologyTestMaster.getLab().getDepartmentName();
		}
		this.normalValue = pathologyTestMaster.getNormalValue();
		this.valueTypeId = pathologyTestMaster.getValueTypeId();
		this.genderWiseValue = pathologyTestMaster.getGenderWiseValue();
		this.unit = pathologyTestMaster.getUnit();
		this.specimenTypeId = pathologyTestMaster.getSpecimenTypeId();
		this.containerTypeId = pathologyTestMaster.getContainerTypeId();
		this.resultTypeId = pathologyTestMaster.getResultTypeId();
		this.isActive = pathologyTestMaster.getIsActive();
		if (pathologyTestMaster.getCustomer() != null) {
			this.customerId = pathologyTestMaster.getCustomer().getCustomerId();
		}
		if (pathologyTestMaster.getHospital() != null) {
			this.hospitalId = pathologyTestMaster.getHospital().getHospitalId();
		}
	}

}
