package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatientVO {

	private Long patientId;
	private String patientCode;
	private Long titleId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String birthDate;
	private Integer birthYear;
	private Boolean isDobAvailable;
	private String gender;
	private Long cityId;
	private String maritalStatus;
	private AddressVO currentAddress;
	private AddressVO permanentAddress;
	private String contactNumber;
	private String mobileNumber;
	private String nationality;
	private String passportNumber;
	private String validTill;
	private String adharNo;
	private String referenceDoctor;
	private Long paymentModeId;
	private String paymentDetails;
	private String relativeName;
	private String relation;
	private String religion;
	private String allergy;
	private String symptoms;
	private Long sponsorCorporateId;
	private Long nomineeTypeId;
	private String nomineeName;
	private Long customerId;
	private Long hospitalId;

	public PatientVO(Long patientId, Long titleId, String firstName, String lastName, String gender, Long cityId, String mobileNumber) {
		this.patientId = patientId;
		this.titleId = titleId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.cityId = cityId;
		this.mobileNumber = mobileNumber;
	}
}
