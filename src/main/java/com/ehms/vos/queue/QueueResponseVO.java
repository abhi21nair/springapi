package com.ehms.vos.queue;

import com.ehms.vos.PatientVO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class QueueResponseVO {

	private Long appointmentId;
	private Long customerId;
	private Long hospitalId;
	private Long moduleId;
	private Long doctorId;
	private Long timeSpent;
	private String fromTime;
	private String toTime;
	private Integer status;
	private String reportTime;
	private String appointmentStartTime;
	private String appointmentEndTime;
	private PatientVO patient;
	private Long bedId;
	private String bedCode;
	private Long wardId;
	private String wardName;
	private Long departmantId;

	public QueueResponseVO(Long appointmentId, Long customerId, Long hospitalId, Long moduleId, Long doctorId, Long timeSpent, String fromTime, String toTime, Integer status, String reportTime,
			String appointmentStartTime, String appointmentEndTime, Long patientId, Long titleId, String patientCode, String firstName, String middleName, String lastName, String mobileNumber,
			String allergies, String birthDate, Integer birthYear, Boolean isDobAvailable, Long bedId, String bedCode, Long wardId, String wardName, Long departmentId) {
		super();
		this.appointmentId = appointmentId;
		this.customerId = customerId;
		this.hospitalId = hospitalId;
		this.moduleId = moduleId;
		this.doctorId = doctorId;
		this.timeSpent = timeSpent;
		this.fromTime = fromTime;
		this.toTime = toTime;
		this.status = status;
		this.reportTime = reportTime;
		this.appointmentStartTime = appointmentStartTime;
		this.appointmentEndTime = appointmentEndTime;
		this.patient = new PatientVO();
		this.patient.setPatientId(patientId);
		this.patient.setTitleId(titleId);
		this.patient.setPatientCode(patientCode);
		this.patient.setFirstName(firstName);
		this.patient.setMiddleName(middleName);
		this.patient.setLastName(lastName);
		this.patient.setMobileNumber(mobileNumber);
		this.patient.setAllergy(allergies);
		this.patient.setBirthDate(birthDate);
		this.patient.setBirthYear(birthYear);
		this.patient.setIsDobAvailable(isDobAvailable);
		this.bedId = bedId;
		this.bedCode = bedCode;
		this.wardId = wardId;
		this.wardName = wardName;
		this.departmantId = departmentId;
	}
}
