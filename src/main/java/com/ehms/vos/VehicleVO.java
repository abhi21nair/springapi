package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class VehicleVO {
	private Long vehicleId;
	private Long averagePerKm;
	private String chasisNumber;
	private Double fuelCapacity;
	private Integer gpsEnabled;
	private String insuranceNumber;
	private String insuranceNumberValidity;
	private Boolean isActive;
	private String rtoRegistrationNumber;
	private String rtoRegistrationNumberValidity;
	private Integer typeOfFuel;
	private Integer vehicleColor;
	private Integer vehicleManufacturer;
	private String vehicleName;
	private String vehicleNumber;
	private Integer yearOfModel;
	private Long vehicleTypeId;
	private Long customerId;
	private Long hospitalId;

	public VehicleVO(Long vehicleId, String insuranceNumberValidity, String rtoRegistrationNumberValidity,
			int typeOfFuel, String vehicleName, String vehicleNumber, int yearOfModel, Long vehicleTypeId,
			boolean isActive) {
		super();
		this.vehicleId = vehicleId;
		this.insuranceNumberValidity = insuranceNumberValidity;
		this.rtoRegistrationNumberValidity = rtoRegistrationNumberValidity;
		this.typeOfFuel = typeOfFuel;
		this.vehicleName = vehicleName;
		this.vehicleNumber = vehicleNumber;
		this.yearOfModel = yearOfModel;
		this.vehicleTypeId = vehicleTypeId;
		this.isActive = isActive;
	}

	public VehicleVO(Long vehicleId, String vehicleName, String vehicleNumber) {
		super();
		this.vehicleId = vehicleId;
		this.vehicleName = vehicleName;
		this.vehicleNumber = vehicleNumber;
	}
}
