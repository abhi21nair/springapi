package com.ehms.vos;

import com.ehms.model.PatientPostmortem;
import com.ehms.utils.DateTimeUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class PatientPostmortemVO {
	private Long patientPostmortemId;
	private Long appointmentId;
	private Long patientId;
	private String deathDate;
	private String causeOfDeath;
	private int deadBodyHanded;
	private String deadBodyHandedTo;
	private String morgueTime;
	private String postmortemTime;

	public PatientPostmortemVO(PatientPostmortem patientPostmortem) {
		this.patientPostmortemId = patientPostmortem.getPatientPostmortemId();
		this.appointmentId = patientPostmortem.getAppointmentId();
		this.patientId = patientPostmortem.getPatientId();
		if (patientPostmortem.getDeathDate() != null) {
			this.deathDate = DateTimeUtil.formatDate(patientPostmortem.getDeathDate());
		}
		this.causeOfDeath = patientPostmortem.getCauseOfDeath();
		this.deadBodyHanded = patientPostmortem.getDeadBodyHanded();
		this.deadBodyHandedTo = patientPostmortem.getDeadBodyHandedTo();
		if (patientPostmortem.getMorgueTime() != null) {
			this.morgueTime = DateTimeUtil.formatTime(patientPostmortem.getMorgueTime());
		}
		if (patientPostmortem.getPostmortemTime() != null) {
			this.postmortemTime = DateTimeUtil.formatTime(patientPostmortem.getPostmortemTime());
		}
	}
}
