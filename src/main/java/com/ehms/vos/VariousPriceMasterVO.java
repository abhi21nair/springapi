package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class VariousPriceMasterVO {
	private Long variousPriceMasterId;
	private Long priceMasterServiceNameId;
	private String priceMasterServiceName;
	private Long departmentId;
	private Long priceMasterGroupTypeId;
	private Long priceMasterSubGroupTypeId;
	private Double normalServiceRate;
	private Double corporateRateInPercent;
	private Double corporateRateInRuppes;
	private Double cghsRateInPercent;
	private Double cghsRateInRuppes;
	private Double emergencyRateInPercent;
	private Double emergencyRateInRuppes;
	private Double otherRateInPercent;
	private Double otherRateInRuppes;
	private Boolean isActive;
	private Long customerId;
	private Long hospitalId;

	public VariousPriceMasterVO(Long variousPriceMasterId, Long priceMasterServiceNameId, String priceMasterServiceName,
			Long departmentId,Long priceMasterGroupTypeId,Long priceMasterSubGroupTypeId , Double normalServiceRate, Boolean isActive) {
		this.variousPriceMasterId = variousPriceMasterId;
		this.priceMasterServiceNameId = priceMasterServiceNameId;
		this.priceMasterServiceName = priceMasterServiceName;
		this.departmentId = departmentId;
		this.priceMasterGroupTypeId = priceMasterGroupTypeId;
		this.priceMasterSubGroupTypeId = priceMasterSubGroupTypeId;
		this.isActive = isActive;
		this.normalServiceRate=normalServiceRate;
	}
}
