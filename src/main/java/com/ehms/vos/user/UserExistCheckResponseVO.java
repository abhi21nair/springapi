package com.ehms.vos.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
public class UserExistCheckResponseVO {

    private boolean exist;

}
