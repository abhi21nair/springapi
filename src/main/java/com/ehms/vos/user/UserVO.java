package com.ehms.vos.user;

import java.util.List;

import com.ehms.vos.EmployeeVO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class UserVO {

	public UserVO(Long userId, String userName, Boolean isActive, Long employeeId, String firstName, String middleName, String lastName) {
		this.userId = userId;
		this.userName = userName;
		this.isActive = isActive;
		if ((firstName != null && !firstName.isEmpty()) || (lastName != null && !lastName.isEmpty())) {
			employee = new EmployeeVO();
			employee.setEmployeeId(employeeId);
			employee.setFirstName(firstName);
			employee.setMiddleName(middleName);
			employee.setLastName(lastName);
		}
	}
	
	public UserVO(Long userId, String userName, Boolean isActive) {
		this.userId = userId;
		this.userName = userName;
		this.isActive = isActive;
	}

	private Long userId;
	private String userName;
	private String password;
	private Boolean isActive;
	private List<Long> rolesIds;
	private EmployeeVO employee;
	private Boolean isSystemUser;

}
