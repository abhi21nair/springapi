package com.ehms.vos;

import com.ehms.model.Department;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class DepartmentVO {

	public DepartmentVO(Department department) {
		this.departmentId = department.getDepartmentId();
		this.departmentName = department.getDepartmentName();
		this.isActive = department.getIsActive();
		if (department.getCustomer() != null) {
			this.customerId = department.getCustomer().getCustomerId();
		}
		if (department.getHospital() != null) {
			this.hospitalId = department.getHospital().getHospitalId();
		}
		this.departmentDescription = department.getDepartmentDescription();
		this.departmentType = department.getDepartmentType();
		this.countryCodeForDepartmentPhoneNumber = department.getCountryCodeForDepartmentPhoneNumber();
		this.departmentPhoneNumber = department.getDepartmentPhoneNumber();
	}

	public DepartmentVO(Long departmentId, String departmentName, boolean isActive, Long customerId, String departmentType) {
		this.departmentId = departmentId;
		this.departmentName = departmentName;
		this.isActive = isActive;
		this.customerId = customerId;
		this.departmentType = departmentType;
	}

	public DepartmentVO(Long departmentId, String departmentName, boolean isActive, Long customerId, Long hospitalId, String departmentType) {
		this.departmentId = departmentId;
		this.departmentName = departmentName;
		this.isActive = isActive;
		this.customerId = customerId;
		this.hospitalId = hospitalId;
		this.departmentType = departmentType;
	}

	private Long departmentId;
	private String departmentName;
	private Boolean isActive = true;
	private Long customerId;
	private Long hospitalId;
	private String departmentDescription;
	private String departmentType;
	private String countryCodeForDepartmentPhoneNumber;
	private String departmentPhoneNumber;

}
