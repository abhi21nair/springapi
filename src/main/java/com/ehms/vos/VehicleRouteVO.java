package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class VehicleRouteVO {

	private Long vehicleRouteId;
	private String destination;
	private Integer gpsBasedRoute;
	private String route;
	private Long routeKm;
	private String routeValidityFrom;
	private String routeValidityTo;
	private String source;
	private Long vehicleId;
	private String vehicleName;
	private String vehicleNumber;
	private Boolean isActive;
	private Long customerId;
	private Long hospitalId;

	public VehicleRouteVO(Long vehicleRouteId, String destination, String route, Long routeKm, String source,
			Long vehicleId, String vehicleName, String vehicleNumber) {
		super();
		this.vehicleRouteId = vehicleRouteId;
		this.destination = destination;
		this.route = route;
		this.routeKm = routeKm;
		this.source = source;
		this.vehicleId = vehicleId;
		this.vehicleName = vehicleName;
		this.vehicleNumber = vehicleNumber;
	}
}
