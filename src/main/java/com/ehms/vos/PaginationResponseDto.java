package com.ehms.vos;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PaginationResponseDto<T> {

    private Integer page;
    private Integer count;
    private Long totalItems;
    private Integer totalPages;
    private List<T> items;

}