package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class DocScheduleVO {
	private Long doctorScheduleId;
	private Long doctorId;
	private Long doctorTitleId;
	private Long doctorDesignationId;
	private String doctorFirstName;
	private String doctorMiddleName;
	private String doctorLastName;
	private int day;
	private String timeFrom;
	private String timeTo;
	private Long appointmentTypeId;
	private int noOfAppointment;
	private Long doctorShiftId;
	private Double doctorFees;

	public DocScheduleVO(Long doctorScheduleId, Long doctorId, String doctorFirstName, String doctorMiddleName, String doctorLastName, int day, Long appointmentTypeId,
			String timeFrom, String timeTo) {
		this.doctorScheduleId = doctorScheduleId;
		this.doctorFirstName = doctorFirstName;
		this.doctorMiddleName = doctorMiddleName;
		this.doctorLastName = doctorLastName;
		this.day = day;
		this.doctorId = doctorId;
		this.appointmentTypeId = appointmentTypeId;
		this.timeFrom = timeFrom;
		this.timeTo = timeTo;
	}

	public DocScheduleVO(Long doctorScheduleId, Long doctorId, Long doctorTitleId, String doctorFirstName, String doctorMiddleName, String doctorLastName, Long doctorDesignationId,
			int day, Long appointmentTypeId, String timeFrom, String timeTo, int noOfAppointment) {
		this.doctorScheduleId = doctorScheduleId;
		this.doctorId = doctorId;
		this.doctorTitleId = doctorTitleId;
		this.doctorFirstName = doctorFirstName;
		this.doctorMiddleName = doctorMiddleName;
		this.doctorLastName = doctorLastName;
		this.doctorDesignationId = doctorDesignationId;
		this.day = day;
		this.appointmentTypeId = appointmentTypeId;
		this.timeFrom = timeFrom;
		this.timeTo = timeTo;
		this.noOfAppointment = noOfAppointment;
	}
}
