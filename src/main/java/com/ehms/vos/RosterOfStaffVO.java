package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class RosterOfStaffVO {

	private Long rosterOfStaffId;
	private Long staffShiftId;
	private Integer day;
	private Long staffNameId;
	private String staffFirstName;
	private String staffMiddleName;
	private String staffLastName;
	private String staffTitleName;
	private String timeFrom;
	private String timeTo;
	private String dateFrom;
	private String dateTo;
	private Integer weeklyOff;
	private String remark;

	public RosterOfStaffVO(Long rosterOfStaffId, Long staffNameId, String staffFirstName, String staffMiddleName,
			String staffLastName, String staffTitleName,Long staffShiftId,
			String timeFrom, String timeTo, String dateFrom, String dateTo) {
		this.rosterOfStaffId = rosterOfStaffId;
		this.staffShiftId = staffShiftId;
		this.staffFirstName = staffFirstName;
		this.staffMiddleName = staffMiddleName;
		this.staffLastName = staffLastName;
		this.staffNameId = staffNameId;
		this.timeFrom = timeFrom;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.timeTo = timeTo;
		this.staffTitleName = staffTitleName;

	}
}
