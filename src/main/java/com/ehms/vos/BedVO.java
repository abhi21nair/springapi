package com.ehms.vos;

import com.ehms.model.Bed;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class BedVO {

	private Long bedId;
	private String bedCode;
	private Long wardId;
 
	public BedVO(Bed bed) {
		if (bed != null) {
			this.bedId = bed.getBedId();
			this.bedCode = bed.getBedCode();
		}
	}

	public BedVO(Long bedId, String bedCode) {
		this.bedId = bedId;
		this.bedCode = bedCode;
	}
}
