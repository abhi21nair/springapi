package com.ehms.vos;

import com.ehms.model.DischargeSheet;
import com.ehms.utils.DateTimeUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class DischargeSheetVO {
	private Long dischargeSheetId;
	private Long appointmentId;
	private Long patientId;
	private Long dischargeCoditionId;
	private String dischargeDate;
	private String deathDate;
	private String causeOfDeath;
	private String advice;
	private String nextAppointment;
	private String otherDetails;

	public DischargeSheetVO(DischargeSheet dischargeSheet) {
		this.dischargeSheetId = dischargeSheet.getDischargeSheetId();
		this.appointmentId = dischargeSheet.getAppointmentId();
		this.patientId = dischargeSheet.getPatientId();
		this.dischargeCoditionId = dischargeSheet.getDischargeCoditionId();
		if (dischargeSheet.getDischargeDate() != null) {
			this.dischargeDate = DateTimeUtil.formatDate(dischargeSheet.getDischargeDate());
		}
		if (dischargeSheet.getDeathDate() != null) {
			this.deathDate = DateTimeUtil.formatDate(dischargeSheet.getDeathDate());
		}
		this.causeOfDeath = dischargeSheet.getCauseOfDeath();
		this.advice = dischargeSheet.getAdvice();
		this.nextAppointment = dischargeSheet.getNextAppointment();
		this.otherDetails = dischargeSheet.getOtherDetails();
	}
}
