package com.ehms.vos.role;

import java.util.ArrayList;
import java.util.List;

import com.ehms.model.LinkRolePrivilege;
import com.ehms.model.Privilege;
import com.ehms.model.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class RoleVO {

	public RoleVO(Role role) {
		this.roleId = role.getRoleId();
		this.roleName = role.getRoleName();
		this.isActive = role.getIsActive();
		this.description = role.getDescription();
		if (role.getCustomer() != null) {
			this.customerId = role.getCustomer().getCustomerId();
		}
		if (role.getHospital() != null) {
			this.hospitalId = role.getHospital().getHospitalId();
		}
		if (role.getLinkRolePrivileges() != null && !role.getLinkRolePrivileges().isEmpty()) {
			this.privileges = new ArrayList<>();
			for (LinkRolePrivilege linkRolePrivilege : role.getLinkRolePrivileges()) {
				if (linkRolePrivilege != null && linkRolePrivilege.getPrivilege() != null) {
					this.privileges.add(new PrivilegeVO(linkRolePrivilege.getPrivilege()));
				}
			}
		}
	}
	
	public RoleVO (Long roleId, String roleName, String description, Boolean isActive, Long customerId, Long hospitalId) {
		this.roleId = roleId;
		this.roleName = roleName;
		this.description = description;
		this.isActive = isActive;
		this.customerId = customerId;
		this.hospitalId = hospitalId;
	}
	
	public RoleVO (Long roleId, String roleName) {
		this.roleId = roleId;
		this.roleName = roleName;
	}

	private Long roleId;
	private String roleName;
	private String description;
	private Boolean isActive;
	private Long customerId;
	private Long hospitalId;
	private List<PrivilegeVO> privileges;

	@JsonIgnore
	public Role getRoleEntity(Role role) {
		role.setDescription(this.description);
		role.setIsActive(this.isActive);
		role.setRoleName(this.roleName);
		role.setIsSystemRole(false);
		if (privileges != null && !privileges.isEmpty()) {
			List<LinkRolePrivilege> linkRolePrivileges = new ArrayList<>();
			LinkRolePrivilege linkRolePrivilege = null;
			for (PrivilegeVO privilegeVO : this.privileges) {
				linkRolePrivilege = new LinkRolePrivilege();
				Privilege privilege = new Privilege();
				privilege.setPrivilegeId(privilegeVO.getPrivilegeId());
				linkRolePrivilege.setPrivilege(privilege);
				linkRolePrivilege.setRole(role);
				linkRolePrivileges.add(linkRolePrivilege);				
			}
			role.setLinkRolePrivileges(linkRolePrivileges);
		}
		return role;
	}
}
