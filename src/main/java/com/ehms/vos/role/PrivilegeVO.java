package com.ehms.vos.role;

import com.ehms.model.Privilege;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PrivilegeVO {

	public PrivilegeVO(Privilege privilege) {
		this.privilegeId = privilege.getPrivilegeId();
		this.privilegeCode = privilege.getPrivilegeCode();
		this.description = privilege.getDescription();
		if (privilege.getModule() != null) {
			this.moduleId = privilege.getModule().getModuleId();
		}
	}

	private Long privilegeId;
	private String privilegeCode;
	private String description;
	private Long moduleId;

}
