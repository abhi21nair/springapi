package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class SalaryLeaveStructureVO {

	private Long salaryLeaveStructureId;
	private EmployeeVO employee;
	private Double basicSalary;
	private Double hra;
	private Double epf;
	private Double ta;
	private Double da;
	private Double tds;
	private Double incomeTax;
	private Double professionalTax;
	private Double personalLoan;
	private Double vehicalLoan;
	private Double houseLoan;
	private Double cca;
	private Double panelDeduction;
	private Double specialAllowance;
	private Double otherAllowance;
	private Double security;
	private Double arrears;
	private Double benefits;
	private Double bonus;
	private Double increments;
	private Double overTime;
	private String dateFrom;
	private String dateTo;

	public SalaryLeaveStructureVO(Long salaryLeaveStructureId, Long employeeId, String firstName, String middleName, String lastName, Long titleId, String employeeCode,
			Double basicSalary, String dateFrom, String dateTo) {
		employee = new EmployeeVO();
		this.salaryLeaveStructureId = salaryLeaveStructureId;
		employee.setEmployeeId(employeeId);
		employee.setFirstName(firstName);
		employee.setMiddleName(middleName);
		employee.setLastName(lastName);
		employee.setTitleId(titleId);
		employee.setEmployeeCode(employeeCode);
		this.basicSalary = basicSalary;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}
}
