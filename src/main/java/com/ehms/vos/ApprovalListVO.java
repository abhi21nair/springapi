package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ApprovalListVO {

	private Long customerId;
	private String customerName;
	private String organizationCode;
	private Long noOfBranches;
	private Long buyingTypeId;
	private Long buyingTypeQuantity;
	private Long noOfPatients;
	private Long customerPaymentId;
	private Long paymentStatusId;
	private Long paymentModeId;
	private String expiryDate;
	private String txDate;
	private Double amount;

}
