package com.ehms.vos;

import com.ehms.model.DietMaster;
import com.ehms.utils.DateTimeUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class DietMasterVO {
	private Long dietMasterId;
	private Long appointmentId;
	private Long patientId;
	private String dietDate;
	private String morningTea;
	private String breakfast;
	private String lunch;
	private String eveningTea;
	private String dinner;

	public DietMasterVO(DietMaster dietMaster) {
		this.dietMasterId = dietMaster.getDietMasterId();
		this.appointmentId = dietMaster.getAppointmentId();
		this.patientId = dietMaster.getPatientId();
		if (dietMaster.getDietDate() != null) {
			this.dietDate = DateTimeUtil.formatDate(dietMaster.getDietDate());
		}
		this.morningTea = dietMaster.getMorningTea();
		this.breakfast = dietMaster.getBreakfast();
		this.lunch = dietMaster.getLunch();
		this.eveningTea = dietMaster.getEveningTea();
		this.dinner = dietMaster.getDinner();
	}
}
