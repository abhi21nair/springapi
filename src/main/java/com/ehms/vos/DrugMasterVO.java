package com.ehms.vos;

import com.ehms.model.DrugMaster;
import com.ehms.utils.DateTimeUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class DrugMasterVO {

	private Long drugMasterId;
	private String genericName;
	private String medicineName;
	private String brandName;
	private Long drugTypeId;
	private Long storeNameId;
	private String expiryDate;
	private Double strength;
	private String unit;
	private Long minOrder;
	private Long maxOrder;
	private Double qty;
	private Long packingId;
	private String batchNumber;
	private Double purchasePrice;
	private Double costPriceInclAllTax;
	private Double mrp;
	private Long diseaseId;
	private Long categoryId;
	private Double taxInPercent;
	private Double taxInRupees;
	private Long customerId;
	private Long hospitalId;

	public DrugMasterVO(Long drugMasterId, String genericName, String medicineName, String brandName, Long drugTypeId, Double strength, Double mrp, Long categoryId) {
		this.drugMasterId = drugMasterId;
		this.genericName = genericName;
		this.medicineName = medicineName;
		this.brandName = brandName;
		this.drugTypeId = drugTypeId;
		this.strength = strength;
		this.mrp = mrp;
		this.categoryId = categoryId;
	}

	public DrugMasterVO(DrugMaster drugMaster) {
		this.drugMasterId = drugMaster.getDrugMasterId();
		this.genericName = drugMaster.getGenericName();
		this.medicineName = drugMaster.getMedicineName();
		this.brandName = drugMaster.getBrandName();
		this.drugTypeId = drugMaster.getDrugTypeId();
		this.storeNameId = drugMaster.getStoreNameId();
		this.expiryDate = DateTimeUtil.formatDate(drugMaster.getExpiryDate());
		this.strength = drugMaster.getStrength();
		this.unit = drugMaster.getUnit();
		this.minOrder = drugMaster.getMinOrder();
		this.maxOrder = drugMaster.getMaxOrder();
		this.qty = drugMaster.getQty();
		this.packingId = drugMaster.getPackingId();
		this.batchNumber = drugMaster.getBatchNumber();
		this.purchasePrice = drugMaster.getPurchasePrice();
		this.costPriceInclAllTax = drugMaster.getCostPriceInclAllTax();
		this.mrp = drugMaster.getMrp();
		this.diseaseId = drugMaster.getDiseaseId();
		this.categoryId = drugMaster.getCategoryId();
		this.taxInPercent = drugMaster.getTaxInPercent();
		this.taxInRupees = drugMaster.getTaxInRupees();
		if (drugMaster.getCustomer() != null) {
			this.customerId = drugMaster.getCustomer().getCustomerId();
		}
		if (drugMaster.getHospital() != null) {
			this.hospitalId = drugMaster.getHospital().getHospitalId();
		}
	}
}
