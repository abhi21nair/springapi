package com.ehms.vos;

import com.ehms.model.TarrifWiseService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class TarrifWiseServiceVO {
	private Long tarrifServiceId;
	private Long tarrifServiceNameId;
	private String tarrifServiceName;
	private Long serviceTypeId;
	private Long departmentId;
	private Double doctorShareInPercent;
	private Double doctorShareInRupees;
	private Double normalServiceRate;
	private Double specialServiceRate;
	private Boolean isActive;
	private Long customerId;
	private Long hospitalId;

	public TarrifWiseServiceVO(Long tarrifServiceId, Long tarrifServiceNameId, String tarrifServiceName, Long serviceTypeId, Long departmentId, boolean isActive) {
		super();
		this.tarrifServiceId = tarrifServiceId;
		this.tarrifServiceNameId = tarrifServiceNameId;
		this.tarrifServiceName = tarrifServiceName;
		this.serviceTypeId = serviceTypeId;
		this.departmentId = departmentId;
		this.isActive = isActive;
	}

	public TarrifWiseServiceVO(TarrifWiseService tarrifWiseService) {
		this.tarrifServiceId = tarrifWiseService.getTarrifServiceId();
		if (tarrifWiseService.getTarrifServiceName() != null) {
			this.tarrifServiceNameId = tarrifWiseService.getTarrifServiceName().getMiscTypeId();
			this.tarrifServiceName = tarrifWiseService.getTarrifServiceName().getMiscValue();
		}
		this.serviceTypeId = tarrifWiseService.getServiceTypeId();
		this.departmentId = tarrifWiseService.getDepartmentId();
		this.doctorShareInPercent = tarrifWiseService.getDoctorShareInPercent();
		this.doctorShareInRupees = tarrifWiseService.getDoctorShareInRupees();
		this.normalServiceRate = tarrifWiseService.getNormalServiceRate();
		this.specialServiceRate = tarrifWiseService.getSpecialServiceRate();
		this.isActive = tarrifWiseService.getIsActive();
		this.customerId = tarrifWiseService.getCustomer().getCustomerId();
		this.hospitalId = tarrifWiseService.getHospital().getHospitalId();
	}
}
