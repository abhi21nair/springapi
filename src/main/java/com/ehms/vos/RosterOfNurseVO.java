package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class RosterOfNurseVO {

	private Long rosterOfNurseId;
	private Long nurseShiftId;
	private Integer day;
	private Long nurseNameId;
	private String nurseFirstName;
	private String nurseMiddleName;
	private String nurseLastName;
	private String nurseTitleName;
	private String timeFrom;
	private String timeTo;
	private String dateFrom;
	private String dateTo;
	private Integer weeklyOff;
	private String remark;

	public RosterOfNurseVO(Long rosterOfNurseId, Long nurseNameId, String nurseFirstName, String nurseMiddleName,
			String nurseLastName, String nurseTitleName,Long nurseShiftId, String timeFrom,
			String timeTo, String dateFrom, String dateTo) {
		this.nurseTitleName = nurseTitleName;
		this.rosterOfNurseId = rosterOfNurseId;
		this.nurseNameId = nurseNameId;
		this.nurseFirstName = nurseFirstName;
		this.nurseMiddleName = nurseMiddleName;
		this.nurseLastName = nurseLastName;
		this.nurseShiftId = nurseShiftId;
		this.timeFrom = timeFrom;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.timeTo = timeTo;

	}
}
