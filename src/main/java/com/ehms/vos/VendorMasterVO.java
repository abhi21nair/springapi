package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class VendorMasterVO {
	
	private Long vendorMasterId;
	private String vendorName;
	private Long vendorTypeId;
	private Long departmentId;
	private AddressVO address;
	private Long phoneNo;
	private String keyContactPersonName;
	private Long keyContactPersonNumber;
	private Long mobileNo;
	private String countryCodeMobileNumber;
	private String countryCodeContactNumber;
	private String email;
	private String gstNo;
	private String dlNo;
	private String panNo;
	private String tanNo;
	private String shopActNo;
	private String vendorNo;
	private Double openingBalance;
	private Boolean isActive;
	private Long customerId;
	private Long hospitalId;
	private Long cityId;
	
	public VendorMasterVO(Long vendorMasterId,String vendorName,Long vendorTypeId, Long departmentId,
			Long cityId,String gstNo,String vendorNo, Boolean isActive) {
		this.vendorMasterId = vendorMasterId;
		this.vendorName = vendorName;
		this.vendorTypeId = vendorTypeId;
		this.departmentId = departmentId;
		this.cityId = cityId;
		this.gstNo = gstNo;
		this.vendorNo = vendorNo;
		this.isActive = isActive;
	}

}
