package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class CustomerPaymentVO {
	private Long customerPaymentId;
	private Long customerId;
	private Double costOfProduct;
	private String gstNumber;
	private String panNumber;
	private Long paymentStatusId;
	private String amcAddition;
	private String otherService1;
	private String otherService1Type;
	private String otherService2;
	private String otherService2Type;
	private Double discount;
	private String discountValidFrom;
	private String discountValidTo;
	private Double discountInPercent;
	private Double value;
	private String discountInPercentValidFrom;
	private String discountInPercentValidTo;
	private Double tax1;
	private Double tax2;
	private Double tax3;
	private Double totalBillingAmount;
	private Long paymentModeId;
	private String expiryDate;
	private Long actionBy;
	private String actionTime;

	public CustomerPaymentVO(Long customerPaymentId, Long paymentStatusId) {
		this.customerPaymentId = customerPaymentId;
		this.paymentStatusId = paymentStatusId;
	}
	
	public CustomerPaymentVO(Long customerPaymentId, Long paymentStatusId, Long customerId, Double totalBillingAmount) {
		this.customerPaymentId = customerPaymentId;
		this.customerId = customerId;
		this.totalBillingAmount = totalBillingAmount;
		this.paymentStatusId = paymentStatusId;
	}

}
