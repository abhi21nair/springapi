package com.ehms.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class SponsorCorporateVO {

	private Long sponsorCorporateId;
	private String countryCodeForKeyContactPersonNumber;
	private String countryCodeForMobileNumber;
	private String countryCodeForPhoneNumber;
	private String effectiveFromDate;
	private String effectiveToDate;
	private String keyContactPerson;
	private String keyContactPersonNumber;
	private Long maximumLimitPatientWise;
	private Long maximumLimitSponsorCorporateWise;
	private String mobileNumber;
	private String phoneNumber;
	private String sponsorCorporateName;
	private String sponsorCorporateNumber;
	private Long sponsorCorporateTypeId;
	private AddressVO address;
	private Long customerId;
	private Long hospitalId;

	public SponsorCorporateVO(Long sponsorCorporateId, String effectiveFromDate, String effectiveToDate,
			String sponsorCorporateName, Long sponsorCorporateTypeId) {
		super();
		this.sponsorCorporateId = sponsorCorporateId;
		this.effectiveFromDate = effectiveFromDate;
		this.effectiveToDate = effectiveToDate;
		this.sponsorCorporateName = sponsorCorporateName;
		this.sponsorCorporateTypeId = sponsorCorporateTypeId;
	}


	public SponsorCorporateVO(Long sponsorCorporateId, String sponsorCorporateName){
		this.sponsorCorporateId = sponsorCorporateId;
		this.sponsorCorporateName = sponsorCorporateName;
	}
}
