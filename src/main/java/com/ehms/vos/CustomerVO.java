package com.ehms.vos;

import java.util.Date;
import java.util.List;

import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.user.UserVO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class CustomerVO {

	private Long customerId;
	private String customerName;
	private String organizationCode;
	private Long noOfBranches;
	private String contactNumber;
	private String countryCodeForSecondaryNumber;
	private String secContactNumber;
	private Integer demoDays;
	private String countryCodeForContactNumber;
	private String referralPersonName;
	private String referralType;
	private AddressVO address;
	private UserVO keyUser;
	private List<Long> moduleIds;
	private Long buyingPreferenceId;
	private Long buyingTypeId;
	private Long buyingTypeQuantity;
	private Long subscriptionTypeId;
	private Boolean isActive;
	private String expiryDate;
	private Long noOfModules;

	public CustomerVO(Long customerId, String customerName, String organizationCode, Date expiryDate, Long noOfBranches, Long noOfModules, boolean isActive) {
		this.customerId = customerId;
		this.customerName = customerName;
		this.organizationCode = organizationCode;
		this.expiryDate = DateTimeUtil.formatDate(expiryDate);
		this.noOfBranches = noOfBranches;
		this.noOfModules = noOfModules;
		this.isActive = isActive;
	}
}
