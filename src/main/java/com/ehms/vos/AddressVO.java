package com.ehms.vos;

import com.ehms.model.Address;
import com.ehms.model.City;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AddressVO {

	private Long addressId;

	private String address1;

	private String address2;

	private String address3;

	private String street;

	private String county;

	private String region;

	private String zipCode;

	private Long cityId;

	private Long stateId;

	private Long countryId;

	public AddressVO(Address address) {
		if (address != null) {
			this.addressId = address.getAddressId();
			this.address1 = address.getAddress1();
			this.address2 = address.getAddress2();
			this.address3 = address.getAddress3();
			this.county = address.getCounty();
			this.region = address.getRegion();
			this.street = address.getStreet();
			this.zipCode = address.getZipcode();
			if (address.getCity() != null) {
				this.cityId = address.getCity().getCityId();
				if (address.getCity().getState() != null) {
					this.stateId = address.getCity().getState().getStateId();
					if (address.getCity().getState().getCountry() != null) {
						this.countryId = address.getCity().getState().getCountry().getCountryId();
					}
				}
			}
		}
	}

	public Address getAddressEntity(Address address, AddressVO addressVO) {
		address.setAddress1(addressVO.getAddress1());
		address.setAddress2(addressVO.getAddress2());
		address.setAddress3(addressVO.getAddress3());
		if (addressVO.getCityId() != null) {
			City city = new City(addressVO.getCityId());
			address.setCity(city);
		}
		address.setCounty(addressVO.getCounty());
		address.setRegion(addressVO.getRegion());
		address.setStreet(addressVO.getStreet());
		address.setZipcode(addressVO.getZipCode());
		return address;
	}

}
