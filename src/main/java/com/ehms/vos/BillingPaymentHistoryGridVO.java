package com.ehms.vos;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class BillingPaymentHistoryGridVO {

	private String createdDT;
	private Double costOfProduct;
	private Long paymentModeId;
	private Long paymentStatusId;
	private Long actionBy;
	private String actionTime;

	public BillingPaymentHistoryGridVO(Date createdDT, Double costOfProduct, Long paymentModeId, Long paymentStatusId,
			Long actionBy, Date actionTime) {
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		this.createdDT = formatter.format(createdDT);
		this.costOfProduct = costOfProduct;
		this.paymentModeId = paymentModeId;
		this.paymentStatusId = paymentStatusId;
		this.actionBy = actionBy;
		this.actionTime = formatter.format(actionTime);
	}

}
