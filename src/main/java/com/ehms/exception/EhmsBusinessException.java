package com.ehms.exception;

public class EhmsBusinessException extends Exception {

	private static final long serialVersionUID = -8391660852381483225L;

	public EhmsBusinessException(Throwable th, String message) {
		super(message, th);
	}

	public EhmsBusinessException(String message) {
		super(message);
	}
}
