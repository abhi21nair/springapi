package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.vehicle.VehicleService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.VehicleVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/vehicles")
public class VehicleController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("vehicleName"));
	@Autowired
	private VehicleService vehicleService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<VehicleVO> getVehiclesList(
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "vehicleName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return vehicleService.getVehiclesList(page, pageSize, sortOn, sortOrder, searchText, customerId, hospitalId);
	}

	@RequestMapping(value = "/{vehicleId}", method = RequestMethod.GET)
	public VehicleVO getVehicleDetailsById(@PathVariable(value = "vehicleId") Long vehicleId,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return vehicleService.getVehicleDetailsById(customerId, hospitalId, vehicleId);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<VehicleVO> getVehiclesList(@PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		return vehicleService.getVehiclesList(customerId, hospitalId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('VEHICLE')")
	public VehicleVO saveVehicle(@RequestBody VehicleVO vehicleVO, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		return vehicleService.saveUpdateVehicle(customerId, hospitalId, vehicleVO);
	}

	@RequestMapping(value = "/{vehicleId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('VEHICLE')")
	public VehicleVO updateVehicle(@RequestBody VehicleVO vehicleVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return vehicleService.saveUpdateVehicle(customerId, hospitalId, vehicleVO);
	}

}
