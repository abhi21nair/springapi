package com.ehms.controllers.queue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.controllers.AbstractController;
import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.queue.QueueService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.queue.QueueResponseVO;

@RestController
@RequestMapping("customers/{customerId}/hospitals/{hospitalId}/appointments/queues")
public class QueueController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("name", "default"));

	@Autowired
	private QueueService queueService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<QueueResponseVO> getCustomers(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize, @RequestParam(value = "date", required = false) String date,
			@RequestParam(value = "moduleId", required = false) Long moduleId, @RequestParam(value = "doctorId", required = false) Long doctorId,
			@RequestParam(value = "sortOn", required = false, defaultValue = "default") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText) throws EhmsBusinessException {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return queueService.getQueueDetails(page, pageSize, sortOn, sortOrder, searchText, customerId, hospitalId, date, moduleId, doctorId);
	}

}
