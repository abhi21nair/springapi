package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.customer.customerpayment.CustomerPaymentService;
import com.ehms.vos.ApprovalListVO;
import com.ehms.vos.BillingAndPaymentVO;
import com.ehms.vos.CustomerApprovalVO;
import com.ehms.vos.CustomerPaymentVO;
import com.ehms.vos.PaginationResponseDto;

@RestController
@RequestMapping("/customers")
public class CustomerPaymentController extends AbstractController {

	private final Set<String> BILLING_PAYMENT_PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("organizationCode", "customerName", "default"));
	private final Set<String> APPROVAL_LIST_PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("organizationCode", "customerName", "txDate", "paymentStatus", "default"));

	@Autowired
	private CustomerPaymentService customerPaymentService;

	@RequestMapping(value = "{customerId}/customerpayments/{id}", method = RequestMethod.GET)
	public CustomerPaymentVO getCustomerPaymentDetails(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "id") Long id) {
		return customerPaymentService.getCustomerPaymentById(customerId, id);
	}

	@RequestMapping(value = "{customerId}/customerpayments/latest", method = RequestMethod.GET)
	public CustomerPaymentVO getCustomersLatestPaymentDetails(@PathVariable(value = "customerId") Long customerId) {
		return customerPaymentService.getLatestCustomerPaymentDetails(customerId);
	}

	@RequestMapping(value = "{customerId}/customerpayments", method = RequestMethod.POST)
	public CustomerPaymentVO saveCustomerPaymentDetails(@RequestBody CustomerPaymentVO customerPaymentVO, @PathVariable(value = "customerId") Long customerId) throws EhmsBusinessException {
		customerPaymentVO.setCustomerId(customerId);
		customerPaymentService.saveUpdateCustomerPayments(customerPaymentVO);
		return customerPaymentVO;
	}

	@RequestMapping(value = "{customerId}/customerpayments/{id}", method = RequestMethod.PUT)
	public CustomerPaymentVO updateCustomerPaymentDetails(@RequestBody CustomerPaymentVO customerPaymentVO, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "id") Long id) throws EhmsBusinessException {
		customerPaymentVO.setCustomerId(customerId);
		customerPaymentVO.setCustomerPaymentId(id);
		customerPaymentService.saveUpdateCustomerPayments(customerPaymentVO);
		return customerPaymentVO;
	}

	@RequestMapping(value = "{customerId}/customerpayments/{id}/action", method = RequestMethod.PUT)
	public CustomerApprovalVO updateCustomerPaymentstatus(@RequestBody CustomerApprovalVO customerPaymentVO, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "id") Long id) throws EhmsBusinessException {
		customerPaymentService.saveUpdateCustomerPaymentApproval(customerPaymentVO);
		return customerPaymentVO;
	}

	@RequestMapping(value = "/customerpayments", method = RequestMethod.GET)
	public PaginationResponseDto<BillingAndPaymentVO> getCustomerPayments(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "default") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(BILLING_PAYMENT_PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return customerPaymentService.getCustomerPayments(page, pageSize, sortOn, sortOrder, searchText);
	}

	@RequestMapping(value = "/customerpayments/paymentlist", method = RequestMethod.GET)
	public PaginationResponseDto<ApprovalListVO> getCustomerPaymentApprovalList(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "default") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(APPROVAL_LIST_PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return customerPaymentService.getCustomerPaymentsForApprovalList(page, pageSize, sortOn, sortOrder, searchText);
	}

}
