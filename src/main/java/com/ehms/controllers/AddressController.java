package com.ehms.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.model.Address;
import com.ehms.services.masterdata.AddressService;

@RestController
@RequestMapping("addresses")
public class AddressController extends AbstractController {

	@Autowired
	private AddressService addressService;

	@RequestMapping(method = RequestMethod.GET, value = "")
	public List<Address> getAddressDetails() {
		return addressService.getAllAddresses();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Address getAddressDetailsById(@PathVariable(value = "id") Long id) {
		return addressService.getAddressDetailsById(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "")
	public void addAddress(@RequestBody Address address) {
		addressService.createAddress(address);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public void updateAddress(@RequestBody Address address, @PathVariable Long id) {
		addressService.updateAddress(address);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void deleteAddress(@PathVariable Long id) {
		addressService.deleteAddress(id);
	}
}
