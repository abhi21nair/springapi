package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.PathologyTestMasterService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.PathologyTestMasterVO;;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/pathologyTestMasters")
public class PathologyTestMasterController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("testName"));

	@Autowired
	private PathologyTestMasterService pathologyTestMasterService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<PathologyTestMasterVO> getPathologyTestMasterDetails(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize, @RequestParam(value = "sortOn", required = false, defaultValue = "testName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder, @RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return pathologyTestMasterService.getPathologyTestMasterDetails(page, pageSize, sortOn, sortOrder, searchText, customerId, hospitalId);
	}

	@RequestMapping(value = "{pathologyTestMasterId}", method = RequestMethod.GET)
	public PathologyTestMasterVO getPathologyTestMaster(@PathVariable(value = "pathologyTestMasterId") Long pathologyTestMasterId, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		return pathologyTestMasterService.getPathologyTestMaster(pathologyTestMasterId);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<PathologyTestMasterVO> getPathologyTestMasterList(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return pathologyTestMasterService.getPathologyTestMasterList(customerId, hospitalId);
	}

	@RequestMapping(value = "{pathologyTestMasterId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('PATHOLOGY_TEST_MASTER')")
	public PathologyTestMasterVO updatePathologyTestMaster(@PathVariable(value = "pathologyTestMasterId") Long pathologyTestMasterId, @RequestBody PathologyTestMasterVO pathologyTestMaster,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		pathologyTestMasterService.saveUpdatePathologyTestMaster(customerId, hospitalId, pathologyTestMaster);
		return pathologyTestMaster;
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('PATHOLOGY_TEST_MASTER')")
	public PathologyTestMasterVO savePathologyTestMaster(@RequestBody PathologyTestMasterVO pathologyTestMaster, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		pathologyTestMasterService.saveUpdatePathologyTestMaster(customerId, hospitalId, pathologyTestMaster);
		return pathologyTestMaster;
	}

}
