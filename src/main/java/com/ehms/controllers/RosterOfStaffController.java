package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.RosterOfStaffService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.RosterOfStaffVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/rosterOfStaffs")
public class RosterOfStaffController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("firstName", "lastName"));
	@Autowired
	private RosterOfStaffService rosterOfStaffService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<RosterOfStaffVO> getRosterOfStaffDetails(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "lastName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return rosterOfStaffService.getRosterOfStaffDetails(page, pageSize, sortOn, sortOrder, searchText, customerId, hospitalId);
	}

	@RequestMapping(value = "/{rosterOfStaffId}", method = RequestMethod.GET)
	public RosterOfStaffVO getRosterOfStaffById(@PathVariable(value = "rosterOfStaffId") Long rosterOfStaffId) {
		return rosterOfStaffService.getRosterOfStaffById(rosterOfStaffId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('ROSTER_OF_STAFF')")
	public RosterOfStaffVO saveRosterOfStaff(@RequestBody RosterOfStaffVO rosterOfStaffVO) throws EhmsBusinessException {
		return rosterOfStaffService.saveUpdateRosterOfStaff(rosterOfStaffVO);
	}

	@RequestMapping(value = "/{rosterOfStaffId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('ROSTER_OF_STAFF')")
	public RosterOfStaffVO updateRosterOfStaff(@RequestBody RosterOfStaffVO rosterOfStaffVO) throws EhmsBusinessException {
		return rosterOfStaffService.saveUpdateRosterOfStaff(rosterOfStaffVO);
	}
}
