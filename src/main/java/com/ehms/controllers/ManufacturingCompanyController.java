package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.manufacturingcompany.ManufacturingCompanyService;
import com.ehms.vos.ManufacturingCompanyVO;
import com.ehms.vos.PaginationResponseDto;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/manufacturingCompanies")
public class ManufacturingCompanyController extends AbstractController {
	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("manufactureName"));
	@Autowired
	private ManufacturingCompanyService manufacturingCompanyService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<ManufacturingCompanyVO> getManufacturingCompanyList(
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "manufactureName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return manufacturingCompanyService.getManufacturingCompanyList(page, pageSize, sortOn, sortOrder, searchText,
				customerId, hospitalId);
	}

	@RequestMapping(value = "/{manufacturingCompanyId}", method = RequestMethod.GET)
	public ManufacturingCompanyVO getManufacturingCompanyDetailsById(
			@PathVariable(value = "manufacturingCompanyId") Long manufacturingCompanyId,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return manufacturingCompanyService.getManufacturingCompanyDetailsById(customerId, hospitalId,
				manufacturingCompanyId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('MANUFACTURING_COMPANY')")
	public ManufacturingCompanyVO saveManufacturingCompany(@RequestBody ManufacturingCompanyVO manufacturingCompanyVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return manufacturingCompanyService.saveUpdateManufacturingCompany(customerId, hospitalId,
				manufacturingCompanyVO);
	}

	@RequestMapping(value = "/{manufacturingCompanyId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('MANUFACTURING_COMPANY')")
	public ManufacturingCompanyVO updateManufacturingCompany(@RequestBody ManufacturingCompanyVO manufacturingCompanyVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return manufacturingCompanyService.saveUpdateManufacturingCompany(customerId, hospitalId,
				manufacturingCompanyVO);
	}
}
