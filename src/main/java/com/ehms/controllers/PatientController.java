package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.PatientService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.PatientVO;

/**
 * Created by synerzip on 28/8/17.
 */
@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/patients")
public class PatientController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("name"));

	@Autowired
	private PatientService patientService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<PatientVO> getPatientList(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize, @RequestParam(value = "sortOn", required = false, defaultValue = "name") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder, @RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return patientService.getPatientList(page, pageSize, sortOn, sortOrder, searchText, customerId, hospitalId);
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public PaginationResponseDto<PatientVO> searchPatientList(@RequestBody PatientVO patientVO, @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize, String sortOrder, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		return patientService.searchPatientList(patientVO, page, pageSize, customerId, hospitalId);
	}

	@RequestMapping(value = "/{patientId}", method = RequestMethod.GET)
	public PatientVO getPatientById(@PathVariable(value = "patientId") Long patientId, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return patientService.getPatientDetails(patientId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('PATIENT_REGISTRATION','HELP_DESK')")
	public PatientVO savePatient(@RequestBody PatientVO patientVO, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId)
			throws EhmsBusinessException {
		return patientService.saveUpdatePatient(customerId, hospitalId, patientVO, true);
	}

	@RequestMapping(value = "/{patientId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('PATIENT_REGISTRATION','HELP_DESK')")
	public PatientVO updatePatient(@RequestBody PatientVO patientVO, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@PathVariable(value = "patientId") Long patientId) throws EhmsBusinessException {
		return patientService.saveUpdatePatient(customerId, hospitalId, patientVO, false);
	}
}
