package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.RosterOfNurseService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.RosterOfNurseVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/rosterOfNurses")
public class RosterOfNurseController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("firstName", "lastName"));

	@Autowired
	private RosterOfNurseService rosterOfNurseService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<RosterOfNurseVO> getRosterOfNurseDetails(
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "lastName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return rosterOfNurseService.getRosterOfNurseDetails(page, pageSize, sortOn, sortOrder, searchText, customerId, hospitalId);
	}

	@RequestMapping(value = "/{rosterOfNurseId}", method = RequestMethod.GET)
	public RosterOfNurseVO getRosterOfNurseById(@PathVariable(value = "rosterOfNurseId") Long rosterOfNurseId) {
		return rosterOfNurseService.getRosterOfNurseById(rosterOfNurseId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('ROSTER_OF_NURSE')")
	public RosterOfNurseVO saveRosterOfNurse(@RequestBody RosterOfNurseVO rosterOfNurseVO) throws EhmsBusinessException {
		return rosterOfNurseService.saveUpdateRosterOfNurse(rosterOfNurseVO);
	}

	@RequestMapping(value = "/{rosterOfNurseId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('ROSTER_OF_NURSE')")
	public RosterOfNurseVO updateRosterOfNurse(@RequestBody RosterOfNurseVO rosterOfNurseVO) throws EhmsBusinessException {
		return rosterOfNurseService.saveUpdateRosterOfNurse(rosterOfNurseVO);
	}
}
