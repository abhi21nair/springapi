package com.ehms.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.ipd.DietMasterService;
import com.ehms.vos.DietMasterVO;
import com.ehms.vos.ResponseVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/patients/{patientId}/dietmasters")
public class DietMasterController {
	@Autowired
	private DietMasterService dietMasterService;

	@RequestMapping(method = RequestMethod.GET)
	public List<DietMasterVO> getDiets(@PathVariable(value = "patientId") Long patientId) {
		return dietMasterService.getDiets(patientId);
	}

	@RequestMapping(method = RequestMethod.POST)
	public DietMasterVO saveDiet(@RequestBody DietMasterVO dietMasterVO) throws EhmsBusinessException {
		return dietMasterService.saveUpdateDiet(dietMasterVO);
	}

	@RequestMapping(value = "/{dietMasterId}", method = RequestMethod.PUT)
	public DietMasterVO updateDiet(@RequestBody DietMasterVO dietMasterVO) throws EhmsBusinessException {
		return dietMasterService.saveUpdateDiet(dietMasterVO);
	}

	@RequestMapping(value = "/{dietMasterId}", method = RequestMethod.DELETE)
	public ResponseVO deleteDiet(@PathVariable Long dietMasterId) {
		return dietMasterService.deleteDiet(dietMasterId);
	}
}
