package com.ehms.controllers;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.sponsorcorporate.SponsorCorporateService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.SponsorCorporateVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/sponsorCorporates")
public class SponsorCorporateController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("sponsorCorporateName"));
	@Autowired
	private SponsorCorporateService sponsorCorporateService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<SponsorCorporateVO> getSponsorCorporateList(
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "sponsorCorporateName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return sponsorCorporateService.getSponsorCorporateList(page, pageSize, sortOn, sortOrder, searchText,
				customerId, hospitalId);
	}

	@RequestMapping(value = "/{sponsorCorporateId}", method = RequestMethod.GET)
	public SponsorCorporateVO getSponsorCorporateDetailsById(
			@PathVariable(value = "sponsorCorporateId") Long sponsorCorporateId,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return sponsorCorporateService.getSponsorCorporateDetailsById(customerId, hospitalId, sponsorCorporateId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('SPONSOR/CORPORATE')")
	public SponsorCorporateVO saveSponsorCorporate(@RequestBody SponsorCorporateVO sponsorCorporateVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) throws EhmsBusinessException {
		return sponsorCorporateService.saveUpdateSponsorCorporate(customerId, hospitalId, sponsorCorporateVO);
	}

	@RequestMapping(value = "/{sponsorCorporateId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('SPONSOR/CORPORATE')")
	public SponsorCorporateVO updateSponsorCorporate(@RequestBody SponsorCorporateVO sponsorCorporateVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) throws EhmsBusinessException {
		return sponsorCorporateService.saveUpdateSponsorCorporate(customerId, hospitalId, sponsorCorporateVO);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<SponsorCorporateVO> getSponsorCorporateList(@PathVariable(value = "customerId") Long customerId,
							 @PathVariable(value = "hospitalId") Long hospitalId) {
		return sponsorCorporateService.getSponsorCorporateList(customerId, hospitalId);
	}
}
