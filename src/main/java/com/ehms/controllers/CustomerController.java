package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.customer.CustomerService;
import com.ehms.vos.CustomerVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ValidationCheckResponseVO;

@RestController
@RequestMapping("/customers")
public class CustomerController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("organizationCode", "customerName"));

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public CustomerVO getCustomerDetails(@PathVariable(value = "id") Long id) {
		return customerService.getCustomersDetails(id);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('CUSTOMER_MANAGEMENT')")
	public CustomerVO saveCustomerDetails(@RequestBody CustomerVO customer) throws EhmsBusinessException {
		customerService.saveUpdateCustomers(customer, true);
		return customer;
	}

	@RequestMapping(value = "/validate", method = RequestMethod.POST)
	public ValidationCheckResponseVO validateCustomerName(@RequestBody CustomerVO customer) {
		return  customerService.validateCustomerName(customer);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('CUSTOMER_MANAGEMENT')")
	public CustomerVO updateCustomerDetails(@RequestBody CustomerVO customer, @PathVariable(value = "id") Long id) throws EhmsBusinessException {
		customer.setCustomerId(id);
		customerService.saveUpdateCustomers(customer, false);
		return customer;
	}

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<CustomerVO> getCustomers(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "customerName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return customerService.getCustomers(page, pageSize, sortOn, sortOrder, searchText);
	}
}
