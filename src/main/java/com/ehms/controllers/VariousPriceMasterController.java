package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.VariousPriceMasterService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.VariousPriceMasterVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/variousPriceMasters")
public class VariousPriceMasterController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("priceMasterServiceName"));
	@Autowired
	private VariousPriceMasterService variousPriceMasterService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<VariousPriceMasterVO> getVariousPriceMasterDetails(
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "priceMasterServiceName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return variousPriceMasterService.getVariousPriceMasterDetails(page, pageSize, sortOn, sortOrder, searchText,
				customerId, hospitalId);
	}

	@RequestMapping(value = "/{variousPriceMasterId}", method = RequestMethod.GET)
	public VariousPriceMasterVO getVariousPriceMasterById(
			@PathVariable(value = "variousPriceMasterId") Long variousPriceMasterId,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return variousPriceMasterService.getVariousPriceMasterById(customerId, hospitalId,
				variousPriceMasterId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('VARIOUS_PRICE_MASTER')")
	public VariousPriceMasterVO saveVariousPriceMaster(@RequestBody VariousPriceMasterVO variousPriceMasterVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return variousPriceMasterService.saveUpdateVariousPriceMaster(customerId, hospitalId, variousPriceMasterVO);
	}

	@RequestMapping(value = "/{variousPriceMasterId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('VARIOUS_PRICE_MASTER')")
	public VariousPriceMasterVO updateVariousPriceMaster(@RequestBody VariousPriceMasterVO variousPriceMasterVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return variousPriceMasterService.saveUpdateVariousPriceMaster(customerId, hospitalId, variousPriceMasterVO);
	}
}
