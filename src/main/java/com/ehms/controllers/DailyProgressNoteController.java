package com.ehms.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.ipd.DailyProgressNoteService;
import com.ehms.vos.DailyProgressNoteVO;
import com.ehms.vos.ResponseVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/patients/{patientId}/dailyprogressnotes")
public class DailyProgressNoteController {
	@Autowired
	private DailyProgressNoteService dailyProgressNoteService;

	@RequestMapping(method = RequestMethod.GET)
	public List<DailyProgressNoteVO> getDailyProgerssNotes(@PathVariable(value = "patientId") Long patientId) {
		return dailyProgressNoteService.getDailyProgerssNotes(patientId);
	}

	@RequestMapping(method = RequestMethod.POST)
	public DailyProgressNoteVO saveDailyProgerssNote(@RequestBody DailyProgressNoteVO dailyProgressNoteVO) throws EhmsBusinessException {
		return dailyProgressNoteService.saveUpdateDailyProgerssNotes(dailyProgressNoteVO);
	}

	@RequestMapping(value = "/{dailyProgressNoteId}", method = RequestMethod.PUT)
	public DailyProgressNoteVO updateDailyProgerssNote(@RequestBody DailyProgressNoteVO dailyProgressNoteVO) throws EhmsBusinessException {
		return dailyProgressNoteService.saveUpdateDailyProgerssNotes(dailyProgressNoteVO);
	}

	@RequestMapping(value = "/{dailyProgressNoteId}", method = RequestMethod.DELETE)
	public ResponseVO deleteDailyProgerssNote(@PathVariable Long dailyProgressNoteId) {
		return dailyProgressNoteService.deleteDailyProgerssNote(dailyProgressNoteId);
	}
}
