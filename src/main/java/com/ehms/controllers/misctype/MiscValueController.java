package com.ehms.controllers.misctype;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.controllers.AbstractController;
import com.ehms.services.misctype.MiscValueService;
import com.ehms.vos.misctype.MiscTypeValuesVO;
import com.ehms.vos.misctype.MiscValueVO;

@RestController
public class MiscValueController extends AbstractController {

	@Autowired
	private MiscValueService miscValueService;

	@RequestMapping(value = "/customers/{customerId}/misctypes/{miscTypeId}/values", method = RequestMethod.POST)
	public MiscValueVO saveMiscValue(@PathVariable(value = "miscTypeId") Long miscTypeId, @PathVariable(value = "customerId") Long customerId, @RequestBody MiscValueVO miscValueVO) {
		return miscValueService.saveUpdateMiscValues(miscTypeId, miscValueVO, true);
	}

	@RequestMapping(value = "/customers/{customerId}/misctypes/{miscTypeId}/values/{id}", method = RequestMethod.PUT)
	public MiscValueVO saveMiscValue(@PathVariable(value = "miscTypeId") Long miscTypeId, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "id") Long miscTypeValueId,
			@RequestBody MiscValueVO miscValueVO) {
		return miscValueService.saveUpdateMiscValues(miscTypeId, miscValueVO, false);
	}

	@RequestMapping(value = "/customers/{customerId}/misctypes/{miscTypeId}/values", method = RequestMethod.GET)
	public List<MiscValueVO> getMiscValues(@PathVariable(value = "miscTypeId") Long miscTypeId, @PathVariable(value = "customerId") Long customerId) {
		return miscValueService.getMiscValues(miscTypeId);
	}

	@RequestMapping(value = "/miscvalues", method = RequestMethod.GET)
	public List<MiscTypeValuesVO> getMiscValues() {
		return miscValueService.getMiscValues();
	}
}
