package com.ehms.controllers.misctype;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.controllers.AbstractController;
import com.ehms.model.MiscType;
import com.ehms.services.misctype.MiscTypeService;

@RestController
@RequestMapping("/misctypes")
public class MiscTypeController extends AbstractController {
	
	@Autowired
	private MiscTypeService miscTypeService;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<MiscType> getAvailableMiscTypes() {
		return miscTypeService.getAvailableMiscTypes();
	}
}
