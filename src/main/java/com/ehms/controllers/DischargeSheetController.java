package com.ehms.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.ipd.DischargeSheetService;
import com.ehms.vos.DischargeSheetVO;
import com.ehms.vos.ResponseVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/patients/{patientId}/dischargesheets")
public class DischargeSheetController {
	@Autowired
	private DischargeSheetService dischargeSheetService;

	@RequestMapping(method = RequestMethod.GET)
	public List<DischargeSheetVO> getDischargeSheets(@PathVariable(value = "patientId") Long patientId) {
		return dischargeSheetService.getDischargeSheets(patientId);
	}

	@RequestMapping(method = RequestMethod.POST)
	public DischargeSheetVO saveDischargeSheet(@RequestBody DischargeSheetVO dischargeSheetVO) throws EhmsBusinessException {
		return dischargeSheetService.saveUpdateDischargeSheet(dischargeSheetVO);
	}

	@RequestMapping(value = "/{dischargeSheetId}", method = RequestMethod.PUT)
	public DischargeSheetVO updateDischargeSheet(@RequestBody DischargeSheetVO dischargeSheetVO) throws EhmsBusinessException {
		return dischargeSheetService.saveUpdateDischargeSheet(dischargeSheetVO);
	}

	@RequestMapping(value = "/{dischargeSheetId}", method = RequestMethod.DELETE)
	public ResponseVO deleteDischargeSheet(@PathVariable Long dischargeSheetId) {
		return dischargeSheetService.deleteDischargeSheet(dischargeSheetId);
	}
}
