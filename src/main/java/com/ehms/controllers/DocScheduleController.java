package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.docschedules.DocScheduleService;
import com.ehms.vos.DocScheduleVO;
import com.ehms.vos.PaginationResponseDto;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/doctors/schedules")
public class DocScheduleController extends AbstractController {
	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("firstName", "lastName"));
	@Autowired
	private DocScheduleService docScheduleService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<DocScheduleVO> getDocScheduleList(
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "lastName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return docScheduleService.getDocScheduleList(page, pageSize, sortOn, sortOrder, searchText, customerId,
				hospitalId);
	}

	@RequestMapping(value = "/{docScheduleId}", method = RequestMethod.GET)
	public DocScheduleVO getDocScheduleDetailsById(@PathVariable(value = "docScheduleId") Long docScheduleId,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return docScheduleService.getDocScheduleDetailsById(docScheduleId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('DOC_SCHEDULE')")
	public DocScheduleVO saveDocSchedule(@RequestBody DocScheduleVO docScheduleVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) throws EhmsBusinessException {
		return docScheduleService.saveUpdateDoctorSchedule(customerId, hospitalId, docScheduleVO);
	}

	@RequestMapping(value = "/{docScheduleId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('DOC_SCHEDULE')")
	public DocScheduleVO updateDoctorSchedule(@RequestBody DocScheduleVO docScheduleVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) throws EhmsBusinessException {
		return docScheduleService.saveUpdateDoctorSchedule(customerId, hospitalId, docScheduleVO);
	}

}
