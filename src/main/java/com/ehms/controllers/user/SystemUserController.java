package com.ehms.controllers.user;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ehms.controllers.AbstractController;
import com.ehms.services.user.UserService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.user.UserVO;

@RestController
public class SystemUserController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("username"));

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/systemUsers/{userId}", method = RequestMethod.GET)
	public UserVO getUserDetails(@PathVariable(value = "userId") Long userId) {
		return userService.getSystemUserDetails(userId);
	}

	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('SYSTEM_USER_MANAGEMENT')")
	@RequestMapping(value = "/systemUsers", method = RequestMethod.POST)
	public UserVO saveUser(@RequestBody UserVO userVO) {
		return userService.saveUpdateSystemUser(userVO);
	}

	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('SYSTEM_USER_MANAGEMENT')")
	@RequestMapping(value = "/systemUsers/{userId}", method = RequestMethod.PUT)
	public UserVO updateUser(@RequestBody UserVO userVO, @PathVariable(value = "userId") Long userId) {
		return userService.saveUpdateSystemUser(userVO);
	}

	@RequestMapping(value = "/systemUsers", method = RequestMethod.GET)
	public PaginationResponseDto<UserVO> getSystemUserList(
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "username") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return userService.getSystemUserList(page, pageSize, sortOn, sortOrder, searchText);
	}

}
