package com.ehms.controllers;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.ward.WardService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ValidationCheckResponseVO;
import com.ehms.vos.WardVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/wards")
public class WardController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("wardName"));

	@Autowired
	private WardService wardService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<WardVO> getWardDetails(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize, @RequestParam(value = "sortOn", required = false, defaultValue = "wardName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder, @RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return wardService.getWardDetails(customerId, hospitalId, page, pageSize, sortOn, sortOrder, searchText);
	}

	@RequestMapping(value = "/{wardId}", method = RequestMethod.GET)
	public WardVO getWardDetails(@PathVariable(value = "wardId") Long wardId) {
		return wardService.getWardDetails(wardId);
	}

	@RequestMapping(value = "beds/available", method = RequestMethod.GET)
	public Collection<WardVO> getAvailableBeds(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) throws EhmsBusinessException {
		return wardService.getAvailableBeds(customerId,hospitalId);
	}

	@RequestMapping(value = "/{wardId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('BED_MANAGEMENT')")
	public WardVO saveUpdateWardDetails(@RequestBody WardVO ward, @PathVariable(value = "wardId") Long wardId) throws EhmsBusinessException {
		wardService.saveUpdateWardDetails(ward, false);
		return ward;
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('BED_MANAGEMENT')")
	public WardVO saveUpdateWardDetails(@RequestBody WardVO ward) {
		wardService.saveUpdateWardDetails(ward, true);
		return ward;
	}

	@RequestMapping(value = "/validate", method = RequestMethod.POST)
	public ValidationCheckResponseVO validateWardName(@RequestBody WardVO ward, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return wardService.validateWardName(ward, customerId, hospitalId);
	}

}