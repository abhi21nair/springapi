package com.ehms.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.DepartmentService;
import com.ehms.vos.DepartmentVO;
import com.ehms.vos.ValidationCheckResponseVO;

@RestController
public class DepartmentController extends AbstractController {

	@Autowired
	private DepartmentService departmentService;

	@RequestMapping(value = "customers/{customerId}/departments/{departmentId}", method = RequestMethod.GET)
	public DepartmentVO getDepartmentDetailsById(@PathVariable(value = "departmentId") Long departmentId, @PathVariable(value = "customerId") Long customerId) {
		return departmentService.getDepartmentDetails(departmentId);
	}

	@RequestMapping(value = "customers/{customerId}/departments", method = RequestMethod.POST)
	public DepartmentVO saveDepartmentDetails(@RequestBody DepartmentVO departmentVO, @PathVariable(value = "customerId") Long customerId) throws EhmsBusinessException {
		departmentService.saveUpdateDepartment(departmentVO, true);
		return departmentVO;
	}

	@RequestMapping(value = "customers/{customerId}/departments/validate", method = RequestMethod.POST)
	public ValidationCheckResponseVO validateDepartment(@RequestBody DepartmentVO departmentVO) {
		ValidationCheckResponseVO validationCheckResponseVO = departmentService.validateDepartment(departmentVO);
		return validationCheckResponseVO;
	}

	@RequestMapping(value = "customers/{customerId}/departments/{departmentId}", method = RequestMethod.PUT)
	public DepartmentVO updateDepartmentDetails(@RequestBody DepartmentVO departmentVO, @PathVariable(value = "departmentId") Long departmentId, @PathVariable(value = "customerId") Long customerId)
			throws EhmsBusinessException {
		departmentService.saveUpdateDepartment(departmentVO, false);
		return departmentVO;
	}

	@RequestMapping(value = "customers/{customerId}/departments", method = RequestMethod.GET)
	public List<DepartmentVO> getDepartments() {
		return departmentService.getDepartments();
	}

	@RequestMapping(value = "systemdepartments", method = RequestMethod.GET)
	public List<DepartmentVO> getSystemDepartments() {
		return departmentService.getSystemDepartments();
	}
}
