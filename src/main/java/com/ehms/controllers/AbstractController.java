package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class AbstractController {
	
	protected final Set<String> PERMITTED_SORT_ORDERS = new HashSet<String>(Arrays.asList("ASC", "DESC"));

}
