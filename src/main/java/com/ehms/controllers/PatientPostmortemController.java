package com.ehms.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.ipd.PatientPostmortemService;
import com.ehms.vos.PatientPostmortemVO;
import com.ehms.vos.ResponseVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/patients/{patientId}/patientpostmortems")
public class PatientPostmortemController {
	@Autowired
	private PatientPostmortemService patientPostmortemService;

	@RequestMapping(method = RequestMethod.GET)
	public List<PatientPostmortemVO> getPatientPostmortems(@PathVariable(value = "patientId") Long patientId) {
		return patientPostmortemService.getPatientPostmortems(patientId);
	}

	@RequestMapping(method = RequestMethod.POST)
	public PatientPostmortemVO savePatientPostmortem(@RequestBody PatientPostmortemVO patientPostmortemVO) throws EhmsBusinessException {
		return patientPostmortemService.saveUpdatePatientPostmortem(patientPostmortemVO);
	}

	@RequestMapping(value = "/{patientPostmortemId}", method = RequestMethod.PUT)
	public PatientPostmortemVO updatePatientPostmortem(@RequestBody PatientPostmortemVO patientPostmortemVO) throws EhmsBusinessException {
		return patientPostmortemService.saveUpdatePatientPostmortem(patientPostmortemVO);
	}

	@RequestMapping(value = "/{patientPostmortemId}", method = RequestMethod.DELETE)
	public ResponseVO deletePatientPostmortem(@PathVariable Long patientPostmortemId) {
		return patientPostmortemService.deletePatientPostmortem(patientPostmortemId);
	}
}
