package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.matrialinstrument.MaterialInstrumentService;
import com.ehms.vos.DrugMasterVO;
import com.ehms.vos.MaterialInstrumentVO;
import com.ehms.vos.PaginationResponseDto;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/materialInstruments")
public class MaterialInstrumentController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("materialInstrumentName"));

	@Autowired
	private MaterialInstrumentService materialInstrumentService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<MaterialInstrumentVO> getMaterialInstrumentList(
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "materialInstrumentName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return materialInstrumentService.getMaterialInstrumentList(page, pageSize, sortOn, sortOrder, searchText,
				customerId, hospitalId);
	}
	
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<MaterialInstrumentVO> getMaterialInstrumentList(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return materialInstrumentService.getMaterialInstrumentList(customerId, hospitalId);
	}
	
	@RequestMapping(value = "/{materialInstrumentId}", method = RequestMethod.GET)
	public MaterialInstrumentVO getMaterialInstrumentDetailsById(
			@PathVariable(value = "materialInstrumentId") Long materialInstrumentId,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return materialInstrumentService.getMaterialInstrumentDetailsById(customerId, hospitalId, materialInstrumentId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('MATERIAL_INSTRUMENT')")
	public MaterialInstrumentVO saveMaterialInstrument(@RequestBody MaterialInstrumentVO materialInstrumentVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return materialInstrumentService.saveUpdateMaterialInstrument(customerId, hospitalId, materialInstrumentVO);
	}

	@RequestMapping(value = "/{materialInstrumentId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('MATERIAL_INSTRUMENT')")
	public MaterialInstrumentVO updateMaterialInstrument(@RequestBody MaterialInstrumentVO materialInstrumentVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return materialInstrumentService.saveUpdateMaterialInstrument(customerId, hospitalId, materialInstrumentVO);
	}
}
