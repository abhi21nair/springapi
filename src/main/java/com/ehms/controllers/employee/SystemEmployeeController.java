package com.ehms.controllers.employee;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.controllers.AbstractController;
import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.EmployeeService;
import com.ehms.vos.EmployeeVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ValidationCheckResponseVO;

@RestController
@RequestMapping("/systememployees")
public class SystemEmployeeController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("firstName", "lastName"));

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<EmployeeVO> getSystemEmployeeList(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize, @RequestParam(value = "sortOn", required = false, defaultValue = "lastName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder, @RequestParam(value = "searchText", required = false, defaultValue = "") String searchText) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return employeeService.getSystemEmployeeList(page, pageSize, sortOn, sortOrder, searchText);
	}

	@RequestMapping(value = "/{employeeId}", method = RequestMethod.GET)
	public EmployeeVO getSystemEmployeeDetailsById(@PathVariable(value = "employeeId") Long employeeId) {
		return employeeService.getSystemEmployeeDetails(employeeId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('SYSTEM_EMPLOYEE_MANAGEMENT')")
	public EmployeeVO saveSystemEmployee(@RequestBody EmployeeVO employeeVO) throws EhmsBusinessException {
		return employeeService.saveUpdateSystemEmployee(employeeVO);
	}

	@RequestMapping(value = "/{employeeId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('SYSTEM_EMPLOYEE_MANAGEMENT')")
	public EmployeeVO updateSystemEmployee(@RequestBody EmployeeVO employeeVO, @PathVariable(value = "employeeId") Long employeeId) throws EhmsBusinessException {
		return employeeService.saveUpdateSystemEmployee(employeeVO);
	}

	@RequestMapping(value = "validate", method = RequestMethod.POST)
	public ValidationCheckResponseVO validateSystemEmployee(@RequestBody EmployeeVO employeeVO) {
		ValidationCheckResponseVO validationCheckResponseVO = employeeService.validateSystemEmployee(employeeVO);
		return validationCheckResponseVO;
	}

	@RequestMapping(value = "/withoutaccount", method = RequestMethod.GET)
	public List<EmployeeVO> getSystemEmployeeList() {
		return employeeService.getSystemEmployeeWithoutEhmsAccount();
	}

}
