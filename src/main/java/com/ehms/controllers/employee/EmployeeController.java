package com.ehms.controllers.employee;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.controllers.AbstractController;
import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.EmployeeService;
import com.ehms.vos.EmployeeVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ValidationCheckResponseVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/employees")
public class EmployeeController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("firstName", "lastName"));

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<EmployeeVO> getEmployeeList(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "lastName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return employeeService.getEmployeeList(page, pageSize, sortOn, sortOrder, searchText, customerId, hospitalId);
	}

	@RequestMapping(value = "/withoutaccount", method = RequestMethod.GET)
	public List<EmployeeVO> getEmployeeList(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return employeeService.getEmployeeWithoutEhmsAccount(customerId, hospitalId);
	}

	@RequestMapping(value = "/{employeeId}", method = RequestMethod.GET)
	public EmployeeVO getEmployeeDetailsById(@PathVariable(value = "employeeId") Long employeeId, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		return employeeService.getEmployeeDetails(employeeId);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<EmployeeVO> getEmployees(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@RequestParam(value = "employeeType", required = false) Integer employeeType) {
		return employeeService.getEmployeesList(customerId, hospitalId, employeeType);
	}

	@RequestMapping(value = "validate", method = RequestMethod.POST)
	public ValidationCheckResponseVO validateEmployee(@RequestBody EmployeeVO employeeVO, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		ValidationCheckResponseVO validationCheckResponseVO = employeeService.validateEmployee(employeeVO, customerId, hospitalId);
		return validationCheckResponseVO;
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('EMPLOYEE_MANAGEMENT')")
	public EmployeeVO saveEmployee(@RequestBody EmployeeVO employeeVO, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId)
			throws EhmsBusinessException {
		return employeeService.saveUpdateEmployee(customerId, hospitalId, employeeVO);
	}

	@RequestMapping(value = "/{employeeId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('EMPLOYEE_MANAGEMENT')")
	public EmployeeVO updateEmployee(@RequestBody EmployeeVO employeeVO, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@PathVariable(value = "employeeId") Long employeeId) throws EhmsBusinessException {
		return employeeService.saveUpdateEmployee(customerId, hospitalId, employeeVO);
	}
}
