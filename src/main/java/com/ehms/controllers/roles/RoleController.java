package com.ehms.controllers.roles;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.controllers.AbstractController;
import com.ehms.services.role.RoleService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ValidationCheckResponseVO;
import com.ehms.vos.role.RoleVO;

@RestController
@RequestMapping("/customers/{customerId}/roles")
public class RoleController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("roleName"));

	@Autowired
	private RoleService roleService;

	@RequestMapping(value = "{roleId}", method = RequestMethod.GET)
	public RoleVO getRoleDetailsById(@PathVariable(value = "roleId") Long roleId,
			@PathVariable(value = "customerId") Long customerId) {
		return roleService.getRoleDetails(roleId);
	}

	@RequestMapping(method = RequestMethod.POST)
	public RoleVO saveRoleDetails(@RequestBody RoleVO roleVO, @PathVariable(value = "customerId") Long customerId) {
		roleService.saveUpdateRole(roleVO, true);
		return roleVO;
	}

	@RequestMapping(value = "validate", method = RequestMethod.POST)
	public ValidationCheckResponseVO validateRoleName(@RequestBody RoleVO roleVO) {
		ValidationCheckResponseVO validationCheckResponseVO = roleService.validateRoleName(roleVO);
		return validationCheckResponseVO;
	}

	@RequestMapping(value = "{roleId}", method = RequestMethod.PUT)
	public RoleVO updateRoleDetails(@RequestBody RoleVO roleVO, @PathVariable(value = "roleId") Long roleId,
			@PathVariable(value = "customerId") Long customerId) {
		roleService.saveUpdateRole(roleVO, false);
		return roleVO;
	}

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<RoleVO> getRoleList(
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "roleName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return roleService.getRolesList(page, pageSize, sortOn, sortOrder, searchText);
	}
	
	@RequestMapping(value = "{customerId}/roles/list", method = RequestMethod.GET)
	public List<RoleVO> getRoleList(@PathVariable(value = "customerId") Long customerId) {
		Assert.notNull(customerId, "Customer Id not provided");
		return roleService.getRolesList();
	}
}
