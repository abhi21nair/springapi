package com.ehms.controllers.roles;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.role.PrivilegeService;
import com.ehms.vos.role.PrivilegeVO;

@RestController
@RequestMapping("/privileges")
public class PrivilegeController {
	
	@Autowired
	private PrivilegeService privilegeService;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<PrivilegeVO> getAvailablePrivileges() {
		return privilegeService.getAvailablePrivileges();
	}
	
	@RequestMapping(value = "/system", method = RequestMethod.GET)
	public List<PrivilegeVO> getAvailableSystemPrivileges() {
		return privilegeService.getAvailableSystemPrivileges();
	}
}
