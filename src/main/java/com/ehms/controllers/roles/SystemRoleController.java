package com.ehms.controllers.roles;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ehms.controllers.AbstractController;
import com.ehms.services.role.RoleService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ValidationCheckResponseVO;
import com.ehms.vos.role.RoleVO;

@RestController
@RequestMapping("/systemRoles")
public class SystemRoleController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("roleName"));

	@Autowired
	private RoleService roleService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<RoleVO> getSystemRolesList(
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "roleName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return roleService.getSystemRolesList(page, pageSize, sortOn, sortOrder, searchText);
	}

	@RequestMapping(value = "{roleId}", method = RequestMethod.GET)
	public RoleVO getSystemRoleDetailsById(@PathVariable(value = "roleId") Long roleId) {
		return roleService.getSystemRoleDetails(roleId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('SYSTEM_ROLE_MANAGEMENT')")
	public RoleVO saveSystemRoleDetails(@RequestBody RoleVO roleVO) {
		roleService.saveUpdateSystemRole(roleVO, true);
		return roleVO;
	}

	@RequestMapping(value = "{roleId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('SYSTEM_ROLE_MANAGEMENT')")
	public RoleVO updateSystemRoleDetails(@RequestBody RoleVO roleVO, @PathVariable(value = "roleId") Long roleId) {
		roleService.saveUpdateSystemRole(roleVO, false);
		return roleVO;
	}

	@RequestMapping(value = "roles/list", method = RequestMethod.GET)
	public List<RoleVO> getSystemUserList() {
		return roleService.getRolesList();
	}

	@RequestMapping(value = "validate", method = RequestMethod.POST)
	public ValidationCheckResponseVO validateSystemRoleName(@RequestBody RoleVO roleVO) {
		ValidationCheckResponseVO validationCheckResponseVO = roleService.validateRoleName(roleVO);
		return validationCheckResponseVO;
	}

}
