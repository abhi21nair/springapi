package com.ehms.controllers.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.model.SubscriptionType;
import com.ehms.services.masterdata.SubscriptionTypeService;

@RestController
@RequestMapping("/subscriptionTypes")
public class SubscriptionTypeController {

	@Autowired
	private SubscriptionTypeService subscriptionTypeService;

	@RequestMapping(method = RequestMethod.GET)
	public List<SubscriptionType> findall() {
		return subscriptionTypeService.getSubscriptionTypeDetails();

	}

}
