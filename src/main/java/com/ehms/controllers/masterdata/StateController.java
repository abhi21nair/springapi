package com.ehms.controllers.masterdata;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.masterdata.StateService;
import com.ehms.vos.StateVO;

@RestController
@RequestMapping("states")
public class StateController {

	@Autowired
	private StateService stateService;

	@RequestMapping(method = RequestMethod.GET)
	public List<StateVO> findAllStates() {
		return stateService.getAllStates();
	}

}
