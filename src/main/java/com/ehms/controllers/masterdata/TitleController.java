package com.ehms.controllers.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.model.Title;
import com.ehms.services.masterdata.TitleService;
@RestController
@RequestMapping("/titles")
public class TitleController {
	@Autowired
	private TitleService titleService;

	@RequestMapping(method = RequestMethod.GET)
	public List<Title> findAllTitles() {
		return titleService.getTitles();
	}
}
