package com.ehms.controllers.masterdata;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.masterdata.CityService;
import com.ehms.vos.CityVO;

@RestController
@RequestMapping("cities")
public class CityController {

	@Autowired
	private CityService cityService;
	
	
	@RequestMapping(method=RequestMethod.GET)
	public List<CityVO> findAllCities(){
		return cityService.getAllCities();
	}
	
}
