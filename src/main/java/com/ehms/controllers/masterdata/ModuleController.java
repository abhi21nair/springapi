package com.ehms.controllers.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.model.Module;
import com.ehms.services.masterdata.ModuleService;

@RestController
@RequestMapping("/modules")
public class ModuleController {
	@Autowired
	private ModuleService moduleService;

	@RequestMapping(method = RequestMethod.GET)
	public List<Module> findall() {
		return moduleService.getModuleDetails();

	}

}
