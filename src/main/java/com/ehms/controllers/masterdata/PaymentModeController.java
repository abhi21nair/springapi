package com.ehms.controllers.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.model.PaymentMode;
import com.ehms.services.masterdata.PaymentModeService;

@RestController
@RequestMapping("paymentmodes")
public class PaymentModeController {
	@Autowired
	private PaymentModeService paymentModeService;

	@RequestMapping(method = RequestMethod.GET)
	public List<PaymentMode> findAllPaymentModes() {
		return paymentModeService.getAllPaymentModes();
	}
}
