package com.ehms.controllers.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.model.PaymentStatus;
import com.ehms.services.masterdata.PaymentStatusService;

@RestController
@RequestMapping("paymentstatus")
public class PaymentStatusController {
	@Autowired
	private PaymentStatusService paymentStatusService;

	@RequestMapping(method = RequestMethod.GET)
	public List<PaymentStatus> findAllPaymentStatus() {
		return paymentStatusService.getAllPaymentStatus();
	}
}
