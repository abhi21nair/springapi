package com.ehms.controllers.masterdata;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.controllers.AbstractController;
import com.ehms.services.masterdata.CountryService;
import com.ehms.vos.CountryVO;

@RestController
@RequestMapping("countries")
public class CountryController extends AbstractController {

	@Autowired
	private CountryService countryService;

	@RequestMapping(method = RequestMethod.GET)
	public List<CountryVO> findAllCountries() {
		return countryService.getAllCountries();
	}
}
