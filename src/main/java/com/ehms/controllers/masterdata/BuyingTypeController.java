package com.ehms.controllers.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.model.BuyingType;
import com.ehms.services.masterdata.BuyingTypeService;

@RestController
@RequestMapping("/buyingTypes")
public class BuyingTypeController {

	@Autowired
	private BuyingTypeService buyingTypeService;

	@RequestMapping(method = RequestMethod.GET)
	public List<BuyingType> findall() {
		return buyingTypeService.getbuyingTypeService();

	}
}