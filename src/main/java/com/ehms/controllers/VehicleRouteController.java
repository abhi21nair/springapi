package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.vehicle.vehicleroute.VehicleRouteService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.VehicleRouteVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/vehicleRoutes")
public class VehicleRouteController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("route"));
	@Autowired
	private VehicleRouteService vehicleRouteService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<VehicleRouteVO> getVehicleRoutesList(
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "route") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return vehicleRouteService.getVehicleRoutesList(page, pageSize, sortOn, sortOrder, searchText, customerId,
				hospitalId);
	}

	@RequestMapping(value = "/{vehicleRouteId}", method = RequestMethod.GET)
	public VehicleRouteVO getVehicleRouteDetailsById(@PathVariable(value = "vehicleRouteId") Long vehicleRouteId,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return vehicleRouteService.getVehicleRouteDetailsById(customerId, hospitalId, vehicleRouteId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('VEHICLE_ROUTE')")
	public VehicleRouteVO saveVehicleRoute(@RequestBody VehicleRouteVO vehicleRouteVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) throws EhmsBusinessException {
		return vehicleRouteService.saveUpdateVehicleRoute(customerId, hospitalId, vehicleRouteVO);
	}

	@RequestMapping(value = "/{vehicleRouteId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('VEHICLE_ROUTE')")
	public VehicleRouteVO updateVehicleRoute(@RequestBody VehicleRouteVO vehicleRouteVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) throws EhmsBusinessException {
		return vehicleRouteService.saveUpdateVehicleRoute(customerId, hospitalId, vehicleRouteVO);
	}
}
