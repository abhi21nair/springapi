package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.VendorMasterService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.VendorMasterVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/vendorMasters")
public class VendorMasterController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("vendorName"));

	@Autowired
	private VendorMasterService vendorMasterService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<VendorMasterVO> getVendorMasterDetails(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "vendorName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return vendorMasterService.getVendorMasterDetails(page, pageSize, sortOn, sortOrder, searchText, customerId, hospitalId);
	}

	@RequestMapping(value = "{vendorMasterId}", method = RequestMethod.GET)
	public VendorMasterVO getVendorMasterById(@PathVariable(value = "vendorMasterId") Long vendorMasterId, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		return vendorMasterService.getVendorMasterById(vendorMasterId);
	}

	@RequestMapping(value = "{vendorMasterId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('VENDOR_MASTER')")
	public VendorMasterVO updateVendorMaster(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@PathVariable(value = "vendorMasterId") Long vendorMasterId, @RequestBody VendorMasterVO vendorMaster) {
		vendorMasterService.saveUpdateVendorMaster(customerId, hospitalId, vendorMaster);
		return vendorMaster;
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('VENDOR_MASTER')")
	public VendorMasterVO saveVendorMaster(@RequestBody VendorMasterVO vendorMaster, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		vendorMasterService.saveUpdateVendorMaster(customerId, hospitalId, vendorMaster);
		return vendorMaster;
	}

}
