package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.DrugMasterService;
import com.ehms.vos.DrugMasterVO;
import com.ehms.vos.PaginationResponseDto;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/drugMasters")
public class DrugMasterController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("genericName"));
	@Autowired
	private DrugMasterService drugMasterService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<DrugMasterVO> getDrugMasterDetails(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize, @RequestParam(value = "sortOn", required = false, defaultValue = "genericName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder, @RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return drugMasterService.getDrugMasterDetails(page, pageSize, sortOn, sortOrder, searchText, customerId, hospitalId);
	}

	@RequestMapping(value = "/{drugMasterId}", method = RequestMethod.GET)
	public DrugMasterVO getDrugMasterById(@PathVariable(value = "drugMasterId") Long drugMasterId, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		return drugMasterService.getDrugMasterById(customerId, hospitalId, drugMasterId);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<DrugMasterVO> getDrugMasterList(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return drugMasterService.getDrugMasterList(customerId, hospitalId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('DRUG_MASTER')")
	public DrugMasterVO saveDrugMaster(@RequestBody DrugMasterVO drugMasterVO, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId)
			throws EhmsBusinessException {
		return drugMasterService.saveUpdateDrugMaster(customerId, hospitalId, drugMasterVO);
	}

	@RequestMapping(value = "/{drugMasterId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('DRUG_MASTER')")
	public DrugMasterVO updateDrugMaster(@RequestBody DrugMasterVO drugMasterVO, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId)
			throws EhmsBusinessException {
		return drugMasterService.saveUpdateDrugMaster(customerId, hospitalId, drugMasterVO);
	}
}
