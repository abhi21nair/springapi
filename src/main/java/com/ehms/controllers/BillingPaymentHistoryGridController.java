package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.customer.BillingPaymentHistoryGridService;
import com.ehms.vos.BillingPaymentHistoryGridVO;
import com.ehms.vos.PaginationResponseDto;

@RestController
@RequestMapping("/customers")
public class BillingPaymentHistoryGridController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("createdDt"));

	@Autowired
	private BillingPaymentHistoryGridService billingPaymentHistoryGridService;

	@RequestMapping(value = "{customerId}/billingPayment/History", method = RequestMethod.GET)

	public PaginationResponseDto<BillingPaymentHistoryGridVO> getCustomerPayments(@PathVariable Long customerId,
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "createdDt") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return billingPaymentHistoryGridService.getGridHistory(page, pageSize, sortOn, sortOrder, customerId);
	}

}
