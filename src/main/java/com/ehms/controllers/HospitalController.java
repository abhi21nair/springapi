package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.HospitalService;
import com.ehms.vos.HospitalVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ValidationCheckResponseVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals")
public class HospitalController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("hospitalName", "hospitalCode"));

	@Autowired
	private HospitalService hospitalService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<HospitalVO> getHospitals(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "hospitalName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText, @PathVariable(value = "customerId") Long customerId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return hospitalService.getHospitalsList(page, pageSize, sortOn, sortOrder, searchText);
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<HospitalVO> getHospitals(@PathVariable(value = "customerId") Long customerId) {
		return hospitalService.getHospitalsList();
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public HospitalVO getHospitalDetails(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "id") Long id) {
		return hospitalService.getHospitalDetails(id);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('HOSPITALS')")
	public HospitalVO updateHospitalDetails(@RequestBody HospitalVO hospital, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "id") Long id) throws EhmsBusinessException {
		hospital.setHospitalId(id);
		hospitalService.saveUpdatehospitals(hospital, false);
		return hospital;
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('HOSPITALS')")
	public HospitalVO saveHospitalDetails(@PathVariable(value = "customerId") Long customerId, @RequestBody HospitalVO hospital) throws EhmsBusinessException {
		hospitalService.saveUpdatehospitals(hospital, true);
		return hospital;
	}
	
	@RequestMapping(value = "/validate", method = RequestMethod.POST)
	public ValidationCheckResponseVO validateHospitalName(@PathVariable(value = "customerId") Long customerId, @RequestBody HospitalVO hospitalVO) {
		return  hospitalService.validateHospitalName(hospitalVO);
	}

}
