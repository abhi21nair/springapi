package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.tarrifwiseService.TarrifWiseServiceService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.TarrifWiseServiceVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/tarrifWiseServices")
public class TarrifWiseServiceController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("tarrifServiceName"));
	@Autowired
	private TarrifWiseServiceService tarrifWiseServiceService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<TarrifWiseServiceVO> getTarrifWiseServiceList(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "tarrifServiceName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder, @RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return tarrifWiseServiceService.getTarrifWiseServiceList(page, pageSize, sortOn, sortOrder, searchText, customerId, hospitalId);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<TarrifWiseServiceVO> getTarrifWiseServiceList(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return tarrifWiseServiceService.getTarrifWiseServiceList(customerId, hospitalId);
	}

	@RequestMapping(value = "/{tarrifWiseServiceId}", method = RequestMethod.GET)
	public TarrifWiseServiceVO getTarrifWiseServiceDetailsById(@PathVariable(value = "tarrifWiseServiceId") Long tarrifWiseServiceId, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		return tarrifWiseServiceService.getTarrifWiseServiceDetailsById(customerId, hospitalId, tarrifWiseServiceId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('TARRIF_WISE_SERVICE')")
	public TarrifWiseServiceVO saveTarrifWiseService(@RequestBody TarrifWiseServiceVO tarrifWiseServiceVO, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		return tarrifWiseServiceService.saveUpdateTarrifWiseService(customerId, hospitalId, tarrifWiseServiceVO);
	}

	@RequestMapping(value = "/{tarrifWiseServiceId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('TARRIF_WISE_SERVICE')")
	public TarrifWiseServiceVO updateTarrifWiseService(@RequestBody TarrifWiseServiceVO tarrifWiseServiceVO, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		return tarrifWiseServiceService.saveUpdateTarrifWiseService(customerId, hospitalId, tarrifWiseServiceVO);
	}
}
