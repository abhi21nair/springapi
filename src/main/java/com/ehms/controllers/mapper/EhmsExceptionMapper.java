package com.ehms.controllers.mapper;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.ehms.exception.EhmsBusinessException;

@ControllerAdvice
public class EhmsExceptionMapper {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@ExceptionHandler(IllegalArgumentException.class)
    public void illegalArgumentExceptionHandler(IllegalArgumentException ex, HttpServletResponse response) {
        try {
            response.sendError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
        } catch (IOException e) {
        	logger.error("IOException inside illegalArgumentExceptionHandler()", e);
        }
    }
	
	@ExceptionHandler(EhmsBusinessException.class)
    public void ehmsBusinessExceptionHandler(EhmsBusinessException ex, HttpServletResponse response) {
        try {
            response.sendError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
        } catch (IOException e) {
        	logger.error("IOException inside illegalArgumentExceptionHandler()", e);
        }
    }

    @ExceptionHandler(Throwable.class)
    public void throwableHandler(Throwable th, HttpServletResponse response){
        try {
            logger.error("Inside throwableHandler()...", th);
            response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), th.getMessage());
        } catch (IOException e) {
            logger.error("IOException inside throwableHandler()", e);
        }
    }
}
