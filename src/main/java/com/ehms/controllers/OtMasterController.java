
package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.OtMasterService;
import com.ehms.vos.OtMasterVO;
import com.ehms.vos.PaginationResponseDto;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/otMasters")
public class OtMasterController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("otName"));

	@Autowired
	private OtMasterService otMasterService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<OtMasterVO> getOtMasterDetails(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "otName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return otMasterService.getOtMasterDetails(page, pageSize, sortOn, sortOrder, searchText, customerId, hospitalId);
	}

	@RequestMapping(value = "{otMasterId}", method = RequestMethod.GET)
	public OtMasterVO getOtMasterById(@PathVariable(value = "otMasterId") Long otMasterId, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		return otMasterService.getOtMasterById(otMasterId);
	}

	@RequestMapping(value = "{otMasterId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('OT_MASTER')")
	public OtMasterVO updateOtMaster(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@PathVariable(value = "otMasterId") Long pathologyTestMasterId, @RequestBody OtMasterVO OtMaster) {
		otMasterService.saveUpdateOtMaster(customerId, hospitalId, OtMaster);
		return OtMaster;
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('OT_MASTER')")
	public OtMasterVO saveUpdateOtMaster(@RequestBody OtMasterVO OtMaster, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		otMasterService.saveUpdateOtMaster(customerId, hospitalId, OtMaster);
		return OtMaster;
	}

}
