package com.ehms.controllers.appointment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.appointment.AppointmentExaminationService;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.appointments.AppointmentExaminationVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/appointments/{appointmentId}/examinations")
public class AppointmentExaminationController {

	@Autowired
	private AppointmentExaminationService appointmentExaminationService;

	@RequestMapping(method = RequestMethod.GET)
	public List<AppointmentExaminationVO> getAppointmentExaminations(@PathVariable(value = "appointmentId") Long appointmentId) {
		return appointmentExaminationService.getAppointmentExaminations(appointmentId);
	}

	@RequestMapping(method = RequestMethod.POST)
	public AppointmentExaminationVO saveAppointmentExamination(@RequestBody AppointmentExaminationVO appointmentExaminationVO) {
		return appointmentExaminationService.saveUpdateAppointmentExamination(appointmentExaminationVO);
	}

	@RequestMapping(value = "/{examinationId}", method = RequestMethod.PUT)
	public AppointmentExaminationVO updateAppointmentExamination(@RequestBody AppointmentExaminationVO appointmentExaminationVO) {
		return appointmentExaminationService.saveUpdateAppointmentExamination(appointmentExaminationVO);
	}

	@RequestMapping(value = "/{examinationId}", method = RequestMethod.DELETE)
	public ResponseVO deleteAppointmentExamination(@PathVariable Long examinationId) {
		return appointmentExaminationService.deleteAppointmentExamination(examinationId);
	}
}
