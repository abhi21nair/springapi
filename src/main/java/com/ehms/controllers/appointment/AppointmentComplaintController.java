package com.ehms.controllers.appointment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.appointment.AppointmentComplaintService;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.appointments.AppointmentComplaintVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/appointments/{appointmentId}/complaints")
public class AppointmentComplaintController {

	@Autowired
	private AppointmentComplaintService appointmentComplaintService;

	@RequestMapping(method = RequestMethod.GET)
	public List<AppointmentComplaintVO> getAppointmentComplaints(@PathVariable(value = "appointmentId") Long appointmentId) {
		return appointmentComplaintService.getAppointmentComplaints(appointmentId);
	}

	@RequestMapping(method = RequestMethod.POST)
	public AppointmentComplaintVO saveAppointmentComplaint(@RequestBody AppointmentComplaintVO appointmentComplaintVO) {
		return appointmentComplaintService.saveUpdateAppointmentComplaint(appointmentComplaintVO);
	}

	@RequestMapping(value = "/{complaintId}", method = RequestMethod.PUT)
	public AppointmentComplaintVO updateAppointmentComplaint(@RequestBody AppointmentComplaintVO appointmentComplaintVO) {
		return appointmentComplaintService.saveUpdateAppointmentComplaint(appointmentComplaintVO);
	}

	@RequestMapping(value = "/{complaintId}", method = RequestMethod.DELETE)
	public ResponseVO deleteAppointmentComplaint(@PathVariable Long complaintId) {
		return appointmentComplaintService.deleteAppointmentComplaint(complaintId);
	}
}
