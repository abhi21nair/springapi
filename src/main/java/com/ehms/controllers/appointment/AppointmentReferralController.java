package com.ehms.controllers.appointment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.appointment.AppointmentReferralService;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.appointments.AppointmentReferralVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/appointments/{appointmentId}/referrals")
public class AppointmentReferralController {

	@Autowired
	private AppointmentReferralService appointmentReferralService;

	@RequestMapping(method = RequestMethod.GET)
	public List<AppointmentReferralVO> getAppointmentReferrals(@PathVariable(value = "appointmentId") Long appointmentId) {
		return appointmentReferralService.getAppointmentReferrals(appointmentId);
	}

	@RequestMapping(method = RequestMethod.POST)
	public AppointmentReferralVO saveAppointmentReferral(@RequestBody AppointmentReferralVO appointmentReferralVO) {
		return appointmentReferralService.saveUpdateAppointmentReferral(appointmentReferralVO);
	}

	@RequestMapping(value = "/{referralId}", method = RequestMethod.PUT)
	public AppointmentReferralVO updateAppointmentReferral(@RequestBody AppointmentReferralVO appointmentReferralVO) {
		return appointmentReferralService.saveUpdateAppointmentReferral(appointmentReferralVO);
	}

	@RequestMapping(value = "/{referralId}", method = RequestMethod.DELETE)
	public ResponseVO deleteAppointmentReferral(@PathVariable Long referralId) {
		return appointmentReferralService.deleteAppointmentReferral(referralId);
	}
}
