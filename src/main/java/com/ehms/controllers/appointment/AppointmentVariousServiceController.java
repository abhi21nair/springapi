package com.ehms.controllers.appointment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.appointment.AppointmentVariousServiceService;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.appointments.AppointmentVariousServiceVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/appointments/{appointmentId}/variousServices")
public class AppointmentVariousServiceController {

	@Autowired
	private AppointmentVariousServiceService appointmentVariousServiceService;

	@RequestMapping(method = RequestMethod.GET)
	public List<AppointmentVariousServiceVO> getAppointmentVariousServices(@PathVariable(value = "appointmentId") Long appointmentId) {
		return appointmentVariousServiceService.getAppointmentVariousServices(appointmentId);
	}

	@RequestMapping(method = RequestMethod.POST)
	public AppointmentVariousServiceVO saveAppointmentVariousService(@RequestBody AppointmentVariousServiceVO appointmentVariousServiceVO) {
		return appointmentVariousServiceService.saveUpdateAppointmentVariousService(appointmentVariousServiceVO);
	}

	@RequestMapping(value = "/{variousServiceId}", method = RequestMethod.PUT)
	public AppointmentVariousServiceVO updateAppointmentVariousService(@RequestBody AppointmentVariousServiceVO appointmentVariousServiceVO) {
		return appointmentVariousServiceService.saveUpdateAppointmentVariousService(appointmentVariousServiceVO);
	}

	@RequestMapping(value = "/{variousServiceId}", method = RequestMethod.DELETE)
	public ResponseVO deleteAppointmentVariousService(@PathVariable Long variousServiceId) {
		return appointmentVariousServiceService.deleteAppointmentVariousService(variousServiceId);
	}
}
