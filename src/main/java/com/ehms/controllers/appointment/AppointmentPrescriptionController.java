package com.ehms.controllers.appointment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.services.appointment.AppointmentPrescriptionService;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.appointments.AppointmentPrescriptionVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/appointments/{appointmentId}/prescriptions")
public class AppointmentPrescriptionController {

	@Autowired
	private AppointmentPrescriptionService appointmentPrescriptionService;

	@RequestMapping(method = RequestMethod.GET)
	public List<AppointmentPrescriptionVO> getAppointmentPrescriptions(@PathVariable(value = "appointmentId") Long appointmentId) {
		return appointmentPrescriptionService.getAppointmentPrescriptions(appointmentId);
	}

	@RequestMapping(method = RequestMethod.POST)
	public AppointmentPrescriptionVO saveAppointmentPrescription(@RequestBody AppointmentPrescriptionVO appointmentPrescriptionVO) {
		return appointmentPrescriptionService.saveUpdateAppointmentPrescription(appointmentPrescriptionVO);
	}

	@RequestMapping(value = "/{prescriptionId}", method = RequestMethod.PUT)
	public AppointmentPrescriptionVO updateAppointmentPrescription(@RequestBody AppointmentPrescriptionVO appointmentPrescriptionVO) {
		return appointmentPrescriptionService.saveUpdateAppointmentPrescription(appointmentPrescriptionVO);
	}

	@RequestMapping(value = "/{prescriptionId}", method = RequestMethod.DELETE)
	public ResponseVO deleteAppointmentPrescription(@PathVariable Long prescriptionId) {
		return appointmentPrescriptionService.deleteAppointmentPrescription(prescriptionId);
	}
}
