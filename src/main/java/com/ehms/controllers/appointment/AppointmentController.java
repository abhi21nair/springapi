package com.ehms.controllers.appointment;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.controllers.AbstractController;
import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.appointment.AppointmentService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.appointments.AppointmentStatsVO;
import com.ehms.vos.appointments.AppointmentVO;
import com.ehms.vos.appointments.PatientAppointmentHistoryVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/appointments")
public class AppointmentController extends AbstractController {

	private final Set<String> PATIENT_QUEUE_PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("name", "default"));
	private final Set<String> PATIENT_HISTORY_PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("reportTime"));

	@Autowired
	private AppointmentService appointmentService;

	@RequestMapping(method = RequestMethod.GET)
	public List<AppointmentStatsVO> getHospitalAppointmentStats(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@RequestParam(value = "date", required = false) String date) throws EhmsBusinessException {
		return appointmentService.getHospitalAppointmentStats(customerId, hospitalId, date);
	}

	@RequestMapping(value = "queue", method = RequestMethod.GET)
	public PaginationResponseDto<AppointmentVO> getQueueDetails(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page, @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "date", required = false) String date, @RequestParam(value = "moduleId", required = false) Long moduleId,
			@RequestParam(value = "doctorId", required = false) Long doctorId, @RequestParam(value = "doctorScheduleId", required = false) Long doctorScheduleId,
			@RequestParam(value = "sortOn", required = false, defaultValue = "default") String sortOn, @RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText) throws EhmsBusinessException {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PATIENT_QUEUE_PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return appointmentService.getQueueDetails(page, pageSize, sortOn, sortOrder, searchText, date, customerId, hospitalId, moduleId, doctorId, doctorScheduleId);
	}

	@RequestMapping(value = "/patients/{patientId}/history", method = RequestMethod.GET)
	public PaginationResponseDto<PatientAppointmentHistoryVO> getPatientAppointmentHistory(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@PathVariable(value = "patientId") Long patientId, @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "3") Integer pageSize, @RequestParam(value = "sortOn", required = false, defaultValue = "reportTime") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder, @RequestParam(value = "searchText", required = false, defaultValue = "") String searchText)
			throws EhmsBusinessException {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PATIENT_HISTORY_PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return appointmentService.getPatientAppointmentHistory(page, pageSize, sortOn, sortOrder, searchText, customerId, hospitalId, patientId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('PATIENT_REGISTRATION','HELP_DESK')")
	public AppointmentVO saveAppointment(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId, @RequestBody AppointmentVO appointmentVO)
			throws EhmsBusinessException {
		return appointmentService.saveAppointment(customerId, hospitalId, appointmentVO);
	}

	@RequestMapping(value = "/{appointmentId}/reportPatient", method = RequestMethod.PUT)
	public ResponseVO updatePatientReportTime(@RequestBody AppointmentVO appointmentVO, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@PathVariable(value = "appointmentId") Long appointmentId) throws EhmsBusinessException {
		return appointmentService.updatePatientReportTime(customerId, hospitalId, appointmentId, appointmentVO);
	}

	@RequestMapping(value = "/{appointmentId}/start", method = RequestMethod.PUT)
	public ResponseVO startAppointment(@RequestBody AppointmentVO appointmentVO, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@PathVariable(value = "appointmentId") Long appointmentId) throws EhmsBusinessException {
		return appointmentService.startAppointment(customerId, hospitalId, appointmentId, appointmentVO);
	}

	@RequestMapping(value = "/{appointmentId}/complete", method = RequestMethod.PUT)
	public ResponseVO completeAppointment(@RequestBody AppointmentVO appointmentVO, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@PathVariable(value = "appointmentId") Long appointmentId) throws EhmsBusinessException {
		return appointmentService.completeAppointment(customerId, hospitalId, appointmentId, appointmentVO);
	}

	@RequestMapping(value = "/{appointmentId}/cancel", method = RequestMethod.PUT)
	public ResponseVO cancelAppointment(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@PathVariable(value = "appointmentId") Long appointmentId) {
		return appointmentService.cancelAppointment(customerId, hospitalId, appointmentId);
	}

	@RequestMapping(value = "/{appointmentId}/discharge", method = RequestMethod.PUT)
	public ResponseVO dischargePatient(@RequestBody AppointmentVO appointmentVO, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@PathVariable(value = "appointmentId") Long appointmentId) throws EhmsBusinessException {
		return appointmentService.dischargePatient(appointmentVO, customerId, hospitalId, appointmentId);
	}

	@RequestMapping(value = "/{appointmentId}/hold", method = RequestMethod.PUT)
	public ResponseVO putAppointmentOnHold(@RequestBody AppointmentVO appointmentVO, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@PathVariable(value = "appointmentId") Long appointmentId) throws EhmsBusinessException {
		return appointmentService.putAppointmentOnHold(customerId, hospitalId, appointmentId, appointmentVO);
	}

	@RequestMapping(value = "/{appointmentId}/resume", method = RequestMethod.PUT)
	public ResponseVO resumeAppointment(@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@PathVariable(value = "appointmentId") Long appointmentId) {
		return appointmentService.resumeAppointment(customerId, hospitalId, appointmentId);
	}

}