package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.SalaryLeaveStructureService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.SalaryLeaveStructureVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/salaryLeaveStructures")
public class SalaryLeaveStructureController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("name"));

	@Autowired
	private SalaryLeaveStructureService salaryLeaveStructureService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<SalaryLeaveStructureVO> getSalaryLeaveStructureDetails(@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "name") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return salaryLeaveStructureService.getSalaryLeaveStructureDetails(page, pageSize, sortOn, sortOrder, searchText, customerId, hospitalId);
	}

	@RequestMapping(value = "/{salaryLeaveStructureId}", method = RequestMethod.GET)
	public SalaryLeaveStructureVO getSalaryLeaveStructureById(@PathVariable(value = "salaryLeaveStructureId") Long salaryLeaveStructureId,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return salaryLeaveStructureService.getSalaryLeaveStructureById(customerId, hospitalId, salaryLeaveStructureId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('SALARY_LEAVE_STRUCTURE')")
	public SalaryLeaveStructureVO saveSalaryLeaveStructure(@RequestBody SalaryLeaveStructureVO salaryLeaveStructureVO, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) throws EhmsBusinessException {
		return salaryLeaveStructureService.saveUpdateSalaryLeaveStructure(customerId, hospitalId, salaryLeaveStructureVO);
	}

	@RequestMapping(value = "/{salaryLeaveStructureId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('SALARY_LEAVE_STRUCTURE')")
	public SalaryLeaveStructureVO updateSalaryLeaveStructure(@RequestBody SalaryLeaveStructureVO salaryLeaveStructureVO, @PathVariable(value = "customerId") Long customerId,
			@PathVariable(value = "hospitalId") Long hospitalId) throws EhmsBusinessException {
		return salaryLeaveStructureService.saveUpdateSalaryLeaveStructure(customerId, hospitalId, salaryLeaveStructureVO);
	}
}
