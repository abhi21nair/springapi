package com.ehms.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.tpaInsurance.TpaInsuranceService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.TpaInsuranceVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/tpaInsurances")

public class TpaInsuranceController extends AbstractController {

	private final Set<String> PERMITTED_SORT_COLUMNS = new HashSet<String>(Arrays.asList("tpaInsuranceName"));
	@Autowired
	private TpaInsuranceService tpaInsuranceService;

	@RequestMapping(method = RequestMethod.GET)
	public PaginationResponseDto<TpaInsuranceVO> getTpaInsuranceList(
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sortOn", required = false, defaultValue = "tpaInsuranceName") String sortOn,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "searchText", required = false, defaultValue = "") String searchText,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		Assert.isTrue(PERMITTED_SORT_ORDERS.contains(sortOrder), "SortOrder is not supported.");
		Assert.isTrue(PERMITTED_SORT_COLUMNS.contains(sortOn), "SortOn column is not supported.");
		return tpaInsuranceService.getTpaInsuranceList(page, pageSize, sortOn, sortOrder, searchText, customerId,
				hospitalId);
	}

	@RequestMapping(value = "/{tpaInsuranceId}", method = RequestMethod.GET)
	public TpaInsuranceVO getTpaInsuranceDetailsById(@PathVariable(value = "tpaInsuranceId") Long tpaInsuranceId,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) {
		return tpaInsuranceService.getTpaInsuranceDetailsById(customerId, hospitalId, tpaInsuranceId);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('TPA/INSURANCE')")
	public TpaInsuranceVO saveTpaInsurance(@RequestBody TpaInsuranceVO tpaInsuranceVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) throws EhmsBusinessException {
		return tpaInsuranceService.saveUpdateTpaInsurance(customerId, hospitalId, tpaInsuranceVO);
	}

	@RequestMapping(value = "/{tpaInsuranceId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('TPA/INSURANCE')")
	public TpaInsuranceVO updateTpaInsurance(@RequestBody TpaInsuranceVO tpaInsuranceVO,
			@PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId) throws EhmsBusinessException {
		return tpaInsuranceService.saveUpdateTpaInsurance(customerId, hospitalId, tpaInsuranceVO);
	}
}
