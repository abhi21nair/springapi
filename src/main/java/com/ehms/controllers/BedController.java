package com.ehms.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.services.ward.BedService;
import com.ehms.vos.BedVO;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.ResponseVO.Status;
import com.ehms.vos.ValidationCheckResponseVO;

@RestController
@RequestMapping("/customers/{customerId}/hospitals/{hospitalId}/wards/{wardId}/beds")
public class BedController {

	@Autowired
	private BedService bedService;

	@RequestMapping(value = "/{bedId}", method = RequestMethod.GET)
	public BedVO getBedDetails(@PathVariable(value = "bedId") Long bedId) {
		return bedService.getBedDetails(bedId);
	}

	@RequestMapping(value = "/{bedId}", method = RequestMethod.PUT)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('BED_MANAGEMENT')")
	public BedVO saveUpdateBedDetails(@RequestBody BedVO bedVO, @PathVariable(value = "bedId") Long bedId) throws EhmsBusinessException {
		bedService.saveUpdateBedDetails(bedVO, false);
		return bedVO;
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('BED_MANAGEMENT')")
	public BedVO saveUpdateBedDetails(@RequestBody BedVO bedVO) throws EhmsBusinessException {
		bedService.saveUpdateBedDetails(bedVO, true);
		return bedVO;
	}

	@RequestMapping(value = "/validate", method = RequestMethod.POST)
	public ValidationCheckResponseVO validateBedCode(@RequestBody BedVO bedVO, @PathVariable(value = "customerId") Long customerId, @PathVariable(value = "hospitalId") Long hospitalId,
			@PathVariable(value = "wardId") Long wardId) throws EhmsBusinessException {
		return bedService.validateBedCode(bedVO, wardId);
	}

	@PreAuthorize("@privilegeSecurityComponent.hasPrivilege('BED_MANAGEMENT')")
	@RequestMapping(method = RequestMethod.DELETE, value = "/{bedId}")
	public ResponseVO deleteBed(@PathVariable Long bedId) {
		bedService.deleteBed(bedId);
		return new ResponseVO(Status.SUCCESS, "Bed deleted successfully");
	}
}
