package com.ehms.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Customer;
import com.ehms.model.DrugMaster;
import com.ehms.model.Hospital;
import com.ehms.repositories.DrugMasterRepository;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.DrugMasterVO;
import com.ehms.vos.PaginationResponseDto;

@Service
public class DrugMasterService {
	@Autowired
	private DrugMasterRepository drugMasterRepository;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public PaginationResponseDto<DrugMasterVO> getDrugMasterDetails(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId, Long hospitalId) {
		PaginationResponseDto<DrugMasterVO> result = new PaginationResponseDto<DrugMasterVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<DrugMasterVO> drugMasters = drugMasterRepository.findDrugMasters("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (drugMasters != null) {
			result.setCount(drugMasters.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(drugMasters.getTotalElements());
			result.setTotalPages(drugMasters.getTotalPages());
			List<DrugMasterVO> drugMastersList = drugMasters.getContent();
			result.setItems(drugMastersList);
		}
		return result;
	}

	public DrugMasterVO saveUpdateDrugMaster(Long customerId, Long hospitalId, DrugMasterVO drugMasterVO) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		DrugMaster drugMaster = getDrugMasterEntity(drugMasterVO);
		drugMasterRepository.save(drugMaster);
		drugMasterVO.setDrugMasterId(drugMaster.getDrugMasterId());
		return drugMasterVO;
	}

	public DrugMasterVO getDrugMasterById(Long customerId, Long hospitalId, Long drugMasterId) {
		Assert.notNull(drugMasterId, "drug master ID not provided");
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		DrugMaster drugMaster = drugMasterRepository.findOne(drugMasterId);
		Assert.notNull(drugMaster, "drug master not found");
		return getDrugMasterVO(drugMaster);
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "genericName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "genericName");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	private DrugMasterVO getDrugMasterVO(DrugMaster drugMaster) {
		DrugMasterVO vo = null;
		if (drugMaster != null) {
			vo = new DrugMasterVO();
			vo.setDrugMasterId(drugMaster.getDrugMasterId());
			vo.setBatchNumber(drugMaster.getBatchNumber());
			vo.setDrugTypeId(drugMaster.getDrugTypeId());
			vo.setBrandName(drugMaster.getBrandName());
			vo.setCategoryId(drugMaster.getCategoryId());
			vo.setCostPriceInclAllTax(drugMaster.getCostPriceInclAllTax());
			vo.setDiseaseId(drugMaster.getDiseaseId());
			vo.setExpiryDate(DateTimeUtil.formatDate(drugMaster.getExpiryDate()));
			vo.setGenericName(drugMaster.getGenericName());
			vo.setMedicineName(drugMaster.getMedicineName());
			vo.setMaxOrder(drugMaster.getMaxOrder());
			vo.setMinOrder(drugMaster.getMinOrder());
			vo.setMrp(drugMaster.getMrp());
			vo.setPackingId(drugMaster.getPackingId());
			vo.setPurchasePrice(drugMaster.getPurchasePrice());
			vo.setQty(drugMaster.getQty());
			vo.setStoreNameId(drugMaster.getStoreNameId());
			vo.setStrength(drugMaster.getStrength());
			vo.setTaxInPercent(drugMaster.getTaxInPercent());
			vo.setTaxInRupees(drugMaster.getTaxInRupees());
			vo.setUnit(drugMaster.getUnit());
			if (drugMaster.getCustomer() != null) {
				vo.setCustomerId(drugMaster.getCustomer().getCustomerId());
			}
			if (drugMaster.getHospital() != null) {
				vo.setHospitalId(drugMaster.getHospital().getHospitalId());
			}
		}
		return vo;
	}

	private DrugMaster getDrugMasterEntity(DrugMasterVO drugMasterVO) throws EhmsBusinessException {
		DrugMaster drugMaster = null;
		if (drugMasterVO.getDrugMasterId() == null) {
			drugMaster = new DrugMaster();
		} else {
			drugMaster = drugMasterRepository.findOne(drugMasterVO.getDrugMasterId());
			Assert.notNull(drugMaster, "drug master with id: " + drugMasterVO.getDrugMasterId() + " not found.");
		}
		drugMaster.setBatchNumber(drugMasterVO.getBatchNumber());
		drugMaster.setBrandName(drugMasterVO.getBrandName());
		drugMaster.setCategoryId(drugMasterVO.getCategoryId());
		drugMaster.setCostPriceInclAllTax(drugMasterVO.getCostPriceInclAllTax());
		drugMaster.setDiseaseId(drugMasterVO.getDiseaseId());
		drugMaster.setDrugMasterId(drugMasterVO.getDrugMasterId());
		drugMaster.setDrugTypeId(drugMasterVO.getDrugTypeId());
		try {
			Assert.notNull(drugMasterVO.getExpiryDate(), "Expiry Date not provided.");
			Date expiryDate = DateTimeUtil.parseDate(drugMasterVO.getExpiryDate());
			if (expiryDate != null) {
				drugMaster.setExpiryDate(expiryDate);
			}
		} catch (ParseException e) {
			logger.error("ParseException for ExpiryDate Inside getDrugMasterEntity() : " + drugMasterVO.getExpiryDate(), e);
			throw new EhmsBusinessException(e, "Unable to parse date while saving/updating Drug master.");
		}
		drugMaster.setGenericName(drugMasterVO.getGenericName());
		drugMaster.setMaxOrder(drugMasterVO.getMaxOrder());
		drugMaster.setMinOrder(drugMasterVO.getMinOrder());
		drugMaster.setMedicineName(drugMasterVO.getMedicineName());
		drugMaster.setMrp(drugMasterVO.getMrp());
		drugMaster.setPackingId(drugMasterVO.getPackingId());
		drugMaster.setPurchasePrice(drugMasterVO.getPurchasePrice());
		drugMaster.setQty(drugMasterVO.getQty());
		drugMaster.setStoreNameId(drugMasterVO.getStoreNameId());
		drugMaster.setStrength(drugMasterVO.getStrength());
		drugMaster.setTaxInPercent(drugMasterVO.getTaxInPercent());
		drugMaster.setTaxInRupees(drugMasterVO.getTaxInRupees());
		drugMaster.setUnit(drugMasterVO.getUnit());
		drugMaster.setStrength(drugMasterVO.getStrength());
		Customer customer = new Customer(drugMasterVO.getCustomerId());
		drugMaster.setCustomer(customer);
		Hospital hospital = new Hospital(drugMasterVO.getHospitalId());
		drugMaster.setHospital(hospital);
		return drugMaster;
	}

	public List<DrugMasterVO> getDrugMasterList(Long customerId, Long hospitalId) {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		List<DrugMasterVO> drugMasterList = new ArrayList<DrugMasterVO>();
		drugMasterList = drugMasterRepository.getDrugMasters(customerId, hospitalId);
		return drugMasterList;
	}
}
