package com.ehms.services.role;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import com.ehms.model.Role;
import com.ehms.repositories.CustomerRepository;
import com.ehms.repositories.HospitalRepository;
import com.ehms.repositories.role.LinkRolePrivilegeRepository;
import com.ehms.repositories.role.RoleRepository;
import com.ehms.security.JwtUser;
import com.ehms.services.user.UserService;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ValidationCheckResponseVO;
import com.ehms.vos.role.RoleVO;

@Service
public class RoleService {

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private HospitalRepository hospitalRepository;

	@Autowired
	private LinkRolePrivilegeRepository linkRolePrivilegeRepository;

	@Autowired
	private UserService userService;

	public RoleVO getRoleDetails(Long id) {
		JwtUser user = userService.getCurrentUser();
		Role role = roleRepository.findOne(id);
		Assert.notNull(role, "Role not found for id: " + id);
		if (role.getIsSystemRole()) {
			Assert.isTrue(user.getCustomerId() == null, "User dose not have access to the role");
		} else {
			Assert.isTrue(!role.getIsSystemRole() && user.getCustomerId() != null && role.getCustomer() != null && role.getCustomer().getCustomerId() != null
					&& role.getCustomer().getCustomerId().equals(user.getCustomerId()), "User dose not have access to the role");
			Assert.isTrue(
					!role.getIsSystemRole() && user.getCustomerId() != null && role.getCustomer() != null && role.getCustomer().getCustomerId() != null
							&& role.getCustomer().getCustomerId().equals(user.getCustomerId()) && ((role.getHospital() == null) || (role.getHospital() != null
									&& role.getHospital().getHospitalId() != null && role.getHospital().getHospitalId().equals(user.getHospitalId()))),
					"User dose not have access to the role");
		}
		return new RoleVO(role);
	}

	@Transactional
	public void saveUpdateRole(RoleVO roleVO, boolean isNewRole) {
		JwtUser user = userService.getCurrentUser();
		Role role = null;
		if (!isNewRole) {
			Assert.isTrue(roleVO != null && roleVO.getRoleId() != null, "Role information not provided");
			role = roleRepository.findOne(roleVO.getRoleId());
			Assert.notNull(role, "Role not found.");
			linkRolePrivilegeRepository.deleteByRoleId(roleVO.getRoleId());
		} else {
			role = new Role();
		}
		role = roleVO.getRoleEntity(role);
		if (user.getCustomerId() != null) {
			role.setCustomer(customerRepository.findOne(user.getCustomerId()));
		}
		if (user.getHospitalId() != null) {
			role.setHospital(hospitalRepository.findOne(user.getHospitalId()));
		}
		roleRepository.save(role);
		roleVO.setRoleId(role.getRoleId());
	}

	public PaginationResponseDto<RoleVO> getRolesList(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText) {
		JwtUser user = userService.getCurrentUser();

		PaginationResponseDto<RoleVO> result = new PaginationResponseDto<RoleVO>();
		Sort sort = new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, sortOn);
		PageRequest pageRequest = new PageRequest(page, pageSize, sort);
		Page<RoleVO> roles = null;
		Assert.notNull(user.getCustomerId(), "Can not determin the customer id of the loggedin customer.");
		if (user.getCustomerId() != null && user.getHospitalId() == null) {
			roles = roleRepository.findCustomerRoles("%" + searchText + "%",
					user.getCustomerId(), pageRequest);
		} else if (user.getCustomerId() != null && user.getHospitalId() != null) {
			roles = roleRepository.findHospitalRoles("%" + searchText + "%", user.getCustomerId(), user.getHospitalId(), pageRequest);
		}
		if (roles != null) {
			result.setCount(roles.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(roles.getTotalElements());
			result.setTotalPages(roles.getTotalPages());
			List<RoleVO> roleList = roles.getContent();
			result.setItems(roleList);
		}
		return result;
	}

	public ValidationCheckResponseVO validateRoleName(RoleVO roleVO) {
		Assert.isTrue(roleVO != null && roleVO.getRoleName() != null && !roleVO.getRoleName().isEmpty(), "Role name not provided");
		Long roleId = null;
		if (roleVO.getCustomerId() == null && roleVO.getHospitalId() == null) {
			if (roleVO.getRoleId() == null) {
				roleId = roleRepository.findRolesByName(roleVO.getRoleName());
			} else {
				roleId = roleRepository.findRoleByNameAndId(roleVO.getRoleName(), roleVO.getRoleId());
			}

		} else if (roleVO.getCustomerId() != null && roleVO.getHospitalId() == null) {
			if (roleVO.getRoleId() == null) {
				roleId = roleRepository.findRoleByNameForCustomer(roleVO.getRoleName(), roleVO.getCustomerId());
			} else {
				roleId = roleRepository.findRoleByNameAndIdForCustomer(roleVO.getRoleName(), roleVO.getRoleId(), roleVO.getCustomerId());
			}
		} else if (roleVO.getCustomerId() != null && roleVO.getHospitalId() != null) {
			if (roleVO.getRoleId() == null) {
				roleId = roleRepository.findRoleByNameForCustomerAndHospital(roleVO.getRoleName(), roleVO.getCustomerId(), roleVO.getHospitalId());
			} else {
				roleId = roleRepository.findRoleByNameAndIdForCustomerAndHospital(roleVO.getRoleName(), roleVO.getRoleId(), roleVO.getCustomerId(), roleVO.getHospitalId());
			}
		} else {
			throw new IllegalArgumentException("Information not consistent");
		}

		if (roleId != null) {
			return new ValidationCheckResponseVO(false);
		} else {
			return new ValidationCheckResponseVO(true);
		}
	}

	public Role getSystemRoleDetailsForRoleName(String roleName) {
		return roleRepository.getSystemRoleDetailsForRoleName(roleName);
	}

	public Role findOne(Long roleId) {
		return roleRepository.findOne(roleId);
	}

	public List<RoleVO> getRolesList() {
		List<RoleVO> result = new ArrayList<>();
		JwtUser user = userService.getCurrentUser();
		Assert.notNull(user, "User not found");
		Long customerId = user.getCustomerId();
		Long hospitalId = user.getHospitalId();
		if (customerId == null && hospitalId == null) {
			result = roleRepository.getSystemRoleList();
		} else if (customerId != null && hospitalId == null) {
			result = roleRepository.getCustomerRoleList(customerId);
		} else if (customerId != null && hospitalId != null) {
			result = roleRepository.getHospitalRoleList(customerId, hospitalId);
		} else {
			throw new IllegalArgumentException("User customer or hospital information is not consistant.");
		}
		return result;
	}

		public RoleVO getSystemRoleDetails(Long id) {
			Role role = roleRepository.findSystemRole(id);
			Assert.notNull(role, "Role not found or role is not a system role: for" + id);
			return new RoleVO(role);
		}

		@Transactional
		public void saveUpdateSystemRole(RoleVO roleVO, boolean isNewRole) {
			Role role = null;
			if (!isNewRole) {
				Assert.isTrue(roleVO != null && roleVO.getRoleId() != null, "Role information not provided");
				role = roleRepository.findSystemRole(roleVO.getRoleId());
				Assert.notNull(role, "Role not found or role is not a system role.");
				linkRolePrivilegeRepository.deleteByRoleId(roleVO.getRoleId());
			} else {
				role = new Role();
			}
			role = roleVO.getRoleEntity(role);
			role.setIsSystemRole(true);
			roleRepository.save(role);
			roleVO.setRoleId(role.getRoleId());
		}

		public List<RoleVO> getRolesForsystemUser() {
			List<RoleVO> result = new ArrayList<>();
			JwtUser user = userService.getCurrentUser();
			Assert.notNull(user, "User not found");
			Long customerId = user.getCustomerId();
			Long hospitalId = user.getHospitalId();
			if (customerId == null && hospitalId == null) {
				result = roleRepository.getSystemRoleList();
			}

			return result;
		}

		public PaginationResponseDto<RoleVO> getSystemRolesList(Integer page, Integer pageSize, String sortOn,
				String sortOrder, String searchText) {
			JwtUser user = userService.getCurrentUser();
			Assert.notNull(user, "User not found");
			PaginationResponseDto<RoleVO> result = new PaginationResponseDto<RoleVO>();
			Sort sort = new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, sortOn);
			PageRequest pageRequest = new PageRequest(page, pageSize, sort);
			Page<RoleVO> roles = null;
			roles = roleRepository.findSystemRoles("%" + searchText + "%",pageRequest);

			if (roles != null) {
				result.setCount(roles.getNumberOfElements());
				result.setPage(page);
				result.setTotalItems(roles.getTotalElements());
				result.setTotalPages(roles.getTotalPages());
				List<RoleVO> roleList = roles.getContent();
				result.setItems(roleList);
			}
			return result;
		}

	}

	
	
	
	

