package com.ehms.services.role;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.repositories.role.PrivilegeRepository;
import com.ehms.security.JwtUser;
import com.ehms.services.user.UserService;
import com.ehms.vos.role.PrivilegeVO;

@Service
public class PrivilegeService {
	
	@Autowired
	private PrivilegeRepository privilegeRepository;
	
	@Autowired
	private UserService userService;
	
	public List<PrivilegeVO> getAvailablePrivileges() {
		JwtUser user = userService.getCurrentUser();
		Assert.notNull(user, "User not found.");
		Assert.notNull(user.getCustomerId(), "Customer not found.");
		return privilegeRepository.getAvailablePrivileges(user.getCustomerId());
	}

	public List<PrivilegeVO> getAvailableSystemPrivileges() {
		JwtUser user = userService.getCurrentUser();
		Assert.notNull(user, "User not found.");
		Assert.isTrue(user.getCustomerId() == null, "User is not a system user");
		Assert.isTrue(user.getHospitalId() == null, "User is not a system user");
		return privilegeRepository.getAvailableSystemPrivileges();
	
	}
}
