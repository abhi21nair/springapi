package com.ehms.services.misctype;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ehms.model.MiscType;
import com.ehms.repositories.misctype.MiscTypeRepository;

@Service
public class MiscTypeService {

	@Autowired
	private MiscTypeRepository miscTypeRepository;

	public List<MiscType> getAvailableMiscTypes() {
		return miscTypeRepository.findAllByOrderByMiscTypeName();
	}

}
