package com.ehms.services.misctype;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.model.MiscType;
import com.ehms.model.MiscValue;
import com.ehms.repositories.misctype.MiscTypeRepository;
import com.ehms.repositories.misctype.MiscValueRepository;
import com.ehms.security.JwtUser;
import com.ehms.services.user.UserService;
import com.ehms.vos.misctype.MiscTypeValuesVO;
import com.ehms.vos.misctype.MiscValueVO;

@Service
public class MiscValueService {

	@Autowired
	private MiscValueRepository miscValueRepository;

	@Autowired
	private MiscTypeRepository miscTypeRepository;

	@Autowired
	private UserService userService;

	public List<MiscValueVO> getMiscValues(Long miscTypeId) {
		Assert.notNull(miscTypeId, "Type not specified");
		List<MiscValueVO> result = null;
		JwtUser user = userService.getCurrentUser();
		Assert.notNull(user, "User not found");
		Assert.notNull(user.getCustomerId(), "Customer ID not found.");
		if (user.getHospitalId() == null) {
			result = miscValueRepository.findCustomerMiscValues(miscTypeId, user.getCustomerId());
		} else {
			result = miscValueRepository.findHospitalMiscValues(miscTypeId, user.getCustomerId(), user.getHospitalId());
		}
		return result;
	}

	public MiscValueVO saveUpdateMiscValues(Long miscTypeId, MiscValueVO miscValueVO, boolean isNewValue) {
		Assert.notNull(miscTypeId, "Type not specified");
		JwtUser user = userService.getCurrentUser();
		Assert.notNull(user, "User not found");
		Assert.notNull(user.getCustomerId(), "Customer ID not found.");
		MiscValue value = null;
		if (isNewValue) {
			value = new MiscValue();
			value.setCustomerId(user.getCustomerId());
			value.setHospitalId(user.getHospitalId());
		} else {
			value = miscValueRepository.findOne(miscValueVO.getMiscTypeValueId());
		}
		value.setMiscTypeId(miscValueVO.getMiscTypeId());
		value.setMiscValue(miscValueVO.getMiscValue());
		miscValueRepository.save(value);
		miscValueVO.setMiscTypeValueId(value.getMiscTypeValueId());
		miscValueVO.setCustomerId(user.getCustomerId());
		miscValueVO.setHospitalId(user.getHospitalId());
		return miscValueVO;
	}

	public List<MiscTypeValuesVO> getMiscValues() {
		List<MiscTypeValuesVO> result = new ArrayList<>();
		Map<Long, List<MiscValueVO>> resultMap = new HashMap<>();
		Map<Long, MiscType> miscTypeMap = new HashMap<>();
		JwtUser user = userService.getCurrentUser();
		Assert.notNull(user, "User not found.");
		List<MiscType> miscTypes = miscTypeRepository.findAll();
		Assert.notNull(miscTypes, "Types not found.");
		List<MiscValue> values = findValueTypesForUser(user);
		populateIntermediateMaps(resultMap, miscTypeMap, miscTypes, values);
		populateResultList(result, resultMap, miscTypeMap);
		return result;
	}

	private void populateIntermediateMaps(Map<Long, List<MiscValueVO>> resultMap, Map<Long, MiscType> miscTypeMap, List<MiscType> miscTypes, List<MiscValue> values) {
		for (MiscType miscType : miscTypes) {
			resultMap.put(miscType.getMiscTypeId(), new ArrayList<MiscValueVO>());
			miscTypeMap.put(miscType.getMiscTypeId(), miscType);
		}
		if (values != null && !values.isEmpty()) {
			for (MiscValue miscValue : values) {
				List<MiscValueVO> miscValues = resultMap.get(miscValue.getMiscTypeId());
				if (miscValues == null) {
					miscValues = new ArrayList<>();
					resultMap.put(miscValue.getMiscTypeId(), miscValues);
				}
				miscValues.add(new MiscValueVO(miscValue));
			}
		}
	}

	private void populateResultList(List<MiscTypeValuesVO> result, Map<Long, List<MiscValueVO>> resultMap, Map<Long, MiscType> miscTypeMap) {
		Set<Long> miscTypeIds = resultMap.keySet();
		for (Long miscTypeId : miscTypeIds) {
			List<MiscValueVO> miscValues = resultMap.get(miscTypeId);
			if (miscValues != null && !miscValues.isEmpty()) {
				MiscType miscType = miscTypeMap.get(miscTypeId);
				Assert.notNull(miscType, "Invalid Misc Type");
				result.add(new MiscTypeValuesVO(miscType, miscValues));
			}
		}
	}

	private List<MiscValue> findValueTypesForUser(JwtUser user) {
		List<MiscValue> values = null;
		if (user.getCustomerId() == null && user.getHospitalId() == null) {
			// System level user
			values = miscValueRepository.findByCustomerIdIsNullAndHospitalIdIsNull();
		} else if (user.getCustomerId() != null && user.getHospitalId() == null) {
			// Customer level user
			values = miscValueRepository.findByCustomerIdAndHospitalIdIsNull(user.getCustomerId());
		} else if (user.getCustomerId() != null && user.getHospitalId() != null) {
			// Hospital level user
			values = miscValueRepository.findByCustomerIdOrHospitalId(user.getCustomerId(), user.getHospitalId());
		} else {
			throw new IllegalArgumentException("Customer or hospital information not consistance for the user");
		}
		return values;
	}
}
