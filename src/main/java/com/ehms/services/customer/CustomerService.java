package com.ehms.services.customer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Address;
import com.ehms.model.BuyingPreference;
import com.ehms.model.BuyingType;
import com.ehms.model.Customer;
import com.ehms.model.LinkCustomerModule;
import com.ehms.model.Module;
import com.ehms.model.SubscriptionType;
import com.ehms.model.User;
import com.ehms.repositories.AddressRepository;
import com.ehms.repositories.CustomerRepository;
import com.ehms.repositories.LinkCutomerModuleRepository;
import com.ehms.security.JwtUser;
import com.ehms.services.user.UserService;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.AddressVO;
import com.ehms.vos.CustomerVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ValidationCheckResponseVO;
import com.ehms.vos.user.UserVO;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private LinkCutomerModuleRepository linkCutomerModuleRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private AddressRepository addressRepository;

	@Value("${organization.code.prefix}")
	private String organizationCode;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public CustomerVO getCustomersDetails(Long id) {
		CustomerVO customerVO = new CustomerVO();
		Customer customer = customerRepository.findOne(id);
		customerVO = getCustomerVO(customerVO, customer);
		return customerVO;
	}

	public PaginationResponseDto<CustomerVO> getCustomers(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText) {
		PaginationResponseDto<CustomerVO> result = new PaginationResponseDto<CustomerVO>();
		Sort sort = new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, sortOn);
		PageRequest pageRequest = new PageRequest(page, pageSize, sort);
		Page<CustomerVO> customers = customerRepository.findCustomers("%" + searchText + "%", "%" + searchText + "%", pageRequest);
		if (customers != null) {
			result.setCount(customers.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(customers.getTotalElements());
			result.setTotalPages(customers.getTotalPages());
			List<CustomerVO> customersList = customers.getContent();
			result.setItems(customersList);
		}
		return result;
	}

	@Transactional
	public void saveUpdateCustomers(CustomerVO customerVO, boolean isNewCustomer) throws EhmsBusinessException {
		Customer customer = getCustomerEntity(customerVO, isNewCustomer);
		customerRepository.save(customer);
		if (isNewCustomer) {
			customer.setOrganizationCode(organizationCode + customer.getCustomerId());
			customerRepository.save(customer);
			customerVO.setOrganizationCode(organizationCode + customer.getCustomerId());
		}
		User user = userService.saveUpdateAdminUser(customer.getCustomerId(), /*hospitalId*/null, customerVO.getKeyUser(), /*isCustomerAdmin*/true, /*isHospitalAdmin*/false);
		customer.setKeyUser(user);
		customerRepository.save(customer);
		AddressVO addressVO = customerVO.getAddress();
		UserVO userVO = customerVO.getKeyUser();
		customerVO.setCustomerId(customer.getCustomerId());
		customerVO.setOrganizationCode(customer.getOrganizationCode());
		if (customer.getAddress() != null) {
			addressVO.setAddressId(customer.getAddress().getAddressId());
		}
		if (user != null) {
			userVO.setUserId(user.getUserId());
		}
	}

	@Transactional
	public ValidationCheckResponseVO validateCustomerName(CustomerVO customerVO) {
		Assert.isTrue(customerVO != null && customerVO.getCustomerName() != null && !customerVO.getCustomerName().isEmpty(), "Customer name not provided");
		Customer customer = null;
		if (customerVO.getCustomerId() == null) {
			customer = customerRepository.findByCustomerNameIgnoreCase(customerVO.getCustomerName());
		} else {
			customer = customerRepository.findByCustomerIdNotAndCustomerNameIgnoreCase(customerVO.getCustomerId(), customerVO.getCustomerName());
		}
		if (customer != null) {
			return new ValidationCheckResponseVO(false);
		} else {
			return new ValidationCheckResponseVO(true);
		}
	}

	private CustomerVO getCustomerVO(CustomerVO customerVO, Customer customer) {
		customerVO.setAddress(new AddressVO(customer.getAddress()));
		if (customer.getBuyingPreference() != null) {
			customerVO.setBuyingPreferenceId(customer.getBuyingPreference().getBuyingPreferenceId());
		}
		if (customer.getBuyingType() != null) {
			customerVO.setBuyingTypeId(customer.getBuyingType().getBuyingTypeId());
		}
		
		customerVO.setBuyingTypeQuantity(customer.getBuyingTypeQuantity());
		customerVO.setKeyUser(userService.getUserVO(customer.getKeyUser()));
		if (customer.getCountryCodeForContactNumber() != null) {
			customerVO.setCountryCodeForContactNumber(customer.getCountryCodeForContactNumber());
		}
		if (customer.getCountryCodeForSecondaryNumber() != null) {
			customerVO.setCountryCodeForSecondaryNumber(customer.getCountryCodeForSecondaryNumber());
		}
		customerVO.setContactNumber(customer.getContactNumber());
		customerVO.setSecContactNumber((customer.getSecContactNumber()));
		customerVO.setCustomerId(customer.getCustomerId());
		customerVO.setCustomerName(customer.getCustomerName());
		customerVO.setIsActive(customer.getIsActive());
		customerVO.setModuleIds(getModuleIds(customer));
		customerVO.setDemoDays(customer.getDemoDays());
		customerVO.setOrganizationCode(customer.getOrganizationCode());
		customerVO.setReferralPersonName(customer.getReferralPersonName());
		if (customer.getReferralType() != null) {
			customerVO.setReferralType(customer.getReferralType());
		}
		if (customer.getSubscriptionType() != null) {
			customerVO.setSubscriptionTypeId(customer.getSubscriptionType().getSubscriptionTypeId());
		}
		customerVO.setExpiryDate(DateTimeUtil.formatDate(customer.getExpiryDate()));
		return customerVO;
	}
	
	private List<Long> getModuleIds(Customer customer) {
		List<Long> moduleIds = null;
		List<LinkCustomerModule> linkCustomerModules = customer.getLinkCustomerModules();
		if (linkCustomerModules != null && !linkCustomerModules.isEmpty()) {
			moduleIds = new ArrayList<Long>();
			for (LinkCustomerModule linkCustomerModule : linkCustomerModules) {
				if (linkCustomerModule.getModule() != null) {
					moduleIds.add(linkCustomerModule.getModule().getModuleId());
				}
			}
		}
		return moduleIds;
	}

	private Customer getCustomerEntity(CustomerVO customerVO, Boolean isNewCustomer) throws EhmsBusinessException {
		Customer customer = null;
		JwtUser user = userService.getCurrentUser();
		Assert.notNull(user, "User not logged-in.");
		if (isNewCustomer) {
			customer = new Customer();
			customer.setOrganizationCode(new Date().toString());
		} else {
			Assert.notNull(customerVO.getCustomerId(), "Customer Id not found in request.");
			customer = customerRepository.findOne(customerVO.getCustomerId());
			Assert.notNull("Customer not found in database");
		}
		updateExpiryDate(customerVO, isNewCustomer, customer);
		AddressVO addressVO = customerVO.getAddress();
		Address address = null;
		if (isNewCustomer) {
			address = new Address();
		} else {
			address = addressRepository.findOne(addressVO.getAddressId());
		}
		customer.setAddress(addressVO.getAddressEntity(address, addressVO));
		BuyingPreference buyingPreference = new BuyingPreference(customerVO.getBuyingPreferenceId());
		customer.setBuyingPreference(buyingPreference);
		BuyingType buyingType = new BuyingType(customerVO.getBuyingTypeId());
		customer.setBuyingType(buyingType);
		customer.setBuyingTypeQuantity(customerVO.getBuyingTypeQuantity());
		
		customer.setContactNumber(customerVO.getContactNumber());
		customer.setCountryCodeForContactNumber(customerVO.getCountryCodeForContactNumber());
		if (customerVO.getCountryCodeForSecondaryNumber() != null) {
			customer.setCountryCodeForSecondaryNumber(customerVO.getCountryCodeForSecondaryNumber());
		}
		customer.setSecContactNumber(customerVO.getSecContactNumber());
		customer.setDemoDays(customerVO.getDemoDays());
		customer.setCustomerName(customerVO.getCustomerName());
		customer.setReferralPersonName(customerVO.getReferralPersonName());
		SubscriptionType subscriptionType = new SubscriptionType(customerVO.getSubscriptionTypeId());
		customer.setSubscriptionType(subscriptionType);
		customer.setReferralType(customerVO.getReferralType());

		customer.setIsActive(customerVO.getIsActive());
		if (!isNewCustomer) {
			linkCutomerModuleRepository.deleteByCustomerId(customerVO.getCustomerId());
		}
		customer.setLinkCustomerModules(getModules(customerVO, isNewCustomer, customer));
		return customer;
	}

	private void updateExpiryDate(CustomerVO customerVO, Boolean isNewCustomer, Customer customer) throws EhmsBusinessException {
		if (isNewCustomer) {
			if (BuyingPreference.BUYING_PREFERENCE_IMMEDIATE.equals(customerVO.getBuyingPreferenceId())) {
				if (SubscriptionType.SUBSCRIPTION_TYPE_MONTHLY.equals(customerVO.getSubscriptionTypeId())) {
					updateExpiryDateAsPerSubscriptionDetails(customer, Calendar.MONTH, 1);
				} else if (SubscriptionType.SUBSCRIPTION_TYPE_YEARLY.equals(customerVO.getSubscriptionTypeId())) {
					updateExpiryDateAsPerSubscriptionDetails(customer, Calendar.YEAR, 1);
				} else if (SubscriptionType.SUBSCRIPTION_TYPE_LIFETIME.equals(customerVO.getSubscriptionTypeId())) {
					updateExpiryDateAsPerSubscriptionDetails(customer, Calendar.YEAR, 5);
				} else {
					logger.error("Invalid subscription type " + customerVO.getSubscriptionTypeId() + " inside updateExpiryDate()");
					throw new EhmsBusinessException("Invalid subscription type: " + customerVO.getSubscriptionTypeId());
				}
			} else if (BuyingPreference.BUYING_PREFERENCE_TRIAL.equals(customerVO.getBuyingPreferenceId())) {
				updateExpiryDateAsPerSubscriptionDetails(customer, Calendar.DATE, customerVO.getDemoDays());
			} else {
				logger.error("Invalid buying type " + customerVO.getBuyingTypeId() + " inside updateExpiryDate()");
				throw new EhmsBusinessException("Invalid buying type: " + customerVO.getBuyingTypeId());
			}
		} else {
			if (customer.getBuyingPreference() != null || customerVO.getBuyingPreferenceId() != null) {
				Long oldBuyingPrefId = customer.getBuyingPreference().getBuyingPreferenceId();
				Long newBuyingPrefId = customerVO.getBuyingPreferenceId();

				if (BuyingPreference.BUYING_PREFERENCE_TRIAL.equals(oldBuyingPrefId) && BuyingPreference.BUYING_PREFERENCE_TRIAL.equals(newBuyingPrefId)) {
					// Do nothing as the buying type has not changed
				} else if (BuyingPreference.BUYING_PREFERENCE_TRIAL.equals(oldBuyingPrefId) && BuyingPreference.BUYING_PREFERENCE_IMMEDIATE.equals(newBuyingPrefId)) {
					// Set today's EOD as expiry date
					customer.setExpiryDate(DateTimeUtil.getEndTime(new Date()));
				} else if (BuyingPreference.BUYING_PREFERENCE_IMMEDIATE.equals(oldBuyingPrefId) && BuyingPreference.BUYING_PREFERENCE_TRIAL.equals(newBuyingPrefId)) {
					// Set to trial for demo days
					updateExpiryDateAsPerSubscriptionDetails(customer, Calendar.DATE, customerVO.getDemoDays());
				} else if (BuyingPreference.BUYING_PREFERENCE_IMMEDIATE.equals(oldBuyingPrefId) && BuyingPreference.BUYING_PREFERENCE_IMMEDIATE.equals(newBuyingPrefId)) {
					if (customer.getSubscriptionType() != null && customerVO.getSubscriptionTypeId() != null) {
						Long oldSubscriptionTypeId = customer.getSubscriptionType().getSubscriptionTypeId();
						Long newSubscriptionTypeId = customerVO.getSubscriptionTypeId();
						if (oldSubscriptionTypeId.equals(newSubscriptionTypeId)) {
							// DO nothing as the buying type and subscription
							// type is same
						} else {
							if (SubscriptionType.SUBSCRIPTION_TYPE_MONTHLY.equals(customerVO.getSubscriptionTypeId())) {
								updateExpiryDateAsPerSubscriptionDetails(customer, Calendar.MONTH, 1);
							} else if (SubscriptionType.SUBSCRIPTION_TYPE_YEARLY.equals(customerVO.getSubscriptionTypeId())) {
								updateExpiryDateAsPerSubscriptionDetails(customer, Calendar.YEAR, 1);
							} else if (SubscriptionType.SUBSCRIPTION_TYPE_LIFETIME.equals(customerVO.getSubscriptionTypeId())) {
								updateExpiryDateAsPerSubscriptionDetails(customer, Calendar.YEAR, 5);
							} else {
								logger.error("Invalid subscription type " + customerVO.getSubscriptionTypeId() + " inside updateExpiryDate()");
								throw new EhmsBusinessException("Invalid subscription type: " + customerVO.getSubscriptionTypeId());
							}
						}
					} else {
						logger.error("Subscription type not set for the customer " + customerVO.getCustomerName());
						throw new EhmsBusinessException("Subscription type not set for the customer.");
					}
				} else {
					logger.error("Invalid buying type " + customerVO.getBuyingTypeId() + " inside updateExpiryDate()");
					throw new EhmsBusinessException("Invalid buying type.");
				}
			} else {
				logger.error("Buying preference not set for the customer " + customerVO.getCustomerName());
				throw new EhmsBusinessException("Buying preference not set for the customer");
			}
		}
	}

	private void updateExpiryDateAsPerSubscriptionDetails(Customer customer, int calenderField, int amountToAdd) {
		Date expiryDate = DateTimeUtil.addCalanderField(calenderField, amountToAdd);
		customer.setExpiryDate(DateTimeUtil.getEndTime(expiryDate));
	}

	private List<LinkCustomerModule> getModules(CustomerVO customerVO, Boolean isNewCustomer, Customer customer) {
		List<LinkCustomerModule> modules = null;
		List<Long> moduleIds = customerVO.getModuleIds();
		if (moduleIds != null && !moduleIds.isEmpty()) {
			modules = new ArrayList<LinkCustomerModule>();
			for (Long moduleId : moduleIds) {
				LinkCustomerModule customerModule = new LinkCustomerModule();
				customerModule.setCustomer(customer);
				Module module = new Module(moduleId);
				customerModule.setModule(module);
				modules.add(customerModule);
			}
		}
		return modules;
	}
}
