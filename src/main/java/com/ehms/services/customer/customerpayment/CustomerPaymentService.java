package com.ehms.services.customer.customerpayment;

import java.text.ParseException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Customer;
import com.ehms.model.CustomerPayment;
import com.ehms.model.PaymentMode;
import com.ehms.model.PaymentStatus;
import com.ehms.model.User;
import com.ehms.repositories.CustomerPaymentAuxDao;
import com.ehms.repositories.CustomerPaymentRepository;
import com.ehms.repositories.CustomerRepository;
import com.ehms.repositories.PaymentModeRepository;
import com.ehms.repositories.PaymentStatusRepository;
import com.ehms.security.JwtUser;
import com.ehms.services.user.UserService;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.ApprovalListVO;
import com.ehms.vos.BillingAndPaymentVO;
import com.ehms.vos.CustomerApprovalVO;
import com.ehms.vos.CustomerPaymentVO;
import com.ehms.vos.PaginationResponseDto;

@Service
public class CustomerPaymentService {
	@Autowired
	private CustomerPaymentRepository customerPaymentRepository;

	@Autowired
	private CustomerPaymentAuxDao customerPaymentAuxDao;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private PaymentModeRepository paymentModeRepository;

	@Autowired
	private PaymentStatusRepository paymentStatusRepository;

	@Autowired
	private UserService userService;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public PaginationResponseDto<BillingAndPaymentVO> getCustomerPayments(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText) {
		return customerPaymentAuxDao.getBillingAndPaymentDetails(page, pageSize, sortOn, sortOrder, searchText);
	}

	public PaginationResponseDto<ApprovalListVO> getCustomerPaymentsForApprovalList(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText) {
		return customerPaymentAuxDao.getBillingAndPaymentApprovalDetails(page, pageSize, sortOn, sortOrder, searchText);
	}

	@Transactional
	public CustomerPaymentVO getCustomerPaymentById(Long customerId, Long id) {
		CustomerPaymentVO customerPaymentVO = new CustomerPaymentVO();
		CustomerPayment customerPayment = customerPaymentRepository.findOne(id);
		customerPaymentVO = getCustomerPaymentVO(customerPaymentVO, customerPayment);
		customerPaymentVO.setCustomerPaymentId(customerPayment.getCustomerPaymentId());
		Assert.isTrue(customerPayment.getCustomer() != null && customerPayment.getCustomer().getCustomerId() != null && customerId != null
				&& customerId.equals(customerPayment.getCustomer().getCustomerId()), "Unable to access payment details of another customer");
		Assert.isTrue(customerPaymentRepository.exists(id), "Could not locate customer payment details.");
		return customerPaymentVO;
	}

	@Transactional
	public void saveUpdateCustomerPayments(CustomerPaymentVO customerPaymentVO) throws EhmsBusinessException {
		Boolean isNewCustomerPayment = false;

		if (customerPaymentVO.getCustomerPaymentId() == null) {
			isNewCustomerPayment = true;
		}
		CustomerPayment customerPayment = getCustomerPaymentEntity(customerPaymentVO, isNewCustomerPayment);
		customerPaymentRepository.save(customerPayment);
		customerPaymentVO.setCustomerPaymentId(customerPayment.getCustomerPaymentId());
	}

	@Transactional
	public void saveUpdateCustomerPaymentApproval(CustomerApprovalVO customerPaymentVO) {
		CustomerPayment customerPayment = null;
		JwtUser user = userService.getCurrentUser();
		Assert.notNull(customerPaymentVO.getCustomerPaymentId(), "Payment ID not sent in request.");
		customerPayment = customerPaymentRepository.findOne(customerPaymentVO.getCustomerPaymentId());
		Assert.notNull(customerPayment, "Invalid Payment ID. Details not found.");
		PaymentStatus paymentStatus = new PaymentStatus();
		paymentStatus.setPaymentStatusId(customerPaymentVO.getPaymentStatusId());
		Assert.notNull(customerPayment.getPaymentStatus(), "Customer payment Details not found.");
		Assert.isTrue(customerPayment.getPaymentStatus().getPaymentStatusId().equals(PaymentStatus.PAYMENT_STATUS_PENDING), "payment status is not pending so can not change payment status");
		Assert.isTrue(customerPaymentVO.getPaymentStatusId().equals(PaymentStatus.PAYMENT_STATUS_APPROVED) || customerPaymentVO.getPaymentStatusId().equals(PaymentStatus.PAYMENT_STATUS_REJECTED),
				"Not a valid payment status");
		customerPayment.setPaymentStatus(paymentStatus);
		if (customerPaymentVO.getPaymentStatusId() == PaymentStatus.PAYMENT_STATUS_APPROVED) {
			if (customerPayment.getCustomer() != null) {
				Customer customer = customerRepository.findOne(customerPayment.getCustomer().getCustomerId());
				customer.setExpiryDate(customerPayment.getExpiryDate());
			}
		}
		User actionBy = new User();
		actionBy.setUserId(user.getuserId());
		customerPayment.setActionBy(actionBy);
		customerPayment.setActionTime(new Date());
		customerPaymentRepository.save(customerPayment);
	}

	private CustomerPayment getCustomerPaymentEntity(CustomerPaymentVO customerPaymentVO, Boolean isNewCustomerPayment) throws EhmsBusinessException {
		CustomerPayment customerPayment = null;
		JwtUser user = userService.getCurrentUser();
		if (isNewCustomerPayment) {
			customerPayment = new CustomerPayment();
			customerPayment.setCreatedDt(new Date());
		} else {
			customerPayment = customerPaymentRepository.findOne(customerPaymentVO.getCustomerPaymentId());
		}
		try {
			PaymentStatus paymentStatus = paymentStatusRepository.findOne(customerPaymentVO.getPaymentStatusId());
			customerPayment.setPaymentStatus(paymentStatus);
			Customer customer = customerRepository.findOne(customerPaymentVO.getCustomerId());
			customerPayment.setCustomer(customer);
			customerPayment.setCostOfProduct(customerPaymentVO.getCostOfProduct());
			customerPayment.setGstNumber(customerPaymentVO.getGstNumber());
			customerPayment.setPanNumber(customerPaymentVO.getPanNumber());
			customerPayment.setAmcAddition(customerPaymentVO.getAmcAddition());
			customerPayment.setOtherService1(customerPaymentVO.getOtherService1());
			customerPayment.setOtherService2(customerPaymentVO.getOtherService2());
			customerPayment.setOtherService1Type(customerPaymentVO.getOtherService1Type());
			customerPayment.setOtherService2Type(customerPaymentVO.getOtherService2Type());
			customerPayment.setDiscount(customerPaymentVO.getDiscount());
			if (customerPaymentVO.getDiscountValidFrom() != null) {
				customerPayment.setDiscountValidFrom(DateTimeUtil.parseDate(customerPaymentVO.getDiscountValidFrom()));
			}
			if (customerPaymentVO.getDiscountValidTo() != null) {
				customerPayment.setDiscountValidTo(DateTimeUtil.parseDate(customerPaymentVO.getDiscountValidTo()));
			}
			customerPayment.setDiscountInPercent(customerPaymentVO.getDiscountInPercent());
			customerPayment.setValue(customerPaymentVO.getValue());
			if (customerPaymentVO.getDiscountInPercentValidFrom() != null) {
				customerPayment.setDiscountInPercentValidFrom(DateTimeUtil.parseDate(customerPaymentVO.getDiscountInPercentValidFrom()));
			}
			if (customerPaymentVO.getDiscountInPercentValidTo() != null) {
				customerPayment.setDiscountInPercentValidTo(DateTimeUtil.parseDate(customerPaymentVO.getDiscountInPercentValidTo()));
			}
			customerPayment.setTax1(customerPaymentVO.getTax1());
			customerPayment.setTax2(customerPaymentVO.getTax2());
			customerPayment.setTax3(customerPaymentVO.getTax3());
			customerPayment.setTotalBillingAmount(customerPaymentVO.getTotalBillingAmount());
			User actionBy = new User();
			actionBy.setUserId(user.getuserId());
			customerPayment.setActionBy(actionBy);
			PaymentMode paymentMode = paymentModeRepository.findOne(customerPaymentVO.getPaymentModeId());
			customerPayment.setPaymentMode(paymentMode);
			customerPayment.setActionTime(new Date());
			if (customerPaymentVO.getExpiryDate() != null) {
				customerPayment.setExpiryDate(DateTimeUtil.parseDate(customerPaymentVO.getExpiryDate()));
			}
		} catch (ParseException e) {
			logger.error("ParseException for CustomerPayment Inside getCustomerPaymentEntity() : ", e);
			throw new EhmsBusinessException(e, "Unable to parse date while saving/updating Customer Payment.");
		}
		return customerPayment;

	}

	private CustomerPaymentVO getCustomerPaymentVO(CustomerPaymentVO customerPaymentVO, CustomerPayment customerPayment) {
		customerPaymentVO.setCustomerId(customerPayment.getCustomer().getCustomerId());
		customerPaymentVO.setCostOfProduct(customerPayment.getCostOfProduct());
		customerPaymentVO.setGstNumber(customerPayment.getGstNumber());
		customerPaymentVO.setPanNumber(customerPayment.getPanNumber());
		customerPaymentVO.setPaymentStatusId(customerPayment.getPaymentStatus().getPaymentStatusId());
		customerPaymentVO.setAmcAddition(customerPayment.getAmcAddition());
		customerPaymentVO.setOtherService1(customerPayment.getOtherService1());
		customerPaymentVO.setOtherService2(customerPayment.getOtherService2());
		customerPaymentVO.setOtherService1Type(customerPayment.getOtherService1Type());
		customerPaymentVO.setOtherService2Type(customerPayment.getOtherService2Type());
		customerPaymentVO.setDiscount(customerPayment.getDiscount());
		if (customerPayment.getDiscountValidFrom() != null) {
			customerPaymentVO.setDiscountValidFrom(DateTimeUtil.formatDate(customerPayment.getDiscountValidFrom()));
		}
		if (customerPayment.getDiscountValidTo() != null) {
			customerPaymentVO.setDiscountValidTo(DateTimeUtil.formatDate(customerPayment.getDiscountValidTo()));
		}
		customerPaymentVO.setDiscountInPercent(customerPayment.getDiscountInPercent());
		customerPaymentVO.setValue(customerPayment.getValue());
		if (customerPayment.getDiscountInPercentValidFrom() != null) {
			customerPaymentVO.setDiscountInPercentValidFrom(DateTimeUtil.formatDate(customerPayment.getDiscountInPercentValidFrom()));
		}
		if (customerPayment.getDiscountInPercentValidTo() != null) {
			customerPaymentVO.setDiscountInPercentValidTo(DateTimeUtil.formatDate(customerPayment.getDiscountInPercentValidTo()));
		}
		customerPaymentVO.setTax1(customerPayment.getTax1());
		customerPaymentVO.setTax2(customerPayment.getTax2());
		customerPaymentVO.setTax3(customerPayment.getTax3());
		customerPaymentVO.setTotalBillingAmount(customerPayment.getTotalBillingAmount());
		customerPaymentVO.setPaymentModeId(customerPayment.getPaymentMode().getPaymentModeId());
		customerPaymentVO.setActionTime(DateTimeUtil.formatDate(customerPayment.getActionTime()));
		if (customerPayment.getExpiryDate() != null) {
			customerPaymentVO.setExpiryDate(DateTimeUtil.formatDate(customerPayment.getExpiryDate()));
		}
		return customerPaymentVO;
	}

	public CustomerPaymentVO getLatestCustomerPaymentDetails(Long customerId) {
		return customerPaymentRepository.getLatestCustomerPaymentDetails(customerId, customerId);
	}
}
