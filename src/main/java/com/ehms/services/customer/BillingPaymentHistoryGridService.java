package com.ehms.services.customer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import com.ehms.repositories.BillingPaymentHistoryGridRepository;
import com.ehms.vos.BillingPaymentHistoryGridVO;
import com.ehms.vos.PaginationResponseDto;

@Service
public class BillingPaymentHistoryGridService {

	@Autowired
	private BillingPaymentHistoryGridRepository billingPaymentHistoryGridRepository;

	public PaginationResponseDto<BillingPaymentHistoryGridVO> getGridHistory(Integer page, Integer pageSize,
			String sortOn, String sortOrder, @PathVariable Long customerId) {
		PaginationResponseDto<BillingPaymentHistoryGridVO> result = new PaginationResponseDto<BillingPaymentHistoryGridVO>();
		Sort sort = new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, sortOn);
		PageRequest pageRequest = new PageRequest(page, pageSize, sort);
		Page<BillingPaymentHistoryGridVO> gridHistory = billingPaymentHistoryGridRepository.findGridHistory(customerId,
				pageRequest);
		if (gridHistory != null) {
			result.setCount(gridHistory.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(gridHistory.getTotalElements());
			result.setTotalPages(gridHistory.getTotalPages());
			List<BillingPaymentHistoryGridVO> gridHistoryList = gridHistory.getContent();
			result.setItems(gridHistoryList);
		}
		return result;
	}

}
