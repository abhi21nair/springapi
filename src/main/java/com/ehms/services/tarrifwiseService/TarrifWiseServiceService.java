package com.ehms.services.tarrifwiseService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.model.Customer;
import com.ehms.model.Hospital;
import com.ehms.model.MiscValue;
import com.ehms.model.TarrifWiseService;
import com.ehms.repositories.TarrifWiseServiceRepository;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.TarrifWiseServiceVO;

@Service
public class TarrifWiseServiceService {
	@Autowired
	private TarrifWiseServiceRepository tarrifWiseServiceRepository;

	public PaginationResponseDto<TarrifWiseServiceVO> getTarrifWiseServiceList(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId, Long hospitalId) {
		PaginationResponseDto<TarrifWiseServiceVO> result = new PaginationResponseDto<TarrifWiseServiceVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<TarrifWiseServiceVO> tarrifWiseServices = tarrifWiseServiceRepository.findTarrifWiseServices("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (tarrifWiseServices != null) {
			result.setCount(tarrifWiseServices.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(tarrifWiseServices.getTotalElements());
			result.setTotalPages(tarrifWiseServices.getTotalPages());
			List<TarrifWiseServiceVO> tarrifWiseServiceList = tarrifWiseServices.getContent();
			result.setItems(tarrifWiseServiceList);

		}
		return result;
	}

	public TarrifWiseServiceVO saveUpdateTarrifWiseService(Long customerId, Long hospitalId, TarrifWiseServiceVO tarrifWiseServiceVO) {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		TarrifWiseService tarrifWiseService = getTarrifWiseServiceEntity(tarrifWiseServiceVO);
		tarrifWiseServiceRepository.save(tarrifWiseService);
		tarrifWiseServiceVO.setTarrifServiceId(tarrifWiseService.getTarrifServiceId());
		return tarrifWiseServiceVO;
	}

	public TarrifWiseServiceVO getTarrifWiseServiceDetailsById(Long customerId, Long hospitalId, Long tarrifWiseServiceId) {
		Assert.notNull(tarrifWiseServiceId, "Tarrif Wise Service ID not provided");
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		TarrifWiseService tarrifWiseService = tarrifWiseServiceRepository.findTarrifWiseService(tarrifWiseServiceId, customerId, hospitalId);
		Assert.notNull(tarrifWiseService, "Tarrif Wise Service not found");
		return getTarrifWiseServiceVO(tarrifWiseService);
	}

	public List<TarrifWiseServiceVO> getTarrifWiseServiceList(Long customerId, Long hospitalId) {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		List<TarrifWiseServiceVO> tarrifWiseServiceList = new ArrayList<TarrifWiseServiceVO>();
		tarrifWiseServiceList = tarrifWiseServiceRepository.getTarrifWiseServices(customerId, hospitalId);
		return tarrifWiseServiceList;
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "tarrifServiceName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "tarrifServiceName.miscValue");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	private TarrifWiseServiceVO getTarrifWiseServiceVO(TarrifWiseService tarrifWiseService) {
		TarrifWiseServiceVO vo = null;
		if (tarrifWiseService != null) {
			vo = new TarrifWiseServiceVO();
			vo.setTarrifServiceId(tarrifWiseService.getTarrifServiceId());
			if (tarrifWiseService.getTarrifServiceName() != null) {
				vo.setTarrifServiceNameId(tarrifWiseService.getTarrifServiceName().getMiscTypeValueId());
			}
			vo.setServiceTypeId(tarrifWiseService.getServiceTypeId());
			vo.setDepartmentId(tarrifWiseService.getDepartmentId());
			vo.setNormalServiceRate(tarrifWiseService.getNormalServiceRate());
			vo.setSpecialServiceRate(tarrifWiseService.getSpecialServiceRate());
			vo.setDoctorShareInPercent(tarrifWiseService.getDoctorShareInPercent());
			vo.setDoctorShareInRupees(tarrifWiseService.getDoctorShareInRupees());
			vo.setIsActive(tarrifWiseService.getIsActive());
			if (tarrifWiseService.getCustomer() != null) {
				vo.setCustomerId(tarrifWiseService.getCustomer().getCustomerId());
			}
			if (tarrifWiseService.getHospital() != null) {
				vo.setHospitalId(tarrifWiseService.getHospital().getHospitalId());
			}
		}
		return vo;
	}

	private TarrifWiseService getTarrifWiseServiceEntity(TarrifWiseServiceVO tarrifWiseServiceVO) {
		TarrifWiseService tarrifWiseService = null;
		if (tarrifWiseServiceVO.getTarrifServiceId() == null) {
			tarrifWiseService = new TarrifWiseService();
		} else {
			tarrifWiseService = tarrifWiseServiceRepository.findOne(tarrifWiseServiceVO.getTarrifServiceId());
			Assert.notNull(tarrifWiseService, "Tarrif wise Service with id: " + tarrifWiseServiceVO.getTarrifServiceId() + " not found.");
		}
		MiscValue miscValue = new MiscValue(tarrifWiseServiceVO.getTarrifServiceNameId());
		tarrifWiseService.setTarrifServiceName(miscValue);
		Assert.notNull(tarrifWiseServiceVO.getDepartmentId(), "Department Id not provided");
		tarrifWiseService.setDepartmentId(tarrifWiseServiceVO.getDepartmentId());
		tarrifWiseService.setServiceTypeId(tarrifWiseServiceVO.getServiceTypeId());
		tarrifWiseService.setNormalServiceRate(tarrifWiseServiceVO.getNormalServiceRate());
		tarrifWiseService.setSpecialServiceRate(tarrifWiseServiceVO.getSpecialServiceRate());
		tarrifWiseService.setDoctorShareInPercent(tarrifWiseServiceVO.getDoctorShareInPercent());
		tarrifWiseService.setDoctorShareInRupees(tarrifWiseServiceVO.getDoctorShareInRupees());
		tarrifWiseService.setIsActive(tarrifWiseServiceVO.getIsActive());
		Customer customer = new Customer(tarrifWiseServiceVO.getCustomerId());
		tarrifWiseService.setCustomer(customer);
		Hospital hospital = new Hospital(tarrifWiseServiceVO.getHospitalId());
		tarrifWiseService.setHospital(hospital);
		return tarrifWiseService;
	}
}
