package com.ehms.services.matrialinstrument;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.model.Customer;
import com.ehms.model.Hospital;
import com.ehms.model.MaterialInstrument;
import com.ehms.repositories.MaterialInstrumentRepository;
import com.ehms.vos.DrugMasterVO;
import com.ehms.vos.MaterialInstrumentVO;
import com.ehms.vos.PaginationResponseDto;

@Service
public class MaterialInstrumentService {
	@Autowired
	private MaterialInstrumentRepository materialInstrumentRepository;

	public PaginationResponseDto<MaterialInstrumentVO> getMaterialInstrumentList(Integer page, Integer pageSize,
			String sortOn, String sortOrder, String searchText, Long customerId, Long hospitalId) {
		PaginationResponseDto<MaterialInstrumentVO> result = new PaginationResponseDto<MaterialInstrumentVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<MaterialInstrumentVO> materialInstruments = materialInstrumentRepository
				.findMaterialInstruments("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (materialInstruments != null) {
			result.setCount(materialInstruments.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(materialInstruments.getTotalElements());
			result.setTotalPages(materialInstruments.getTotalPages());
			List<MaterialInstrumentVO> materialInstrumentList = materialInstruments.getContent();
			result.setItems(materialInstrumentList);
		}
		return result;
	}

	public MaterialInstrumentVO getMaterialInstrumentDetailsById(Long customerId, Long hospitalId,
			Long materialInstrumentId) {
		Assert.notNull(materialInstrumentId, "Material/Instrument ID not provided");
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		MaterialInstrument materialInstrument = materialInstrumentRepository
				.findMaterialInstrument(materialInstrumentId, customerId, hospitalId);
		Assert.notNull(materialInstrument, "Material/Instrument not found");
		return getMaterialInstrumentVO(materialInstrument);
	}

	public MaterialInstrumentVO saveUpdateMaterialInstrument(Long customerId, Long hospitalId,
			MaterialInstrumentVO materialInstrumentVO) {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		MaterialInstrument materialInstrument = getMaterialInstrumentEntity(materialInstrumentVO);
		materialInstrumentRepository.save(materialInstrument);
		materialInstrumentVO.setMaterialInstrumentId(materialInstrument.getMaterialInstrumentId());
		return materialInstrumentVO;
	}

	private MaterialInstrumentVO getMaterialInstrumentVO(MaterialInstrument materialInstrument) {
		MaterialInstrumentVO vo = null;
		if (materialInstrument != null) {
			vo = new MaterialInstrumentVO();
			vo.setMaterialInstrumentId(materialInstrument.getMaterialInstrumentId());
			vo.setIsActive(materialInstrument.getIsActive());
			vo.setCharges(materialInstrument.getCharges());
			vo.setDepartmentId(materialInstrument.getDepartmentId());
			if (materialInstrument.getHospital() != null) {
				vo.setHospitalId(materialInstrument.getHospital().getHospitalId());
			}
			if (materialInstrument.getCustomer() != null) {
				vo.setCustomerId(materialInstrument.getCustomer().getCustomerId());
			}
			vo.setMaterialInstrumentName(materialInstrument.getMaterialInstrumentName());
			vo.setMaterialInstrumentNumber(materialInstrument.getMaterialInstrumentNumber());
			vo.setMaterialInstrumentSubName(materialInstrument.getMaterialInstrumentSubName());
			vo.setMaterialInstrumentTypeId(materialInstrument.getMaterialInstrumentTypeId());
		}
		return vo;
	}

	private MaterialInstrument getMaterialInstrumentEntity(MaterialInstrumentVO materialInstrumentVO) {
		MaterialInstrument materialInstrument = null;
		if (materialInstrumentVO.getMaterialInstrumentId() == null) {
			materialInstrument = new MaterialInstrument();
		} else {
			materialInstrument = materialInstrumentRepository.findOne(materialInstrumentVO.getMaterialInstrumentId());
			Assert.notNull(materialInstrument,
					"Material/Instrument with id: " + materialInstrumentVO.getMaterialInstrumentId() + " not found.");
		}
		materialInstrument.setMaterialInstrumentTypeId(materialInstrumentVO.getMaterialInstrumentTypeId());
		Assert.notNull(materialInstrumentVO.getDepartmentId(), "Department Id not provided");
		materialInstrument.setDepartmentId(materialInstrumentVO.getDepartmentId());
		materialInstrument.setMaterialInstrumentName(materialInstrumentVO.getMaterialInstrumentName());
		materialInstrument.setIsActive(materialInstrumentVO.getIsActive());
		materialInstrument.setCharges(materialInstrumentVO.getCharges());
		materialInstrument.setMaterialInstrumentNumber(materialInstrumentVO.getMaterialInstrumentNumber());
		materialInstrument.setMaterialInstrumentSubName(materialInstrumentVO.getMaterialInstrumentSubName());
		Customer customer = new Customer(materialInstrumentVO.getCustomerId());
		materialInstrument.setCustomer(customer);
		Hospital hospital = new Hospital(materialInstrumentVO.getHospitalId());
		materialInstrument.setHospital(hospital);
		return materialInstrument;

	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "materialInstrumentName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC,
					"materialInstrumentName");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	public List<MaterialInstrumentVO> getMaterialInstrumentList(Long customerId, Long hospitalId) {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		List<MaterialInstrumentVO> materialInstrumentList = new ArrayList<MaterialInstrumentVO>();
		materialInstrumentList = materialInstrumentRepository.getMaterialInstrument(customerId, hospitalId);
		return materialInstrumentList;
	}

	
}
