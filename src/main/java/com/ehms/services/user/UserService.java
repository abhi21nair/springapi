package com.ehms.services.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Employee;
import com.ehms.model.LinkUserRole;
import com.ehms.model.MiscType;
import com.ehms.model.MiscValue;
import com.ehms.model.Role;
import com.ehms.model.User;
import com.ehms.repositories.misctype.MiscTypeRepository;
import com.ehms.repositories.misctype.MiscValueRepository;
import com.ehms.repositories.user.LinkUserRoleRepository;
import com.ehms.security.JwtUser;
import com.ehms.security.repository.UserRepository;
import com.ehms.services.EmployeeService;
import com.ehms.services.role.RoleService;
import com.ehms.vos.EmployeeVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.ResponseVO.Status;
import com.ehms.vos.ValidationCheckResponseVO;
import com.ehms.vos.user.LinkUserRoleVO;
import com.ehms.vos.user.ResetPasswordVO;
import com.ehms.vos.user.UserVO;

import io.jsonwebtoken.lang.Assert;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleService roleService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private MiscValueRepository miscValueRepository;

	@Autowired
	private MiscTypeRepository miscTypeRepository;

	@Autowired
	private LinkUserRoleRepository linkUserRoleRepository;

	@Value("${application.default.password}")
	private String defaultPassword;

	@Autowired
	private PasswordEncoder passwordEncoder;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public JwtUser getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return (JwtUser) auth.getPrincipal();
	}

	@Transactional
	public User saveUpdateAdminUser(Long customerId, Long hospitalId, UserVO userVO, Boolean isCustomerAdmin, Boolean isHospitalAdmin) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer was not saved properly");
		Assert.notNull(userVO, "User information not provided.");
		EmployeeVO employeeVO = userVO.getEmployee();
		Assert.notNull(employeeVO, "Employee information not provided.");
		if (isCustomerAdmin && employeeVO.getEmployeeId() == null) {
			Assert.notNull(employeeVO.getDesignation(), "Designation of key user not provided.");
			MiscValue value = new MiscValue();
			value.setCustomerId(customerId);
			value.setHospitalId(null);
			MiscType designationType = miscTypeRepository.findByMiscTypeName("DESIGNATION");
			Assert.notNull(designationType, "Misc type for designation not found.");
			value.setMiscTypeId(designationType.getMiscTypeId());
			value.setMiscValue(employeeVO.getDesignation());
			miscValueRepository.save(value);
			Assert.notNull(value.getMiscTypeValueId(), "Error while saving misc type value for designation");
			employeeVO.setDesignationId(value.getMiscTypeValueId());
		} else if (isCustomerAdmin && employeeVO.getEmployeeId() != null) {
			Assert.notNull(employeeVO.getDesignation(), "Designation of key user not provided.");
			Assert.notNull(employeeVO.getDesignationId(), "Designation ID of key user not provided.");
			MiscValue designationMiscValue = miscValueRepository.findOne(employeeVO.getDesignationId());
			Assert.notNull(designationMiscValue, "Misc Type for designation not found");
			Assert.isTrue(designationMiscValue.getCustomerId() != null && designationMiscValue.getCustomerId().equals(customerId), "Incorrect designation information");
			if (designationMiscValue.getMiscValue() != null && !designationMiscValue.getMiscValue().equalsIgnoreCase(employeeVO.getDesignation())) {
				Long miscValueId = miscValueRepository.findCustomerDesignationMiscTypeId(employeeVO.getDesignation(), customerId, employeeVO.getDesignationId());
				if (miscValueId != null) {
					employeeVO.setDesignationId(miscValueId);
				} else {
					designationMiscValue.setMiscValue(employeeVO.getDesignation());
					miscValueRepository.save(designationMiscValue);
				}
			}
		} else if (isHospitalAdmin && employeeVO.getEmployeeId() == null) {
			employeeVO.setDesignationId(miscValueRepository.findSystemDesignationMiscTypeId(MiscValue.MISC_VALUE_HOSPITAL_ADMIN));
		}
		Employee employees = employeeService.saveUpdateAdminEmployee(customerId, hospitalId, employeeVO);
		employeeVO.setEmployeeId(employees.getEmployeeId());
		User user = getUserEntity(userVO, employees, isCustomerAdmin, isHospitalAdmin);
		userRepository.save(user);
		return user;
	}

	private User getUserEntity(UserVO userVO, Employee employees, Boolean isCustomerAdmin, Boolean isHospitalAdmin) {
		User user = null;
		if (userVO.getUserId() == null) {
			user = new User();
			user.setUsername(userVO.getUserName());
			user.setPassword(passwordEncoder.encode(userVO.getPassword() != null ? userVO.getPassword() : defaultPassword));
			List<LinkUserRole> roles = new ArrayList<>();
			LinkUserRole link = null;
			if (isCustomerAdmin) {
				link = new LinkUserRole();
				link.setRole(roleService.getSystemRoleDetailsForRoleName(Role.ROLE_NAME_CUSTOMER_ADMIN));
				link.setUser(user);
				roles.add(link);
			} else if (isHospitalAdmin) {
				link = new LinkUserRole();
				link.setRole(roleService.getSystemRoleDetailsForRoleName(Role.ROLE_NAME_HOSPITAL_ADMIN));
				link.setUser(user);
				roles.add(link);
			} else {
				List<Long> roleIds = userVO.getRolesIds();
				for (Long roleId : roleIds) {
					link = new LinkUserRole();
					link.setRole(roleService.findOne(roleId));
					link.setUser(user);
					roles.add(link);
				}
			}
			user.setLinkUserRoles(roles);
		} else {
			user = userRepository.findOne(userVO.getUserId());
			Assert.notNull(user, "User with id: " + userVO.getUserId() + " not found.");
			if (user.getEmployee() != null) {
				linkUserRoleRepository.deleteByUserId(userVO.getUserId(), userVO.getUserId());
			} else {
				linkUserRoleRepository.deleteBySystemUserId(userVO.getUserId(), userVO.getUserId());

			}
			List<LinkUserRole> roles = new ArrayList<>();
			LinkUserRole link = null;
			List<Long> roleIds = userVO.getRolesIds();
			for (Long roleId : roleIds) {
				link = new LinkUserRole();
				link.setRole(roleService.findOne(roleId));
				link.setUser(user);
				roles.add(link);
			}
			user.setLinkUserRoles(roles);
		}
		user.setEmployee(employees);
		user.setIsSystemUser(userVO.getIsSystemUser());
		user.setEnabled(userVO.getIsActive());
		return user;
	}

	public UserVO getUserVO(User user) {
		UserVO vo = null;
		if (user != null) {
			vo = new UserVO();
			vo.setUserId(user.getUserId());
			vo.setUserName(user.getUsername());
			vo.setIsActive(user.getEnabled());
			vo.setIsSystemUser(user.getIsSystemUser());
			List<LinkUserRole> linkUserRoles = user.getLinkUserRoles();
			if (linkUserRoles != null && !linkUserRoles.isEmpty()) {
				List<Long> roleIds = new ArrayList<>();
				for (LinkUserRole linkUserRole : linkUserRoles) {
					if (linkUserRole != null) {
						Role role = linkUserRole.getRole();
						if (role != null) {
							roleIds.add(role.getRoleId());
						}
					}
				}
				vo.setRolesIds(roleIds);
				if (user.getEmployee() != null) {
					vo.setEmployee(employeeService.getEmployeeVO(user.getEmployee()));
				}

			}
		}
		return vo;
	}

	public ValidationCheckResponseVO validateUsername(UserVO userVO) {
		User user = null;
		if (userVO.getUserId() == null) {
			user = userRepository.findByUsernameIgnoreCase(userVO.getUserName());
		} else {
			user = userRepository.findByUserIdNotAndUsernameIgnoreCase(userVO.getUserId(), userVO.getUserName());
		}
		if (user == null) {
			return new ValidationCheckResponseVO(true);
		} else {
			return new ValidationCheckResponseVO(false);
		}
	}

	public PaginationResponseDto<UserVO> getUserList(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId, Long hospitalId) {
		PaginationResponseDto<UserVO> result = new PaginationResponseDto<UserVO>();
		Sort sort = new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, sortOn);
		PageRequest pageRequest = new PageRequest(page, pageSize, sort);
		JwtUser user = getCurrentUser();
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Assert.notNull(user, "User not found.");
		Assert.notNull(user.getuserId(), "User ID not found.");
		Page<UserVO> users = userRepository.findUsersOfHospital("%" + searchText + "%", customerId, hospitalId, user.getuserId(), pageRequest);
		if (users != null) {
			result.setCount(users.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(users.getTotalElements());
			result.setTotalPages(users.getTotalPages());
			List<UserVO> userList = users.getContent();
			populateRoles(userList);
			result.setItems(userList);
		}
		return result;
	}

	private void populateRoles(List<UserVO> userList) {
		if (userList != null && !userList.isEmpty()) {
			List<Long> userIds = new ArrayList<>();
			for (UserVO userVO : userList) {
				userIds.add(userVO.getUserId());
			}
			List<LinkUserRoleVO> linkVOs = linkUserRoleRepository.getUserRolesListForUsers(userIds);
			if (linkVOs != null && !linkVOs.isEmpty()) {
				Map<Long, List<Long>> userRolesMap = new HashMap<>();
				for (LinkUserRoleVO linkUserRoleVO : linkVOs) {
					Long userId = linkUserRoleVO.getUserId();
					Long roleId = linkUserRoleVO.getRoleId();
					List<Long> userRoles = userRolesMap.get(userId);
					if (userRoles == null) {
						userRoles = new ArrayList<>();
						userRolesMap.put(userId, userRoles);
					}
					userRoles.add(roleId);
				}
				for (UserVO userVO : userList) {
					userVO.setRolesIds(userRolesMap.get(userVO.getUserId()));
				}
			}
		}
	}

	public UserVO getUserDetails(Long userId) {
		Assert.notNull(userId, "User id not provided");
		User user = userRepository.findOne(userId);
		Assert.notNull(user, "User not found.");
		return getUserVO(user);
	}

	@Transactional
	public UserVO saveUpdateUser(UserVO userVO) {
		Employee employee = null;
		Assert.notNull(userVO, "User information not found");
		if (userVO.getEmployee() != null) {
			employee = new Employee(userVO.getEmployee().getEmployeeId());
			Assert.isTrue(userVO.getEmployee() != null && userVO.getEmployee().getEmployeeId() != null, "Employee information not provided");
		}
		User user = getUserEntity(userVO, employee, false, false);
		userRepository.save(user);
		userVO.setUserId(user.getUserId());
		return userVO;
	}

	public UserVO getSystemUserDetails(Long userId) {
		return getUserDetails(userId);
	}

	@Transactional
	public UserVO saveUpdateSystemUser(UserVO userVO) {
		return saveUpdateUser(userVO);
	}

	public PaginationResponseDto<UserVO> getSystemUserList(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText) {
		PaginationResponseDto<UserVO> result = new PaginationResponseDto<UserVO>();
		Sort sort = new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, sortOn);
		PageRequest pageRequest = new PageRequest(page, pageSize, sort);
		Page<UserVO> systemUsers = null;
		systemUsers = userRepository.findSystemUsers("%" + searchText + "%", pageRequest);
		if (systemUsers != null) {
			result.setCount(systemUsers.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(systemUsers.getTotalElements());
			result.setTotalPages(systemUsers.getTotalPages());
			List<UserVO> systemUserList = systemUsers.getContent();
			populateRoles(systemUserList);
			result.setItems(systemUserList);
		}
		return result;
	}

	public ResponseVO resetUserPassword(ResetPasswordVO resetPasswordVO) {
		User user = null;
		if (resetPasswordVO.getUserId() != null) {
			user = userRepository.findOne(resetPasswordVO.getUserId());
			Assert.notNull(user, "User not found");
			Assert.isTrue(resetPasswordVO.getOldPassword().length() != 0, "Old password not provided");
			Assert.isTrue(resetPasswordVO.getNewPassword().length() != 0, "New password not provided");
			if (passwordEncoder.matches(resetPasswordVO.getOldPassword(), user.getPassword())) {
				user.setPassword(passwordEncoder.encode(resetPasswordVO.getNewPassword()));
			} else {
				return new ResponseVO(Status.FAILURE, "Old Password does not match");
			}
			userRepository.save(user);
			return new ResponseVO(Status.SUCCESS, "Password Changed successfully");
		}
		return new ResponseVO(Status.FAILURE, "Password Change Failure");
	}

	@Transactional
	public ResponseVO forgotPassword(String username) {
		Assert.notNull(username, "User name not provided.");
		User user = userRepository.findByUsernameIgnoreCase(username);
		Assert.notNull(user, "User with username: " + username + " not found.");
		Assert.isTrue((user.getEmployee() != null && user.getEmployee().getEmail() != null && !user.getEmployee().getEmail().isEmpty()) || user.getEmployee() == null,
				"Email not registered for this User");// to do
		String tempPassword = RandomStringUtils.randomAlphabetic(8);
		logger.info("Passowrd--->" + tempPassword);
		userRepository.updateUserPassword(passwordEncoder.encode(tempPassword), username);
		userRepository.flush();
		return new ResponseVO(Status.SUCCESS, "Password has been sent on your registerd Email Id successfully");
	}
}
