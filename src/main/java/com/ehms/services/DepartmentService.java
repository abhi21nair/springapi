package com.ehms.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Customer;
import com.ehms.model.Department;
import com.ehms.model.Hospital;
import com.ehms.repositories.DepartmentRepository;
import com.ehms.security.JwtUser;
import com.ehms.services.user.UserService;
import com.ehms.vos.DepartmentVO;
import com.ehms.vos.ValidationCheckResponseVO;

@Service
public class DepartmentService {

	@Autowired
	private DepartmentRepository departmentRepository;

	@Autowired
	private UserService userService;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public List<DepartmentVO> getDepartments() {
		JwtUser user = userService.getCurrentUser();
		Assert.isTrue(user != null && user.getCustomerId() != null, "Customer not found");
		if (user.getHospitalId() == null) {
			return departmentRepository.findCustomerDepartments(user.getCustomerId());
		} else {
			return departmentRepository.findCustomerHospitalDepartments(user.getCustomerId(), user.getHospitalId());
		}
	}

	public List<DepartmentVO> getSystemDepartments() {
		JwtUser user = userService.getCurrentUser();
		Assert.isTrue(user != null, "User not found");
		if (user.getHospitalId() == null && user.getCustomerId() == null) {
			return departmentRepository.findSystemDepartments();
		} else {
			throw new IllegalArgumentException("Information not consistance for the user");
		}
	}

	@Transactional
	public void saveUpdateDepartment(DepartmentVO departmentVO, boolean isNewDepartment) throws EhmsBusinessException {
		Department department = getDepartmentEntity(departmentVO, isNewDepartment);
		departmentRepository.save(department);
		if (isNewDepartment) {
			departmentVO.setDepartmentId(department.getDepartmentId());
		}
		logger.debug("Department " + departmentVO.getDepartmentName() + " Saved/Updated Successfully");
	}

	public DepartmentVO getDepartmentDetails(Long id) {
		Department department = departmentRepository.findOne(id);
		Assert.notNull(department, "Department not found");
		Assert.notNull(department.getCustomer(), "Invalid department.");
		JwtUser user = userService.getCurrentUser();
		Assert.isTrue(department.getCustomer().getCustomerId() != null && department.getCustomer().getCustomerId().equals(user.getCustomerId()), "Do not have access to this department");
		return new DepartmentVO(department);
	}

	private Department getDepartmentEntity(DepartmentVO departmentVO, boolean isNewDepartment) throws EhmsBusinessException {
		Department department = new Department();
		JwtUser user = userService.getCurrentUser();
		if (!isNewDepartment) {
			department = departmentRepository.findOne(departmentVO.getDepartmentId());
		}
		department.setDepartmentName(departmentVO.getDepartmentName());
		department.setDepartmentDescription(departmentVO.getDepartmentDescription());
		department.setDepartmentType(departmentVO.getDepartmentType());
		department.setCountryCodeForDepartmentPhoneNumber(departmentVO.getCountryCodeForDepartmentPhoneNumber());
		department.setDepartmentPhoneNumber(departmentVO.getDepartmentPhoneNumber());
		department.setIsActive(departmentVO.getIsActive());
		Assert.notNull(user.getCustomerId(), "Customer information not found");
		departmentVO.setCustomerId(user.getCustomerId());
		Customer customer = new Customer(user.getCustomerId());
		department.setCustomer(customer);
		if (user.getHospitalId() != null) {
			Hospital hospital = new Hospital(user.getHospitalId());
			department.setHospital(hospital);
			departmentVO.setHospitalId(user.getHospitalId());
		} else {
			department.setHospital(null);
		}
		return department;
	}

	public ValidationCheckResponseVO validateDepartment(DepartmentVO departmentVO) {
		Assert.isTrue(departmentVO != null && departmentVO.getDepartmentName() != null && !departmentVO.getDepartmentName().isEmpty(), "Department name not provided");
		Long departmentId = null;

		if (departmentVO.getCustomerId() == null && departmentVO.getHospitalId() == null) {
			if (departmentVO.getDepartmentId() == null) {
				departmentId = departmentRepository.findDepartmentByName(departmentVO.getDepartmentName());
			} else {
				departmentId = departmentRepository.findDepartmentByNameAndId(departmentVO.getDepartmentName(), departmentVO.getDepartmentId());
			}

		} else if (departmentVO.getCustomerId() != null && departmentVO.getHospitalId() == null) {
			if (departmentVO.getDepartmentId() == null) {
				departmentId = departmentRepository.findDepartmentForCustomerByName(departmentVO.getDepartmentName(), departmentVO.getCustomerId());
			} else {
				departmentId = departmentRepository.findDepartmentForCustomerByNameAndId(departmentVO.getDepartmentName(), departmentVO.getDepartmentId(), departmentVO.getCustomerId());
			}
		} else if (departmentVO.getCustomerId() != null && departmentVO.getHospitalId() != null) {

			if (departmentVO.getDepartmentId() == null) {
				departmentId = departmentRepository.findDepartmentForCustomerAndHospitalByName(departmentVO.getDepartmentName(), departmentVO.getCustomerId(), departmentVO.getHospitalId());
			} else {
				departmentId = departmentRepository.findDepartmentForCustomerAndHospitalByNameAndId(departmentVO.getDepartmentName(), departmentVO.getDepartmentId(), departmentVO.getCustomerId(),
						departmentVO.getHospitalId());
			}

		} else {
			logger.error("Inside validateDepartment() : Information is not consistent");
			throw new IllegalArgumentException("Information not consistent");
		}
		if (departmentId != null) {
			return new ValidationCheckResponseVO(false);
		} else {
			return new ValidationCheckResponseVO(true);
		}
	}
}
