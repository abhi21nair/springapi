package com.ehms.services.tpaInsurance;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Address;
import com.ehms.model.Customer;
import com.ehms.model.Hospital;
import com.ehms.model.TpaInsurance;
import com.ehms.repositories.AddressRepository;
import com.ehms.repositories.TpaInsuranceRepository;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.AddressVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.TpaInsuranceVO;

@Service
public class TpaInsuranceService {
	@Autowired
	private TpaInsuranceRepository tpaInsuranceRepository;

	@Autowired
	private AddressRepository addressRepository;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public PaginationResponseDto<TpaInsuranceVO> getTpaInsuranceList(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId,
			Long hospitalId) {
		PaginationResponseDto<TpaInsuranceVO> result = new PaginationResponseDto<TpaInsuranceVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<TpaInsuranceVO> tpaInsurances = tpaInsuranceRepository.findTpaInsurances("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (tpaInsurances != null) {
			result.setCount(tpaInsurances.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(tpaInsurances.getTotalElements());
			result.setTotalPages(tpaInsurances.getTotalPages());
			List<TpaInsuranceVO> tpaInsuranceList = tpaInsurances.getContent();
			result.setItems(tpaInsuranceList);

		}
		return result;
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "tpaInsuranceName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "tpaInsuranceName");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	public TpaInsuranceVO getTpaInsuranceDetailsById(Long customerId, Long hospitalId, Long tpaInsuranceId) {
		Assert.notNull(tpaInsuranceId, "Tpa/Insurance ID not provided");
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		TpaInsurance tpaInsurance = tpaInsuranceRepository.findTpaInsurance(tpaInsuranceId, customerId, hospitalId);
		Assert.notNull(tpaInsurance, "Tpa/Insurance not found");
		return getTpaInsuranceVO(tpaInsurance);
	}

	public TpaInsuranceVO saveUpdateTpaInsurance(Long customerId, Long hospitalId, TpaInsuranceVO tpaInsuranceVO) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		TpaInsurance tpaInsurance = getTpaInsuranceEntity(tpaInsuranceVO);
		tpaInsuranceRepository.save(tpaInsurance);
		AddressVO addressVO = tpaInsuranceVO.getAddress();
		if (tpaInsurance.getAddress() != null) {
			addressVO.setAddressId(tpaInsurance.getAddress().getAddressId());
		}
		tpaInsuranceVO.setTpaInsuranceId(tpaInsurance.getTpaInsuranceId());
		return tpaInsuranceVO;
	}

	private TpaInsurance getTpaInsuranceEntity(TpaInsuranceVO tpaInsuranceVO) throws EhmsBusinessException {
		TpaInsurance tpaInsurance = null;

		if (tpaInsuranceVO.getTpaInsuranceId() == null) {
			tpaInsurance = new TpaInsurance();

		} else {
			tpaInsurance = tpaInsuranceRepository.findOne(tpaInsuranceVO.getTpaInsuranceId());
			Assert.notNull(tpaInsurance, "Tpa/Insurance with id: " + tpaInsuranceVO.getTpaInsuranceId() + " not found.");
		}
		AddressVO addressVO = tpaInsuranceVO.getAddress();
		Address address = null;
		if (tpaInsurance.getAddress() == null) {
			address = new Address();
		} else {
			address = addressRepository.findOne(addressVO.getAddressId());
		}

		tpaInsurance.setAddress(addressVO.getAddressEntity(address, addressVO));
		tpaInsurance.setCountryCodeForKeyContactPersonNumber(tpaInsuranceVO.getCountryCodeForKeyContactPersonNumber());
		tpaInsurance.setCountryCodeForMobileNumber(tpaInsuranceVO.getCountryCodeForMobileNumber());
		tpaInsurance.setCountryCodeForPhoneNumber(tpaInsuranceVO.getCountryCodeForPhoneNumber());
		Customer customer = new Customer(tpaInsuranceVO.getCustomerId());
		tpaInsurance.setCustomer(customer);
		try {			
			Assert.notNull(tpaInsuranceVO.getEffectiveFromDate(), "Effective From Date not provided.");
			Assert.notNull(tpaInsuranceVO.getEffectiveToDate(), "Effective To Date not provided.");
			Date effectiveFromDate = DateTimeUtil.parseDate(tpaInsuranceVO.getEffectiveFromDate());
			Date effectiveToDate = DateTimeUtil.parseDate(tpaInsuranceVO.getEffectiveToDate());
			Assert.isTrue(effectiveFromDate!=null && effectiveToDate!=null && effectiveToDate.after(effectiveFromDate), "From Date should be before To Date");
			tpaInsurance.setEffectiveFromDate(effectiveFromDate);
			tpaInsurance.setEffectiveToDate(effectiveToDate);
		} catch (ParseException e) {
			logger.error("ParseException Inside getTpaInsuranceEntity() : " + " EffectiveFromDate " + tpaInsuranceVO.getEffectiveFromDate()
					+ " EffectiveToDate " + tpaInsuranceVO.getEffectiveToDate(), e);
			throw new EhmsBusinessException(e, "Unable to parse date while saving/updating Tpa/Insurance.");
		}
		Hospital hospital = new Hospital(tpaInsuranceVO.getHospitalId());
		tpaInsurance.setHospital(hospital);
		tpaInsurance.setKeyContactPerson(tpaInsuranceVO.getKeyContactPerson());
		tpaInsurance.setKeyContactPersonNumber(tpaInsuranceVO.getKeyContactPersonNumber());
		tpaInsurance.setMobileNumber(tpaInsuranceVO.getMobileNumber());
		tpaInsurance.setPhoneNumber(tpaInsuranceVO.getPhoneNumber());
		tpaInsurance.setTpaInsuranceClaimTypeId(tpaInsuranceVO.getTpaInsuranceClaimTypeId());
		tpaInsurance.setTpaInsuranceName(tpaInsuranceVO.getTpaInsuranceName());
		tpaInsurance.setTpaInsuranceNumber(tpaInsuranceVO.getTpaInsuranceNumber());
		tpaInsurance.setTpaInsuranceTypeId(tpaInsuranceVO.getTpaInsuranceTypeId());
		return tpaInsurance;
	}

	private TpaInsuranceVO getTpaInsuranceVO(TpaInsurance tpaInsurance) {
		TpaInsuranceVO vo = null;
		if (tpaInsurance != null) {
			vo = new TpaInsuranceVO();
			vo.setTpaInsuranceId(tpaInsurance.getTpaInsuranceId());
			vo.setAddress(new AddressVO(tpaInsurance.getAddress()));
			vo.setCountryCodeForKeyContactPersonNumber(tpaInsurance.getCountryCodeForKeyContactPersonNumber());
			vo.setCountryCodeForMobileNumber(tpaInsurance.getCountryCodeForMobileNumber());
			vo.setCountryCodeForPhoneNumber(tpaInsurance.getCountryCodeForPhoneNumber());
			if (tpaInsurance.getCustomer() != null) {
				vo.setCustomerId(tpaInsurance.getCustomer().getCustomerId());
			}
			vo.setEffectiveFromDate(DateTimeUtil.formatDate(tpaInsurance.getEffectiveFromDate()));
			vo.setEffectiveToDate(DateTimeUtil.formatDate(tpaInsurance.getEffectiveToDate()));
			if (tpaInsurance.getHospital() != null) {
				vo.setHospitalId(tpaInsurance.getHospital().getHospitalId());
			}
			vo.setKeyContactPerson(tpaInsurance.getKeyContactPerson());
			vo.setKeyContactPersonNumber(tpaInsurance.getKeyContactPersonNumber());
			vo.setMobileNumber(tpaInsurance.getMobileNumber());
			vo.setPhoneNumber(tpaInsurance.getPhoneNumber());
			vo.setTpaInsuranceClaimTypeId(tpaInsurance.getTpaInsuranceClaimTypeId());
			vo.setTpaInsuranceName(tpaInsurance.getTpaInsuranceName());
			vo.setTpaInsuranceNumber(tpaInsurance.getTpaInsuranceNumber());
			vo.setTpaInsuranceTypeId(tpaInsurance.getTpaInsuranceTypeId());
		}
		return vo;
	}

}
