package com.ehms.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import com.ehms.model.Address;
import com.ehms.model.Customer;
import com.ehms.model.Department;
import com.ehms.model.Hospital;
import com.ehms.model.VendorMaster;
import com.ehms.repositories.AddressRepository;
import com.ehms.repositories.VendorMasterRepository;
import com.ehms.vos.AddressVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.VendorMasterVO;

@Service
public class VendorMasterService {

	@Autowired
	private VendorMasterRepository vendorMasterRepository;

	@Autowired
	private AddressRepository addressRepository;

	public PaginationResponseDto<VendorMasterVO> getVendorMasterDetails(Integer page, Integer pageSize, String sortOn,
			String sortOrder, String searchText, Long customerId, Long hospitalId) {
		PaginationResponseDto<VendorMasterVO> result = new PaginationResponseDto<VendorMasterVO>();
		Sort sort = new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, sortOn);
		PageRequest pageRequest = new PageRequest(page, pageSize, sort);
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<VendorMasterVO> vendorMasterDetails = vendorMasterRepository.findVendorMasters("%" + searchText + "%",
				customerId, hospitalId, pageRequest);
		if (vendorMasterDetails != null) {
			result.setCount(vendorMasterDetails.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(vendorMasterDetails.getTotalElements());
			result.setTotalPages(vendorMasterDetails.getTotalPages());
			List<VendorMasterVO> vendorMasterDetailsList = vendorMasterDetails.getContent();
			result.setItems(vendorMasterDetailsList);
		}
		return result;
	}

	public VendorMasterVO getVendorMasterById(Long vendorMasterId) {
		Assert.notNull(vendorMasterId, "vendorMaster Id not provided.");
		VendorMaster vendorMaster = vendorMasterRepository.findOne(vendorMasterId);
		Assert.notNull(vendorMaster, "vendorMaster not found.");
		return getVendorMasterVO(vendorMaster);
	}

	private VendorMasterVO getVendorMasterVO(VendorMaster vendorMaster) {
		Assert.notNull(vendorMaster, "vendorMaster information not found.");
		VendorMasterVO vo = null;
		if (vendorMaster != null) {
			vo = new VendorMasterVO();
			vo.setVendorMasterId(vendorMaster.getVendorMasterId());
			vo.setVendorName(vendorMaster.getVendorName());
			vo.setDlNo(vendorMaster.getDlNo());
			vo.setAddress(new AddressVO(vendorMaster.getAddress()));
			vo.setEmail(vendorMaster.getEmail());
			vo.setGstNo(vendorMaster.getGstNo());
			vo.setKeyContactPersonNumber(vendorMaster.getKeyContactPersonNumber());
			vo.setKeyContactPersonName(vendorMaster.getKeyContactPersonName());
			vo.setCountryCodeContactNumber(vendorMaster.getCountryCodeContactNumber());
			vo.setCountryCodeMobileNumber(vendorMaster.getCountryCodeMobileNumber());
			vo.setOpeningBalance(vendorMaster.getOpeningBalance());
			vo.setPanNo(vendorMaster.getPanNo());
			vo.setPhoneNo(vendorMaster.getPhoneNo());
			vo.setShopActNo(vendorMaster.getShopActNo());
			vo.setMobileNo(vendorMaster.getMobileNo());
			vo.setTanNo(vendorMaster.getTanNo());
			vo.setVendorNo(vendorMaster.getVendorNo());
			vo.setVendorTypeId(vendorMaster.getVendorTypeId());
			if (vendorMaster.getDepartment() != null) {
				vo.setDepartmentId(vendorMaster.getDepartment().getDepartmentId());
			}
			vo.setIsActive(vendorMaster.getIsActive());
			if (vendorMaster.getCustomer() != null) {
				vo.setCustomerId(vendorMaster.getCustomer().getCustomerId());
			}
			if (vendorMaster.getHospital() != null) {
				vo.setHospitalId(vendorMaster.getHospital().getHospitalId());
			}
		}
		return vo;
	}

	@Transactional
	public VendorMaster saveUpdateVendorMaster(Long customerId, Long hospitalId, VendorMasterVO vendorMasterVO) {
		Assert.notNull(customerId, "Customer ID not provided.");
		vendorMasterVO.setCustomerId(customerId);
		vendorMasterVO.setHospitalId(hospitalId);
		VendorMaster vendorMaster = getVenderMasterEntity(vendorMasterVO);
		vendorMasterRepository.save(vendorMaster);
		AddressVO addressVO = vendorMasterVO.getAddress();
		if (vendorMaster.getAddress() != null) {
			addressVO.setAddressId(vendorMaster.getAddress().getAddressId());
		}
		vendorMasterVO.setVendorMasterId(vendorMaster.getVendorMasterId());
		return vendorMaster;
	  }

	private VendorMaster getVenderMasterEntity(VendorMasterVO vendorMasterVO) {
		VendorMaster vendorMaster = null;
		if (vendorMasterVO.getVendorMasterId() == null) {
			vendorMaster = new VendorMaster();
		} else {
			vendorMaster = vendorMasterRepository.findOne(vendorMasterVO.getVendorMasterId());
			Assert.notNull(vendorMaster, "vendorMaster with id: " + vendorMasterVO.getVendorMasterId() + " not found.");
		}

		AddressVO addressVO = vendorMasterVO.getAddress();
		Address address = null;
		if (vendorMaster.getAddress() == null) {
			address = new Address();
		} else {
			address = addressRepository.findOne(addressVO.getAddressId());
		}

		vendorMaster.setAddress(addressVO.getAddressEntity(address, addressVO));
		vendorMaster.setDlNo(vendorMasterVO.getDlNo());
		vendorMaster.setGstNo(vendorMasterVO.getGstNo());
		vendorMaster.setEmail(vendorMasterVO.getEmail());
		vendorMaster.setKeyContactPersonName(vendorMasterVO.getKeyContactPersonName());
		vendorMaster.setKeyContactPersonNumber(vendorMasterVO.getKeyContactPersonNumber());
		vendorMaster.setOpeningBalance(vendorMasterVO.getOpeningBalance());
		vendorMaster.setPanNo(vendorMasterVO.getPanNo());
		vendorMaster.setPhoneNo(vendorMasterVO.getPhoneNo());
		vendorMaster.setMobileNo(vendorMasterVO.getMobileNo());
		vendorMaster.setCountryCodeContactNumber(vendorMasterVO.getCountryCodeContactNumber());
		vendorMaster.setCountryCodeMobileNumber(vendorMasterVO.getCountryCodeMobileNumber());
		vendorMaster.setShopActNo(vendorMasterVO.getShopActNo());
		vendorMaster.setVendorMasterId(vendorMasterVO.getVendorMasterId());
		vendorMaster.setVendorName(vendorMasterVO.getVendorName());
		vendorMaster.setVendorTypeId(vendorMasterVO.getVendorTypeId());
		vendorMaster.setTanNo(vendorMasterVO.getTanNo());
		vendorMaster.setVendorNo(vendorMasterVO.getVendorNo());
		Assert.notNull(vendorMasterVO.getDepartmentId(), "Department Id not provided");
		Department department = new Department(vendorMasterVO.getDepartmentId());
		vendorMaster.setDepartment(department);
		vendorMaster.setIsActive(vendorMasterVO.getIsActive());
		Assert.notNull(vendorMasterVO.getCustomerId(), "Customer information not provided");
		Customer customer = new Customer(vendorMasterVO.getCustomerId());
		Assert.notNull(customer, "Customer not found");
		vendorMaster.setCustomer(customer);
		if (vendorMasterVO.getHospitalId() != null) {
			Hospital hospital = new Hospital(vendorMasterVO.getHospitalId());
			vendorMaster.setHospital(hospital);
		}
		return vendorMaster;
	}

}
