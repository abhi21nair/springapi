package com.ehms.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Address;
import com.ehms.model.Customer;
import com.ehms.model.Hospital;
import com.ehms.model.Patient;
import com.ehms.repositories.AddressRepository;
import com.ehms.repositories.CustomerRepository;
import com.ehms.repositories.HospitalRepository;
import com.ehms.repositories.PatientRepository;
import com.ehms.repositories.specifications.patient.PatientSpecificationFactory;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.AddressVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.PatientVO;

@Service
public class PatientService {

	@Autowired
	private PatientRepository patientRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private HospitalRepository hospitalRepository;

	@Autowired
	private PatientSpecificationFactory patientSpecificationFactory;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public Patient populatePatientEntity(PatientVO patientVO, Patient patient) throws EhmsBusinessException {
		patient.setTitleId(patientVO.getTitleId());
		patient.setFirstName(patientVO.getFirstName());
		patient.setMiddleName(patientVO.getMiddleName());
		patient.setLastName(patientVO.getLastName());
		try {
			patient.setBirthDate(DateTimeUtil.parseDate(patientVO.getBirthDate()));
		} catch (ParseException e) {
			logger.error("Error while parsing birth date: " + patientVO.getBirthDate(), e);
			throw new EhmsBusinessException(e, "Unable to parse birth date " + patientVO.getBirthDate());
		}
		patient.setBirthYear(patientVO.getBirthYear());
		patient.setIsDobAvailable(patientVO.getIsDobAvailable());
		patient.setGender(patientVO.getGender());
		patient.setMaritalStatus(patientVO.getMaritalStatus());
		patient.setNationality(patientVO.getNationality());
		patient.setAllergy(patientVO.getAllergy());
		patient.setReferenceDoctor(patientVO.getReferenceDoctor());
		if (patientVO.getCurrentAddress() != null) {
			Assert.notNull(patientVO.getCurrentAddress().getAddress1(), "Address not provided.");
			Assert.notNull(patientVO.getCurrentAddress().getCityId(), "City Id not provided.");
			Assert.notNull(patientVO.getCurrentAddress().getZipCode(), "Zipcode not provided.");
			AddressVO currentAddressVO = patientVO.getCurrentAddress();
			Address currentAddress = null;
			if (currentAddressVO.getAddressId() == null && currentAddressVO != null) {
				currentAddress = new Address();
			} else {
				currentAddress = addressRepository.findOne(currentAddressVO.getAddressId());
				Assert.notNull(currentAddress, "Current address is not found");
			}
			patient.setCurrentAddress(currentAddressVO.getAddressEntity(currentAddress, currentAddressVO));
		} else {
			patient.setCurrentAddress(null);
		}
		Address permanentAddress = null;
		if (patientVO.getPermanentAddress() != null) {
			Assert.notNull(patientVO.getPermanentAddress().getAddress1(), "Address not provided.");
			Assert.notNull(patientVO.getPermanentAddress().getCityId(), "City Id not provided.");
			Assert.notNull(patientVO.getPermanentAddress().getZipCode(), "Zipcode not provided.");
			AddressVO permanentAddressVO = patientVO.getPermanentAddress();
			if (permanentAddressVO.getAddressId() == null && permanentAddressVO != null) {
				permanentAddress = new Address();
			} else {
				permanentAddress = addressRepository.findOne(permanentAddressVO.getAddressId());
				Assert.notNull(permanentAddress, "Permanent address is not found");
			}
			patient.setPermanentAddress(permanentAddressVO.getAddressEntity(permanentAddress, permanentAddressVO));
		} else {
			patient.setPermanentAddress(null);
		}
		patient.setContactNumber(patientVO.getContactNumber());
		patient.setMobileNumber(patientVO.getMobileNumber());
		patient.setNationality(patientVO.getNationality());
		patient.setPassportNumber(patientVO.getPassportNumber());

		if (patientVO.getValidTill() != null) {
			try {
				patient.setValidTill(DateTimeUtil.parseDate(patientVO.getValidTill()));
			} catch (ParseException e) {
				logger.error("Error while parsing passport valid till date :" + patientVO.getValidTill(), e);
				throw new EhmsBusinessException(e, "Unable to parse ValidTill Date: " + patientVO.getValidTill());
			}
		}
		patient.setAdharNo(patientVO.getAdharNo());
		patient.setRelativeName(patientVO.getRelativeName());
		patient.setRelation(patientVO.getRelation());
		patient.setReligion(patientVO.getReligion());
		if (patientVO.getSponsorCorporateId() != null) {
			patient.setSponsorCorporateId(patientVO.getSponsorCorporateId());
		}
		patient.setNomineeType(patientVO.getNomineeTypeId());
		patient.setNomineeName(patientVO.getNomineeName());
		Assert.notNull(patientVO.getCustomerId(), "Customer information not provided");
		Customer customer = new Customer(patientVO.getCustomerId());
		Assert.notNull(customer, "Customer not found");
		patient.setCustomer(customer);
		if (patientVO.getHospitalId() != null) {
			Hospital hospital = new Hospital(patientVO.getHospitalId());
			patient.setHospital(hospital);
		}
		return patient;
	}

	public PatientVO getPatientVO(Patient patient) {
		PatientVO vo = null;
		if (patient != null) {
			vo = new PatientVO();
			vo.setPatientId(patient.getPatientId());
			vo.setTitleId(patient.getTitleId());
			vo.setPatientCode(patient.getPatientCode());
			vo.setFirstName(patient.getFirstName());
			vo.setMiddleName(patient.getMiddleName());
			vo.setLastName(patient.getLastName());
			vo.setBirthDate(DateTimeUtil.formatDate(patient.getBirthDate()));
			vo.setBirthYear(patient.getBirthYear());
			vo.setIsDobAvailable(patient.getIsDobAvailable());
			vo.setGender(patient.getGender());
			vo.setMaritalStatus(patient.getMaritalStatus());
			vo.setCurrentAddress(new AddressVO(patient.getCurrentAddress()));
			vo.setPermanentAddress(new AddressVO(patient.getPermanentAddress()));
			vo.setContactNumber(patient.getContactNumber());
			vo.setMobileNumber(patient.getMobileNumber());
			vo.setNationality(patient.getNationality());
			vo.setAllergy(patient.getAllergy());
			vo.setPassportNumber(patient.getPassportNumber());
			vo.setValidTill(DateTimeUtil.formatDate(patient.getValidTill()));
			vo.setAdharNo(patient.getAdharNo());
			vo.setReferenceDoctor(patient.getReferenceDoctor());
			vo.setRelativeName(patient.getRelativeName());
			vo.setRelation(patient.getRelation());
			vo.setReligion(patient.getReligion());
			vo.setSponsorCorporateId(patient.getSponsorCorporateId());
			vo.setNomineeTypeId(patient.getNomineeType());
			vo.setNomineeName(patient.getNomineeName());
			if (patient.getCustomer() != null) {
				vo.setCustomerId(patient.getCustomer().getCustomerId());
			}
			if (patient.getHospital() != null) {
				vo.setHospitalId(patient.getHospital().getHospitalId());
			}
		}
		return vo;
	}

	public PatientVO getPatientGridVO(Patient patient) {
		PatientVO vo = null;
		if (patient != null) {
			vo = new PatientVO();
			vo.setPatientId(patient.getPatientId());
			vo.setTitleId(patient.getTitleId());
			vo.setPatientCode(patient.getPatientCode());
			vo.setFirstName(patient.getFirstName());
			vo.setMiddleName(patient.getMiddleName());
			vo.setLastName(patient.getLastName());
			vo.setGender(patient.getGender());
			vo.setMobileNumber(patient.getMobileNumber());
		}
		return vo;
	}

	public PaginationResponseDto<PatientVO> getPatientList(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId, Long hospitalId) {
		PaginationResponseDto<PatientVO> result = new PaginationResponseDto<PatientVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<PatientVO> patient = patientRepository.getPatientList("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (patient != null) {
			result.setCount(patient.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(patient.getTotalElements());
			result.setTotalPages(patient.getTotalPages());
			List<PatientVO> patientList = patient.getContent();
			result.setItems(patientList);
		}
		return result;
	}

	public PaginationResponseDto<PatientVO> searchPatientList(PatientVO patientVO, Integer page, Integer pageSize, Long customerId, Long hospitalId) {
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		PaginationResponseDto<PatientVO> result = new PaginationResponseDto<PatientVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject("ASC", "name"));
		Page<Patient> patients = patientRepository.findAll(patientSpecificationFactory.extendedPatientSearch(patientVO, customerId, hospitalId), pageRequest);
		if (patients != null) {
			result.setCount(patients.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(patients.getTotalElements());
			result.setTotalPages(patients.getTotalPages());
			List<Patient> patientList = patients.getContent();
			List<PatientVO> vos = new ArrayList<>();
			if (patientList != null) {
				for (Patient patient : patientList) {
					vos.add(getPatientGridVO(patient));
				}
			}
			result.setItems(vos);
		}
		return result;
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "name":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "firstName", "lastName");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	public PatientVO getPatientDetails(Long patientId) {
		Assert.notNull(patientId, "Patient Id not provided");
		Patient patient = patientRepository.findOne(patientId);
		Assert.notNull(patient, "Patient with patientId " + patientId + " Not Found");
		return getPatientVO(patient);
	}

	@Transactional
	public PatientVO saveUpdatePatient(Long customerId, Long hospitalId, PatientVO patientVO, Boolean isNewPatient) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		Assert.isTrue(customerRepository.exists(customerId), "Customer not found.");
		Assert.isTrue(hospitalRepository.exists(hospitalId), "Hospital not found.");
		Long currentAddressId = null;
		Long permanentAddressId = null;
		patientVO.setCustomerId(customerId);
		patientVO.setHospitalId(hospitalId);
		Patient patient = null;
		if (patientVO.getPatientId() == null) {
			patient = new Patient();
			String patientCode = "C" + patientVO.getCustomerId() + "H" + patientVO.getHospitalId() + "_" + String.valueOf(new Date().getTime());
			patient.setPatientCode(patientCode);
		} else {
			patient = patientRepository.findOne(patientVO.getPatientId());
			Assert.notNull(patient, "Patient with id: " + patientVO.getPatientId() + " not found.");
			patientVO.setPatientCode(patient.getPatientCode());
			if (patient.getCurrentAddress() != null && patient.getCurrentAddress().getAddressId() != null) {
				currentAddressId = patient.getCurrentAddress().getAddressId();
			}
			if (patient.getPermanentAddress() != null && patient.getPermanentAddress().getAddressId() != null) {
				permanentAddressId = patient.getPermanentAddress().getAddressId();
			}
		}
		Patient patientToBeSaved = populatePatientEntity(patientVO, patient);
		patientRepository.save(patientToBeSaved);
		patientVO.setPatientId(patientToBeSaved.getPatientId());
		if (isNewPatient) {
			Assert.notNull(patientToBeSaved.getPatientId(), "Patient was not saved.");
			String patientCode = "C" + customerId + "H" + hospitalId + "_" + patientToBeSaved.getPatientId();
			patientToBeSaved.setPatientCode(patientCode);
			patientVO.setPatientCode(patientCode);
			patientRepository.save(patientToBeSaved);
			if (patientVO.getCurrentAddress() != null && patient.getCurrentAddress() != null) {
				patientVO.getCurrentAddress().setAddressId(patient.getCurrentAddress().getAddressId());
			}
			if (patientVO.getPermanentAddress() != null && patient.getPermanentAddress() != null) {
				patientVO.getPermanentAddress().setAddressId(patient.getPermanentAddress().getAddressId());
			}
		} else {
			removeAddressIfNull(currentAddressId, permanentAddressId, patientVO);
		}
		return patientVO;
	}

	private void removeAddressIfNull(Long currentAddressId, Long permanentAddressId, PatientVO patientVO) {
		if (currentAddressId != null && patientVO.getCurrentAddress() == null) {
			addressRepository.delete(currentAddressId);
		}
		if (permanentAddressId != null && patientVO.getPermanentAddress() == null) {
			addressRepository.delete(permanentAddressId);
		}

	}
}
