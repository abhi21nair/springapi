
package com.ehms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.ehms.model.Customer;
import com.ehms.model.Department;
import com.ehms.model.Hospital;
import com.ehms.model.PathologyTestMaster;
import com.ehms.repositories.PathologyTestMasterRepository;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.PathologyTestMasterVO;

@Service
public class PathologyTestMasterService {

	@Autowired
	private PathologyTestMasterRepository pathologyTestMasterRepository;

	public PaginationResponseDto<PathologyTestMasterVO> getPathologyTestMasterDetails(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId,
			Long hospitalId) {
		PaginationResponseDto<PathologyTestMasterVO> result = new PaginationResponseDto<PathologyTestMasterVO>();
		Sort sort = new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, sortOn);
		PageRequest pageRequest = new PageRequest(page, pageSize, sort);
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<PathologyTestMasterVO> pathologyTestMasterDetails = pathologyTestMasterRepository.findpathologyTestMasterDetails("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (pathologyTestMasterDetails != null) {
			result.setCount(pathologyTestMasterDetails.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(pathologyTestMasterDetails.getTotalElements());
			result.setTotalPages(pathologyTestMasterDetails.getTotalPages());
			List<PathologyTestMasterVO> pathologyTestMasterDetailsList = pathologyTestMasterDetails.getContent();
			result.setItems(pathologyTestMasterDetailsList);
		}
		return result;
	}

	public PathologyTestMasterVO getPathologyTestMaster(Long pathologyTestMasterId) {
		Assert.notNull(pathologyTestMasterId, "pathologyTestMaster Id not provided.");
		PathologyTestMaster pathologyTestMaster = pathologyTestMasterRepository.findOne(pathologyTestMasterId);
		Assert.notNull(pathologyTestMaster, "pathologyTestMaster not found.");
		return getPathologyTestMasterVO(pathologyTestMaster);
	}

	private PathologyTestMasterVO getPathologyTestMasterVO(PathologyTestMaster pathologyTestMaster) {
		Assert.notNull(pathologyTestMaster, "pathologyTestMaster information not found.");
		PathologyTestMasterVO vo = null;
		if (pathologyTestMaster != null) {
			vo = new PathologyTestMasterVO();
			vo.setPathologyTestMasterId(pathologyTestMaster.getPathologyTestMasterId());
			vo.setGenderWiseValue(pathologyTestMaster.getGenderWiseValue());
			vo.setContainerTypeId(pathologyTestMaster.getContainerTypeId());
			if (pathologyTestMaster.getLab() != null) {
				vo.setLabId(pathologyTestMaster.getLab().getDepartmentId());
			}
			if (pathologyTestMaster.getLab() != null) {
				vo.setLabName(pathologyTestMaster.getLab().getDepartmentName());
			}
			vo.setNormalValue(pathologyTestMaster.getNormalValue());
			vo.setResultTypeId(pathologyTestMaster.getResultTypeId());
			vo.setSpecimenTypeId(pathologyTestMaster.getSpecimenTypeId());
			vo.setSubTestName(pathologyTestMaster.getSubTestName());
			vo.setTestName(pathologyTestMaster.getTestName());
			vo.setUnit(pathologyTestMaster.getUnit());
			vo.setValueTypeId(pathologyTestMaster.getValueTypeId());
			vo.setSpecimenTypeId(pathologyTestMaster.getSpecimenTypeId());
			vo.setIsActive(pathologyTestMaster.getIsActive());
			if (pathologyTestMaster.getCustomer() != null) {
				vo.setCustomerId(pathologyTestMaster.getCustomer().getCustomerId());
			}
			if (pathologyTestMaster.getHospital() != null) {
				vo.setHospitalId(pathologyTestMaster.getHospital().getHospitalId());
			}
		}
		return vo;
	}

	@Transactional
	public PathologyTestMaster saveUpdatePathologyTestMaster(Long customerId, Long hospitalId, PathologyTestMasterVO pathologyTestMasterVO) {

		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(customerId, "Hospital ID not provided.");
		pathologyTestMasterVO.setCustomerId(customerId);
		pathologyTestMasterVO.setHospitalId(hospitalId);
		PathologyTestMaster pathologyTestMaster = getPathologyTestMasterEntity(pathologyTestMasterVO);
		pathologyTestMasterRepository.save(pathologyTestMaster);
		pathologyTestMasterVO.setPathologyTestMasterId(pathologyTestMaster.getPathologyTestMasterId());
		return pathologyTestMaster;
	}

	private PathologyTestMaster getPathologyTestMasterEntity(PathologyTestMasterVO pathologyTestMasterVO) {
		PathologyTestMaster pathologyTestMaster = null;

		if (pathologyTestMasterVO.getPathologyTestMasterId() == null) {
			pathologyTestMaster = new PathologyTestMaster();
		} else {
			pathologyTestMaster = pathologyTestMasterRepository.findOne(pathologyTestMasterVO.getPathologyTestMasterId());
			Assert.notNull(pathologyTestMaster, "pathologyTestMaster with id: " + pathologyTestMasterVO.getPathologyTestMasterId() + " not found.");
		}

		pathologyTestMaster.setContainerTypeId(pathologyTestMasterVO.getContainerTypeId());
		pathologyTestMaster.setGenderWiseValue(pathologyTestMasterVO.getGenderWiseValue());
		Assert.notNull(pathologyTestMasterVO.getLabId(), "Lab Id not provided");
		Department department = new Department(pathologyTestMasterVO.getLabId());
		pathologyTestMaster.setLab(department);
		pathologyTestMaster.setNormalValue(pathologyTestMasterVO.getNormalValue());
		pathologyTestMaster.setResultTypeId(pathologyTestMasterVO.getResultTypeId());
		pathologyTestMaster.setSpecimenTypeId(pathologyTestMasterVO.getSpecimenTypeId());
		pathologyTestMaster.setSubTestName(pathologyTestMasterVO.getSubTestName());
		pathologyTestMaster.setTestName(pathologyTestMasterVO.getTestName());
		pathologyTestMaster.setUnit(pathologyTestMasterVO.getUnit());
		pathologyTestMaster.setValueTypeId(pathologyTestMasterVO.getValueTypeId());
		pathologyTestMaster.setIsActive(pathologyTestMasterVO.getIsActive());

		Assert.notNull(pathologyTestMasterVO.getCustomerId(), "Customer information not provided");
		Customer customer = new Customer(pathologyTestMasterVO.getCustomerId());
		Assert.notNull(customer, "Customer not found");
		pathologyTestMaster.setCustomer(customer);

		if (pathologyTestMasterVO.getHospitalId() != null) {
			Hospital hospital = new Hospital(pathologyTestMasterVO.getHospitalId());
			pathologyTestMaster.setHospital(hospital);
		}
		return pathologyTestMaster;
	}

	public List<PathologyTestMasterVO> getPathologyTestMasterList(Long customerId, Long hospitalId) {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		List<PathologyTestMasterVO> pathologyTestMasterList = new ArrayList<PathologyTestMasterVO>();
		pathologyTestMasterList = pathologyTestMasterRepository.getPathologyTestMastersList(customerId, hospitalId);
		return pathologyTestMasterList;
	}
}
