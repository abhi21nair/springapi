package com.ehms.services.sponsorcorporate;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Address;
import com.ehms.model.Customer;
import com.ehms.model.Hospital;
import com.ehms.model.SponsorCorporate;
import com.ehms.repositories.AddressRepository;
import com.ehms.repositories.SponsorCorporateRepository;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.AddressVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.SponsorCorporateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Service
public class SponsorCorporateService {
	@Autowired
	private SponsorCorporateRepository sponsorCorporateRepository;

	@Autowired
	private AddressRepository addressRepository;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public PaginationResponseDto<SponsorCorporateVO> getSponsorCorporateList(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId,
			Long hospitalId) {
		PaginationResponseDto<SponsorCorporateVO> result = new PaginationResponseDto<SponsorCorporateVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<SponsorCorporateVO> sponsorCorporates = sponsorCorporateRepository.findSponsorsCorporates("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (sponsorCorporates != null) {
			result.setCount(sponsorCorporates.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(sponsorCorporates.getTotalElements());
			result.setTotalPages(sponsorCorporates.getTotalPages());
			List<SponsorCorporateVO> sponsorCorporateList = sponsorCorporates.getContent();
			result.setItems(sponsorCorporateList);

		}
		return result;
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "sponsorCorporateName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "sponsorCorporateName");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	public SponsorCorporateVO getSponsorCorporateDetailsById(Long customerId, Long hospitalId, Long sponsorCorporateId) {
		Assert.notNull(sponsorCorporateId, "Sponsor/Corporate ID not provided");
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		SponsorCorporate sponsorCorporate = sponsorCorporateRepository.findSponsorCorporate(sponsorCorporateId, customerId, hospitalId);
		Assert.notNull(sponsorCorporate, "Sponsor/Corporate not found");
		return getSponsorCorporateVO(sponsorCorporate);
	}

	public SponsorCorporateVO saveUpdateSponsorCorporate(Long customerId, Long hospitalId, SponsorCorporateVO sponsorCorporateVO) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		SponsorCorporate sponsorCorporate = getSponsorCorporateEntity(sponsorCorporateVO);
		sponsorCorporateRepository.save(sponsorCorporate);
		AddressVO addressVO = sponsorCorporateVO.getAddress();
		if (sponsorCorporate.getAddress() != null) {
			addressVO.setAddressId(sponsorCorporate.getAddress().getAddressId());
		}
		sponsorCorporateVO.setSponsorCorporateId(sponsorCorporate.getSponsorCorporateId());
		return sponsorCorporateVO;
	}

	private SponsorCorporate getSponsorCorporateEntity(SponsorCorporateVO sponsorCorporateVO) throws EhmsBusinessException {
		SponsorCorporate sponsorCorporate = null;

		if (sponsorCorporateVO.getSponsorCorporateId() == null) {
			sponsorCorporate = new SponsorCorporate();

		} else {
			sponsorCorporate = sponsorCorporateRepository.findOne(sponsorCorporateVO.getSponsorCorporateId());
			Assert.notNull(sponsorCorporate, "Sponsor/Corporate with id: " + sponsorCorporateVO.getSponsorCorporateId() + " not found.");
		}
		AddressVO addressVO = sponsorCorporateVO.getAddress();
		Address address = null;
		if (sponsorCorporate.getAddress() == null) {
			address = new Address();
		} else {
			address = addressRepository.findOne(addressVO.getAddressId());
		}

		sponsorCorporate.setAddress(addressVO.getAddressEntity(address, addressVO));
		sponsorCorporate.setCountryCodeForKeyContactPersonNumber(sponsorCorporateVO.getCountryCodeForKeyContactPersonNumber());
		sponsorCorporate.setCountryCodeForMobileNumber(sponsorCorporateVO.getCountryCodeForMobileNumber());
		sponsorCorporate.setCountryCodeForPhoneNumber(sponsorCorporateVO.getCountryCodeForPhoneNumber());
		Customer customer = new Customer(sponsorCorporateVO.getCustomerId());
		sponsorCorporate.setCustomer(customer);
		try {
			Assert.notNull(sponsorCorporateVO.getEffectiveFromDate(), "Effective From Date not provided.");
			Assert.notNull(sponsorCorporateVO.getEffectiveToDate(), "Effective To Date not provided.");
			Date effectiveToDate = DateTimeUtil.parseDate(sponsorCorporateVO.getEffectiveToDate());
			Date effectiveFromDate = DateTimeUtil.parseDate(sponsorCorporateVO.getEffectiveFromDate());
			Assert.isTrue(effectiveToDate != null && effectiveFromDate != null && effectiveToDate.after(effectiveFromDate), "From date should be before To date");
			sponsorCorporate.setEffectiveFromDate(effectiveFromDate);
			sponsorCorporate.setEffectiveToDate(effectiveToDate);
		} catch (ParseException e1) {
			logger.error("Error while parsing date in getSponsorCorporateEntity() :" + " EffectiveFromDate " + sponsorCorporateVO.getEffectiveFromDate() + " EffectiveToDate "
					+ sponsorCorporateVO.getEffectiveToDate(), e1);
			throw new EhmsBusinessException(e1, "Unable to parse date while saving/updating sponsor corporate.");
		}
		Hospital hospital = new Hospital(sponsorCorporateVO.getHospitalId());
		sponsorCorporate.setHospital(hospital);
		sponsorCorporate.setKeyContactPerson(sponsorCorporateVO.getKeyContactPerson());
		sponsorCorporate.setKeyContactPersonNumber(sponsorCorporateVO.getKeyContactPersonNumber());
		sponsorCorporate.setMobileNumber(sponsorCorporateVO.getMobileNumber());
		sponsorCorporate.setPhoneNumber(sponsorCorporateVO.getPhoneNumber());

		sponsorCorporate.setSponsorCorporateName(sponsorCorporateVO.getSponsorCorporateName());
		sponsorCorporate.setSponsorCorporateNumber(sponsorCorporateVO.getSponsorCorporateNumber());
		sponsorCorporate.setSponsorCorporateTypeId(sponsorCorporateVO.getSponsorCorporateTypeId());
		sponsorCorporate.setMaximumLimitPatientWise(sponsorCorporateVO.getMaximumLimitPatientWise());
		sponsorCorporate.setMaximumLimitSponsorCorporateWise(sponsorCorporateVO.getMaximumLimitSponsorCorporateWise());
		return sponsorCorporate;
	}

	private SponsorCorporateVO getSponsorCorporateVO(SponsorCorporate sponsorCorporate) {
		SponsorCorporateVO vo = null;
		if (sponsorCorporate != null) {
			vo = new SponsorCorporateVO();
			vo.setSponsorCorporateId(sponsorCorporate.getSponsorCorporateId());
			vo.setMaximumLimitPatientWise(sponsorCorporate.getMaximumLimitPatientWise());
			vo.setMaximumLimitSponsorCorporateWise(sponsorCorporate.getMaximumLimitSponsorCorporateWise());
			vo.setAddress(new AddressVO(sponsorCorporate.getAddress()));
			vo.setCountryCodeForKeyContactPersonNumber(sponsorCorporate.getCountryCodeForKeyContactPersonNumber());
			vo.setCountryCodeForMobileNumber(sponsorCorporate.getCountryCodeForMobileNumber());
			vo.setCountryCodeForPhoneNumber(sponsorCorporate.getCountryCodeForPhoneNumber());
			if (sponsorCorporate.getCustomer() != null) {
				vo.setCustomerId(sponsorCorporate.getCustomer().getCustomerId());
			}
			vo.setEffectiveFromDate(DateTimeUtil.formatDate(sponsorCorporate.getEffectiveFromDate()));
			vo.setEffectiveToDate(DateTimeUtil.formatDate(sponsorCorporate.getEffectiveToDate()));
			if (sponsorCorporate.getHospital() != null) {
				vo.setHospitalId(sponsorCorporate.getHospital().getHospitalId());
			}
			vo.setKeyContactPerson(sponsorCorporate.getKeyContactPerson());
			vo.setKeyContactPersonNumber(sponsorCorporate.getKeyContactPersonNumber());
			vo.setMobileNumber(sponsorCorporate.getMobileNumber());
			vo.setPhoneNumber(sponsorCorporate.getPhoneNumber());
			vo.setSponsorCorporateName(sponsorCorporate.getSponsorCorporateName());
			vo.setSponsorCorporateNumber(sponsorCorporate.getSponsorCorporateNumber());
			vo.setSponsorCorporateTypeId(sponsorCorporate.getSponsorCorporateTypeId());
		}
		return vo;
	}

	public List<SponsorCorporateVO> getSponsorCorporateList(Long customerId, Long hospitalId){
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		return sponsorCorporateRepository.getSponsorCorporateList(customerId,hospitalId);
	}
}
