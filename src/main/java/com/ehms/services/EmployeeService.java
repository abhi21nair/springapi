package com.ehms.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Address;
import com.ehms.model.Customer;
import com.ehms.model.Department;
import com.ehms.model.Employee;
import com.ehms.model.Hospital;
import com.ehms.model.MiscValue;
import com.ehms.model.Title;
import com.ehms.repositories.AddressRepository;
import com.ehms.repositories.DepartmentRepository;
import com.ehms.repositories.EmployeeRepository;
import com.ehms.repositories.misctype.MiscValueRepository;
import com.ehms.security.JwtUser;
import com.ehms.services.user.UserService;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.AddressVO;
import com.ehms.vos.EmployeeVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ValidationCheckResponseVO;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private DepartmentRepository departmentRepository;

	@Autowired
	private MiscValueRepository miscValueRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private UserService userService;

	public Employee getEmployeeEntity(EmployeeVO employeeVO) throws EhmsBusinessException {
		Employee employee = null;
		if (employeeVO.getEmployeeId() == null) {
			employee = new Employee();
			employee.setEmployeeCode(employeeVO.getEmployeeCode());

		} else {
			employee = employeeRepository.findOne(employeeVO.getEmployeeId());
			Assert.notNull(employee, "Employee with id: " + employeeVO.getEmployeeId() + " not found.");
			employeeVO.setEmployeeCode(employee.getEmployeeCode());
		}

		if (employeeVO.getTitleId() != null) {
			Title title = new Title(employeeVO.getTitleId());
			employee.setTitle(title);
		}

		employee.setFirstName(employeeVO.getFirstName());
		employee.setMiddleName(employeeVO.getMiddleName());
		employee.setLastName(employeeVO.getLastName());
		employee.setEmployeeType(employeeVO.getEmployeeType());
		Assert.notNull(employeeVO.getDesignationId(), "Employee designation not provided");
		MiscValue designation = miscValueRepository.findOne(employeeVO.getDesignationId());
		Assert.notNull(designation, "Employee designation is incorrect");
		employee.setDesignation(designation);

		if (employeeVO.getDepartmentId() != null) {
			Department department = departmentRepository.findOne(employeeVO.getDepartmentId());
			Assert.notNull(department, "Department is incorrect");
			employee.setDepartment(department);

		}

		try {
			employee.setBirthDate(DateTimeUtil.parseDate(employeeVO.getBirthDate()));
		} catch (ParseException e) {
			throw new EhmsBusinessException(e, "Unable to parse birth date: " + employeeVO.getBirthDate());
		}
		employee.setBirthYear(employeeVO.getBirthYear());
		employee.setIsDobAvailable(employeeVO.getIsDOBAvailable());
		try {
			employee.setJoiningDate(DateTimeUtil.parseDate(employeeVO.getJoiningDate()));
		} catch (ParseException e) {
			throw new EhmsBusinessException(e, "Unable to parse joining date: " + employeeVO.getJoiningDate());

		}
		employee.setGender(employeeVO.getGender());
		employee.setMaritalStatus(employeeVO.getMaritalStatus());
		employee.setNationality(employeeVO.getNationality());
		employee.setAdharNo(employeeVO.getAdharNo());

		if (employeeVO.getOtherIdTypeId() != null) {
			MiscValue otherIdType = miscValueRepository.findOne(employeeVO.getOtherIdTypeId());
			Assert.notNull(otherIdType, "Employee other ID type is incorrect");
			employee.setOtherIdType(otherIdType);
		}

		employee.setOtherIdNo(employeeVO.getOtherIdNo());
		try {
			employee.setOtherIdValidTill(DateTimeUtil.parseDate(employeeVO.getOtherIdValidTill()));
		} catch (ParseException e) {
			throw new EhmsBusinessException(e, "Unable to parse other id valid till date: " + employeeVO.getOtherIdValidTill());
		}
		employee.setPanNo(employeeVO.getPanNo());
		employee.setTanNo(employeeVO.getTanNo());
		employee.setQualification(employeeVO.getQualification());

		if (employeeVO.getSpecializationId() != null) {
			MiscValue specialization = miscValueRepository.findOne(employeeVO.getSpecializationId());
			Assert.notNull(specialization, "Employee specialization is incorrect");
			employee.setSpecialization(specialization);
		}
		if (employeeVO.getJoiningTypeId() != null) {
			MiscValue joiningType = miscValueRepository.findOne(employeeVO.getJoiningTypeId());
			Assert.notNull(joiningType, "Employee joining type is incorrect");
			employee.setJoiningType(joiningType);
		}
		employee.setHospitalSharePc(employeeVO.getHospitalSharePc());
		employee.setHospitalShareRs(employeeVO.getHospitalShareRs());
		employee.setDoctorSharePc(employeeVO.getDoctorSharePc());
		employee.setDoctorShareRs(employeeVO.getDoctorShareRs());
		AddressVO currentAddressVO = employeeVO.getCurrentAddress();
		Address currentAddress = null;
		if (currentAddressVO.getAddressId() == null) {
			currentAddress = new Address();
		} else {
			currentAddress = addressRepository.findOne(currentAddressVO.getAddressId());
			Assert.notNull(currentAddress, "Current address is not found");
		}
		employee.setCurrentAddress(currentAddressVO.getAddressEntity(currentAddress, currentAddressVO));
		AddressVO permanentAddressVO = employeeVO.getPermanentAddress();
		Address permanentAddress = null;
		if (permanentAddressVO.getAddressId() == null) {
			permanentAddress = new Address();
		} else {
			permanentAddress = addressRepository.findOne(permanentAddressVO.getAddressId());
			Assert.notNull(permanentAddress, "Permanent address is not found");
		}
		employee.setPermanentAddress(permanentAddressVO.getAddressEntity(permanentAddress, permanentAddressVO));
		employee.setEmail(employeeVO.getEmail());
		employee.setCountryCodeForContactNumber(employeeVO.getCountryCodeForContactNumber());
		employee.setContactNumber(employeeVO.getContactNumber());
		employee.setCountryCodeForMobileNumber(employeeVO.getCountryCodeForMobileNumber());
		employee.setMobileNumber(employeeVO.getMobileNumber());
		employee.setNomineeName(employeeVO.getNomineeName());
		employee.setNomineeRelation(employeeVO.getNomineeRelation());
		employee.setIsActive(employeeVO.getIsActive());

		if (employeeVO.getCustomerId() != null) {
			Customer customer = new Customer(employeeVO.getCustomerId());
			employee.setCustomer(customer);
		}

		if (employeeVO.getHospitalId() != null) {
			Hospital hospital = new Hospital(employeeVO.getHospitalId());
			employee.setHospital(hospital);
		}
		return employee;
	}

	@JsonIgnore
	public EmployeeVO getEmployeeVO(Employee employee) {
		EmployeeVO vo = null;
		if (employee != null) {
			vo = new EmployeeVO();
			vo.setEmployeeId(employee.getEmployeeId());
			if (employee.getTitle() != null) {
				vo.setTitleId(employee.getTitle().getTitleId());
			}
			vo.setFirstName(employee.getFirstName());
			vo.setMiddleName(employee.getMiddleName());
			vo.setLastName(employee.getLastName());
			if (employee.getDesignation() != null) {
				vo.setDesignationId(employee.getDesignation().getMiscTypeValueId());
				vo.setDesignation(employee.getDesignation().getMiscValue());
			}
			if (employee.getDepartment() != null) {
				vo.setDepartmentId(employee.getDepartment().getDepartmentId());
			}
			vo.setEmployeeCode(employee.getEmployeeCode());
			vo.setEmployeeType(employee.getEmployeeType());
			vo.setBirthDate(DateTimeUtil.formatDate(employee.getBirthDate()));
			vo.setBirthYear(employee.getBirthYear());
			vo.setIsDOBAvailable(employee.getIsDobAvailable());
			vo.setJoiningDate(DateTimeUtil.formatDate(employee.getJoiningDate()));
			vo.setGender(employee.getGender());
			vo.setMaritalStatus(employee.getMaritalStatus());
			vo.setNationality(employee.getNationality());
			vo.setAdharNo(employee.getAdharNo());
			if (employee.getOtherIdType() != null) {
				vo.setOtherIdTypeId(employee.getOtherIdType().getMiscTypeValueId());
			}
			vo.setOtherIdNo(employee.getOtherIdNo());
			vo.setOtherIdValidTill(DateTimeUtil.formatDate(employee.getOtherIdValidTill()));
			vo.setPanNo(employee.getPanNo());
			vo.setTanNo(employee.getTanNo());
			vo.setQualification(employee.getQualification());
			if (employee.getSpecialization() != null) {
				vo.setSpecializationId(employee.getSpecialization().getMiscTypeValueId());
			}
			if (employee.getJoiningType() != null) {
				vo.setJoiningTypeId(employee.getJoiningType().getMiscTypeValueId());
			}
			vo.setHospitalSharePc(employee.getHospitalSharePc());
			vo.setHospitalShareRs(employee.getHospitalShareRs());
			vo.setDoctorSharePc(employee.getDoctorSharePc());
			vo.setDoctorShareRs(employee.getDoctorShareRs());
			vo.setCurrentAddress(new AddressVO(employee.getCurrentAddress()));
			vo.setPermanentAddress(new AddressVO(employee.getPermanentAddress()));
			vo.setEmail(employee.getEmail());
			vo.setCountryCodeForContactNumber(employee.getCountryCodeForContactNumber());
			vo.setContactNumber(employee.getContactNumber());
			vo.setCountryCodeForMobileNumber(employee.getCountryCodeForMobileNumber());
			vo.setMobileNumber(employee.getMobileNumber());
			vo.setNomineeName(employee.getNomineeName());
			vo.setNomineeRelation(employee.getNomineeRelation());
			vo.setIsActive(employee.getIsActive());
			if (employee.getCustomer() != null) {
				vo.setCustomerId(employee.getCustomer().getCustomerId());
			}
			if (employee.getHospital() != null) {
				vo.setHospitalId(employee.getHospital().getHospitalId());
			}
		}
		return vo;
	}

	@Transactional
	public Employee saveUpdateAdminEmployee(Long customerId, Long hospitalId, EmployeeVO employeeVO) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not provided.");
		employeeVO.setCustomerId(customerId);
		employeeVO.setHospitalId(hospitalId);
		Employee employee = getEmployeeEntity(employeeVO);
		employeeRepository.save(employee);
		return employee;
	}

	public PaginationResponseDto<EmployeeVO> getEmployeeList(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId, Long hospitalId) {
		PaginationResponseDto<EmployeeVO> result = new PaginationResponseDto<EmployeeVO>();
		JwtUser user = userService.getCurrentUser();
		Sort sort = new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, sortOn);
		PageRequest pageRequest = new PageRequest(page, pageSize, sort);
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Assert.notNull(user, "User not found.");
		Assert.notNull(user.getEmployeeId(), "Employee Id not found.");
		Page<EmployeeVO> employees = employeeRepository.findEmployeesOfHospital("%" + searchText + "%", customerId, hospitalId, user.getEmployeeId(), pageRequest);
		if (employees != null) {
			result.setCount(employees.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(employees.getTotalElements());
			result.setTotalPages(employees.getTotalPages());
			List<EmployeeVO> employeesList = employees.getContent();
			result.setItems(employeesList);
		}
		return result;
	}

	public EmployeeVO getEmployeeDetails(Long employeeId) {
		Assert.notNull(employeeId, "Employee ID not provided");
		Employee employee = employeeRepository.findOne(employeeId);
		Assert.notNull(employee, "Employee with Id " + employeeId + " not found");
		return getEmployeeVO(employee);
	}

	public EmployeeVO getSystemEmployeeDetails(Long employeeId) {
		Assert.notNull(employeeId, "Employee ID not provided");
		Employee employee = employeeRepository.findSystemEmployee(employeeId);
		Assert.notNull(employee, "Employee with Id " + employeeId + " not found");
		return getEmployeeVO(employee);
	}

	@Transactional
	public EmployeeVO saveUpdateEmployee(Long customerId, Long hospitalId, EmployeeVO employeeVO) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		employeeVO.setCustomerId(customerId);
		employeeVO.setHospitalId(hospitalId);
		Employee employee = getEmployeeEntity(employeeVO);
		employeeRepository.save(employee);
		employeeVO.setEmployeeId(employee.getEmployeeId());
		if (employee.getCurrentAddress() != null && employeeVO.getCurrentAddress() != null) {
			employeeVO.getCurrentAddress().setAddressId(employee.getCurrentAddress().getAddressId());
		}
		if (employee.getPermanentAddress() != null && employeeVO.getPermanentAddress() != null) {
			employeeVO.getPermanentAddress().setAddressId(employee.getPermanentAddress().getAddressId());
		}
		return employeeVO;
	}

	@Transactional
	public EmployeeVO saveUpdateSystemEmployee(EmployeeVO employeeVO) throws EhmsBusinessException {
		Employee employee = getEmployeeEntity(employeeVO);
		employeeRepository.save(employee);
		employeeVO.setEmployeeId(employee.getEmployeeId());
		if (employee.getCurrentAddress() != null && employeeVO.getCurrentAddress() != null) {
			employeeVO.getCurrentAddress().setAddressId(employee.getCurrentAddress().getAddressId());
		}
		if (employee.getPermanentAddress() != null && employeeVO.getPermanentAddress() != null) {
			employeeVO.getPermanentAddress().setAddressId(employee.getPermanentAddress().getAddressId());
		}
		return employeeVO;
	}

	public List<EmployeeVO> getEmployeeWithoutEhmsAccount(Long customerId, Long hospitalId) {
		Assert.notNull(customerId, "Customer not found.");
		Assert.notNull(hospitalId, "Hospital not found.");
		return employeeRepository.getEmployeeWithoutEhmsAccount(customerId, hospitalId);
	}

	public List<EmployeeVO> getSystemEmployeeWithoutEhmsAccount() {
		return employeeRepository.getSystemEmployeeWithoutEhmsAccount();
	}

	public List<EmployeeVO> getEmployeesList(Long customerId, Long hospitalId, Integer employeeType) {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		List<EmployeeVO> employeeList = new ArrayList<EmployeeVO>();
		if (employeeType == null) {
			employeeList = employeeRepository.getEmployeeListForAllEmployeeTypes(customerId, hospitalId);
		} else {
			switch (employeeType) {
			case Employee.EMPLOYEE_TYPE_DOCTOR:
			case Employee.EMPLOYEE_TYPE_NURSE:
			case Employee.EMPLOYEE_TYPE_STAFF:
				employeeList = employeeRepository.getEmployeeList(customerId, hospitalId, employeeType);
				break;
			default:
				throw new IllegalArgumentException("Given Employee Type Does not exist");
			}
		}
		return employeeList;

	}

	public ValidationCheckResponseVO validateEmployee(EmployeeVO employeeVO, Long customerId, Long hospitalId) {
		Assert.isTrue(employeeVO != null && employeeVO.getEmployeeCode() != null, "Employee code not provided");
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		Long employeeId = null;
		if (customerId != null && hospitalId != null) {
			employeeId = employeeRepository.findEmployeesForCustomerAndHospital(employeeVO.getEmployeeCode(), customerId, hospitalId);
		} else {
			throw new IllegalArgumentException("Information not consistent");
		}

		if (employeeId != null) {
			return new ValidationCheckResponseVO(false);
		} else {
			return new ValidationCheckResponseVO(true);
		}
	}

	public ValidationCheckResponseVO validateSystemEmployee(EmployeeVO employeeVO) {
		Assert.isTrue(employeeVO != null && employeeVO.getEmployeeCode() != null, "Employee code not provided");
		Long employeeId = employeeRepository.findSystemEmployees(employeeVO.getEmployeeCode());
		if (employeeId != null) {
			return new ValidationCheckResponseVO(false);
		} else {
			return new ValidationCheckResponseVO(true);
		}
	}

	public PaginationResponseDto<EmployeeVO> getSystemEmployeeList(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText) {
		PaginationResponseDto<EmployeeVO> result = new PaginationResponseDto<EmployeeVO>();
		Sort sort = new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, sortOn);
		PageRequest pageRequest = new PageRequest(page, pageSize, sort);
		Page<EmployeeVO> employees = employeeRepository.findSystemEmployees("%" + searchText + "%", pageRequest);
		if (employees != null) {
			result.setCount(employees.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(employees.getTotalElements());
			result.setTotalPages(employees.getTotalPages());
			List<EmployeeVO> employeesList = employees.getContent();
			result.setItems(employeesList);
		}
		return result;
	}
}
