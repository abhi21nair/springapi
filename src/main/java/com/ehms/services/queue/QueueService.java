package com.ehms.services.queue;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.repositories.queue.QueueRepository;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.queue.QueueResponseVO;

import io.jsonwebtoken.lang.Assert;

@Service
public class QueueService {

	@Autowired
	private QueueRepository queueRepository;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public PaginationResponseDto<QueueResponseVO> getQueueDetails(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId,
			Long hospitalId, String date, Long moduleId, Long doctorId) throws EhmsBusinessException {
		Assert.isTrue(moduleId != null || doctorId != null, "Doctor or module information not provided.");
		Assert.isTrue(date != null && !date.trim().isEmpty(), "Date is not provided");
		Date queryDate = null;
		try {
			queryDate = DateTimeUtil.parseDate(date);
		} catch (ParseException e) {
			logger.error("Error while parsing date: " + date + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Error while parsing date: " + date + " due to: " + e.getMessage());
		}
		Assert.notNull(queryDate, "Date can not be parsed");
		PaginationResponseDto<QueueResponseVO> result = new PaginationResponseDto<QueueResponseVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Page<QueueResponseVO> queueData = queueRepository.getQueueDetails("%" + searchText + "%", customerId, hospitalId, queryDate, moduleId, doctorId, pageRequest);
		if (queueData != null) {
			result.setCount(queueData.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(queueData.getTotalElements());
			result.setTotalPages(queueData.getTotalPages());
			List<QueueResponseVO> queuePatientList = queueData.getContent();
			result.setItems(queuePatientList);
		}
		return result;
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "default":
			return JpaSort.unsafe(Sort.Direction.DESC, "(CASE WHEN a.reportTime IS NULL THEN 0 ELSE 1 END)").and(new Sort(Sort.Direction.ASC, "reportTime"))
					.and(JpaSort.unsafe(Sort.Direction.DESC, "(CASE WHEN a.createdDt IS NULL THEN 0 ELSE 1 END)")).and(new Sort(Sort.Direction.ASC, "createdDt"));
		case "name":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "patient.firstName", "patient.lastName");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

}
