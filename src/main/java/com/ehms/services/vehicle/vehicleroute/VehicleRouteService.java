package com.ehms.services.vehicle.vehicleroute;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Customer;
import com.ehms.model.Hospital;
import com.ehms.model.Vehicle;
import com.ehms.model.VehicleRoute;
import com.ehms.repositories.VehicleRouteRepository;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.VehicleRouteVO;

@Service
public class VehicleRouteService {
	@Autowired
	private VehicleRouteRepository vehicleRouteRepository;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public PaginationResponseDto<VehicleRouteVO> getVehicleRoutesList(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId,
			Long hospitalId) {
		PaginationResponseDto<VehicleRouteVO> result = new PaginationResponseDto<VehicleRouteVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<VehicleRouteVO> vehicleRoutes = vehicleRouteRepository.findVehicleRoutes("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (vehicleRoutes != null) {
			result.setCount(vehicleRoutes.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(vehicleRoutes.getTotalElements());
			result.setTotalPages(vehicleRoutes.getTotalPages());
			List<VehicleRouteVO> vehicleRouteList = vehicleRoutes.getContent();
			result.setItems(vehicleRouteList);
		}
		return result;
	}

	public VehicleRouteVO saveUpdateVehicleRoute(Long customerId, Long hospitalId, VehicleRouteVO vehicleRouteVO) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		VehicleRoute vehicleRoute = getVehicleRouteEntity(vehicleRouteVO);
		vehicleRouteRepository.save(vehicleRoute);
		vehicleRouteVO.setVehicleRouteId(vehicleRoute.getVehicleRouteId());
		logger.debug("Vehicle " + vehicleRouteVO.getVehicleName() + " Saved/Updated Successfully");
		return vehicleRouteVO;
	}

	public VehicleRouteVO getVehicleRouteDetailsById(Long customerId, Long hospitalId, Long vehicleRouteId) {
		Assert.notNull(vehicleRouteId, "Vehicle Route ID not provided");
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		VehicleRoute vehicleRoute = vehicleRouteRepository.findVehicleRoute(vehicleRouteId, customerId, hospitalId);
		Assert.notNull(vehicleRoute, "Vehicle Route not found");
		return getVehicleRouteVO(vehicleRoute);
	}

	private VehicleRoute getVehicleRouteEntity(VehicleRouteVO vehicleRouteVO) throws EhmsBusinessException {
		VehicleRoute vehicleRoute = null;
		if (vehicleRouteVO.getVehicleRouteId() == null) {
			vehicleRoute = new VehicleRoute();
		} else {
			vehicleRoute = vehicleRouteRepository.findOne(vehicleRouteVO.getVehicleRouteId());
			Assert.notNull(vehicleRoute, "Vehicle Route with id: " + vehicleRouteVO.getVehicleRouteId() + " not found.");
		}
		vehicleRoute.setDestination(vehicleRouteVO.getDestination());
		vehicleRoute.setGpsBasedRoute(vehicleRouteVO.getGpsBasedRoute());
		vehicleRoute.setRoute(vehicleRouteVO.getRoute());
		vehicleRoute.setRouteKm(vehicleRouteVO.getRouteKm());
		try {
			Date routeValidityFrom = DateTimeUtil.parseDate(vehicleRouteVO.getRouteValidityFrom());
			Date routeValidityTo = DateTimeUtil.parseDate(vehicleRouteVO.getRouteValidityTo());
			if (routeValidityFrom != null && routeValidityTo != null) {
				Assert.isTrue(routeValidityTo.after(routeValidityFrom), "From date should be before To date");
				vehicleRoute.setRouteValidityFrom(routeValidityFrom);
				vehicleRoute.setRouteValidityTo(routeValidityTo);
			}
		} catch (ParseException e) {
			logger.error("ParseException in getVehicleRouteEntity(): " + " RouteValidityFrom : " + vehicleRouteVO.getRouteValidityFrom() + "RouteValidityTo : "
					+ vehicleRouteVO.getRouteValidityTo(), e);
			throw new EhmsBusinessException(e, "Unable to parse date while saving/updating Vehicle Route.");
		}
		vehicleRoute.setSource(vehicleRouteVO.getSource());
		Vehicle vehicle = new Vehicle(vehicleRouteVO.getVehicleId());
		vehicleRoute.setVehicle(vehicle);
		vehicleRoute.setIsActive(vehicleRouteVO.getIsActive());
		Customer customer = new Customer(vehicleRouteVO.getCustomerId());
		vehicleRoute.setCustomer(customer);
		Hospital hospital = new Hospital(vehicleRouteVO.getHospitalId());
		vehicleRoute.setHospital(hospital);
		return vehicleRoute;
	}

	private VehicleRouteVO getVehicleRouteVO(VehicleRoute vehicleRoute) {
		VehicleRouteVO vo = null;
		if (vehicleRoute != null) {
			vo = new VehicleRouteVO();
			vo.setVehicleRouteId(vehicleRoute.getVehicleRouteId());
			vo.setDestination(vehicleRoute.getDestination());
			vo.setGpsBasedRoute(vehicleRoute.getGpsBasedRoute());
			vo.setRoute(vehicleRoute.getRoute());
			vo.setRouteKm(vehicleRoute.getRouteKm());
			vo.setRouteValidityFrom(DateTimeUtil.formatDate(vehicleRoute.getRouteValidityFrom()));
			vo.setRouteValidityTo(DateTimeUtil.formatDate(vehicleRoute.getRouteValidityTo()));
			vo.setSource(vehicleRoute.getSource());
			vo.setIsActive(vehicleRoute.getIsActive());
			if (vehicleRoute.getVehicle() != null) {
				vo.setVehicleId(vehicleRoute.getVehicle().getVehicleId());
				vo.setVehicleName(vehicleRoute.getVehicle().getVehicleName());
				vo.setVehicleNumber(vehicleRoute.getVehicle().getVehicleNumber());
			}
			if (vehicleRoute.getCustomer() != null) {
				vo.setCustomerId(vehicleRoute.getCustomer().getCustomerId());
			}
			if (vehicleRoute.getHospital() != null) {
				vo.setHospitalId(vehicleRoute.getHospital().getHospitalId());
			}
		}
		return vo;
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "route":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "route");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

}
