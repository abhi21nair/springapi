package com.ehms.services.vehicle;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.model.Customer;
import com.ehms.model.Hospital;
import com.ehms.model.Vehicle;
import com.ehms.repositories.VehicleRepository;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.VehicleVO;

@Service
public class VehicleService {
	@Autowired
	private VehicleRepository vehicleRepository;

	public PaginationResponseDto<VehicleVO> getVehiclesList(Integer page, Integer pageSize, String sortOn,
			String sortOrder, String searchText, Long customerId, Long hospitalId) {
		PaginationResponseDto<VehicleVO> result = new PaginationResponseDto<VehicleVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<VehicleVO> vehicles = vehicleRepository.findVehicles("%" + searchText + "%", customerId, hospitalId,
				pageRequest);
		if (vehicles != null) {
			result.setCount(vehicles.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(vehicles.getTotalElements());
			result.setTotalPages(vehicles.getTotalPages());
			List<VehicleVO> vehicleList = vehicles.getContent();
			result.setItems(vehicleList);

		}
		return result;
	}

	public VehicleVO getVehicleDetailsById(Long customerId, Long hospitalId, Long vehicleId) {
		Assert.notNull(vehicleId, "Vehicle ID not provided");
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		Vehicle vehicle = vehicleRepository.findVehicle(vehicleId, customerId, hospitalId);
		Assert.notNull(vehicle, "Vehicle not found");
		return getVehicleVO(vehicle);
	}

	public VehicleVO saveUpdateVehicle(Long customerId, Long hospitalId, VehicleVO vehicleVO) {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		Vehicle vehicle = getVehicleEntity(vehicleVO);
		vehicleRepository.save(vehicle);
		vehicleVO.setVehicleId(vehicle.getVehicleId());
		return vehicleVO;
	}

	public List<VehicleVO> getVehiclesList(Long customerId, Long hospitalId) {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		return vehicleRepository.getVehicleList(customerId, hospitalId);
	}
	
	private Vehicle getVehicleEntity(VehicleVO vehicleVO) {
		Vehicle vehicle = null;
		if (vehicleVO.getVehicleId() == null) {
			vehicle = new Vehicle();
		} else {
			vehicle = vehicleRepository.findOne(vehicleVO.getVehicleId());
			Assert.notNull(vehicle, "Vehicle with id: " + vehicleVO.getVehicleId() + " not found.");
		}
		vehicle.setIsActive(vehicleVO.getIsActive());
		vehicle.setAveragePerKm(vehicleVO.getAveragePerKm());
		vehicle.setChasisNumber(vehicleVO.getChasisNumber());
		Customer customer = new Customer(vehicleVO.getCustomerId());
		vehicle.setCustomer(customer);
		vehicle.setFuelCapacity(vehicleVO.getFuelCapacity());
		vehicle.setGpsEnabled(vehicleVO.getGpsEnabled());
		Hospital hospital = new Hospital(vehicleVO.getHospitalId());
		vehicle.setHospital(hospital);
		vehicle.setInsuranceNumber(vehicleVO.getInsuranceNumber());
		vehicle.setInsuranceNumberValidity(vehicleVO.getInsuranceNumberValidity());
		vehicle.setRtoRegistrationNumber(vehicleVO.getRtoRegistrationNumber());
		vehicle.setRtoRegistrationNumberValidity(vehicleVO.getRtoRegistrationNumberValidity());
		vehicle.setTypeOfFuel(vehicleVO.getTypeOfFuel());
		vehicle.setVehicleColor(vehicleVO.getVehicleColor());
		vehicle.setVehicleManufacturer(vehicleVO.getVehicleManufacturer());
		vehicle.setVehicleName(vehicleVO.getVehicleName());
		vehicle.setVehicleNumber(vehicleVO.getVehicleNumber());
		vehicle.setVehicleTypeId(vehicleVO.getVehicleTypeId());
		vehicle.setYearOfModel(vehicleVO.getYearOfModel());
		return vehicle;
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "vehicleName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC,
					"vehicleName");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	private VehicleVO getVehicleVO(Vehicle vehicle) {
		VehicleVO vo = null;
		if (vehicle != null) {
			vo = new VehicleVO();
			vo.setIsActive(vehicle.getIsActive());
			vo.setAveragePerKm(vehicle.getAveragePerKm());
			vo.setChasisNumber(vehicle.getChasisNumber());
			vo.setFuelCapacity(vehicle.getFuelCapacity());
			vo.setGpsEnabled(vehicle.getGpsEnabled());
			vo.setInsuranceNumber(vehicle.getInsuranceNumber());
			vo.setInsuranceNumberValidity(vehicle.getInsuranceNumberValidity());
			vo.setRtoRegistrationNumber(vehicle.getRtoRegistrationNumber());
			vo.setRtoRegistrationNumberValidity(vehicle.getRtoRegistrationNumberValidity());
			vo.setTypeOfFuel(vehicle.getTypeOfFuel());
			vo.setVehicleColor(vehicle.getVehicleColor());
			vo.setVehicleId(vehicle.getVehicleId());
			vo.setVehicleManufacturer(vehicle.getVehicleManufacturer());
			vo.setVehicleName(vehicle.getVehicleName());
			vo.setVehicleNumber(vehicle.getVehicleNumber());
			vo.setVehicleTypeId(vehicle.getVehicleTypeId());
			vo.setYearOfModel(vehicle.getYearOfModel());
			if (vehicle.getCustomer() != null) {
				vo.setCustomerId(vehicle.getCustomer().getCustomerId());
			}
			if (vehicle.getHospital() != null) {
				vo.setHospitalId(vehicle.getHospital().getHospitalId());
			}
		}
		return vo;
	}

}
