package com.ehms.services.ipd;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.DischargeSheet;
import com.ehms.repositories.DischargeSheetRepository;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.DischargeSheetVO;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.ResponseVO.Status;

@Service
public class DischargeSheetService {
	@Autowired
	private DischargeSheetRepository dischargeSheetRepository;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public List<DischargeSheetVO> getDischargeSheets(Long patientId) {
		Assert.notNull(patientId, "Patient ID not provided");
		List<DischargeSheetVO> dischargeSheets = dischargeSheetRepository.findDischargeSheets(patientId);
		return dischargeSheets;
	}

	public DischargeSheetVO saveUpdateDischargeSheet(DischargeSheetVO dischargeSheetVO) throws EhmsBusinessException {
		DischargeSheet dischargeSheet = getDischargeSheetEntity(dischargeSheetVO);
		dischargeSheetRepository.save(dischargeSheet);
		dischargeSheetVO.setDischargeSheetId(dischargeSheet.getDischargeSheetId());
		return dischargeSheetVO;
	}

	public ResponseVO deleteDischargeSheet(Long dischargeSheetId) {
		Assert.notNull(dischargeSheetId, "Discharge Sheet Id not provided");
		DischargeSheet dischargeSheetToDelete = dischargeSheetRepository.findOne(dischargeSheetId);
		Assert.notNull(dischargeSheetToDelete, "Discharge Sheet not found");
		if (dischargeSheetToDelete != null) {
			dischargeSheetRepository.delete(dischargeSheetId);
			return new ResponseVO(Status.SUCCESS, "Discharge Sheet Deleted successfully");
		}
		return new ResponseVO(Status.FAILURE, "Discharge Sheet deletion Failure");
	}

	private DischargeSheet getDischargeSheetEntity(DischargeSheetVO dischargeSheetVO) throws EhmsBusinessException {
		DischargeSheet dischargeSheet = null;
		if (dischargeSheetVO.getDischargeSheetId() == null) {
			dischargeSheet = new DischargeSheet();
		} else {
			dischargeSheet = dischargeSheetRepository.findOne(dischargeSheetVO.getDischargeSheetId());
			Assert.notNull(dischargeSheet, "Discharge sheet with id: " + dischargeSheetVO.getDischargeSheetId() + " not found.");
		}
		Assert.notNull(dischargeSheetVO.getPatientId(), "Patient Id not provided");
		Assert.notNull(dischargeSheetVO.getAppointmentId(), "Appointment Id not provided");
		dischargeSheet.setAdvice(dischargeSheetVO.getAdvice());
		dischargeSheet.setCauseOfDeath(dischargeSheetVO.getCauseOfDeath());
		try {
			dischargeSheet.setDeathDate(DateTimeUtil.parseDate(dischargeSheetVO.getDeathDate()));
		} catch (ParseException e) {
			logger.error("Unable to parse the Death date: " + dischargeSheetVO.getDeathDate() + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Unable to parse the Death date: " + dischargeSheetVO.getDeathDate() + " due to: " + e.getMessage());
		}
		dischargeSheet.setDischargeCoditionId(dischargeSheetVO.getDischargeCoditionId());
		try {
			dischargeSheet.setDischargeDate(DateTimeUtil.parseDate(dischargeSheetVO.getDischargeDate()));
		} catch (ParseException e) {
			logger.error("Unable to parse the Discharge date: " + dischargeSheetVO.getDischargeDate() + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Unable to parse the Discharge date: " + dischargeSheetVO.getDischargeDate() + " due to: " + e.getMessage());
		}
		dischargeSheet.setAppointmentId(dischargeSheetVO.getAppointmentId());
		dischargeSheet.setNextAppointment(dischargeSheetVO.getNextAppointment());
		dischargeSheet.setOtherDetails(dischargeSheetVO.getOtherDetails());
		dischargeSheet.setPatientId(dischargeSheetVO.getPatientId());
		return dischargeSheet;
	}

}
