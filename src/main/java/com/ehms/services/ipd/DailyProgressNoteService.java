package com.ehms.services.ipd;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.DailyProgressNote;
import com.ehms.repositories.DailyProgressNoteRepository;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.DailyProgressNoteVO;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.ResponseVO.Status;

@Service
public class DailyProgressNoteService {
	@Autowired
	private DailyProgressNoteRepository dailyProgressNoteRepository;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public List<DailyProgressNoteVO> getDailyProgerssNotes(Long patientId) {
		Assert.notNull(patientId, "Patient ID not provided");
		List<DailyProgressNoteVO> dailyProgressNotes = dailyProgressNoteRepository.findDailyProgressNotes(patientId);
		return dailyProgressNotes;
	}

	public DailyProgressNoteVO saveUpdateDailyProgerssNotes(DailyProgressNoteVO dailyProgressNoteVO) throws EhmsBusinessException {
		DailyProgressNote dailyProgressNote = getDailyProgressNoteEntity(dailyProgressNoteVO);
		dailyProgressNoteRepository.save(dailyProgressNote);
		dailyProgressNoteVO.setDailyProgressNoteId(dailyProgressNote.getDailyProgressNoteId());
		return dailyProgressNoteVO;
	}

	public ResponseVO deleteDailyProgerssNote(Long dailyProgressNoteId) {
		Assert.notNull(dailyProgressNoteId, "Daily progress note Id not provided");
		DailyProgressNote dailyProgressNoteToDelete = dailyProgressNoteRepository.findOne(dailyProgressNoteId);
		Assert.notNull(dailyProgressNoteToDelete, "Daily progress note not found");
		if (dailyProgressNoteToDelete != null) {
			dailyProgressNoteRepository.delete(dailyProgressNoteId);
			return new ResponseVO(Status.SUCCESS, "Daily progress note Deleted successfully");
		}
		return new ResponseVO(Status.FAILURE, "Daily progress note deletion Failure");
	}

	private DailyProgressNote getDailyProgressNoteEntity(DailyProgressNoteVO dailyProgressNoteVO) throws EhmsBusinessException {
		DailyProgressNote dailyProgressNote = null;
		if (dailyProgressNoteVO.getDailyProgressNoteId() == null) {
			dailyProgressNote = new DailyProgressNote();
		} else {
			dailyProgressNote = dailyProgressNoteRepository.findOne(dailyProgressNoteVO.getDailyProgressNoteId());
			Assert.notNull(dailyProgressNote, "Daily progress note with id: " + dailyProgressNoteVO.getDailyProgressNoteId() + " not found.");
		}
		Assert.notNull(dailyProgressNoteVO.getPatientId(), "Patient Id not provided");
		Assert.notNull(dailyProgressNoteVO.getAppointmentId(), "Appointment Id not provided");
		dailyProgressNote.setAppointmentId(dailyProgressNoteVO.getAppointmentId());
		dailyProgressNote.setDoctorDayProgressNotes(dailyProgressNoteVO.getDoctorDayProgressNotes());
		dailyProgressNote.setDoctorNightProgressNotes(dailyProgressNoteVO.getDoctorNightProgressNotes());
		dailyProgressNote.setPatientId(dailyProgressNoteVO.getPatientId());
		try {
			dailyProgressNote.setProgressDate(DateTimeUtil.parseDate(dailyProgressNoteVO.getProgressDate()));
		} catch (ParseException e) {
			logger.error("Unable to parse the Progress date: " + dailyProgressNoteVO.getProgressDate() + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Unable to parse the Progress date: " + dailyProgressNoteVO.getProgressDate() + " due to: " + e.getMessage());
		}
		dailyProgressNote.setPreviousNotes(dailyProgressNoteVO.getPreviousNotes());
		return dailyProgressNote;
	}

}
