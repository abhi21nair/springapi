package com.ehms.services.ipd;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.PatientPostmortem;
import com.ehms.repositories.PatientPostmortemRepository;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.PatientPostmortemVO;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.ResponseVO.Status;

@Service
public class PatientPostmortemService {
	@Autowired
	private PatientPostmortemRepository patientPostmortemRepository;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public List<PatientPostmortemVO> getPatientPostmortems(Long patientId) {
		Assert.notNull(patientId, "Patient ID not provided");
		List<PatientPostmortemVO> patientPostmortems = patientPostmortemRepository.findPatientPostmortems(patientId);
		return patientPostmortems;
	}

	public PatientPostmortemVO saveUpdatePatientPostmortem(PatientPostmortemVO patientPostmortemVO) throws EhmsBusinessException {
		PatientPostmortem patientPostmortem = getPatientPostmortemEntity(patientPostmortemVO);
		patientPostmortemRepository.save(patientPostmortem);
		patientPostmortemVO.setPatientPostmortemId(patientPostmortem.getPatientPostmortemId());
		return patientPostmortemVO;
	}

	public ResponseVO deletePatientPostmortem(Long patientPostmortemId) {
		Assert.notNull(patientPostmortemId, "Patient postmortem Id not provided");
		PatientPostmortem patientPostmortemToDelete = patientPostmortemRepository.findOne(patientPostmortemId);
		Assert.notNull(patientPostmortemToDelete, "Patient postmortem not found");
		if (patientPostmortemToDelete != null) {
			patientPostmortemRepository.delete(patientPostmortemId);
			return new ResponseVO(Status.SUCCESS, "Patient postmortem Deleted successfully");
		}
		return new ResponseVO(Status.FAILURE, "Patient postmortem deletion Failure");
	}

	private PatientPostmortem getPatientPostmortemEntity(PatientPostmortemVO patientPostmortemVO) throws EhmsBusinessException {
		PatientPostmortem patientPostmortem = null;
		if (patientPostmortemVO.getPatientPostmortemId() == null) {
			patientPostmortem = new PatientPostmortem();
		} else {
			patientPostmortem = patientPostmortemRepository.findOne(patientPostmortemVO.getPatientPostmortemId());
			Assert.notNull(patientPostmortem, "Patient postmortem with id: " + patientPostmortemVO.getPatientPostmortemId() + " not found.");
		}
		Assert.notNull(patientPostmortemVO.getPatientId(), "Patient Id not provided");
		Assert.notNull(patientPostmortemVO.getAppointmentId(), "Appointment Id not provided");
		patientPostmortem.setCauseOfDeath(patientPostmortemVO.getCauseOfDeath());
		patientPostmortem.setDeadBodyHanded(patientPostmortemVO.getDeadBodyHanded());
		patientPostmortem.setDeadBodyHandedTo(patientPostmortemVO.getDeadBodyHandedTo());
		try {
			patientPostmortem.setDeathDate(DateTimeUtil.parseDate(patientPostmortemVO.getDeathDate()));
		} catch (ParseException e) {
			logger.error("Unable to parse the Death date: " + patientPostmortemVO.getDeathDate() + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Unable to parse the Death date: " + patientPostmortemVO.getDeathDate() + " due to: " + e.getMessage());
		}
		try {
			patientPostmortem.setMorgueTime(DateTimeUtil.parseTime(patientPostmortemVO.getMorgueTime()));
		} catch (ParseException e) {
			logger.error("Unable to parse the Morgue time: " + patientPostmortemVO.getMorgueTime() + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Unable to parse the Morgue time: " + patientPostmortemVO.getMorgueTime() + " due to: " + e.getMessage());
		}
		patientPostmortem.setPatientId(patientPostmortemVO.getPatientId());
		patientPostmortem.setAppointmentId(patientPostmortemVO.getAppointmentId());
		try {
			patientPostmortem.setPostmortemTime(DateTimeUtil.parseTime(patientPostmortemVO.getPostmortemTime()));
		} catch (ParseException e) {
			logger.error("Unable to parse the Postmortem time: " + patientPostmortemVO.getPostmortemTime() + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Unable to parse the Postmortem date: " + patientPostmortemVO.getPostmortemTime() + " due to: " + e.getMessage());
		}
		return patientPostmortem;
	}

}
