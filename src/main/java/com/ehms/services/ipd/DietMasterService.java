package com.ehms.services.ipd;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.DietMaster;
import com.ehms.repositories.DietMasterRepository;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.DietMasterVO;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.ResponseVO.Status;

@Service
public class DietMasterService {

	@Autowired
	private DietMasterRepository dietMasterRepository;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public List<DietMasterVO> getDiets(Long patientId) {
		Assert.notNull(patientId, "Patient ID not provided");
		List<DietMasterVO> diets = dietMasterRepository.findDiets(patientId);
		return diets;
	}

	public DietMasterVO saveUpdateDiet(DietMasterVO dietMasterVO) throws EhmsBusinessException {
		DietMaster dietMaster = getDietEntity(dietMasterVO);
		dietMasterRepository.save(dietMaster);
		dietMasterVO.setDietMasterId(dietMaster.getDietMasterId());
		return dietMasterVO;
	}

	public ResponseVO deleteDiet(Long dietMasterId) {
		Assert.notNull(dietMasterId, "Diet Master Id not provided");
		DietMaster dietMasterToDelete = dietMasterRepository.findOne(dietMasterId);
		Assert.notNull(dietMasterToDelete, "Diet Master not found");
		if (dietMasterToDelete != null) {
			dietMasterRepository.delete(dietMasterId);
			return new ResponseVO(Status.SUCCESS, "Diet Deleted successfully");
		}
		return new ResponseVO(Status.FAILURE, "Diet deletion Failure");
	}

	private DietMaster getDietEntity(DietMasterVO dietMasterVO) throws EhmsBusinessException {
		DietMaster dietMaster = null;
		if (dietMasterVO.getDietMasterId() == null) {
			dietMaster = new DietMaster();
		} else {
			dietMaster = dietMasterRepository.findOne(dietMasterVO.getDietMasterId());
			Assert.notNull(dietMaster, "Diet with id: " + dietMasterVO.getDietMasterId() + " not found.");
		}
		Assert.notNull(dietMasterVO.getPatientId(), "Patient Id not provided");
		Assert.notNull(dietMasterVO.getAppointmentId(), "Appointment Id not provided");
		dietMaster.setBreakfast(dietMasterVO.getBreakfast());
		try {
			dietMaster.setDietDate(DateTimeUtil.parseDate(dietMasterVO.getDietDate()));
		} catch (ParseException e) {
			logger.error("Unable to parse the Diet date: " + dietMasterVO.getDietDate() + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Unable to parse the Diet date: " + dietMasterVO.getDietDate() + " due to: " + e.getMessage());
		}
		dietMaster.setAppointmentId(dietMasterVO.getAppointmentId());
		dietMaster.setDinner(dietMasterVO.getDinner());
		dietMaster.setLunch(dietMasterVO.getLunch());
		dietMaster.setPatientId(dietMasterVO.getPatientId());
		dietMaster.setMorningTea(dietMasterVO.getMorningTea());
		dietMaster.setEveningTea(dietMasterVO.getEveningTea());
		return dietMaster;
	}
}
