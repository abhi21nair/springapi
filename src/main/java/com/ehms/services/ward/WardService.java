package com.ehms.services.ward;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.ehms.model.Bed;
import com.ehms.model.Customer;
import com.ehms.model.Hospital;
import com.ehms.model.Ward;
import com.ehms.repositories.WardRepository;
import com.ehms.vos.BedVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ValidationCheckResponseVO;
import com.ehms.vos.WardBedFlatVO;
import com.ehms.vos.WardVO;

@Service
public class WardService {

	@Autowired
	private WardRepository wardRepository;

	@Autowired
	private BedService bedService;

	public PaginationResponseDto<WardVO> getWardDetails(Long customerId, Long hospitalId, Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText) {
		PaginationResponseDto<WardVO> result = new PaginationResponseDto<WardVO>();
		Sort sort = new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, sortOn);
		PageRequest pageRequest = new PageRequest(page, pageSize, sort);
		Page<WardVO> wardDetails = wardRepository.findWardsOfHospital("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (wardDetails != null) {
			result.setCount(wardDetails.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(wardDetails.getTotalElements());
			result.setTotalPages(wardDetails.getTotalPages());
			List<WardVO> wardDetailsList = wardDetails.getContent();
			result.setItems(wardDetailsList);
		}
		return result;
	}

	public WardVO getWardDetails(Long wardId) {
		Assert.notNull(wardId, "Ward ID not provided.");
		Ward ward = wardRepository.findOne(wardId);
		Assert.notNull(ward, "Ward not found.");
		WardVO wardVO = getWardVO(ward);
		return wardVO;
	}

	private WardVO getWardVO(Ward ward) {
		Assert.notNull(ward, "Ward information not found.");
		WardVO wardVO = new WardVO();
		wardVO.setWardId(ward.getWardId());
		wardVO.setFloorId(ward.getFloorId());
		wardVO.setWardName(ward.getWardName());
		wardVO.setWardTypeId(ward.getWardTypeId());
		wardVO.setBedCategoryId(ward.getBedCategoryId());
		wardVO.setBedChargesFullDay(ward.getBedChargesFullDay());
		wardVO.setBedChargesHourly(ward.getBedChargesHourly());
		List<Bed> bedlist = ward.getBeds();
		wardVO.setBedVO(getBedsVO(bedlist));
		if (ward.getCustomer() != null) {
			wardVO.setCustomerId(ward.getCustomer().getCustomerId());
		}
		if (ward.getHospital() != null) {
			wardVO.setHospitalId(ward.getHospital().getHospitalId());
		}
		return wardVO;
	}

	List<BedVO> getBedsVO(List<Bed> bedlist) {
		List<BedVO> bedVOs = new ArrayList<>();
		for (Bed bed : bedlist) {
			bedVOs.add(bedService.getBedVO(bed));
		}
		return bedVOs;
	}

	private List<Bed> getBedEntity(List<BedVO> bedVOlists, Ward ward, Boolean isNewWard) {
		List<Bed> beds = new ArrayList<>();
		if (bedVOlists != null) {
			for (BedVO bedVO : bedVOlists) {
				Bed bed = bedService.getBedEntity(bedVO, isNewWard);
				bed.setWard(ward);
				beds.add(bed);
			}
		}
		return beds;
	}

	@Transactional
	public void saveUpdateWardDetails(WardVO wardVO, Boolean isNewWard) {
		Ward ward = getWardEntity(wardVO, isNewWard);
		wardRepository.save(ward);
		wardVO.setWardId(ward.getWardId());

	}

	private Ward getWardEntity(WardVO wardVO, Boolean isNewWard) {
		Ward ward = null;
		if (isNewWard) {
			ward = new Ward();
			ward.setBeds(getBedEntity(wardVO.getBedVO(), ward, isNewWard));
			ward.setCustomer(new Customer(wardVO.getCustomerId()));
			ward.setHospital(new Hospital(wardVO.getHospitalId()));
		} else {
			ward = wardRepository.findOne(wardVO.getWardId());
		}
		ward.setBedCategoryId(wardVO.getBedCategoryId());
		ward.setBedChargesFullDay(wardVO.getBedChargesFullDay());
		ward.setBedChargesHourly(wardVO.getBedChargesHourly());
		ward.setFloorId(wardVO.getFloorId());
		ward.setWardName(wardVO.getWardName());
		ward.setWardTypeId(wardVO.getWardTypeId());
		return ward;
	}

	public ValidationCheckResponseVO validateWardName(WardVO ward, Long customerId, Long hospitalId) {
		Assert.notNull(customerId, "customer ID not provided");
		Assert.notNull(hospitalId, "Hospital ID not provided");
		Assert.isTrue(ward != null && ward.getWardName() != null && !ward.getWardName().isEmpty(), "Ward name to validate not provided.");
		Long wardId = null;
		if (ward.getWardId() == null) {
			wardId = wardRepository.findByWardNameIgnoreCaseAndCustomerHospital(ward.getWardName(), customerId, hospitalId);
		} else {
			wardId = wardRepository.findByWardNameIgnoreCaseAndCustomerHospitalWardIdNot(ward.getWardName(), customerId, hospitalId, ward.getWardId());
		}
		if (wardId == null) {
			return new ValidationCheckResponseVO(true);
		} else {
			return new ValidationCheckResponseVO(false);
		}
	}

	public Collection<WardVO> getAvailableBeds(Long customerId, Long hospitalId) {
		Assert.notNull(customerId, "customer ID not provided");
		Assert.notNull(hospitalId, "Hospital ID not provided");
		List<WardBedFlatVO> wardBedFlatVO = wardRepository.findAvailableBeds(customerId, hospitalId);
		return populateIntermediateMaps(wardBedFlatVO);
	}

	private Collection<WardVO> populateIntermediateMaps(List<WardBedFlatVO> wardBedFlatVO) {
		LinkedHashMap<Long, WardVO> resultMap = new LinkedHashMap<Long, WardVO>();
		WardVO ward = null;
		for (WardBedFlatVO wardBedVO : wardBedFlatVO) {
			ward = resultMap.get(wardBedVO.getWardId());
			if (ward == null) {
				ward = new WardVO();
				ward.setWardId(wardBedVO.getWardId());
				ward.setWardName(wardBedVO.getWardName());
				resultMap.put(wardBedVO.getWardId(), ward);
			}
			ward.addBed(wardBedVO);
		}
		return resultMap.values();
	}
}
