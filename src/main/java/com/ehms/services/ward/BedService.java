package com.ehms.services.ward;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Bed;
import com.ehms.model.Ward;
import com.ehms.repositories.BedRepository;
import com.ehms.vos.BedVO;
import com.ehms.vos.ValidationCheckResponseVO;

@Service
public class BedService {

	@Autowired
	private BedRepository bedRepository;

	public BedVO getBedDetails(Long bedId) {
		Assert.notNull(bedId, "Bed ID not provided.");
		Bed bed = bedRepository.findOne(bedId);
		Assert.notNull(bed, "Bed not found.");
		BedVO bedVO = getBedVO(bed);
		return bedVO;
	}

	public BedVO getBedVO(Bed bed) {
		Assert.notNull(bed, "Bed information not provided.");
		BedVO vo = new BedVO();
		vo.setBedCode(bed.getBedCode());
		vo.setBedId(bed.getBedId());
		if (bed.getWard() != null) {
			vo.setWardId(bed.getWard().getWardId());
		}
		return vo;
	}

	@Transactional
	public void saveUpdateBedDetails(BedVO bedVO, boolean isNewBed) throws EhmsBusinessException {
		Bed bed = getBedEntity(bedVO, isNewBed);
		bedRepository.save(bed);
		bedVO.setBedId(bed.getBedId());
	}

	public Bed getBedEntity(BedVO bedVO, Boolean isNewBed) {
		Bed bed = null;
		if (isNewBed) {
			bed = new Bed();
		} else {
			Assert.notNull(bedVO.getBedId(), "bed Id not found in request.");
			bed = bedRepository.findOne(bedVO.getBedId());
			Assert.notNull(bed, "Bed not found in database.");
		}
		bed.setBedCode(bedVO.getBedCode());
		if (bedVO.getWardId() != null) {
			bed.setWard(new Ward(bedVO.getWardId()));
		}
		return bed;
	}

	public void deleteBed(Long id) {
		Assert.isTrue(bedRepository.exists(id), "Bed with ID: " + id + " does not exist.");
		bedRepository.delete(id);
	}

	public ValidationCheckResponseVO validateBedCode(BedVO bedVO, Long wardId) {
		Assert.notNull(wardId, "Ward ID not provided");
		Assert.isTrue(bedVO != null && bedVO.getBedCode() != null && !bedVO.getBedCode().isEmpty(), "Bed code not provided for validation");
		Long bedId = null;
		if (bedVO.getBedId() == null) {
			bedId = bedRepository.findByBedCodeIgnoreCaseAndWard(bedVO.getBedCode(), wardId);
		} else {
			bedId = bedRepository.findByBedCodeIgnoreCaseAndWardAndBedIdNot(bedVO.getBedCode(), wardId);
		}
		if (bedId == null) {
			return new ValidationCheckResponseVO(true);
		} else {
			return new ValidationCheckResponseVO(false);
		}
	}
}
