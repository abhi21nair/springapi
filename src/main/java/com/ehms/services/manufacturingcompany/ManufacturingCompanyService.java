package com.ehms.services.manufacturingcompany;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.model.Address;
import com.ehms.model.Customer;
import com.ehms.model.Hospital;
import com.ehms.model.ManufacturingCompany;
import com.ehms.repositories.AddressRepository;
import com.ehms.repositories.ManufacturingCompanyRepository;
import com.ehms.vos.AddressVO;
import com.ehms.vos.ManufacturingCompanyVO;
import com.ehms.vos.PaginationResponseDto;

@Service
public class ManufacturingCompanyService {
	@Autowired
	private ManufacturingCompanyRepository manufacturingCompanyRepository;

	@Autowired
	private AddressRepository addressRepository;

	public PaginationResponseDto<ManufacturingCompanyVO> getManufacturingCompanyList(Integer page, Integer pageSize,
			String sortOn, String sortOrder, String searchText, Long customerId, Long hospitalId) {
		PaginationResponseDto<ManufacturingCompanyVO> result = new PaginationResponseDto<ManufacturingCompanyVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<ManufacturingCompanyVO> manufacturingCompanies = manufacturingCompanyRepository
				.findManufacturingCompanies("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (manufacturingCompanies != null) {
			result.setCount(manufacturingCompanies.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(manufacturingCompanies.getTotalElements());
			result.setTotalPages(manufacturingCompanies.getTotalPages());
			List<ManufacturingCompanyVO> manufacturingCompaniesList = manufacturingCompanies.getContent();
			result.setItems(manufacturingCompaniesList);

		}
		return result;
	}

	public ManufacturingCompanyVO getManufacturingCompanyDetailsById(Long customerId, Long hospitalId,
			Long manufacturingCompanyId) {
		Assert.notNull(manufacturingCompanyId, "Manufacturing Company ID not provided");
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		ManufacturingCompany manufacturingCompany = manufacturingCompanyRepository
				.findManufacturingComapny(manufacturingCompanyId, customerId, hospitalId);
		Assert.notNull(manufacturingCompany, "Manufacturing Company not found");
		return getManufacturingCompanyVO(manufacturingCompany);
	}

	public ManufacturingCompanyVO saveUpdateManufacturingCompany(Long customerId, Long hospitalId,
			ManufacturingCompanyVO manufacturingCompanyVO) {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		ManufacturingCompany manufacturingCompany = getManufacturingCompanyEntity(manufacturingCompanyVO);
		AddressVO addressVO = manufacturingCompanyVO.getAddress();
		manufacturingCompanyRepository.save(manufacturingCompany);
		if (manufacturingCompany.getAddress() != null) {
			addressVO.setAddressId(manufacturingCompany.getAddress().getAddressId());
		}
		manufacturingCompanyVO.setManufacturingCompanyId(manufacturingCompany.getManufacturingCompanyId());
		return manufacturingCompanyVO;
	}

	private ManufacturingCompany getManufacturingCompanyEntity(ManufacturingCompanyVO manufacturingCompanyVO) {
		ManufacturingCompany manufacturingCompany = null;

		if (manufacturingCompanyVO.getManufacturingCompanyId() == null) {
			manufacturingCompany = new ManufacturingCompany();

		} else {
			manufacturingCompany = manufacturingCompanyRepository
					.findOne(manufacturingCompanyVO.getManufacturingCompanyId());
			Assert.notNull(manufacturingCompany, "Manufacturing Company with id: "
					+ manufacturingCompanyVO.getManufacturingCompanyId() + " not found.");
		}

		AddressVO addressVO = manufacturingCompanyVO.getAddress();
		Address address = null;
		if (manufacturingCompany.getAddress() == null) {
			address = new Address();
		} else {
			address = addressRepository.findOne(addressVO.getAddressId());
		}

		manufacturingCompany.setAddress(addressVO.getAddressEntity(address, addressVO));
		manufacturingCompany.setCcForKeyContactPersonNumber(manufacturingCompanyVO.getCcForKeyContactPersonNumber());
		manufacturingCompany.setCountryCodeForMobileNumber(manufacturingCompanyVO.getCountryCodeForMobileNumber());
		manufacturingCompany.setCountryCodeForPhoneNumber(manufacturingCompanyVO.getCountryCodeForPhoneNumber());
		Customer customer = new Customer(manufacturingCompanyVO.getCustomerId());
		manufacturingCompany.setCustomer(customer);
		Hospital hospital = new Hospital(manufacturingCompanyVO.getHospitalId());
		manufacturingCompany.setHospital(hospital);
		manufacturingCompany.setKeyContactPerson(manufacturingCompanyVO.getKeyContactPerson());
		manufacturingCompany.setKeyContactPersonNumber(manufacturingCompanyVO.getKeyContactPersonNumber());
		manufacturingCompany.setMobileNumber(manufacturingCompanyVO.getMobileNumber());
		manufacturingCompany.setPhoneNumber(manufacturingCompanyVO.getPhoneNumber());
		manufacturingCompany.setIsActive(manufacturingCompanyVO.getIsActive());
		manufacturingCompany.setDlNo(manufacturingCompanyVO.getDlNo());
		manufacturingCompany.setEmail(manufacturingCompanyVO.getEmail());
		manufacturingCompany.setGstNo(manufacturingCompanyVO.getGstNo());
		manufacturingCompany.setManufactureName(manufacturingCompanyVO.getManufactureName());
		manufacturingCompany.setManufactureNumber(manufacturingCompanyVO.getManufactureNumber());
		manufacturingCompany.setOpeningBalance(manufacturingCompanyVO.getOpeningBalance());
		manufacturingCompany.setPanNo(manufacturingCompanyVO.getPanNo());
		manufacturingCompany.setShopActNo(manufacturingCompanyVO.getShopActNo());
		manufacturingCompany.setTanNo(manufacturingCompanyVO.getTanNo());
		manufacturingCompany.setVendorSupplierNo(manufacturingCompanyVO.getVendorSupplierNo());
		return manufacturingCompany;
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "manufactureName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC,
					"manufactureName");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	private ManufacturingCompanyVO getManufacturingCompanyVO(ManufacturingCompany manufacturingCompany) {
		ManufacturingCompanyVO vo = null;
		if (manufacturingCompany != null) {
			vo = new ManufacturingCompanyVO();
			vo.setManufacturingCompanyId(manufacturingCompany.getManufacturingCompanyId());
			vo.setAddress(new AddressVO(manufacturingCompany.getAddress()));
			vo.setCcForKeyContactPersonNumber(manufacturingCompany.getCcForKeyContactPersonNumber());
			vo.setCountryCodeForMobileNumber(manufacturingCompany.getCountryCodeForMobileNumber());
			vo.setCountryCodeForPhoneNumber(manufacturingCompany.getCountryCodeForPhoneNumber());
			if (manufacturingCompany.getCustomer() != null) {
				vo.setCustomerId(manufacturingCompany.getCustomer().getCustomerId());
			}
			if (manufacturingCompany.getHospital() != null) {
				vo.setHospitalId(manufacturingCompany.getHospital().getHospitalId());
			}
			vo.setKeyContactPerson(manufacturingCompany.getKeyContactPerson());
			vo.setKeyContactPersonNumber(manufacturingCompany.getKeyContactPersonNumber());
			vo.setMobileNumber(manufacturingCompany.getMobileNumber());
			vo.setPhoneNumber(manufacturingCompany.getPhoneNumber());
			vo.setIsActive(manufacturingCompany.getIsActive());
			vo.setDlNo(manufacturingCompany.getDlNo());
			vo.setEmail(manufacturingCompany.getEmail());
			vo.setGstNo(manufacturingCompany.getGstNo());
			vo.setManufactureName(manufacturingCompany.getManufactureName());
			vo.setManufactureNumber(manufacturingCompany.getManufactureNumber());
			vo.setOpeningBalance(manufacturingCompany.getOpeningBalance());
			vo.setPanNo(manufacturingCompany.getPanNo());
			vo.setShopActNo(manufacturingCompany.getShopActNo());
			vo.setTanNo(manufacturingCompany.getTanNo());
			vo.setVendorSupplierNo(manufacturingCompany.getVendorSupplierNo());
		}
		return vo;
	}

}
