package com.ehms.services.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ehms.model.Address;
import com.ehms.repositories.AddressRepository;

@Service
public class AddressService {

	@Autowired
	private AddressRepository addressRepository;

	@Transactional
	public List<Address> getAllAddresses() {
		return addressRepository.findAll();
	}

	@Transactional
	public Address createAddress(Address address) {
		return addressRepository.save(address);
	}

	@Transactional
	public Address getAddressDetailsById(Long id) {
		return addressRepository.findOne(id);
	}

	@Transactional
	public Address updateAddress(Address address) {
		Address addressToUpdate = getAddressDetailsById(address.getAddressId());
		if (addressToUpdate != null) {
			return addressRepository.save(address);
		}
		return null;
	}

	@Transactional
	public Address deleteAddress(Long id) {
		Address addressToDelete = getAddressDetailsById(id);
		if (addressToDelete != null) {
			addressRepository.delete(id);
			return addressToDelete;
		}
		return null;
	}
}
