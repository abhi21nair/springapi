package com.ehms.services.masterdata;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ehms.model.Module;
import com.ehms.repositories.ModuleRepository;

@Service
public class ModuleService {
	@Autowired
	private ModuleRepository moduleRepository;

	public List<Module> getModuleDetails() {
		return moduleRepository.findAll();
	}

}
