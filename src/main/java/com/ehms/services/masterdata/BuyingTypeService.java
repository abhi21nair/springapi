package com.ehms.services.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ehms.model.BuyingType;
import com.ehms.repositories.ByingTypeRepository;

@Service
public class BuyingTypeService {
	@Autowired
	private ByingTypeRepository buyingTypeRepository;

	public List<BuyingType> getbuyingTypeService() {
		return buyingTypeRepository.findAll();
	}
}