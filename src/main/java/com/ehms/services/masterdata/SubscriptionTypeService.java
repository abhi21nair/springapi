package com.ehms.services.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ehms.model.SubscriptionType;
import com.ehms.repositories.SubscriptionTypeRepository;


@Service
public class SubscriptionTypeService {

	
	@Autowired
	private SubscriptionTypeRepository subscriptionTypeRepository;

	public List<SubscriptionType> getSubscriptionTypeDetails() {
		return subscriptionTypeRepository.findAll();
	}	
}
