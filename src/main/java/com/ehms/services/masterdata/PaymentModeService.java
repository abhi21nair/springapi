package com.ehms.services.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ehms.model.PaymentMode;
import com.ehms.repositories.PaymentModeRepository;

@Service
public class PaymentModeService {
	@Autowired
	private PaymentModeRepository paymentModeRepository;

	public List<PaymentMode> getAllPaymentModes() {
		return paymentModeRepository.findAll();
	}
}
