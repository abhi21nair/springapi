package com.ehms.services.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ehms.repositories.CityRepository;
import com.ehms.vos.CityVO;

@Service
public class CityService {

	 @Autowired
	    private CityRepository cityRepository;

		public List<CityVO> getAllCities() {
			  return cityRepository.findAllCities();
		}
}
