package com.ehms.services.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ehms.repositories.CountryRepository;
import com.ehms.vos.CountryVO;

@Service
public class CountryService {

	@Autowired
	private CountryRepository countryRepository;

	public List<CountryVO> getAllCountries() {
		return countryRepository.findAllCountries();
	}
}
