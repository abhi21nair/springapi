package com.ehms.services.masterdata;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ehms.model.BuyingPreference;
import com.ehms.repositories.BuyingPreferenceRepository;

@Service
public class BuyingPreferenceService {
	@Autowired
	private BuyingPreferenceRepository buyingPreferenceRepository;

	public List<BuyingPreference> getBuyingPreferenceDetails() {
		return buyingPreferenceRepository.findAll();
	}
}
