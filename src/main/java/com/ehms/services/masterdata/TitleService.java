package com.ehms.services.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ehms.model.Title;
import com.ehms.repositories.masterdata.TitleRepository;

@Service
public class TitleService {
	@Autowired
	private TitleRepository titleRepository;

	public List<Title> getTitles() {
		return titleRepository.findAll();
	}
}
