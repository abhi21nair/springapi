package com.ehms.services.masterdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ehms.repositories.StateRepository;
import com.ehms.vos.StateVO;

@Service
public class StateService {

	@Autowired
	private StateRepository stateRepository;

	public List<StateVO> getAllStates() {
		return stateRepository.findAllStates();
	}

}
