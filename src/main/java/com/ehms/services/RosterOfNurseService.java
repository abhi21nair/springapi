package com.ehms.services;

import java.sql.Time;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Employee;
import com.ehms.model.RosterOfNurse;
import com.ehms.repositories.RosterOfNurseRepository;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.RosterOfNurseVO;

@Service
public class RosterOfNurseService {
	@Autowired
	private RosterOfNurseRepository rosterOfNurseRepository;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public PaginationResponseDto<RosterOfNurseVO> getRosterOfNurseDetails(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId,
			Long hospitalId) {
		PaginationResponseDto<RosterOfNurseVO> result = new PaginationResponseDto<RosterOfNurseVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Page<RosterOfNurseVO> rosterOfNurses = rosterOfNurseRepository.findRosterOfNurse("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (rosterOfNurses != null) {
			result.setCount(rosterOfNurses.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(rosterOfNurses.getTotalElements());
			result.setTotalPages(rosterOfNurses.getTotalPages());
			List<RosterOfNurseVO> rosterOfNurseList = rosterOfNurses.getContent();
			result.setItems(rosterOfNurseList);

		}
		return result;
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "firstName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "nurseName.firstName");
		case "lastName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "nurseName.lastName");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	public RosterOfNurseVO getRosterOfNurseById(Long rosterOfNurseId) {
		Assert.notNull(rosterOfNurseId, "rosterOfNurseId ID not provided");
		RosterOfNurse rosterOfNurse = rosterOfNurseRepository.findOne(rosterOfNurseId);
		Assert.notNull(rosterOfNurse, "rosterOfNurseId not found");
		return getRosterOfNurseVO(rosterOfNurse);
	}

	public RosterOfNurseVO saveUpdateRosterOfNurse(RosterOfNurseVO rosterOfNurseVO) throws EhmsBusinessException {
		RosterOfNurse rosterOfNurse = getRosterOfNurseEntity(rosterOfNurseVO);
		rosterOfNurseRepository.save(rosterOfNurse);
		rosterOfNurseVO.setRosterOfNurseId(rosterOfNurse.getRosterOfNurseId());
		return rosterOfNurseVO;
	}

	private RosterOfNurse getRosterOfNurseEntity(RosterOfNurseVO rosterOfNurseVO) throws EhmsBusinessException {
		RosterOfNurse rosterOfNurse = null;

		if (rosterOfNurseVO.getRosterOfNurseId() == null) {
			rosterOfNurse = new RosterOfNurse();
		} else {
			rosterOfNurse = rosterOfNurseRepository.findOne(rosterOfNurseVO.getRosterOfNurseId());
			Assert.notNull(rosterOfNurse, "RosterOfNurse with id: " + rosterOfNurseVO.getRosterOfNurseId() + " not found.");
		}

		try {
			Assert.notNull(rosterOfNurseVO.getDateFrom(), "From Date not provided.");
			Assert.notNull(rosterOfNurseVO.getDateTo(), "To Date not provided.");
			Assert.notNull(rosterOfNurseVO.getTimeFrom(), "From Time not provided.");
			Assert.notNull(rosterOfNurseVO.getTimeTo(), "To Time not provided.");
			Date dateFrom = DateTimeUtil.parseDate(rosterOfNurseVO.getDateFrom());
			Date dateTo = DateTimeUtil.parseDate(rosterOfNurseVO.getDateTo());
			Time timeFrom = DateTimeUtil.parseTime(rosterOfNurseVO.getTimeFrom());
			Time timeTo = DateTimeUtil.parseTime(rosterOfNurseVO.getTimeTo());
			Assert.isTrue(dateTo != null && dateFrom != null && dateTo.after(dateFrom), "From date should be before To date");
			rosterOfNurse.setDateFrom(dateFrom);
			rosterOfNurse.setDateTo(dateTo);
			Assert.isTrue(timeFrom != null && (DateTimeUtil.parseTime(RosterOfNurse.TIME_MAX)).after(timeFrom), "Invalid time");
			Assert.isTrue(timeTo != null && timeFrom != null && timeTo.after(timeFrom), "From time should be before To time");
			rosterOfNurse.setTimeFrom(timeFrom);
			rosterOfNurse.setTimeTo(timeTo);
		} catch (ParseException e) {
			logger.error("ParseException Inside getRosterOfNurseEntity() : " + "Time From" + rosterOfNurseVO.getTimeFrom() + "Time To" + rosterOfNurseVO.getTimeTo() + "Date From"
					+ rosterOfNurseVO.getDateFrom() + "Date To" + rosterOfNurseVO.getDateTo(), e);
			throw new EhmsBusinessException(e, "Unable to parse date time while saving/updating roster of nurse.");
		}
		Assert.isTrue(((rosterOfNurseVO.getDay() > -1 && rosterOfNurseVO.getDay() < 7)), "Incorrect Day mentioned in the Roster Of Nurse.");
		rosterOfNurse.setDay(rosterOfNurseVO.getDay());

		Employee nurseName = new Employee(rosterOfNurseVO.getNurseNameId());
		rosterOfNurse.setNurseName(nurseName);
		rosterOfNurse.setRemark(rosterOfNurseVO.getRemark());
		rosterOfNurse.setNurseShift(rosterOfNurseVO.getNurseShiftId());
		rosterOfNurse.setWeeklyOff(rosterOfNurseVO.getWeeklyOff());
		return rosterOfNurse;
	}

	private RosterOfNurseVO getRosterOfNurseVO(RosterOfNurse rosterOfNurse) {
		RosterOfNurseVO vo = null;
		if (rosterOfNurse != null) {
			vo = new RosterOfNurseVO();
			vo.setRosterOfNurseId(rosterOfNurse.getRosterOfNurseId());
			if (rosterOfNurse.getNurseName() != null) {
				vo.setNurseFirstName(rosterOfNurse.getNurseName().getFirstName());
				vo.setNurseMiddleName(rosterOfNurse.getNurseName().getMiddleName());
				vo.setNurseLastName(rosterOfNurse.getNurseName().getLastName());
				if (rosterOfNurse.getNurseName().getTitle() != null) {
					vo.setNurseTitleName(rosterOfNurse.getNurseName().getTitle().getTitleName());
				}
			}
			if (rosterOfNurse.getNurseName() != null) {
				vo.setNurseNameId(rosterOfNurse.getNurseName().getEmployeeId());
			}
			vo.setDateFrom(DateTimeUtil.formatDate(rosterOfNurse.getDateFrom()));
			vo.setDateTo(DateTimeUtil.formatDate(rosterOfNurse.getDateTo()));
			vo.setTimeFrom(DateTimeUtil.formatTime(rosterOfNurse.getTimeFrom()));
			vo.setTimeTo(DateTimeUtil.formatTime(rosterOfNurse.getTimeTo()));
			vo.setDay(rosterOfNurse.getDay());
			vo.setRemark(rosterOfNurse.getRemark());
			vo.setNurseShiftId(rosterOfNurse.getNurseShift());
			vo.setWeeklyOff(rosterOfNurse.getWeeklyOff());
		}
		return vo;
	}

}
