package com.ehms.services;

import java.sql.Time;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Employee;
import com.ehms.model.RosterOfStaff;
import com.ehms.repositories.RosterOfStaffRepository;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.RosterOfStaffVO;

@Service
public class RosterOfStaffService {
	@Autowired
	private RosterOfStaffRepository rosterOfStaffRepository;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public PaginationResponseDto<RosterOfStaffVO> getRosterOfStaffDetails(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId,
			Long hospitalId) {
		PaginationResponseDto<RosterOfStaffVO> result = new PaginationResponseDto<RosterOfStaffVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Page<RosterOfStaffVO> rosterOfStaffs = rosterOfStaffRepository.findRosterOfStaff("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (rosterOfStaffs != null) {
			result.setCount(rosterOfStaffs.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(rosterOfStaffs.getTotalElements());
			result.setTotalPages(rosterOfStaffs.getTotalPages());
			List<RosterOfStaffVO> rosterOfStaffList = rosterOfStaffs.getContent();
			result.setItems(rosterOfStaffList);

		}
		return result;
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "firstName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "staffName.firstName");
		case "lastName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "staffName.lastName");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	public RosterOfStaffVO getRosterOfStaffById(Long rosterOfStaffId) {
		Assert.notNull(rosterOfStaffId, "rosterOfStaffId ID not provided");
		RosterOfStaff rosterOfStaff = rosterOfStaffRepository.findOne(rosterOfStaffId);
		Assert.notNull(rosterOfStaff, "rosterOfStaffId not found");
		return getRosterOfStaffVO(rosterOfStaff);
	}

	public RosterOfStaffVO saveUpdateRosterOfStaff(RosterOfStaffVO rosterOfStaffVO) throws EhmsBusinessException {
		RosterOfStaff rosterOfStaff = getRosterOfStaffEntity(rosterOfStaffVO);
		rosterOfStaffRepository.save(rosterOfStaff);
		rosterOfStaffVO.setRosterOfStaffId(rosterOfStaff.getRosterOfStaffId());
		return rosterOfStaffVO;
	}

	private RosterOfStaff getRosterOfStaffEntity(RosterOfStaffVO rosterOfStaffVO) throws EhmsBusinessException {
		RosterOfStaff rosterOfStaff = null;

		if (rosterOfStaffVO.getRosterOfStaffId() == null) {
			rosterOfStaff = new RosterOfStaff();
		} else {
			rosterOfStaff = rosterOfStaffRepository.findOne(rosterOfStaffVO.getRosterOfStaffId());
			Assert.notNull(rosterOfStaff, "RosterOfStaff with id: " + rosterOfStaffVO.getRosterOfStaffId() + " not found.");
		}

		try {
			Assert.notNull(rosterOfStaffVO.getDateFrom(), "From Date not provided.");
			Assert.notNull(rosterOfStaffVO.getDateTo(), "To Date not provided.");
			Assert.notNull(rosterOfStaffVO.getTimeFrom(), "From Time not provided.");
			Assert.notNull(rosterOfStaffVO.getTimeTo(), "To Time not provided.");
			Date dateFrom = DateTimeUtil.parseDate(rosterOfStaffVO.getDateFrom());
			Date dateTo = DateTimeUtil.parseDate(rosterOfStaffVO.getDateTo());
			Time timeFrom = DateTimeUtil.parseTime(rosterOfStaffVO.getTimeFrom());
			Time timeTo = DateTimeUtil.parseTime(rosterOfStaffVO.getTimeTo());
			Assert.isTrue(dateTo != null && dateFrom != null && dateTo.after(dateFrom), "From date should be before To date");
			rosterOfStaff.setDateFrom(dateFrom);
			rosterOfStaff.setDateTo(dateTo);
			Assert.isTrue(timeFrom != null && (DateTimeUtil.parseTime(RosterOfStaff.TIME_MAX)).after(timeFrom), "Invalid time");
			Assert.isTrue(timeTo != null && timeFrom != null && timeTo.after(timeFrom), "From time should be before To time");
			rosterOfStaff.setTimeFrom(timeFrom);
			rosterOfStaff.setTimeTo(timeTo);
		} catch (ParseException e1) {
			logger.error("ParseException Inside getRosterOfStaffEntity() : " + "Time From" + rosterOfStaffVO.getTimeFrom() + "Time To" + rosterOfStaffVO.getTimeTo() + "Date From"
					+ rosterOfStaffVO.getDateFrom() + "Date To" + rosterOfStaffVO.getDateTo(), e1);
			throw new EhmsBusinessException(e1, "Unable to parse date time while saving/updating roster of staff.");
		}
		Assert.isTrue(((rosterOfStaffVO.getDay() > -1 && rosterOfStaffVO.getDay() < 7)), "Incorrect Day mentioned in the rosterOfStaff.");
		rosterOfStaff.setDay(rosterOfStaffVO.getDay());
		Employee staffName = new Employee(rosterOfStaffVO.getStaffNameId());
		rosterOfStaff.setStaffName(staffName);
		rosterOfStaff.setRemark(rosterOfStaffVO.getRemark());
		rosterOfStaff.setStaffShift(rosterOfStaffVO.getStaffShiftId());
		rosterOfStaff.setWeeklyOff(rosterOfStaffVO.getWeeklyOff());
		return rosterOfStaff;
	}

	private RosterOfStaffVO getRosterOfStaffVO(RosterOfStaff rosterOfStaff) {
		RosterOfStaffVO vo = null;
		if (rosterOfStaff != null) {
			vo = new RosterOfStaffVO();
			vo.setRosterOfStaffId(rosterOfStaff.getRosterOfStaffId());
			if (rosterOfStaff.getStaffName() != null) {
				vo.setStaffFirstName(rosterOfStaff.getStaffName().getFirstName());
				vo.setStaffMiddleName(rosterOfStaff.getStaffName().getMiddleName());
				vo.setStaffLastName(rosterOfStaff.getStaffName().getLastName());
				if (rosterOfStaff.getStaffName().getTitle() != null) {
					vo.setStaffTitleName(rosterOfStaff.getStaffName().getTitle().getTitleName());
				}
			}
			if (rosterOfStaff.getStaffName() != null) {
				vo.setStaffNameId(rosterOfStaff.getStaffName().getEmployeeId());
			}
			vo.setDateFrom(DateTimeUtil.formatDate(rosterOfStaff.getDateFrom()));
			vo.setDateTo(DateTimeUtil.formatDate(rosterOfStaff.getDateTo()));
			vo.setTimeFrom(DateTimeUtil.formatTime(rosterOfStaff.getTimeFrom()));
			vo.setTimeTo(DateTimeUtil.formatTime(rosterOfStaff.getTimeTo()));
			vo.setDay(rosterOfStaff.getDay());
			vo.setRemark(rosterOfStaff.getRemark());
			vo.setStaffShiftId(rosterOfStaff.getStaffShift());
			vo.setWeeklyOff(rosterOfStaff.getWeeklyOff());
		}
		return vo;
	}

}
