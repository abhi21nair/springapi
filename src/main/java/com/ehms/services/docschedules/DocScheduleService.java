package com.ehms.services.docschedules;

import java.sql.Time;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.exception.ExceptionConstants;
import com.ehms.model.DocSchedule;
import com.ehms.model.Employee;
import com.ehms.model.MiscValue;
import com.ehms.repositories.DocScheduleRepository;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.DocScheduleVO;
import com.ehms.vos.PaginationResponseDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Service
public class DocScheduleService {

	@Autowired
	private DocScheduleRepository docScheduleRepository;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public PaginationResponseDto<DocScheduleVO> getDocScheduleList(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId, Long hospitalId) {
		PaginationResponseDto<DocScheduleVO> result = new PaginationResponseDto<DocScheduleVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<DocScheduleVO> docSchedules = docScheduleRepository.findDocSchedules("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (docSchedules != null) {
			result.setCount(docSchedules.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(docSchedules.getTotalElements());
			result.setTotalPages(docSchedules.getTotalPages());
			List<DocScheduleVO> DocScheduleList = docSchedules.getContent();
			result.setItems(DocScheduleList);
		}
		return result;
	}

	public DocScheduleVO getDocScheduleDetailsById(Long docScheduleId) {
		Assert.notNull(docScheduleId, "DocScedule ID not provided");
		DocSchedule docSchedule = docScheduleRepository.findOne(docScheduleId);
		Assert.notNull(docSchedule, "DocScedule not found");
		return getDocScheduleVO(docSchedule);
	}

	@JsonIgnore
	public DocScheduleVO getDocScheduleVO(DocSchedule docSchedule) {
		DocScheduleVO vo = null;
		if (docSchedule != null) {
			vo = new DocScheduleVO();
			vo.setDoctorScheduleId(docSchedule.getDoctorScheduleId());
			if (docSchedule.getDoctor() != null) {
				vo.setDoctorFirstName(docSchedule.getDoctor().getFirstName());
				vo.setDoctorMiddleName(docSchedule.getDoctor().getMiddleName());
				vo.setDoctorLastName(docSchedule.getDoctor().getLastName());
			}
			if (docSchedule.getDoctor() != null) {
				vo.setDoctorId(docSchedule.getDoctor().getEmployeeId());
			}
			vo.setDay(docSchedule.getDay());
			Date timeFrom = docSchedule.getTimeFrom();
			vo.setTimeFrom(DateTimeUtil.formatTime(timeFrom));
			Date timeTo = docSchedule.getTimeTo();
			vo.setTimeTo(DateTimeUtil.formatTime(timeTo));
			if (docSchedule.getAppointmentType() != null) {
				vo.setAppointmentTypeId(docSchedule.getAppointmentType());
			}
			vo.setNoOfAppointment(docSchedule.getNoOfAppointment());
			if (docSchedule.getDoctorShift() != null) {
				vo.setDoctorShiftId(docSchedule.getDoctorShift());
			}
			vo.setDoctorFees(docSchedule.getDoctorFees());
		}
		return vo;
	}

	public DocScheduleVO saveUpdateDoctorSchedule(Long customerId, Long hospitalId, DocScheduleVO docScheduleVO) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		validateForConflict(docScheduleVO);
		DocSchedule docSchedule = getDocScheduleEntity(docScheduleVO);
		docScheduleRepository.save(docSchedule);
		docScheduleVO.setDoctorScheduleId(docSchedule.getDoctorScheduleId());
		return docScheduleVO;
	}

	private void validateForConflict(DocScheduleVO docScheduleVO) throws EhmsBusinessException {
		List<Long> otherSchedules = new ArrayList<>();
		Time timeFrom = null;
		Time timeTo = null;
		try {
			timeFrom = DateTimeUtil.parseTime(docScheduleVO.getTimeFrom());
			timeTo = DateTimeUtil.parseTime(docScheduleVO.getTimeTo());
		} catch (ParseException e) {
			logger.error("Error while parsing time " + "TimeFrom" + docScheduleVO.getTimeFrom() + "TimeTo" + docScheduleVO.getTimeTo(), e);
			throw new EhmsBusinessException(e, "Unable to parse time while saving/updating Doctor Schedule.");
		}
		otherSchedules = docScheduleRepository.findSchedulesForDoctor(docScheduleVO.getDoctorId(), docScheduleVO.getDay(), timeFrom, timeTo, docScheduleVO.getDoctorScheduleId());
		if (otherSchedules != null && otherSchedules.size() != 0) {
			throw new EhmsBusinessException(ExceptionConstants.SCHEDULE_CONFLICT);
		}
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "firstName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "doctor.firstName");
		case "lastName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "doctor.lastName");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	private DocSchedule getDocScheduleEntity(DocScheduleVO docScheduleVO) throws EhmsBusinessException {
		DocSchedule docSchedule = null;
		if (docScheduleVO.getDoctorScheduleId() == null) {
			docSchedule = new DocSchedule();
		} else {
			docSchedule = docScheduleRepository.findOne(docScheduleVO.getDoctorScheduleId());
			Assert.notNull(docSchedule, "Doctor Schedule with id: " + docScheduleVO.getDoctorScheduleId() + " not found.");
		}

		Assert.notNull(docScheduleVO.getDoctorId(), "Doctor Id not provided");
		Employee employee = new Employee(docScheduleVO.getDoctorId());
		docSchedule.setDoctor(employee);
		try {
			Time timeFrom = DateTimeUtil.parseTime(docScheduleVO.getTimeFrom());
			Time timeTo = DateTimeUtil.parseTime(docScheduleVO.getTimeTo());
			Assert.isTrue((DateTimeUtil.parseTime(DocSchedule.TIME_MAX)).after(timeFrom), "Invalid time");
			Assert.isTrue(timeTo.after(timeFrom), "From time should be before To time");
			docSchedule.setTimeFrom(DateTimeUtil.parseTime(docScheduleVO.getTimeFrom()));
			docSchedule.setTimeTo(DateTimeUtil.parseTime(docScheduleVO.getTimeTo()));
		} catch (ParseException e3) {
			logger.error("Error while parsing time in getDocScheduleEntity()" + "TimeTo" + docScheduleVO.getTimeTo() + "TimeFrom" + docScheduleVO.getTimeFrom(), e3);
			throw new EhmsBusinessException(e3, "Unable to parse time while saving/updating Doctor Schedule.");
		}
		Assert.isTrue(((docScheduleVO.getDay() > 0 && docScheduleVO.getDay() < 8)), "Incorrect Day mentioned in the doctor schedule.");
		docSchedule.setDay(docScheduleVO.getDay());
		Assert.notNull(docScheduleVO.getAppointmentTypeId(), "Appointment Type not provided");
		MiscValue appointmentType = new MiscValue(docScheduleVO.getAppointmentTypeId());
		docSchedule.setAppointmentType(appointmentType.getMiscTypeValueId());
		docSchedule.setNoOfAppointment(docScheduleVO.getNoOfAppointment());
		if (docScheduleVO.getDoctorShiftId() != null) {
			MiscValue doctorShift = new MiscValue(docScheduleVO.getDoctorShiftId());
			docSchedule.setDoctorShift(doctorShift.getMiscTypeValueId());
		}
		docSchedule.setDoctorFees(docScheduleVO.getDoctorFees());
		return docSchedule;
	}

	public List<DocScheduleVO> getDoctorSchedulesAtHospitalForDay(Long customerId, Long hospitalId, int day) {
		Assert.isTrue((day > 0 && day < 8), "Incorrect Day value.");
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		return docScheduleRepository.getDoctorSchedulesAtHospitalForDay(customerId, hospitalId, day);
	}

}
