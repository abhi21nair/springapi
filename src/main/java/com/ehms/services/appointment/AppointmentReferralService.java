package com.ehms.services.appointment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.model.AppointmentReferral;
import com.ehms.repositories.appointment.AppointmentReferralRepository;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.ResponseVO.Status;
import com.ehms.vos.appointments.AppointmentReferralVO;

@Service
public class AppointmentReferralService {

	@Autowired
	private AppointmentReferralRepository appointmentReferralRepository;

	public List<AppointmentReferralVO> getAppointmentReferrals(Long appointmentId) {
		Assert.notNull(appointmentId, "Appointment ID not provided");
		List<AppointmentReferralVO> appointmentReferrals = appointmentReferralRepository.findAppointmentReferrals(appointmentId);
		return appointmentReferrals;
	}

	public AppointmentReferralVO saveUpdateAppointmentReferral(AppointmentReferralVO appointmentReferralVO) {
		AppointmentReferral appointmentReferral = getAppointmentReferralEntity(appointmentReferralVO);
		appointmentReferralRepository.save(appointmentReferral);
		appointmentReferralVO.setReferralId(appointmentReferral.getReferralId());
		return appointmentReferralVO;
	}

	public ResponseVO deleteAppointmentReferral(Long referralId) {
		Assert.notNull(referralId, "Referral Id not provided");
		AppointmentReferral appointmentReferralToDelete = appointmentReferralRepository.findOne(referralId);
		Assert.notNull(appointmentReferralToDelete, "Appointment Referral not found");
		if (appointmentReferralToDelete != null) {
			appointmentReferralRepository.delete(referralId);
			return new ResponseVO(Status.SUCCESS, "Appointment Referral Deleted successfully");
		}
		return new ResponseVO(Status.FAILURE, "Appointment Referral deletion Failure");

	}

	private AppointmentReferral getAppointmentReferralEntity(AppointmentReferralVO appointmentReferralVO) {
		AppointmentReferral appointmentReferral = null;
		if (appointmentReferralVO.getReferralId() == null) {
			appointmentReferral = new AppointmentReferral();
		} else {
			appointmentReferral = appointmentReferralRepository.findOne(appointmentReferralVO.getReferralId());
			Assert.notNull(appointmentReferral, "Appointment Referral with id: " + appointmentReferralVO.getReferralId() + " not found.");
		}
		Assert.notNull(appointmentReferralVO.getAppointmentId(), "Appointment Id not provided");
		appointmentReferral.setAppointmentId(appointmentReferralVO.getAppointmentId());
		appointmentReferral.setDepartmentId(appointmentReferralVO.getDepartmentId());
		appointmentReferral.setGeneralNotes(appointmentReferralVO.getGeneralNotes());
		appointmentReferral.setInstructionToWarden(appointmentReferralVO.getInstructionToWarden());
		appointmentReferral.setMlcCase(appointmentReferralVO.getMlcCase());
		appointmentReferral.setProvisionalNotes(appointmentReferralVO.getProvisionalNotes());
		return appointmentReferral;
	}

}
