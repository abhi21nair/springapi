package com.ehms.services.appointment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.model.AppointmentPrescription;
import com.ehms.repositories.appointment.AppointmentPrescriptionRepository;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.ResponseVO.Status;
import com.ehms.vos.appointments.AppointmentPrescriptionVO;

@Service
public class AppointmentPrescriptionService {

	@Autowired
	private AppointmentPrescriptionRepository appointmentPrescriptionRepository;

	public List<AppointmentPrescriptionVO> getAppointmentPrescriptions(Long appointmentId) {
		Assert.notNull(appointmentId, "Appointment ID not provided");
		List<AppointmentPrescriptionVO> appointmentPrescriptions = appointmentPrescriptionRepository.findAppointmentPrescriptions(appointmentId);
		return appointmentPrescriptions;
	}

	public AppointmentPrescriptionVO saveUpdateAppointmentPrescription(AppointmentPrescriptionVO appointmentPrescriptionVO) {
		AppointmentPrescription appointmentPrescription = getAppointmentPrescriptionEntity(appointmentPrescriptionVO);
		appointmentPrescriptionRepository.save(appointmentPrescription);
		appointmentPrescriptionVO.setPrescriptionId(appointmentPrescription.getPrescriptionId());
		return appointmentPrescriptionVO;
	}

	public ResponseVO deleteAppointmentPrescription(Long prescriptionId) {
		Assert.notNull(prescriptionId, "Prescription Id not provided");
		AppointmentPrescription appointmentPrescriptionToDelete = appointmentPrescriptionRepository.findOne(prescriptionId);
		Assert.notNull(appointmentPrescriptionToDelete, "Appointment Prescription not found");
		if (appointmentPrescriptionToDelete != null) {
			appointmentPrescriptionRepository.delete(prescriptionId);
			return new ResponseVO(Status.SUCCESS, "Appointment Prescription Deleted successfully");
		}
		return new ResponseVO(Status.FAILURE, "Appointment Prescription deletion Failure");
	}

	private AppointmentPrescription getAppointmentPrescriptionEntity(AppointmentPrescriptionVO appointmentPrescriptionVO) {
		AppointmentPrescription appointmentPrescription = null;
		if (appointmentPrescriptionVO.getPrescriptionId() == null) {
			appointmentPrescription = new AppointmentPrescription();
		} else {
			appointmentPrescription = appointmentPrescriptionRepository.findOne(appointmentPrescriptionVO.getPrescriptionId());
			Assert.notNull(appointmentPrescription, "Appointment Prescription with id: " + appointmentPrescriptionVO.getPrescriptionId() + " not found.");
		}
		Assert.notNull(appointmentPrescriptionVO.getAppointmentId(), "Appointment Id not provided");
		appointmentPrescription.setAppointmentId(appointmentPrescriptionVO.getAppointmentId());
		appointmentPrescription.setDrugMasterId(appointmentPrescriptionVO.getDrugMasterId());
		appointmentPrescription.setDuration(appointmentPrescriptionVO.getDuration());
		appointmentPrescription.setDurationUnit(appointmentPrescriptionVO.getDurationUnit());
		appointmentPrescription.setQuantity(appointmentPrescriptionVO.getQuantity());
		appointmentPrescription.setRoute(appointmentPrescriptionVO.getRoute());
		return appointmentPrescription;
	}
}
