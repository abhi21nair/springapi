package com.ehms.services.appointment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.model.AppointmentComplaint;
import com.ehms.repositories.appointment.AppointmentComplaintRepository;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.ResponseVO.Status;
import com.ehms.vos.appointments.AppointmentComplaintVO;

@Service
public class AppointmentComplaintService {

	@Autowired
	private AppointmentComplaintRepository appointmentComplaintRepository;

	public List<AppointmentComplaintVO> getAppointmentComplaints(Long appointmentId) {
		Assert.notNull(appointmentId, "Appointment ID not provided");
		List<AppointmentComplaintVO> appointmentComplaints = appointmentComplaintRepository.findAppointmentComplaints(appointmentId);
		return appointmentComplaints;
	}

	public AppointmentComplaintVO saveUpdateAppointmentComplaint(AppointmentComplaintVO appointmentComplaintVO) {
		AppointmentComplaint appointmentComplaint = getAppointmentComplaintEntity(appointmentComplaintVO);
		appointmentComplaintRepository.save(appointmentComplaint);
		appointmentComplaintVO.setComplaintId(appointmentComplaint.getComplaintId());
		return appointmentComplaintVO;
	}

	public ResponseVO deleteAppointmentComplaint(Long complaintId) {
		Assert.notNull(complaintId, "Complaint Id not provided");
		AppointmentComplaint appointmentComplaintToDelete = appointmentComplaintRepository.findOne(complaintId);
		Assert.notNull(appointmentComplaintToDelete, "Appointment Complaint not found");
		if (appointmentComplaintToDelete != null) {
			appointmentComplaintRepository.delete(complaintId);
			return new ResponseVO(Status.SUCCESS, "Appointment Complaint Deleted successfully");
		}
		return new ResponseVO(Status.FAILURE, "Appointment Complaint deletion Failure");
	}

	private AppointmentComplaint getAppointmentComplaintEntity(AppointmentComplaintVO appointmentComplaintVO) {
		AppointmentComplaint appointmentComplaint = null;
		if (appointmentComplaintVO.getComplaintId() == null) {
			appointmentComplaint = new AppointmentComplaint();
		} else {
			appointmentComplaint = appointmentComplaintRepository.findOne(appointmentComplaintVO.getComplaintId());
			Assert.notNull(appointmentComplaint, "Appointment Complaint with id: " + appointmentComplaintVO.getComplaintId() + " not found.");
		}
		Assert.notNull(appointmentComplaintVO.getAppointmentId(), "Appointment Id not provided");
		appointmentComplaint.setAppointmentId(appointmentComplaintVO.getAppointmentId());
		appointmentComplaint.setBloodPressure(appointmentComplaintVO.getBloodPressure());
		appointmentComplaint.setComplaints(appointmentComplaintVO.getComplaints());
		appointmentComplaint.setDiagnosis(appointmentComplaintVO.getDiagnosis());
		appointmentComplaint.setExaminationPulse(appointmentComplaintVO.getExaminationPulse());
		appointmentComplaint.setGeneralNotes(appointmentComplaintVO.getGeneralNotes());
		appointmentComplaint.setHeight(appointmentComplaintVO.getHeight());
		appointmentComplaint.setWeight(appointmentComplaintVO.getWeight());
		appointmentComplaint.setLabNotes(appointmentComplaintVO.getLabNotes());
		appointmentComplaint.setPathologyTestMasterId(appointmentComplaintVO.getPathologyTestMasterId());
		appointmentComplaint.setRespiration(appointmentComplaintVO.getRespiration());
		appointmentComplaint.setTemperature(appointmentComplaintVO.getTemperature());
		return appointmentComplaint;
	}
}
