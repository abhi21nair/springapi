package com.ehms.services.appointment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.model.AppointmentVariousService;
import com.ehms.repositories.appointment.AppointmentVariousServiceRepository;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.ResponseVO.Status;
import com.ehms.vos.appointments.AppointmentVariousServiceVO;

@Service
public class AppointmentVariousServiceService {

	@Autowired
	private AppointmentVariousServiceRepository appointmentVariousServiceRepository;

	public List<AppointmentVariousServiceVO> getAppointmentVariousServices(Long appointmentId) {
		Assert.notNull(appointmentId, "Appointment ID not provided");
		List<AppointmentVariousServiceVO> appointmentVariousServices = appointmentVariousServiceRepository.findAppointmentVariousServices(appointmentId);
		return appointmentVariousServices;
	}

	public AppointmentVariousServiceVO saveUpdateAppointmentVariousService(AppointmentVariousServiceVO appointmentVariousServiceVO) {
		AppointmentVariousService appointmentVariousService = getAppointmentVariousServiceEntity(appointmentVariousServiceVO);
		appointmentVariousServiceRepository.save(appointmentVariousService);
		appointmentVariousServiceVO.setVariousServiceId(appointmentVariousService.getVariousServiceId());
		return appointmentVariousServiceVO;
	}

	public ResponseVO deleteAppointmentVariousService(Long variousServiceId) {
		Assert.notNull(variousServiceId, "VariousService Id not provided");
		AppointmentVariousService appointmentVariousServiceToDelete = appointmentVariousServiceRepository.findOne(variousServiceId);
		Assert.notNull(appointmentVariousServiceToDelete, "Appointment Various Service not found");
		if (appointmentVariousServiceToDelete != null) {
			appointmentVariousServiceRepository.delete(variousServiceId);
			return new ResponseVO(Status.SUCCESS, "Appointment Various Service Deleted successfully");
		}
		return new ResponseVO(Status.FAILURE, "Appointment Various Service deletion Failure");
	}

	private AppointmentVariousService getAppointmentVariousServiceEntity(AppointmentVariousServiceVO appointmentVariousServiceVO) {
		AppointmentVariousService appointmentVariousService = null;
		if (appointmentVariousServiceVO.getVariousServiceId() == null) {
			appointmentVariousService = new AppointmentVariousService();
		} else {
			appointmentVariousService = appointmentVariousServiceRepository.findOne(appointmentVariousServiceVO.getVariousServiceId());
			Assert.notNull(appointmentVariousService, "Appointment Various Service with id: " + appointmentVariousServiceVO.getVariousServiceId() + " not found.");
		}
		Assert.notNull(appointmentVariousServiceVO.getAppointmentId(), "Appointment Id not provided");
		appointmentVariousService.setAppointmentId(appointmentVariousServiceVO.getAppointmentId());
		appointmentVariousService.setQuantity(appointmentVariousServiceVO.getQuantity());
		appointmentVariousService.setTarrifWiseServiceId(appointmentVariousServiceVO.getTarrifWiseServiceId());
		appointmentVariousService.setTotalAmount(appointmentVariousServiceVO.getTotalAmount());
		return appointmentVariousService;
	}

}
