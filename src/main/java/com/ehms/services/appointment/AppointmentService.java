package com.ehms.services.appointment;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Appointment;
import com.ehms.model.Bed;
import com.ehms.model.Customer;
import com.ehms.model.Department;
import com.ehms.model.DocSchedule;
import com.ehms.model.Employee;
import com.ehms.model.Hospital;
import com.ehms.model.Module;
import com.ehms.model.Patient;
import com.ehms.repositories.BedRepository;
import com.ehms.repositories.DailyProgressNoteRepository;
import com.ehms.repositories.DietMasterRepository;
import com.ehms.repositories.DischargeSheetRepository;
import com.ehms.repositories.LinkCustomerModuleRepository;
import com.ehms.repositories.PatientPostmortemRepository;
import com.ehms.repositories.appointment.AppointmentComplaintRepository;
import com.ehms.repositories.appointment.AppointmentExaminationRepository;
import com.ehms.repositories.appointment.AppointmentPrescriptionRepository;
import com.ehms.repositories.appointment.AppointmentReferralRepository;
import com.ehms.repositories.appointment.AppointmentRepository;
import com.ehms.repositories.appointment.AppointmentVariousServiceRepository;
import com.ehms.services.docschedules.DocScheduleService;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.DailyProgressNoteVO;
import com.ehms.vos.DietMasterVO;
import com.ehms.vos.DischargeSheetVO;
import com.ehms.vos.DocScheduleVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.PatientPostmortemVO;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.ResponseVO.Status;
import com.ehms.vos.appointments.AppointmentComplaintVO;
import com.ehms.vos.appointments.AppointmentExaminationVO;
import com.ehms.vos.appointments.AppointmentPrescriptionVO;
import com.ehms.vos.appointments.AppointmentReferralVO;
import com.ehms.vos.appointments.AppointmentStatsDBVO;
import com.ehms.vos.appointments.AppointmentStatsVO;
import com.ehms.vos.appointments.AppointmentVO;
import com.ehms.vos.appointments.AppointmentVariousServiceVO;
import com.ehms.vos.appointments.PatientAppointmentHistoryVO;

@Service
public class AppointmentService {

	private static final String KEY_DOCTOR_APPT = "DOC_APT";
	private static final String KEY_MODULE_APPT = "MODULE_APT";

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AppointmentRepository appointmentRepository;

	@Autowired
	private LinkCustomerModuleRepository customerModuleRepository;

	@Autowired
	private AppointmentComplaintRepository appointmentComplaintRepository;

	@Autowired
	private AppointmentExaminationRepository appointmentExaminationRepository;

	@Autowired
	private AppointmentPrescriptionRepository appointmentPrescriptionRepository;

	@Autowired
	private AppointmentReferralRepository appointmentReferralRepository;

	@Autowired
	private AppointmentVariousServiceRepository appointmentVariousServiceRepository;

	@Autowired
	private BedRepository bedRepository;

	@Autowired
	private DietMasterRepository dietMasterRepository;

	@Autowired
	private DailyProgressNoteRepository dailyProgressNoteRepository;

	@Autowired
	private DischargeSheetRepository dischargeSheetRepository;

	@Autowired
	private PatientPostmortemRepository patientPostmortemRepository;

	@Autowired
	private DocScheduleService docScheduleService;

	public List<AppointmentStatsVO> getHospitalAppointmentStats(Long customerId, Long hospitalId, String date) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		if (date == null) {
			date = DateTimeUtil.formatDateWithoutTime(new Date());
		}
		Date appointmentDate = parseAppointmentDate(date);
		Assert.notNull(appointmentDate, "Appointment date is null.");
		Map<String, Map<Long, AppointmentStatsVO>> resultMap = populateStatsMap(customerId, hospitalId, appointmentDate);
		List<AppointmentStatsDBVO> appointmentStatsOfDay = appointmentRepository.findAppointmentsStatsOfAppointmentDate(customerId, hospitalId, appointmentDate);
		return populateStats(resultMap, appointmentStatsOfDay);
	}

	public AppointmentVO saveAppointment(Long customerId, Long hospitalId, AppointmentVO appointmentVO) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		Appointment appointment = getAppointmentEntity(appointmentVO, customerId, hospitalId);
		appointmentRepository.save(appointment);
		appointmentVO.setAppointmentId(appointment.getAppointmentId());
		return appointmentVO;
	}

	private Appointment getAppointmentEntity(AppointmentVO appointmentVO, Long customerId, Long hospitalId) throws EhmsBusinessException {
		Assert.notNull(appointmentVO, "Appointment detatils not provided");
		Assert.isTrue(appointmentVO.getModuleId() != null || (appointmentVO.getDocScheduleId() != null && appointmentVO.getDoctorId() != null),
				"Either Module Id or Doctor Schedule Id and doctor Id should be provided.");
		Assert.notNull(appointmentVO.getPatientId(), "Patient ID not provided.");
		Appointment appointment = null;
		if (appointmentVO.getAppointmentId() == null) {
			appointment = new Appointment();
		}

		if (appointmentVO.getModuleId() != null && appointmentVO.getModuleId() == Appointment.IPD_MODULE_ID) {
			admitPatient(appointmentVO, appointment);
		} else {
			Assert.notNull(appointmentVO.getFromTime(), "From Time not provided.");
			Assert.notNull(appointmentVO.getToTime(), "To Time not provided.");
			appointment.setStatus(Appointment.APPOINTMENT_SCHEDULED);
			try {
				appointment.setFromTime(DateTimeUtil.parseTimestamp(appointmentVO.getFromTime()));
				appointment.setToTime(DateTimeUtil.parseTimestamp(appointmentVO.getToTime()));
			} catch (ParseException e) {
				logger.error("Error while parsing Timestamp due to: " + e.getMessage(), e);
				throw new EhmsBusinessException(e, "Unable to parse Timestamp while saving Appointment due to: " + e.getMessage());
			}
		}

		Customer customer = new Customer(customerId);
		appointment.setCustomer(customer);
		Hospital hospital = new Hospital(hospitalId);
		appointment.setHospital(hospital);
		appointment.setTimeSpent(0L);
		if (appointmentVO.getModuleId() != null) {
			Module module = new Module(appointmentVO.getModuleId());
			appointment.setModule(module);
		}
		if (appointmentVO.getDoctorId() != null) {
			Employee doctor = new Employee(appointmentVO.getDoctorId());
			appointment.setDoctor(doctor);
		}
		if (appointmentVO.getPatientId() != null) {
			Patient patient = new Patient(appointmentVO.getPatientId());
			appointment.setPatient(patient);
		}
		if (appointmentVO.getDocScheduleId() != null) {
			DocSchedule docSchedule = new DocSchedule(appointmentVO.getDocScheduleId());
			appointment.setDocSchedule(docSchedule);
		}
		return appointment;
	}

	private void admitPatient(AppointmentVO appointmentVO, Appointment appointment) throws EhmsBusinessException {
		Assert.notNull(appointmentVO.getBedId(), "Bed ID not found.");
		Assert.notNull(appointmentVO.getDepartmentId(), "Department ID not found.");
		Assert.notNull(appointmentVO.getReportTime(), "Report Time not provided.");
		Bed bed = bedRepository.findOne(appointmentVO.getBedId());
		Assert.notNull(bed, "Bed not found.");
		Assert.isTrue(bed.getPatient() == null, "Bed is Occupied by another patient");
		appointment.setBed(bed);
		Department department = new Department(appointmentVO.getDepartmentId());
		appointment.setDepartment(department);
		Patient patient = new Patient(appointmentVO.getPatientId());
		bed.setPatient(patient);
		appointment.setStatus(Appointment.APPOINTMENT_IN_PROGESS);
		Timestamp appointmentStartTime = null;
		Timestamp reportTime = null;
		Timestamp fromTime = null;
		Timestamp toTime = null;
		try {
			appointmentStartTime = DateTimeUtil.parseTimestamp(appointmentVO.getReportTime());
			reportTime = DateTimeUtil.parseTimestamp(appointmentVO.getReportTime());
			fromTime = DateTimeUtil.parseTimestamp(appointmentVO.getReportTime());
			toTime = DateTimeUtil.parseTimestamp(appointmentVO.getReportTime());
		} catch (ParseException | EhmsBusinessException e) {
			logger.error("Unable to parse the Admit time: " + appointmentVO.getReportTime() + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Unable to parse the Admit date: " + appointmentVO.getReportTime() + " due to: " + e.getMessage());
		}
		if (appointmentStartTime == null || reportTime == null || fromTime == null || toTime == null) {
			throw new EhmsBusinessException("Admit Time is not in proper format");
		}
		appointment.setAppointmentStartTime(appointmentStartTime);
		appointment.setReportTime(reportTime);
		appointment.setFromTime(fromTime);
		appointment.setToTime(toTime);
	}

	public ResponseVO updatePatientReportTime(Long customerId, Long hospitalId, Long appointmentId, AppointmentVO appointmentVO) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		Assert.notNull(appointmentId, "Appointment Id not found.");
		Assert.notNull(appointmentVO.getReportTime(), "Report Time not found.");
		Appointment appointment = appointmentRepository.findOne(appointmentId);
		Assert.notNull(appointment, "Appointment not found.");
		Assert.isTrue(appointment.getStatus() == Appointment.APPOINTMENT_SCHEDULED, "Appointment status should be scheduled to mark the patient as reported");
		Timestamp reportTime = null;
		try {
			reportTime = DateTimeUtil.parseTimestamp(appointmentVO.getReportTime());
		} catch (ParseException | EhmsBusinessException e) {
			logger.error("Unable to parse the report time: " + appointmentVO.getReportTime() + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Unable to parse the report date: " + appointmentVO.getReportTime() + " due to: " + e.getMessage());
		}
		if (reportTime == null) {
			throw new EhmsBusinessException("Report Time is not in proper format");
		}
		appointment.setReportTime(reportTime);
		appointmentRepository.save(appointment);
		return new ResponseVO(Status.SUCCESS, "Report time updated successfully");
	}

	public ResponseVO startAppointment(Long customerId, Long hospitalId, Long appointmentId, AppointmentVO appointmentVO) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		Assert.notNull(appointmentId, "Appointment Id not found.");
		Assert.notNull(appointmentVO.getAppointmentStartTime(), "Appointment start time not found.");
		Appointment appointment = appointmentRepository.findOne(appointmentId);
		Assert.notNull(appointment, "Appointment not found.");
		Assert.isTrue(appointment.getStatus() == Appointment.APPOINTMENT_SCHEDULED, "Appointment status should be scheduled to start the appointment");
		Timestamp appointmentStartTime = null;
		try {
			appointmentStartTime = DateTimeUtil.parseTimestamp(appointmentVO.getAppointmentStartTime());
		} catch (ParseException | EhmsBusinessException e) {
			logger.error("Unable to parse the Appointment time: " + appointmentVO.getAppointmentStartTime() + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Unable to parse the Appointment date: " + appointmentVO.getAppointmentStartTime() + " due to: " + e.getMessage());
		}
		if (appointmentStartTime == null) {
			throw new EhmsBusinessException("Appointment Start Time is not in proper format");
		}

		appointment.setAppointmentStartTime(appointmentStartTime);
		appointment.setStatus(Appointment.APPOINTMENT_IN_PROGESS);
		appointmentRepository.save(appointment);
		return new ResponseVO(Status.SUCCESS, "Appointment Start time updated successfully");
	}

	public ResponseVO completeAppointment(Long customerId, Long hospitalId, Long appointmentId, AppointmentVO appointmentVO) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		Assert.notNull(appointmentId, "Appointment Id not found.");
		Appointment appointment = appointmentRepository.findOne(appointmentId);
		Assert.notNull(appointmentVO.getAppointmentEndTime(), "Appointment end time not found.");
		Assert.notNull(appointment, "Appointment not found.");
		Assert.notNull(appointmentVO.getTimeSpent(), "Appointment time spent not provided");
		Assert.isTrue(appointment.getStatus() == Appointment.APPOINTMENT_IN_PROGESS || appointment.getStatus() == Appointment.APPOINTMENT_ON_HOLD,
				"Appointment status should be in progress or on hold to mark it complete");
		Timestamp appointmentEndTime = null;
		try {
			appointmentEndTime = DateTimeUtil.parseTimestamp(appointmentVO.getAppointmentEndTime());
		} catch (ParseException | EhmsBusinessException e) {
			logger.error("Unable to parse the Appointment time: " + appointmentVO.getAppointmentEndTime() + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Unable to parse the Appointment date: " + appointmentVO.getAppointmentEndTime() + " due to: " + e.getMessage());
		}
		if (appointmentEndTime == null) {
			throw new EhmsBusinessException("Appointment End Time is not in proper format");
		}
		appointment.setTimeSpent(appointmentVO.getTimeSpent());
		appointment.setAppointmentEndTime(appointmentEndTime);
		appointment.setStatus(Appointment.APPOINTMENT_COMPLETE);
		appointmentRepository.save(appointment);
		return new ResponseVO(Status.SUCCESS, "Appointment End time updated successfully");
	}

	public ResponseVO cancelAppointment(Long customerId, Long hospitalId, Long appointmentId) {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		Assert.notNull(appointmentId, "Appointment Id not found.");
		Appointment appointment = appointmentRepository.findOne(appointmentId);
		Assert.notNull(appointment, "Appointment not found.");
		Assert.isTrue(
				appointment.getStatus() == Appointment.APPOINTMENT_SCHEDULED || appointment.getStatus() == Appointment.APPOINTMENT_IN_PROGESS
						|| appointment.getStatus() == Appointment.APPOINTMENT_ON_HOLD || appointment.getStatus() == Appointment.APPOINTMENT_COMPLETE,
				"Appointment status should be scheduled or in progress or on hold or complete to cancel the appointment");
		appointment.setStatus(Appointment.APPOINTMENT_CANCEL);
		appointmentRepository.save(appointment);
		return new ResponseVO(Status.SUCCESS, "Status updated successfully");
	}

	public ResponseVO putAppointmentOnHold(Long customerId, Long hospitalId, Long appointmentId, AppointmentVO appointmentVO) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		Assert.notNull(appointmentId, "Appointment Id not found.");
		Appointment appointment = appointmentRepository.findOne(appointmentId);
		Assert.notNull(appointment, "Appointment not found.");
		Assert.isTrue(appointment.getStatus() == Appointment.APPOINTMENT_IN_PROGESS, "Appointment status should be in progress to mark it on hold");
		Assert.notNull(appointmentVO.getTimeSpent(), "Appointment time spent not provided");
		appointment.setTimeSpent(appointmentVO.getTimeSpent());
		appointment.setStatus(Appointment.APPOINTMENT_ON_HOLD);
		appointmentRepository.save(appointment);
		return new ResponseVO(Status.SUCCESS, "Appointment time spent updated successfully");
	}

	public ResponseVO resumeAppointment(Long customerId, Long hospitalId, Long appointmentId) {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		Assert.notNull(appointmentId, "Appointment Id not found.");
		Appointment appointment = appointmentRepository.findOne(appointmentId);
		Assert.notNull(appointment, "Appointment not found.");
		Assert.isTrue(appointment.getStatus() == Appointment.APPOINTMENT_ON_HOLD, "Appointment status should be on hold to resume the appointment");
		appointment.setStatus(Appointment.APPOINTMENT_IN_PROGESS);
		appointmentRepository.save(appointment);
		return new ResponseVO(Status.SUCCESS, "Appointment resumed successfully");
	}

	private Date parseAppointmentDate(String date) throws EhmsBusinessException {
		Date appointmentDate = null;
		try {
			appointmentDate = DateTimeUtil.parseDate(date);
		} catch (ParseException e) {
			logger.error("Unable to parse the appointment date: " + date + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Unable to parse the appointment date: " + date + " due to: " + e.getMessage());
		}
		return appointmentDate;
	}

	private List<AppointmentStatsVO> populateStats(Map<String, Map<Long, AppointmentStatsVO>> resultMap, List<AppointmentStatsDBVO> appointmentStatsOfDay) {
		List<AppointmentStatsVO> finalModuleDocAppointmentStats = new ArrayList<>();
		Map<Long, AppointmentStatsVO> moduleAppointmentStats = resultMap.get(KEY_MODULE_APPT);
		Map<Long, AppointmentStatsVO> doctorAppointmentStats = resultMap.get(KEY_DOCTOR_APPT);
		if (appointmentStatsOfDay != null && !appointmentStatsOfDay.isEmpty()) {
			AppointmentStatsVO stats = null;
			for (AppointmentStatsDBVO appointmentStatsVO : appointmentStatsOfDay) {
				stats = getStatsBasedOnAppointmentDetails(moduleAppointmentStats, doctorAppointmentStats, stats, appointmentStatsVO);
				populateStatsData(stats, appointmentStatsVO);
			}
		}
		populateStatsResult(finalModuleDocAppointmentStats, moduleAppointmentStats, doctorAppointmentStats);
		return finalModuleDocAppointmentStats;
	}

	private void populateStatsData(AppointmentStatsVO stats, AppointmentStatsDBVO appointmentStatsVO) {
		if (stats != null) {
			stats.setTotalAppointments(appointmentStatsVO.getNoOfPatients());
			stats.setReportedPatients(appointmentStatsVO.getNoOfReportedPatients());
		}
	}

	private AppointmentStatsVO getStatsBasedOnAppointmentDetails(Map<Long, AppointmentStatsVO> moduleAppointmentStats, Map<Long, AppointmentStatsVO> doctorAppointmentStats, AppointmentStatsVO stats,
			AppointmentStatsDBVO appointmentStatsVO) {
		if (appointmentStatsVO.getModuleId() != null) {
			if (moduleAppointmentStats != null) {
				stats = moduleAppointmentStats.get(appointmentStatsVO.getModuleId());
			}
		} else if (appointmentStatsVO.getDoctorScheduleId() != null) {
			if (doctorAppointmentStats != null) {
				stats = doctorAppointmentStats.get(appointmentStatsVO.getDoctorScheduleId());
			}
		}
		return stats;
	}

	private void populateStatsResult(List<AppointmentStatsVO> finalModuleDocAppointmentStats, Map<Long, AppointmentStatsVO> moduleAppointmentStats,
			Map<Long, AppointmentStatsVO> doctorAppointmentStats) {
		if (moduleAppointmentStats != null) {
			finalModuleDocAppointmentStats.addAll(moduleAppointmentStats.values());
		}
		if (doctorAppointmentStats != null) {
			finalModuleDocAppointmentStats.addAll(doctorAppointmentStats.values());
		}
	}

	private Map<String, Map<Long, AppointmentStatsVO>> populateStatsMap(Long customerId, Long hospitalId, Date appointmentDate) throws EhmsBusinessException {
		Map<String, Map<Long, AppointmentStatsVO>> result = initStatsMap();
		populateQueueableModuleInitStats(customerId, result);
		populateDoctorInitialStats(customerId, hospitalId, appointmentDate, result);
		return result;
	}

	private void populateDoctorInitialStats(Long customerId, Long hospitalId, Date appointmentDate, Map<String, Map<Long, AppointmentStatsVO>> result) throws EhmsBusinessException {
		int day = DateTimeUtil.getDayNumber(appointmentDate);
		List<DocScheduleVO> docSchedulesAtHospitalForDay = docScheduleService.getDoctorSchedulesAtHospitalForDay(customerId, hospitalId, day);
		if (docSchedulesAtHospitalForDay != null && !docSchedulesAtHospitalForDay.isEmpty()) {
			Map<Long, AppointmentStatsVO> doctorAppointmentStats = result.get(KEY_DOCTOR_APPT);
			for (DocScheduleVO docSchedule : docSchedulesAtHospitalForDay) {
				doctorAppointmentStats.put(docSchedule.getDoctorScheduleId(), new AppointmentStatsVO(docSchedule));
			}
		}
	}

	private void populateQueueableModuleInitStats(Long customerId, Map<String, Map<Long, AppointmentStatsVO>> result) {
		List<Long> queueableModules = customerModuleRepository.findQueueableCustomerModuleIds(customerId);
		if (queueableModules != null && !queueableModules.isEmpty()) {
			Map<Long, AppointmentStatsVO> moduleAppointmentStats = result.get(KEY_MODULE_APPT);
			for (Long queueableModule : queueableModules) {
				moduleAppointmentStats.put(queueableModule, new AppointmentStatsVO(queueableModule));
			}
		}
	}

	private Map<String, Map<Long, AppointmentStatsVO>> initStatsMap() {
		Map<String, Map<Long, AppointmentStatsVO>> result = new HashMap<>();
		result.put(KEY_DOCTOR_APPT, new HashMap<>());
		result.put(KEY_MODULE_APPT, new HashMap<>());
		return result;
	}

	public PaginationResponseDto<AppointmentVO> getQueueDetails(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, String date, Long customerId, Long hospitalId,
			Long moduleId, Long doctorId, Long doctorScheduleId) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		Assert.isTrue((moduleId != null) || (doctorId != null && doctorScheduleId != null), "Either Module Id or Doctor Schedule Id and doctor Id should be provided.");
		if (date == null) {
			date = DateTimeUtil.formatDateWithoutTime(new Date());
		}
		Date appointmentDate = parseAppointmentDate(date);
		Assert.notNull(appointmentDate, "Appointment date is null.");
		PaginationResponseDto<AppointmentVO> result = new PaginationResponseDto<AppointmentVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObjectForQueue(sortOrder, sortOn));
		Page<AppointmentVO> patientQueue = appointmentRepository.getQueueDetails("%" + searchText + "%", customerId, hospitalId, appointmentDate, moduleId, doctorId, doctorScheduleId, pageRequest);
		if (patientQueue != null) {
			result.setCount(patientQueue.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(patientQueue.getTotalElements());
			result.setTotalPages(patientQueue.getTotalPages());
			List<AppointmentVO> patientQueueList = patientQueue.getContent();
			result.setItems(patientQueueList);
		}
		return result;
	}

	public PaginationResponseDto<PatientAppointmentHistoryVO> getPatientAppointmentHistory(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText, Long customerId,
			Long hospitalId, Long patientId) {
		Assert.notNull(customerId, "Customer ID not found.");
		Assert.notNull(hospitalId, "Hospital ID not found.");
		Assert.notNull(patientId, "Patient ID not found.");
		PaginationResponseDto<PatientAppointmentHistoryVO> result = new PaginationResponseDto<PatientAppointmentHistoryVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObjectForPatientHistory(sortOrder, sortOn));
		Page<PatientAppointmentHistoryVO> patientHistoryDetails = appointmentRepository.getPatientHistory(customerId, hospitalId, patientId, pageRequest);
		if (patientHistoryDetails != null) {
			result.setCount(patientHistoryDetails.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(patientHistoryDetails.getTotalElements());
			result.setTotalPages(patientHistoryDetails.getTotalPages());
			List<PatientAppointmentHistoryVO> patientHistoryList = patientHistoryDetails.getContent();
			List<Long> appointmentIds = new ArrayList<>();
			LinkedHashMap<Long, PatientAppointmentHistoryVO> historyMap = new LinkedHashMap<Long, PatientAppointmentHistoryVO>();
			for (PatientAppointmentHistoryVO patientAppointmentHistory : patientHistoryList) {
				appointmentIds.add(patientAppointmentHistory.getAppointmentId());
				historyMap.put(patientAppointmentHistory.getAppointmentId(), patientAppointmentHistory);
			}
			if (appointmentIds.size() != 0 && !appointmentIds.isEmpty()) {
				populateAppointmentComplaints(appointmentIds, historyMap);
				populateAppointmentPrescriptions(appointmentIds, historyMap);
				populateAppointmentExaminations(appointmentIds, historyMap);
				populateAppointmentReferrals(appointmentIds, historyMap);
				populateAppointmentVariousServices(appointmentIds, historyMap);
				populateDietMaster(appointmentIds, historyMap);
				populateDailyProgress(appointmentIds, historyMap);
				populateDischrageSheet(appointmentIds, historyMap);
				populatePatientPostmortem(appointmentIds, historyMap);
			}
			result.setItems(patientHistoryList);
		}
		return result;
	}

	@Transactional
	public ResponseVO dischargePatient(AppointmentVO appointmentVO, Long customerId, Long hospitalId, Long appointmentId) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		Assert.notNull(appointmentId, "Appointment Id not found.");
		Assert.notNull(appointmentVO.getToTime(), "Discharge date not provided.");
		Appointment appointment = appointmentRepository.findOne(appointmentId);
		Assert.notNull(appointment, "Appointment not found.");
		Assert.isTrue(appointment.getModule() != null && appointment.getModule().getModuleId() == Appointment.IPD_MODULE_ID, "This is not an IPD appointment.");
		appointment.setStatus(Appointment.APPOINTMENT_COMPLETE);
		Timestamp appointmentEndTime = null;
		Timestamp toTime = null;
		try {
			appointmentEndTime = DateTimeUtil.parseTimestamp(appointmentVO.getToTime());
			toTime = DateTimeUtil.parseTimestamp(appointmentVO.getToTime());
		} catch (ParseException | EhmsBusinessException e) {
			logger.error("Unable to parse the Discharge date: " + appointmentVO.getToTime() + " due to: " + e.getMessage(), e);
			throw new EhmsBusinessException(e, "Unable to parse the Discharge date: " + appointmentVO.getToTime() + " due to: " + e.getMessage());
		}
		if (appointmentEndTime == null || toTime == null) {
			throw new EhmsBusinessException("Discharge Time is not in proper format");
		}
		appointment.setToTime(toTime);
		appointment.setAppointmentEndTime(appointmentEndTime);
		appointmentRepository.save(appointment);				
		bedRepository.freeBedByBedId(appointmentVO.getBedId());		
		return new ResponseVO(Status.SUCCESS, "Discharge done successfully");
	}

	private void populatePatientPostmortem(List<Long> appointmentIds, LinkedHashMap<Long, PatientAppointmentHistoryVO> historyMap) {
		List<PatientPostmortemVO> patientPostmortems = patientPostmortemRepository.findPatientPostmortemsForMultipleIds(appointmentIds);
		for (PatientPostmortemVO patientPostmortemVO : patientPostmortems) {
			PatientAppointmentHistoryVO patientAppointmentHistoryVO = historyMap.get(patientPostmortemVO.getAppointmentId());
			patientAppointmentHistoryVO.addAppointmentPatientPostmortem(patientPostmortemVO);
		}
	}

	private void populateDischrageSheet(List<Long> appointmentIds, LinkedHashMap<Long, PatientAppointmentHistoryVO> historyMap) {
		List<DischargeSheetVO> dischargeSheets = dischargeSheetRepository.findDischargeSheetForMultipleIds(appointmentIds);
		for (DischargeSheetVO dischargeSheetVO : dischargeSheets) {
			PatientAppointmentHistoryVO patientAppointmentHistoryVO = historyMap.get(dischargeSheetVO.getAppointmentId());
			patientAppointmentHistoryVO.addAppointmentDischargeSheet(dischargeSheetVO);
		}
	}

	private void populateDailyProgress(List<Long> appointmentIds, LinkedHashMap<Long, PatientAppointmentHistoryVO> historyMap) {
		List<DailyProgressNoteVO> dailyProgressNotes = dailyProgressNoteRepository.findDailyProgressNoteForMultipleIds(appointmentIds);
		for (DailyProgressNoteVO dailyProgressNoteVO : dailyProgressNotes) {
			PatientAppointmentHistoryVO patientAppointmentHistoryVO = historyMap.get(dailyProgressNoteVO.getAppointmentId());
			patientAppointmentHistoryVO.addAppointmentDailyProgressNotes(dailyProgressNoteVO);
		}
	}

	private void populateDietMaster(List<Long> appointmentIds, LinkedHashMap<Long, PatientAppointmentHistoryVO> historyMap) {
		List<DietMasterVO> dietMasters = dietMasterRepository.findDietMastersForMultipleIds(appointmentIds);
		for (DietMasterVO dietMasterVO : dietMasters) {
			PatientAppointmentHistoryVO patientAppointmentHistoryVO = historyMap.get(dietMasterVO.getAppointmentId());
			patientAppointmentHistoryVO.addAppointmentDietMaster(dietMasterVO);
		}
	}

	private void populateAppointmentVariousServices(List<Long> appointmentIds, LinkedHashMap<Long, PatientAppointmentHistoryVO> historyMap) {
		List<AppointmentVariousServiceVO> appointmentExaminations = appointmentVariousServiceRepository.findAppointmentVariousServicesForMultipleIds(appointmentIds);
		for (AppointmentVariousServiceVO appointmentVariousServiceVO : appointmentExaminations) {
			PatientAppointmentHistoryVO patientAppointmentHistoryVO = historyMap.get(appointmentVariousServiceVO.getAppointmentId());
			patientAppointmentHistoryVO.addAppointmentVariousService(appointmentVariousServiceVO);
		}
	}

	private void populateAppointmentReferrals(List<Long> appointmentIds, LinkedHashMap<Long, PatientAppointmentHistoryVO> historyMap) {
		List<AppointmentReferralVO> appointmentReferrals = appointmentReferralRepository.findAppointmentReferralsForMultipleIds(appointmentIds);
		for (AppointmentReferralVO appointmentReferralVO : appointmentReferrals) {
			PatientAppointmentHistoryVO patientAppointmentHistoryVO = historyMap.get(appointmentReferralVO.getAppointmentId());
			patientAppointmentHistoryVO.addAppointmentReferral(appointmentReferralVO);
		}
	}

	private void populateAppointmentExaminations(List<Long> appointmentIds, LinkedHashMap<Long, PatientAppointmentHistoryVO> historyMap) {
		List<AppointmentExaminationVO> appointmentExaminations = appointmentExaminationRepository.findAppointmentExaminationsForMultipleIds(appointmentIds);
		for (AppointmentExaminationVO appointmentExaminationVO : appointmentExaminations) {
			PatientAppointmentHistoryVO patientAppointmentHistoryVO = historyMap.get(appointmentExaminationVO.getAppointmentId());
			patientAppointmentHistoryVO.addAppointmentExamination(appointmentExaminationVO);
		}
	}

	private void populateAppointmentComplaints(List<Long> appointmentIds, LinkedHashMap<Long, PatientAppointmentHistoryVO> historyMap) {
		List<AppointmentComplaintVO> appointmentComplaints = appointmentComplaintRepository.findAppointmentComplaintsForMultipleIds(appointmentIds);
		for (AppointmentComplaintVO appointmentComplaintVO : appointmentComplaints) {
			PatientAppointmentHistoryVO patientAppointmentHistoryVO = historyMap.get(appointmentComplaintVO.getAppointmentId());
			patientAppointmentHistoryVO.addAppointmentComplaint(appointmentComplaintVO);
		}
	}

	private void populateAppointmentPrescriptions(List<Long> appointmentIds, LinkedHashMap<Long, PatientAppointmentHistoryVO> historyMap) {
		List<AppointmentPrescriptionVO> appointmentPrescriptions = appointmentPrescriptionRepository.findAppointmentPrescriptionsForMultipleIds(appointmentIds);
		for (AppointmentPrescriptionVO appointmentPrescriptionVO : appointmentPrescriptions) {
			PatientAppointmentHistoryVO patientAppointmentHistoryVO = historyMap.get(appointmentPrescriptionVO.getAppointmentId());
			patientAppointmentHistoryVO.addAppointmentPrescription(appointmentPrescriptionVO);
		}
	}

	private Sort getSortObjectForQueue(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "default":
			return JpaSort.unsafe(Sort.Direction.DESC, "(CASE WHEN reportTime IS NULL THEN 0 ELSE 1 END)").and(new Sort(Sort.Direction.ASC, "reportTime"))
					.and(JpaSort.unsafe(Sort.Direction.DESC, "(CASE WHEN createdDt IS NULL THEN 0 ELSE 1 END)")).and(new Sort(Sort.Direction.ASC, "createdDt"));
		case "name":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "patient.firstName", "patient.lastName");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	private Sort getSortObjectForPatientHistory(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "reportTime":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "reportTime");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}
}
