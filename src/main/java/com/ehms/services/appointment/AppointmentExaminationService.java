package com.ehms.services.appointment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.model.AppointmentExamination;
import com.ehms.repositories.appointment.AppointmentExaminationRepository;
import com.ehms.vos.ResponseVO;
import com.ehms.vos.ResponseVO.Status;
import com.ehms.vos.appointments.AppointmentExaminationVO;

@Service
public class AppointmentExaminationService {

	@Autowired
	private AppointmentExaminationRepository appointmentExaminationRepository;

	public List<AppointmentExaminationVO> getAppointmentExaminations(Long appointmentId) {
		Assert.notNull(appointmentId, "Appointment ID not provided");
		List<AppointmentExaminationVO> appointmentExaminations = appointmentExaminationRepository.findAppointmentExaminations(appointmentId);
		return appointmentExaminations;
	}

	public AppointmentExaminationVO saveUpdateAppointmentExamination(AppointmentExaminationVO appointmentExaminationVO) {
		AppointmentExamination appointmentExamination = getAppointmentExaminationEntity(appointmentExaminationVO);
		appointmentExaminationRepository.save(appointmentExamination);
		appointmentExaminationVO.setExaminationId(appointmentExamination.getExaminationId());
		return appointmentExaminationVO;
	}

	public ResponseVO deleteAppointmentExamination(Long examinationId) {
		Assert.notNull(examinationId, "Examination Id not provided");
		AppointmentExamination appointmentExaminationToDelete = appointmentExaminationRepository.findOne(examinationId);
		Assert.notNull(appointmentExaminationToDelete, "Appointment Examination not found");
		if (appointmentExaminationToDelete != null) {
			appointmentExaminationRepository.delete(examinationId);
			return new ResponseVO(Status.SUCCESS, "Appointment Examination Deleted successfully");
		}
		return new ResponseVO(Status.FAILURE, "Appointment Examination deletion Failure");
	}

	private AppointmentExamination getAppointmentExaminationEntity(AppointmentExaminationVO appointmentExaminationVO) {
		AppointmentExamination appointmentExamination = null;
		if (appointmentExaminationVO.getExaminationId() == null) {
			appointmentExamination = new AppointmentExamination();
		} else {
			appointmentExamination = appointmentExaminationRepository.findOne(appointmentExaminationVO.getExaminationId());
			Assert.notNull(appointmentExamination, "Appointment Examination with id: " + appointmentExaminationVO.getExaminationId() + " not found.");
		}
		Assert.notNull(appointmentExaminationVO.getAppointmentId(), "Appointment Id not provided");
		appointmentExamination.setAppointmentId(appointmentExaminationVO.getAppointmentId());
		appointmentExamination.setAbdominalSystem(appointmentExaminationVO.getAbdominalSystem());
		appointmentExamination.setCardioVascularSystem(appointmentExaminationVO.getCardioVascularSystem());
		appointmentExamination.setCentralNervousSystem(appointmentExaminationVO.getCentralNervousSystem());
		appointmentExamination.setLocalExamination(appointmentExaminationVO.getLocalExamination());
		appointmentExamination.setObgGynicExamination(appointmentExaminationVO.getObgGynicExamination());
		appointmentExamination.setRespiratorySystem(appointmentExaminationVO.getRespiratorySystem());
		return appointmentExamination;
	}

}
