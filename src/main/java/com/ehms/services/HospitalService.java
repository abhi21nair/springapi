package com.ehms.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Address;
import com.ehms.model.Customer;
import com.ehms.model.Hospital;
import com.ehms.model.User;
import com.ehms.repositories.AddressRepository;
import com.ehms.repositories.HospitalRepository;
import com.ehms.security.JwtUser;
import com.ehms.services.user.UserService;
import com.ehms.vos.AddressVO;
import com.ehms.vos.HospitalVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.ValidationCheckResponseVO;
import com.ehms.vos.user.UserVO;

@Service
public class HospitalService {

	@Autowired
	private HospitalRepository hospitalRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private UserService userService;

	@Value("${hospital.code.prefix}")
	private String hospitalCodePrefix;

	public PaginationResponseDto<HospitalVO> getHospitalsList(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText) {
		PaginationResponseDto<HospitalVO> result = new PaginationResponseDto<HospitalVO>();
		Sort sort = new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, sortOn);
		PageRequest pageRequest = new PageRequest(page, pageSize, sort);
		JwtUser user = userService.getCurrentUser();
		Assert.notNull(user.getCustomerId(), "Can not determin the customer id of the loggedin customer.");
		Page<HospitalVO> hospitals = hospitalRepository.findHospitals("%" + searchText + "%", user.getCustomerId(), pageRequest);
		if (hospitals != null) {
			result.setCount(hospitals.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(hospitals.getTotalElements());
			result.setTotalPages(hospitals.getTotalPages());
			List<HospitalVO> hospitalsList = hospitals.getContent();
			result.setItems(hospitalsList);
		}
		return result;
	}

	@Transactional
	public void saveUpdatehospitals(HospitalVO hospitalVO, boolean isNewHospital) throws EhmsBusinessException {
		Hospital hospital = getHospitalEntityByVO(hospitalVO, isNewHospital);
		hospitalRepository.save(hospital);
		if (isNewHospital) {
			Assert.notNull(hospital.getHospitalId(), "issue while generation of hospital id.");
			hospital.setHospitalCode(hospitalCodePrefix + hospital.getHospitalId());
			hospitalRepository.save(hospital);
			hospitalVO.setHospitalCode(hospitalCodePrefix + hospital.getHospitalId());
		}
		User user = userService.saveUpdateAdminUser(hospitalVO.getCustomerId(), hospital.getHospitalId(), hospitalVO.getKeyUser(), /* isCustomerAdmin */false,
				/* isHospitalAdmin */true);
		hospital.setKeyUser(user);
		hospitalRepository.save(hospital);
		AddressVO addressVO = hospitalVO.getAddress();
		UserVO userVO = hospitalVO.getKeyUser();
		hospitalVO.setHospitalId(hospital.getHospitalId());
		if (hospital.getAddress() != null) {
			addressVO.setAddressId(hospital.getAddress().getAddressId());
		}

		if (hospital.getKeyUser() != null) {
			userVO.setUserId(hospital.getKeyUser().getUserId());
		}
	}

	private Hospital getHospitalEntityByVO(HospitalVO hospitalVO, Boolean isNewHospital) {
		JwtUser user = userService.getCurrentUser();
		Assert.notNull(user, "User information not found");
		Assert.notNull(user.getCustomerId(), "Customer id can not be found for logged-in user");
		Hospital hospital = null;
		if (isNewHospital) {
			hospital = new Hospital();
			hospital.setHospitalCode(String.valueOf(new Date().getTime()));
		} else {
			hospital = hospitalRepository.findOne(hospitalVO.getHospitalId());
		}
		hospital.setFaxNumber(hospitalVO.getFaxNumber());
		Address address = null;
		AddressVO addressVO = hospitalVO.getAddress();
		if (isNewHospital) {
			address = new Address();
		} else {
			Assert.isTrue(addressVO != null & addressVO.getAddressId() != null, "Address information is not provided");
			address = addressRepository.findOne(addressVO.getAddressId());
			Assert.notNull(address, "Address with id: " + addressVO.getAddressId() + " can not be found");
		}
		hospital.setAddress(addressVO.getAddressEntity(address, addressVO));
		hospital.setHospitalName(hospitalVO.getHospitalName());
		hospital.setIsActive(hospitalVO.getIsActive());
		hospital.setContactNumber(hospitalVO.getContactNumber());
		hospital.setCountryCodeForContactNumber(hospitalVO.getCountryCodeForContactNumber());
		hospital.setPhoneNumber(hospitalVO.getPhoneNumber());
		hospital.setCountryCodeForPhoneNumber(hospitalVO.getCountryCodeForPhoneNumber());
		Customer customer = new Customer(user.getCustomerId());
		hospital.setCustomer(customer);
		return hospital;
	}

	public HospitalVO getHospitalDetails(Long id) {
		HospitalVO hospitalVO = new HospitalVO();
		Hospital hospital = hospitalRepository.findOne(id);
		Assert.notNull(hospital, "Hospital not found");
		hospitalVO.setHospitalId(hospital.getHospitalId());
		hospitalVO.setHospitalName(hospital.getHospitalName());
		hospitalVO.setHospitalCode(hospital.getHospitalCode());
		hospitalVO.setIsActive(hospital.getIsActive());
		hospitalVO.setFaxNumber(hospital.getFaxNumber());
		hospitalVO.setAddress(new AddressVO(hospital.getAddress()));
		hospitalVO.setKeyUser(userService.getUserVO(hospital.getKeyUser()));
		hospitalVO.setContactNumber(hospital.getContactNumber());
		hospitalVO.setCountryCodeForContactNumber(hospital.getCountryCodeForContactNumber());
		hospitalVO.setPhoneNumber(hospital.getPhoneNumber());
		hospitalVO.setCountryCodeForPhoneNumber(hospital.getCountryCodeForContactNumber());
		if (hospital.getCustomer() != null) {
			hospitalVO.setCustomerId(hospital.getCustomer().getCustomerId());
		}
		return hospitalVO;
	}

	public ValidationCheckResponseVO validateHospitalName(HospitalVO hospitalVO) {
		Assert.isTrue(hospitalVO != null && hospitalVO.getHospitalName() != null && !hospitalVO.getHospitalName().isEmpty(), "Hospital name not provided");
		Assert.isTrue(hospitalVO.getCustomerId() != null, "Customer information not provided");
		Hospital hospital = null;
		if (hospitalVO.getHospitalId() == null) {
			hospital = hospitalRepository.findByCustomerCustomerIdAndHospitalNameIgnoreCase(hospitalVO.getCustomerId(), hospitalVO.getHospitalName());
		} else {
			hospital = hospitalRepository.findByCustomerCustomerIdAndHospitalIdNotAndHospitalNameIgnoreCase(hospitalVO.getCustomerId(), hospitalVO.getHospitalId(),
					hospitalVO.getHospitalName());
		}
		if (hospital != null) {
			return new ValidationCheckResponseVO(false);
		} else {
			return new ValidationCheckResponseVO(true);
		}
	}

	public List<HospitalVO> getHospitalsList() {
		JwtUser user = userService.getCurrentUser();
		Assert.notNull(user, "User not found.");
		Assert.notNull(user.getCustomerId(), "Customer ID not found.");
		return hospitalRepository.getHospitalList(user.getCustomerId());
	}
}
