package com.ehms.services;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ehms.exception.EhmsBusinessException;
import com.ehms.model.Employee;
import com.ehms.model.SalaryLeaveStructure;
import com.ehms.repositories.SalaryLeaveStructureRepository;
import com.ehms.utils.DateTimeUtil;
import com.ehms.vos.EmployeeVO;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.SalaryLeaveStructureVO;

@Service
public class SalaryLeaveStructureService {
	@Autowired
	private SalaryLeaveStructureRepository salaryLeaveStructureRepository;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public PaginationResponseDto<SalaryLeaveStructureVO> getSalaryLeaveStructureDetails(Integer page, Integer pageSize, String sortOn, String sortOrder, String searchText,
			Long customerId, Long hospitalId) {
		PaginationResponseDto<SalaryLeaveStructureVO> result = new PaginationResponseDto<SalaryLeaveStructureVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<SalaryLeaveStructureVO> salaryLeaveStructures = salaryLeaveStructureRepository.findSalaryLeaveStructure("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (salaryLeaveStructures != null) {
			result.setCount(salaryLeaveStructures.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(salaryLeaveStructures.getTotalElements());
			result.setTotalPages(salaryLeaveStructures.getTotalPages());
			List<SalaryLeaveStructureVO> salaryLeaveStructuresList = salaryLeaveStructures.getContent();
			result.setItems(salaryLeaveStructuresList);

		}
		return result;
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "name":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, "employee.firstName", "employee.lastName");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	public SalaryLeaveStructureVO getSalaryLeaveStructureById(Long customerId, Long hospitalId, Long salaryLeaveStructureId) {
		Assert.notNull(salaryLeaveStructureId, "salaryLeaveStructure ID not provided");
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		SalaryLeaveStructure salaryLeaveStructure = salaryLeaveStructureRepository.findOne(salaryLeaveStructureId);
		Assert.notNull(salaryLeaveStructure, "salaryLeaveStructureId not found");
		return getSalaryLeaveStructureVO(salaryLeaveStructure);
	}

	public SalaryLeaveStructureVO saveUpdateSalaryLeaveStructure(Long customerId, Long hospitalId, SalaryLeaveStructureVO salaryLeaveStructureVO) throws EhmsBusinessException {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		SalaryLeaveStructure salaryLeaveStructure = null;
		salaryLeaveStructure = getSalaryLeaveStructureEntity(salaryLeaveStructureVO, customerId, hospitalId);
		salaryLeaveStructureRepository.save(salaryLeaveStructure);
		salaryLeaveStructureVO.setSalaryLeaveStructureId(salaryLeaveStructure.getSalaryLeaveStructureId());
		return salaryLeaveStructureVO;
	}

	private SalaryLeaveStructure getSalaryLeaveStructureEntity(SalaryLeaveStructureVO salaryLeaveStructureVO, Long customerId, Long hospitalId) throws EhmsBusinessException {
		SalaryLeaveStructure salaryLeaveStructure = null;

		if (salaryLeaveStructureVO.getSalaryLeaveStructureId() == null) {
			salaryLeaveStructure = new SalaryLeaveStructure();
		} else {
			salaryLeaveStructure = salaryLeaveStructureRepository.findOne(salaryLeaveStructureVO.getSalaryLeaveStructureId());
			Assert.notNull(salaryLeaveStructure, "salaryLeaveStructure with id: " + salaryLeaveStructureVO.getSalaryLeaveStructureId() + " not found.");
		}

		if (salaryLeaveStructureVO.getDateTo() != null && salaryLeaveStructureVO.getDateFrom() != null) {
			try {
				Date dateFrom = DateTimeUtil.parseDate(salaryLeaveStructureVO.getDateFrom());
				Date dateTo = DateTimeUtil.parseDate(salaryLeaveStructureVO.getDateTo());
				if (dateFrom != null && dateTo != null) {
					Assert.isTrue(dateTo.after(dateFrom), "From date should be before To date");
					salaryLeaveStructure.setDateFrom(dateFrom);
					salaryLeaveStructure.setDateTo(dateTo);
				}
			} catch (ParseException e1) {
				logger.error("Error parsing date in getSalaryLeaveStructureEntity() :" + " From Date " + salaryLeaveStructureVO.getDateFrom() + " To Date "
						+ salaryLeaveStructureVO.getDateTo(), e1);
				throw new EhmsBusinessException(e1, "Error parsing date in getSalaryLeaveStructureEntity() :" + " From Date " + salaryLeaveStructureVO.getDateFrom() + " To Date "
						+ salaryLeaveStructureVO.getDateTo());
			}
		}
		salaryLeaveStructure.setOverTime(salaryLeaveStructureVO.getOverTime());
		if (salaryLeaveStructureVO.getEmployee() != null) {
			Employee employee = new Employee(salaryLeaveStructureVO.getEmployee().getEmployeeId());
			salaryLeaveStructure.setEmployee(employee);
		}
		salaryLeaveStructure.setArrears(salaryLeaveStructureVO.getArrears());
		salaryLeaveStructure.setBasicSalary(salaryLeaveStructureVO.getBasicSalary());
		salaryLeaveStructure.setBenefits(salaryLeaveStructureVO.getBenefits());
		salaryLeaveStructure.setBonus(salaryLeaveStructureVO.getBenefits());
		salaryLeaveStructure.setCca(salaryLeaveStructureVO.getCca());
		salaryLeaveStructure.setDa(salaryLeaveStructureVO.getDa());
		salaryLeaveStructure.setEpf(salaryLeaveStructureVO.getEpf());
		salaryLeaveStructure.setHouseLoan(salaryLeaveStructureVO.getHouseLoan());
		salaryLeaveStructure.setHra(salaryLeaveStructureVO.getHra());
		salaryLeaveStructure.setIncomeTax(salaryLeaveStructureVO.getIncomeTax());
		salaryLeaveStructure.setIncrements(salaryLeaveStructureVO.getIncrements());
		salaryLeaveStructure.setOtherAllowance(salaryLeaveStructureVO.getOtherAllowance());
		salaryLeaveStructure.setPanelDeduction(salaryLeaveStructureVO.getPanelDeduction());
		salaryLeaveStructure.setPersonalLoan(salaryLeaveStructureVO.getPersonalLoan());
		salaryLeaveStructure.setProfessionalTax(salaryLeaveStructureVO.getProfessionalTax());
		salaryLeaveStructure.setSecurity(salaryLeaveStructureVO.getSecurity());
		salaryLeaveStructure.setSpecialAllowance(salaryLeaveStructureVO.getSpecialAllowance());
		salaryLeaveStructure.setTa(salaryLeaveStructureVO.getTa());
		salaryLeaveStructure.setTds(salaryLeaveStructureVO.getTds());
		salaryLeaveStructure.setVehicalLoan(salaryLeaveStructureVO.getVehicalLoan());

		return salaryLeaveStructure;
	}

	private SalaryLeaveStructureVO getSalaryLeaveStructureVO(SalaryLeaveStructure salaryLeaveStructure) {
		SalaryLeaveStructureVO vo = null;
		if (salaryLeaveStructure != null) {
			vo = new SalaryLeaveStructureVO();
			vo.setSalaryLeaveStructureId(salaryLeaveStructure.getSalaryLeaveStructureId());
			vo.setEmployee(new EmployeeVO(salaryLeaveStructure.getEmployee()));
			vo.setDateFrom(DateTimeUtil.formatDate(salaryLeaveStructure.getDateFrom()));
			vo.setDateTo(DateTimeUtil.formatDate(salaryLeaveStructure.getDateTo()));
			vo.setOverTime(salaryLeaveStructure.getOverTime());
			vo.setArrears(salaryLeaveStructure.getArrears());
			vo.setBasicSalary(salaryLeaveStructure.getBasicSalary());
			vo.setBenefits(salaryLeaveStructure.getBenefits());
			vo.setBonus(salaryLeaveStructure.getBonus());
			vo.setCca(salaryLeaveStructure.getCca());
			vo.setDa(salaryLeaveStructure.getDa());
			vo.setEpf(salaryLeaveStructure.getEpf());
			vo.setHouseLoan(salaryLeaveStructure.getHouseLoan());
			vo.setHra(salaryLeaveStructure.getHra());
			vo.setIncomeTax(salaryLeaveStructure.getIncomeTax());
			vo.setIncrements(salaryLeaveStructure.getIncrements());
			vo.setOtherAllowance(salaryLeaveStructure.getOtherAllowance());
			vo.setPanelDeduction(salaryLeaveStructure.getPanelDeduction());
			vo.setPersonalLoan(salaryLeaveStructure.getPersonalLoan());
			vo.setProfessionalTax(salaryLeaveStructure.getProfessionalTax());
			vo.setSalaryLeaveStructureId(salaryLeaveStructure.getSalaryLeaveStructureId());
			vo.setSecurity(salaryLeaveStructure.getSecurity());
			vo.setSpecialAllowance(salaryLeaveStructure.getSpecialAllowance());
			vo.setTa(salaryLeaveStructure.getTa());
			vo.setTds(salaryLeaveStructure.getTds());
			vo.setVehicalLoan(salaryLeaveStructure.getVehicalLoan());
		}
		return vo;
	}

}
