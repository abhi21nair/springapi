package com.ehms.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import com.ehms.model.Customer;
import com.ehms.model.Department;
import com.ehms.model.Hospital;
import com.ehms.model.OtMaster;
import com.ehms.repositories.OtMasterRepository;
import com.ehms.vos.OtMasterVO;
import com.ehms.vos.PaginationResponseDto;

@Service
public class OtMasterService {

	@Autowired
	private OtMasterRepository otMasterRepository;

	public PaginationResponseDto<OtMasterVO> getOtMasterDetails(Integer page, Integer pageSize, String sortOn,
			String sortOrder, String searchText, Long customerId, Long hospitalId) {
		PaginationResponseDto<OtMasterVO> result = new PaginationResponseDto<OtMasterVO>();
		Sort sort = new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC, sortOn);
		PageRequest pageRequest = new PageRequest(page, pageSize, sort);
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<OtMasterVO> otMasterDetails = otMasterRepository.findOtMasterDetails("%" + searchText + "%", customerId,
				hospitalId, pageRequest);
		if (otMasterDetails != null) {
			result.setCount(otMasterDetails.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(otMasterDetails.getTotalElements());
			result.setTotalPages(otMasterDetails.getTotalPages());
			List<OtMasterVO> otMasterDetailsList = otMasterDetails.getContent();
			result.setItems(otMasterDetailsList);
		}
		return result;

	}

	public OtMasterVO getOtMasterById(Long otMasterId) {
		Assert.notNull(otMasterId, "OtMaster Id not provided.");
		OtMaster otMaster = otMasterRepository.findOne(otMasterId);
		Assert.notNull(otMaster, "OtMaster not found.");
		return getOtMasterVO(otMaster);
	}

	private OtMasterVO getOtMasterVO(OtMaster otMaster) {
		Assert.notNull(otMaster, "otMaster information not found.");
		OtMasterVO vo = null;
		if (otMaster != null) {
			vo = new OtMasterVO();
			vo.setOtCharges(otMaster.getOtCharges());
			vo.setOtMasterId(otMaster.getOtMasterId());
			vo.setOtName(otMaster.getOtName());
			vo.setOtNo(otMaster.getOtNo());
			vo.setOtMasterId(otMaster.getOtMasterId());
			vo.setOtLocation(otMaster.getOtLocation());
			if (otMaster.getOtType() != null) {
				vo.setOtTypeId(otMaster.getOtType().getDepartmentId());
			}
			vo.setIsActive(otMaster.getIsActive());
			if (otMaster.getCustomer() != null) {
				vo.setCustomerId(otMaster.getCustomer().getCustomerId());
			}
			if (otMaster.getHospital() != null) {
				vo.setHospitalId(otMaster.getHospital().getHospitalId());
			}
		}
		return vo;
	}

	@Transactional
	public OtMaster saveUpdateOtMaster(Long customerId, Long hospitalId, OtMasterVO otMasterVO) {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Customer ID not provided.");
		otMasterVO.setCustomerId(customerId);
		otMasterVO.setHospitalId(hospitalId);
		OtMaster otMaster = getOtMasterEntity(otMasterVO);
		otMasterRepository.save(otMaster);
		otMasterVO.setOtMasterId(otMaster.getOtMasterId());
		return otMaster;

	}

	private OtMaster getOtMasterEntity(OtMasterVO otMasterVO) {
		OtMaster otMaster = null;
		if (otMasterVO.getOtMasterId() == null) {
			otMaster = new OtMaster();
		} else {
			otMaster = otMasterRepository.findOne(otMasterVO.getOtMasterId());
			Assert.notNull(otMaster, "otMaster with id: " + otMasterVO.getOtMasterId() + " not found.");
		}
		otMaster.setOtCharges(otMasterVO.getOtCharges());
		otMaster.setOtLocation(otMasterVO.getOtLocation());
		otMaster.setOtName(otMasterVO.getOtName());
		otMaster.setOtNo(otMasterVO.getOtNo());
		Assert.notNull(otMasterVO.getOtTypeId(), "OtType Id not provided");
		Department department = new Department(otMasterVO.getOtTypeId());
		otMaster.setOtType(department);
		otMaster.setIsActive(otMasterVO.getIsActive());

		Assert.notNull(otMasterVO.getCustomerId(), "Customer information not provided");
		Customer customer = new Customer(otMasterVO.getCustomerId());
		Assert.notNull(customer, "Customer not found");
		otMaster.setCustomer(customer);

		if (otMasterVO.getHospitalId() != null) {
			Hospital hospital = new Hospital(otMasterVO.getHospitalId());
			otMaster.setHospital(hospital);
		}
		return otMaster;
	}

}
