package com.ehms.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import com.ehms.model.Customer;
import com.ehms.model.Department;
import com.ehms.model.Hospital;
import com.ehms.model.MiscValue;
import com.ehms.model.VariousPriceMaster;
import com.ehms.repositories.VariousPriceMasterRepository;
import com.ehms.vos.PaginationResponseDto;
import com.ehms.vos.VariousPriceMasterVO;

@Service
public class VariousPriceMasterService {
	@Autowired
	private VariousPriceMasterRepository variousPriceMasterRepository;

	public PaginationResponseDto<VariousPriceMasterVO> getVariousPriceMasterDetails(Integer page, Integer pageSize,
			String sortOn, String sortOrder, String searchText, Long customerId, Long hospitalId) {
		PaginationResponseDto<VariousPriceMasterVO> result = new PaginationResponseDto<VariousPriceMasterVO>();
		PageRequest pageRequest = new PageRequest(page, pageSize, getSortObject(sortOrder, sortOn));
		Assert.notNull(customerId, "Customer information not provided.");
		Assert.notNull(hospitalId, "Hospital information not provided.");
		Page<VariousPriceMasterVO> variousPriceMasters = variousPriceMasterRepository
				.findVariousPriceMasters("%" + searchText + "%", customerId, hospitalId, pageRequest);
		if (variousPriceMasters != null) {
			result.setCount(variousPriceMasters.getNumberOfElements());
			result.setPage(page);
			result.setTotalItems(variousPriceMasters.getTotalElements());
			result.setTotalPages(variousPriceMasters.getTotalPages());
			List<VariousPriceMasterVO> variousPriceMastersList = variousPriceMasters.getContent();
			result.setItems(variousPriceMastersList);
		}
		return result;
	}

	public VariousPriceMasterVO saveUpdateVariousPriceMaster(Long customerId, Long hospitalId,
			VariousPriceMasterVO variousPriceMasterVO) {
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		VariousPriceMaster variousPriceMaster = getVariousPriceMasterEntity(variousPriceMasterVO);
		variousPriceMasterRepository.save(variousPriceMaster);
		variousPriceMasterVO.setVariousPriceMasterId(variousPriceMaster.getVariousPriceMasterId());
		return variousPriceMasterVO;
	}

	public VariousPriceMasterVO getVariousPriceMasterById(Long customerId, Long hospitalId, Long variousPriceMasterId) {
		Assert.notNull(variousPriceMasterId, "various price master ID not provided");
		Assert.notNull(customerId, "Customer ID not provided.");
		Assert.notNull(hospitalId, "Hospital ID not provided.");
		VariousPriceMaster variousPriceMaster = variousPriceMasterRepository
				.findVariousPriceMaster(variousPriceMasterId, customerId, hospitalId);
		Assert.notNull(variousPriceMaster, "Various price master not found");
		return getVariousPriceMasterVO(variousPriceMaster);
	}

	private Sort getSortObject(String sortOrder, String sortOn) {
		switch (sortOn) {
		case "priceMasterServiceName":
			return new Sort("ASC".equalsIgnoreCase(sortOrder) ? Sort.Direction.ASC : Sort.Direction.DESC,
					"priceMasterServiceName.miscValue");
		default:
			throw new IllegalArgumentException("Sort order by this column is not supported");
		}
	}

	private VariousPriceMasterVO getVariousPriceMasterVO(VariousPriceMaster variousPriceMaster) {
		VariousPriceMasterVO vo = null;
		if (variousPriceMaster != null) {
			vo = new VariousPriceMasterVO();
			vo.setVariousPriceMasterId(variousPriceMaster.getVariousPriceMasterId());
			if (variousPriceMaster.getPriceMasterServiceName() != null) {
				vo.setPriceMasterServiceName(variousPriceMaster.getPriceMasterServiceName().getMiscValue());
			}
			if (variousPriceMaster.getPriceMasterServiceName() != null) {
				vo.setPriceMasterServiceNameId(variousPriceMaster.getPriceMasterServiceName().getMiscTypeValueId());
			}
			vo.setPriceMasterGroupTypeId(variousPriceMaster.getPriceMasterGroupTypeId());
			vo.setPriceMasterSubGroupTypeId(variousPriceMaster.getPriceMasterSubGroupTypeId());
			if (variousPriceMaster.getDepartment() != null) {
				vo.setDepartmentId(variousPriceMaster.getDepartment().getDepartmentId());
			}
			vo.setNormalServiceRate(variousPriceMaster.getNormalServiceRate());
			vo.setCghsRateInPercent(variousPriceMaster.getCghsRateInPercent());
			vo.setCghsRateInRuppes(variousPriceMaster.getCghsRateInRuppes());
			vo.setCorporateRateInPercent(variousPriceMaster.getCorporateRateInPercent());
			vo.setCorporateRateInRuppes(variousPriceMaster.getCorporateRateInRuppes());
			vo.setEmergencyRateInPercent(variousPriceMaster.getEmergencyRateInPercent());
			vo.setEmergencyRateInRuppes(variousPriceMaster.getEmergencyRateInRuppes());
			vo.setOtherRateInPercent(variousPriceMaster.getOtherRateInPercent());
			vo.setOtherRateInRuppes(variousPriceMaster.getOtherRateInRuppes());
			vo.setIsActive(variousPriceMaster.getIsActive());
			if (variousPriceMaster.getCustomer() != null) {
				vo.setCustomerId(variousPriceMaster.getCustomer().getCustomerId());
			}
			if (variousPriceMaster.getHospital() != null) {
				vo.setHospitalId(variousPriceMaster.getHospital().getHospitalId());
			}
		}
		return vo;
	}

	private VariousPriceMaster getVariousPriceMasterEntity(VariousPriceMasterVO variousPriceMasterVO) {
		VariousPriceMaster variousPriceMaster = null;
		if (variousPriceMasterVO.getVariousPriceMasterId() == null) {
			variousPriceMaster = new VariousPriceMaster();
		} else {
			variousPriceMaster = variousPriceMasterRepository.findOne(variousPriceMasterVO.getVariousPriceMasterId());
			Assert.notNull(variousPriceMaster,
					"various price master with id: " + variousPriceMasterVO.getVariousPriceMasterId() + " not found.");
		}
		Assert.notNull(variousPriceMasterVO.getDepartmentId(), "Department Id not provided");
		Department department = new Department(variousPriceMasterVO.getDepartmentId());
		variousPriceMaster.setDepartment(department);
		MiscValue miscValue = new MiscValue(variousPriceMasterVO.getPriceMasterServiceNameId());
		variousPriceMaster.setPriceMasterServiceName(miscValue);
		variousPriceMaster.setNormalServiceRate(variousPriceMasterVO.getNormalServiceRate());
		variousPriceMaster.setPriceMasterGroupTypeId(variousPriceMasterVO.getPriceMasterGroupTypeId());
		variousPriceMaster.setPriceMasterSubGroupTypeId(variousPriceMasterVO.getPriceMasterSubGroupTypeId());
		variousPriceMaster.setCghsRateInPercent(variousPriceMasterVO.getCghsRateInPercent());
		variousPriceMaster.setCghsRateInRuppes(variousPriceMasterVO.getCghsRateInPercent());
		variousPriceMaster.setCorporateRateInPercent(variousPriceMasterVO.getCorporateRateInPercent());
		variousPriceMaster.setCorporateRateInRuppes(variousPriceMasterVO.getCorporateRateInRuppes());
		variousPriceMaster.setEmergencyRateInPercent(variousPriceMasterVO.getEmergencyRateInPercent());
		variousPriceMaster.setEmergencyRateInRuppes(variousPriceMasterVO.getEmergencyRateInRuppes());
		variousPriceMaster.setOtherRateInPercent(variousPriceMasterVO.getOtherRateInPercent());
		variousPriceMaster.setOtherRateInRuppes(variousPriceMasterVO.getOtherRateInRuppes());
		variousPriceMaster.setIsActive(variousPriceMasterVO.getIsActive());
		Customer customer = new Customer(variousPriceMasterVO.getCustomerId());
		variousPriceMaster.setCustomer(customer);
		Hospital hospital = new Hospital(variousPriceMasterVO.getHospitalId());
		variousPriceMaster.setHospital(hospital);
		return variousPriceMaster;
	}
}
